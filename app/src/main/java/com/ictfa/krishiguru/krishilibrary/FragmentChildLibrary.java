package com.ictfa.krishiguru.krishilibrary;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.ArticalDetail.ArticleDetailActivity;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.customViews.GridSpacingItemDecoration;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.krishilibrary.Object.Article;
import com.ictfa.krishiguru.krishilibrary.Object.ArticleImages;
import com.ictfa.krishiguru.krishilibrary.adapter.ArticleAdapter;
import com.ictfa.krishiguru.realm.RealmController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;


/**
 * Created by DAT on 9/1/2015.
 */
public class FragmentChildLibrary extends Fragment implements ArticleAdapter.OnItemClickListner {
    String id;
    String atricleTypeKey;
    @BindView(R.id.rv_articles)
    RecyclerView rvArticles;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    private ArrayList<Article> articleList;
    private ArticleAdapter mAdapter;
    private Realm realm;
    private SharedPreference sharedPreference;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_child_library, container, false);
        ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        pbLoading.setVisibility(View.GONE);
        sharedPreference = new SharedPreference(getActivity());

        //get realm instance
        this.realm = RealmController.with(this).getRealm();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvArticles.setLayoutManager(linearLayoutManager);
        rvArticles.addItemDecoration(new GridSpacingItemDecoration(2, CommonMethods.dpToPx(getActivity(), 1), true));
        rvArticles.setItemAnimator(new DefaultItemAnimator());
        if (bundle != null) {
            id = bundle.getString("id");
            atricleTypeKey = bundle.getString("atricleTypeKey");

            if (!sharedPreference.getBoolValues(atricleTypeKey)) {
                pbLoading.setVisibility(View.VISIBLE);
                getArticles();
            } else {
                articleList = new ArrayList<>();
                RealmResults<Article> realmResults = realm.where(Article.class).equalTo("libraryType", atricleTypeKey).findAll();
                for (Article a : realmResults)
                    articleList.add(a);

                populateArticle();
                getArticles();
            }
        }
        return view;
    }

    private void getArticles() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = UrlHelper.BASE_URL + "api/v3/library/list?apikey=" + UrlHelper.API_KEY + "&type_id=" + id;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        pd.hidepd();
                        pbLoading.setVisibility(View.GONE);
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONObject jsonResponse = object.getJSONObject("code");
                            int status = jsonResponse.getInt("status");
                            String message = jsonResponse.getString("message");
                            if (!message.equalsIgnoreCase("listed")) {
//                                alerts.showSuccessAlert(message);
                            } else {
                                articleList = new ArrayList<>();
                                Article article;
                                JSONArray articleListJson = object.getJSONArray("data");
                                for (int i = 0; i < articleListJson.length(); i++) {
                                    article = new Article();
                                    JSONObject type = (JSONObject) articleListJson.get(i);
                                    article.setId(type.getInt("id"));
                                    article.setLibraryType(type.getString("library_type"));
                                    article.setLibraryTitle(type.getString("library_title"));
                                    article.setDescription(type.getString("description"));
                                    article.setCommentCount(type.getInt("comment_count"));
                                    article.setLikeCount(type.getInt("like_count"));
                                    article.setShareCount(type.getInt("share_count"));
                                    article.setLiked(type.getBoolean("is_liked"));
                                    article.setCreatedAt(type.getString("created_at"));
                                    article.setAuthor(type.getString("author"));
                                    JSONArray imageListJson = type.getJSONArray("images");
                                    ArticleImages articleImages;
                                    ArrayList<ArticleImages> listImages = new ArrayList<>();

                                    if (imageListJson != null) {
                                        for (int j = 0; j < imageListJson.length(); j++) {
                                            JSONObject imageObj = imageListJson.getJSONObject(j);
                                            articleImages = new ArticleImages();
                                            articleImages.setName(imageObj.getString("name"));
                                            articleImages.setLocation(imageObj.getString("location"));
//                                            listImages.add(articleImages);
                                            article.setImage("http://admin.ict4agri.com" + imageObj.getString("location"));
                                            break;
                                        }
//                                        article.setListImages(listImages);
                                    }
                                    articleList.add(article);
                                    populateArticle();
                                    sharedPreference.setKeyValues(atricleTypeKey, true);
                                }

                                for (Article b : articleList) {
                                    // Persist your data easily
                                    realm.beginTransaction();
                                    realm.copyToRealmOrUpdate(b);
                                    realm.commitTransaction();
                                }
                            }
//                            mAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            pbLoading.setVisibility(View.GONE);
                            e.printStackTrace();
//                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
//                            alerts.showToastMsg(e.getMessage());
                            pbLoading.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pbLoading.setVisibility(View.GONE);
//                pd.hidepd();
//                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void populateArticle() {
        mAdapter = new ArticleAdapter(getActivity(), articleList);
        mAdapter.setListners(this);
        rvArticles.setAdapter(mAdapter);
    }


    @Override
    public void onItemClicked(Article article) {
        Intent intent = new Intent(getActivity(), ArticleDetailActivity.class);
        Bundle b = new Bundle();
        b.putSerializable(CommonDef.ARTICLE_ID, (Serializable) article.getId());
        intent.putExtras(b);
        startActivity(intent);
    }
}
