package com.ictfa.krishiguru.krishilibrary.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.krishilibrary.Object.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pragt on 3/5/17.
 */

public class ArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    ArrayList<Article> listArticle;
    OnItemClickListner onItemClickListner;

    public ArticleAdapter(Context mContext, ArrayList<Article> listArticle) {
        this.mContext = mContext;
        this.listArticle = listArticle;

    }

    public void setListners(OnItemClickListner onItemClickListner)
    {
        this.onItemClickListner = onItemClickListner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_article, parent, false);
        return new myCategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myCategoriesViewHolder) {
            final myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;
            holder1.tvTitle.setText(listArticle.get(position).getLibraryTitle());
            holder1.tvType.setText(listArticle.get(position).getLibraryType());
            holder1.tvAuthor.setText("Auther/source: " + listArticle.get(position).getAuthor());
            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListner.onItemClicked(listArticle.get(position));
                }
            });

            if (!listArticle.get(position).getImage().equalsIgnoreCase(""))
            {
                    Picasso.with(mContext).load(listArticle.get(position).getImage()).
                        placeholder(mContext.getResources().getDrawable(R.drawable.images)).resize(500, 500).
                        into(holder1.ivImage);
            }else
            {
                holder1.ivImage.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.images));
            }
        }
    }

    @Override
    public int getItemCount() {
        return listArticle.size();
    }


    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_type)
        TextView tvType;
        @BindView(R.id.tv_author)
        TextView tvAuthor;
        @BindView(R.id.ivImage)
        ImageView ivImage;


        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListner
    {
        void onItemClicked(Article article);
    }
}
