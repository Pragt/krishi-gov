package com.ictfa.krishiguru.krishilibrary.Object;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Prajeet on 13/07/2017.
 */

public class LibraryType extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;

    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
