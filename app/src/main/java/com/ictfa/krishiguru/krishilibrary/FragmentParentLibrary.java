package com.ictfa.krishiguru.krishilibrary;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ictfa.krishiguru.R;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by DAT on 9/1/2015.
 */
public class FragmentParentLibrary extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private CircleIndicator indicator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent, container, false);
        getIDs(view);
        setEvents();
        return view;
    }

    private void getIDs(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.my_viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.my_tab_layout);
        indicator = (CircleIndicator) view.findViewById(R.id.indicator);
        adapter = new ViewPagerAdapter(getFragmentManager(), getActivity(), viewPager, tabLayout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            viewPager.setAdapter(adapter);
        }

    }

    private void setEvents() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    super.onTabSelected(tab);
                    viewPager.setCurrentItem(tab.getPosition());
                    Log.d("Selected", "Selected " + tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    super.onTabUnselected(tab);
                    Log.d("Unselected", "Unselected " + tab.getPosition());
                }
            });
        }
    }

    public void addPage(String type, String id) {
        Bundle bundle = new Bundle();
        bundle.putString("atricleTypeKey", type);
        bundle.putString("id", id);
        FragmentChildLibrary fragmentChild = new FragmentChildLibrary();
        fragmentChild.setArguments(bundle);
        fragmentChild.setArguments(bundle);
        adapter.addFrag(fragmentChild, type);
        adapter.notifyDataSetChanged();
        if (adapter.getCount() > 0) tabLayout.setupWithViewPager(viewPager);

        viewPager.setCurrentItem(0);
        indicator.setViewPager(viewPager);
        setupTabLayout();
    }


    public void setupTabLayout() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setCustomView(adapter.getTabView(i));
        }
    }

    public void setPageLimit(int size) {
        viewPager.setOffscreenPageLimit(size);
    }
}
