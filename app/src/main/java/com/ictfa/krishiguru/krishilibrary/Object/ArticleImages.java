package com.ictfa.krishiguru.krishilibrary.Object;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by Prajeet on 13/07/2017.
 */

public class ArticleImages extends RealmObject implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private String location;
}
