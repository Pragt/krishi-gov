package com.ictfa.krishiguru.krishilibrary;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.krishilibrary.Object.LibraryType;

import java.util.ArrayList;

/**
 * Created by Prajeet on 13/07/2017.
 */

public class KrishiLibraryActivity extends AppCompatActivity {

    private FragmentParentLibrary fragmentParent;
    public  ArrayList<LibraryType> listLibraryType;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_krishi_library);
        getSupportActionBar().setTitle(getResources().getString(R.string.krishilibrary));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fragmentParent = (FragmentParentLibrary) this.getSupportFragmentManager().findFragmentById(R.id.fragmentParentLibrary);
        init();

        Bundle b = getIntent().getExtras();
        if (b != null) {
            listLibraryType = (ArrayList<LibraryType>) b.getSerializable(CommonDef.LIBRARY_TYPE);
        }
        fragmentParent.setPageLimit(listLibraryType.size());


        for (int i = 0; i < listLibraryType.size(); i++) {
            fragmentParent.addPage(listLibraryType.get(i).getType(), listLibraryType.get(i).getId());
        }
    }

    private void init() {
        listLibraryType = new ArrayList<>();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
