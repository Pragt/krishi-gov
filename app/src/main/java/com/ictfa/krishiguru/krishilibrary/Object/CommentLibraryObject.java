package com.ictfa.krishiguru.krishilibrary.Object;

import java.io.Serializable;

/**
 * Created by prajit on 10/24/17.
 */

public class CommentLibraryObject implements Serializable{
    public String comment;
    public String commentedBy;
    public String createdAt;
    public String id;
}
