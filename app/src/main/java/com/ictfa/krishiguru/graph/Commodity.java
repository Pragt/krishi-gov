package com.ictfa.krishiguru.graph;

import java.io.Serializable;

/**
 * Created by Prajeet on 03/08/2017.
 */

public class Commodity implements Serializable {
    public String id;
    public String name;
}
