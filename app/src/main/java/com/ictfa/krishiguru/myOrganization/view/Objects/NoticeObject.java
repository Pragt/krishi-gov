package com.ictfa.krishiguru.myOrganization.view.Objects;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Prajeet on 06/07/2017.
 */

public class NoticeObject extends RealmObject implements Serializable{
    @PrimaryKey
    public int id;
    public String org_name;
    public String notice;
    public String phone;
    public String type;
    public String title;
    public String noticeFile;
    public int comment_count;
    public RealmList<Comment> listComment;
    public int days_remaining;
    public String postedDate= "";
    public String valid_to;
    public String toll_free;
    public String contact_person;
    public int share_count;
    public String imageUrl;
}
