package com.ictfa.krishiguru.myOrganization.view.Objects;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Prajeet on 06/07/2017.
 */

public class Comment extends RealmObject implements Serializable {
    public String comment;
    public String commented_by;
    public String date;
    public String direction;
    @PrimaryKey
    public int comment_id;
    public int timezone_type;
    public String timezone;
}
