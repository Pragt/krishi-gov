package com.ictfa.krishiguru.myOrganization.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.fullNoticeView.FullNoticeViewActivity;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.myOrganization.view.Objects.NoticeObject;
import com.ictfa.krishiguru.ui.fragment.ImageShowFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pragt on 3/5/17.
 */

public class NoticeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<NoticeObject> listNotice;
    onItemClickListner listner;
    private Activity mContext;
    boolean isFromDashBoard;

    public NoticeAdapter(Context mContext, ArrayList<NoticeObject> listNotice, onItemClickListner mListner, boolean isFromDashboard) {
        this.mContext = (Activity) mContext;
        this.listNotice = listNotice;
        listner = mListner;
        this.isFromDashBoard = isFromDashboard;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (!isFromDashBoard) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_notice, parent, false);
            return new myNoticeViewHolder(view);
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_notice_dashboard, parent, false);
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.width = (int) (parent.getWidth() * 0.45);
            view.setLayoutParams(layoutParams);
            return new myNoticeDashbopardViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myNoticeViewHolder) {
            final myNoticeViewHolder holder1 = (myNoticeViewHolder) holder;
            holder1.tvOrgName.setText(listNotice.get(position).org_name);
            if (listNotice.get(position).days_remaining == 0)
                holder1.tvDaysLeft.setText("म्याद सकियो");
            else if (listNotice.get(position).days_remaining == 1) {
                holder1.tvDaysLeft.setText("आज, १ दिन बांकी");
            } else
                holder1.tvDaysLeft.setText(listNotice.get(position).days_remaining + " दिन बाकी ");

            holder1.tvNotice.setText(listNotice.get(position).notice);
            holder1.tvCommentCount.setText(listNotice.get(position).comment_count + " Comments");
            holder1.tvTitle.setText(listNotice.get(position).title);
            holder1.tvType.setText(listNotice.get(position).type);

            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.onItemClicked(position);
                    Intent intent = new Intent(mContext, FullNoticeViewActivity.class);
                    try {
                        intent.putExtra(CommonDef.NOTICE, listNotice.get(position));
                        mContext.startActivityForResult(intent, CommonDef.REQUEST_COMMENT);
                    } catch (Exception exx) {
                    }
                }
            });

            if (listNotice.get(position).imageUrl.equalsIgnoreCase(""))
                holder1.ivPhoto.setVisibility(View.GONE);
            else {
                Picasso.with(mContext).load(listNotice.get(position).imageUrl).resize(500, 500).into(holder1.ivPhoto);
                holder1.ivPhoto.setVisibility(View.VISIBLE);
            }

            if (!listNotice.get(position).imageUrl.equalsIgnoreCase(""))
                Picasso.with(mContext).load(listNotice.get(position).imageUrl).into(holder1.ivPhoto);

            holder1.ivPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ImageShowFragment imageShowFragment = ImageShowFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putString("image", listNotice.get(position).imageUrl);
                    imageShowFragment.setArguments(bundle);
                    imageShowFragment.show(mContext.getFragmentManager(), "sad");
                }
            });

            holder1.llCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(listNotice.get(position).phone.trim())));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(callIntent);
                }
            });

            holder1.tvCreatedAt.setText(listNotice.get(position).postedDate);

            holder1.llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.onShareToFb(holder1.itemView, listNotice.get(position).imageUrl);
                }
            });

        } else {
            final myNoticeDashbopardViewHolder holder1 = (myNoticeDashbopardViewHolder) holder;
            if (listNotice.get(position).title.isEmpty())
                holder1.tvTitle.setText(listNotice.get(position).notice);
            else
                holder1.tvTitle.setText(listNotice.get(position).title);
            holder1.tvType.setText(listNotice.get(position).type);

            if (listNotice.get(position).days_remaining == 0)
                holder1.tvDeadline.setText("म्याद सकियो");
            else if (listNotice.get(position).days_remaining == 1) {
                holder1.tvDeadline.setText("आज, १ दिन बांकी");
            } else
                holder1.tvDeadline.setText(listNotice.get(position).days_remaining + " दिन बाकी ");
            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.onItemClicked(position);
                    Intent intent = new Intent(mContext, FullNoticeViewActivity.class);
                    try {
                        intent.putExtra(CommonDef.NOTICE, listNotice.get(position));
                        mContext.startActivityForResult(intent, CommonDef.REQUEST_COMMENT);
                    } catch (Exception exx) {
                        Toast.makeText(mContext, mContext.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            if (!listNotice.get(position).imageUrl.equalsIgnoreCase(""))
                Picasso.with(mContext).load(listNotice.get(position).imageUrl).resize(500, 500).placeholder(mContext.getResources().getDrawable(R.drawable.img)).into(holder1.ivPhoto);

        }
    }

    @Override
    public int getItemCount() {
        if (isFromDashBoard) {
            if (listNotice.size() > 10)
                return 10;
            else
                return listNotice.size();
        } else
            return listNotice.size();
    }


    public interface onItemClickListner {
        void onItemClicked(int position);

        void onShareToFb(View itemView, String imageUrl);

    }

    public class myNoticeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvType)
        TextView tvType;
        @BindView(R.id.tv_org_name)
        TextView tvOrgName;
        @BindView(R.id.tv_day_left)
        TextView tvDaysLeft;
        @BindView(R.id.tv_notice)
        TextView tvNotice;
        @BindView(R.id.tv_comment_count)
        TextView tvCommentCount;
        @BindView(R.id.tv_created_at)
        TextView tvCreatedAt;
        @BindView(R.id.ll_call)
        LinearLayout llCall;
        @BindView(R.id.ll_share)
        LinearLayout llShare;
        @BindView(R.id.ivPhoto)
        ImageView ivPhoto;


        public myNoticeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class myNoticeDashbopardViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvType)
        TextView tvType;
        @BindView(R.id.tvDeadline)
        TextView tvDeadline;
        @BindView(R.id.ivPhoto)
        ImageView ivPhoto;


        public myNoticeDashbopardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
