package com.ictfa.krishiguru.myOrganization.view.Objects;

import java.io.Serializable;

/**
 * Created by Prajeet on 01/07/2017.
 */

public class Organisation implements Serializable{
    public String id;
    public String organization_name;
    public String district;
    public String phone;
    public String toll_free;
    public int isSubscribed;
    public String contact_person;
    public String zone;
}
