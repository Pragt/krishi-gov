package com.ictfa.krishiguru.myOrganization.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myOrganization.view.Objects.NoticeObject;
import com.ictfa.krishiguru.myOrganization.view.Objects.Organisation;
import com.ictfa.krishiguru.myOrganization.view.Objects.OrganizationsType;
import com.ictfa.krishiguru.myProfile.UserType;
import com.ictfa.krishiguru.orgainzation.OrganizationsActivity;
import com.ictfa.krishiguru.questionPost.SharePostToFB;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.registerOrganization.object.Zone;
import com.ictfa.krishiguru.registerOrganization.view.RegisterOrganizationActivity;
import com.ictfa.krishiguru.ui.adapter.OrganizationCommentAdapter;
import com.ictfa.krishiguru.ui.dtos.CommentOrganizationDtos;
import com.ictfa.krishiguru.writeNotice.WriteNoticeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class HamroOrganizationActivity extends AppCompatActivity implements NoticeAdapter.onItemClickListner {
    SharedPreference sharedPreference;
    Alerts alerts;
    @BindView(R.id.rv_notice)
    RecyclerView rvNotice;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.fab)
    FloatingActionButton fabPlus;
    ArrayList<NoticeObject> listNotice;
    private List<CommentOrganizationDtos> commentOrganizationDtoses = new ArrayList<>();
    private CustomProgressDialog pd;
    private NoticeAdapter mAdapter;
    private int noticePos;
    private OrganizationCommentAdapter commentAdapter;
    private Realm realm;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreference = new SharedPreference(this);
        setContentView(R.layout.activity_hamro_organization);
        getSupportActionBar().setTitle("अनुदान तथा तालिम सूचना");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.e("RESSULT", "Success");
                //   getAndAddSlots();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.e("RESSULT", "error=" + error.toString());
//                Toast.makeText(Complete_Run_Activity.this, "Unable to Share Image to Facebook.", Toast.LENGTH_SHORT).show();
            }
        });

        commentAdapter = new OrganizationCommentAdapter(HamroOrganizationActivity.this, commentOrganizationDtoses);

        init();
        this.realm = RealmController.with(this).getRealm();
        pd.showpd(getResources().getString(R.string.loading_notice));
        getNoticeListing();
    }

    @OnClick(R.id.fab)
    void onAddNoticeClicked() {
        Intent intent = new Intent(HamroOrganizationActivity.this, WriteNoticeActivity.class);
        startActivity(intent);
    }

    private void getNoticeListing() {
        listNotice = new ArrayList<>();
        mAdapter.notifyDataSetChanged();
        RealmResults<NoticeObject> realmResults = realm.where(NoticeObject.class).findAll();
        for (NoticeObject a : realmResults) {
            listNotice.add(a);
            pd.hidepd();
        }

        mAdapter.notifyDataSetChanged();
        mAdapter = new NoticeAdapter(HamroOrganizationActivity.this, listNotice, new NoticeAdapter.onItemClickListner() {
            @Override
            public void onItemClicked(int position) {

            }

            @Override
            public void onShareToFb(View itemView, String imageUrl) {
                if (isStoragePermissionGranted()) {
                    Bitmap b = getBitmapRootView(itemView);
                    SharePostToFB.share(HamroOrganizationActivity.this, b, imageUrl);
                }
            }
        }, false);
        rvNotice.setAdapter(mAdapter);

        RequestQueue queue = Volley.newRequestQueue(HamroOrganizationActivity.this);
        String url = UrlHelper.BASE_URL + "api/v3/organisation/notices?apikey=" + UrlHelper.API_KEY;
        Log.d("prajit", url);
        listNotice = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        swipeRefreshLayout.setRefreshing(false);
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(response);
//                            JSONObject jsonResponse = object.getJSONObject("code");
//                            int status = jsonResponse.getInt("status");
//                            String message = jsonResponse.getString("message");
//                            if (status != 1) {
//                                alerts.showSuccessAlert(message);
//                            } else {
                            NoticeObject noticeObject;
                            JSONArray noticeList = object.getJSONArray("data");
                            for (int i = 0; i < noticeList.length(); i++) {
                                JSONObject notice = noticeList.getJSONObject(i);
                                noticeObject = new NoticeObject();
                                noticeObject.id = Integer.parseInt(notice.getString("id"));
                                noticeObject.org_name = notice.getString("organisation_name");
                                noticeObject.notice = notice.getString("notice");
                                noticeObject.valid_to = notice.getString("valid_to");
                                noticeObject.phone = notice.getString("phone");
                                noticeObject.toll_free = notice.getString("toll_free");
                                noticeObject.contact_person = notice.getString("contact_person");
                                noticeObject.comment_count = notice.getInt("comment_count");
                                noticeObject.share_count = notice.getInt("share_count");
                                noticeObject.days_remaining = notice.getInt("remaining");
                                noticeObject.postedDate = notice.getString("created_at");
                                noticeObject.imageUrl = notice.getString("image_url");
                                noticeObject.type = notice.getString("type");
                                noticeObject.title = notice.getString("title");
                                noticeObject.noticeFile = notice.getString("notice_file");
                                listNotice.add(noticeObject);
//
                            }
                            mAdapter.notifyDataSetChanged();
                            mAdapter = new NoticeAdapter(HamroOrganizationActivity.this, listNotice, new NoticeAdapter.onItemClickListner() {
                                @Override
                                public void onItemClicked(int position) {

                                }

                                @Override
                                public void onShareToFb(View itemView, String imageUrl) {
                                    if (isStoragePermissionGranted()) {
                                        Bitmap b = getBitmapRootView(itemView);
                                        shareToFb(b, imageUrl);
                                    }
                                }
                            }, false);
                            rvNotice.setAdapter(mAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.hidepd();
                error.printStackTrace();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private void init() {
        pd = new CustomProgressDialog(this);
        alerts = new Alerts(this);
        listNotice = new ArrayList<>();

        rvNotice.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvNotice.setLayoutManager(linearLayoutManager);
        rvNotice.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new NoticeAdapter(this, listNotice, new NoticeAdapter.onItemClickListner() {
            @Override
            public void onItemClicked(int position) {

            }

            @Override
            public void onShareToFb(View itemView, String imageUrl) {
                if (isStoragePermissionGranted()) {
                    Bitmap b = getBitmapRootView(itemView);
//                    SharePostToFB.share(HamroOrganizationActivity.this, b, imageUrl);
                    shareToFb(b, imageUrl);
                }
            }
        }, false);
        rvNotice.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNoticeListing();
            }
        });

    }

    private void shareToFb(Bitmap b, String imageUrl) {
        if (shareDialog.canShow(SharePhotoContent.class)) {

            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(b)
                    .setCaption("Hey... \nI earned by money by running/walking for the charity.")
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();
            shareDialog.show(content);
        }
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            case R.id.mero_sanstha:
                // app icon in action bar clicked; go home
                getOrganizationList();
                return true;
            case R.id.register_sanstha:
                // app icon in action bar clicked; go home
                intent = new Intent(HamroOrganizationActivity.this, RegisterOrganizationActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getOrganizationList() {
        pd.showpd(getResources().getString(R.string.loading_organizations));
        RequestQueue queue = Volley.newRequestQueue(HamroOrganizationActivity.this);
        String url = UrlHelper.BASE_URL + "api/v3/organisations/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/list?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (pd != null) {
                            pd.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                JSONObject object = new JSONObject(utfStr);
                                JSONObject jsonResponse = object.getJSONObject("code");
                                int status = jsonResponse.getInt("status");
                                String message = jsonResponse.getString("message");
                                if (status != 1) {
                                    alerts.showSuccessAlert(message);
                                } else {
                                    JSONArray listObject = object.getJSONArray("data");
                                    JSONObject zoneObject;
                                    Zone zone;
                                    ArrayList<OrganizationsType> listOrgType = new ArrayList<>();
                                    ArrayList<Organisation> listOrganisation;
                                    Organisation organisation;
                                    OrganizationsType organizationsType;
                                    for (int i = 0; i < listObject.length(); i++) {
                                        JSONObject jsonObject = (JSONObject) listObject.get(i);
                                        organizationsType = new OrganizationsType();
                                        organizationsType.organizations_name = jsonObject.names().getString(0);
                                        listOrganisation = new ArrayList<>();
                                        JSONArray jsonListOrg = jsonObject.getJSONArray(organizationsType.organizations_name);
                                        for (int j = 0; j < jsonListOrg.length(); j++) {
                                            JSONObject jsonObjectOrg = (JSONObject) jsonListOrg.get(j);
                                            organisation = new Organisation();
                                            organisation.id = jsonObjectOrg.getString("id");
                                            organisation.organization_name = jsonObjectOrg.getString("organisation_name");
                                            organisation.zone = jsonObjectOrg.getString("zone");
                                            organisation.district = jsonObjectOrg.getString("district");
                                            organisation.phone = jsonObjectOrg.getString("phone");
                                            organisation.toll_free = jsonObjectOrg.getString("toll_free");
                                            organisation.contact_person = jsonObjectOrg.getString("contact_person");
                                            organisation.isSubscribed = jsonObjectOrg.getInt("subscribed");
                                            listOrganisation.add(organisation);
                                        }
                                        organizationsType.listOrganisations = listOrganisation;
                                        listOrgType.add(organizationsType);
                                    }

                                    JSONObject profileJson = object.getJSONObject("profile");
                                    Organisation profile = new Organisation();
                                    if (profileJson != null) {
                                        profile.id = profileJson.getString("id");
                                        profile.organization_name = profileJson.getString("organisation_name");
                                        profile.zone = profileJson.getString("zone");
                                        profile.district = profileJson.getString("district");
                                        profile.phone = profileJson.getString("phone");
                                        profile.toll_free = profileJson.getString("toll_free");
                                        profile.contact_person = profileJson.getString("contact_person");

                                    }

                                    goToOrganisationPage(listOrgType, profile);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                alerts.showToastMsg(e.getMessage());
                            } catch (UnsupportedEncodingException e) {
                                alerts.showToastMsg(e.getMessage());
                                e.printStackTrace();
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null) {
                    pd.hidepd();
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                }
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void goToOrganisationPage(ArrayList<OrganizationsType> listOrgType, Organisation profile) {
        Intent intent = new Intent(HamroOrganizationActivity.this, OrganizationsActivity.class);
        intent.putExtra(CommonDef.ORGANIZATION_LIST, listOrgType);
        intent.putExtra(CommonDef.PROFILE, profile);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.organization_menu, menu);
//        if (sharedPreference.getStringValues(CommonDef.SharedPreference.USERTYPE).equalsIgnoreCase("7"))
//        {
//            MenuItem item = menu.findItem(R.id.register_sanstha);
//            item.setVisible(false);
//        }
        if (sharedPreference.getStringValues(CommonDef.SharedPreference.USERTYPE).equalsIgnoreCase(UserType.TYPE_ORGANISATION)) {
            MenuItem item = menu.findItem(R.id.register_sanstha);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public void onItemClicked(int position) {
        noticePos = position;
    }

    @Override
    public void onShareToFb(View itemView, String imageUrl) {
        if (isStoragePermissionGranted()) {
            Bitmap b = getBitmapRootView(itemView);
            shareToFb(b, imageUrl);
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("Test", "Permission is granted");
                return true;
            } else {

                Log.v("Test", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("Test", "Permission is granted");
            return true;
        }
    }

    private Bitmap getBitmapRootView(View view) {
        View rootView = view;
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("prajit", "onBackPressed: " + "bacpressed 1");
        if (requestCode == CommonDef.REQUEST_COMMENT) {
            if (resultCode == RESULT_OK) {
                if (listNotice.size() > noticePos) {
                    NoticeObject noticeObject = (NoticeObject) data.getSerializableExtra(CommonDef.NOTICE);
                    listNotice.get(noticePos).listComment = noticeObject.listComment;
                    listNotice.get(noticePos).comment_count = noticeObject.comment_count;
                    mAdapter.notifyItemChanged(noticePos);
                    Log.i("prajit", "onBackPressed: " + "bacpressed 234");
                }
            }
        } else {
//            if (resultCode == RESULT_OK)
//                Toast.makeText(this, "call share api", Toast.LENGTH_SHORT).show();
            try {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("test", "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}

