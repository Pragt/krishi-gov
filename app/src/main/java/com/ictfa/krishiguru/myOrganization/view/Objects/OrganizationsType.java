package com.ictfa.krishiguru.myOrganization.view.Objects;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Prajeet on 01/07/2017.
 */

public class OrganizationsType implements Serializable {
    public String organizations_name;
    public ArrayList<Organisation> listOrganisations;

}
