package com.ictfa.krishiguru.myProfile;

/**
 * Created by Prajeet on 25/07/2017.
 */

public interface UserType {
     String TYPE_FARMER = "farmer";
     String TYPE_ORGANISATION = "organisation";
     String TYPE_TRADER = "trader";
}
