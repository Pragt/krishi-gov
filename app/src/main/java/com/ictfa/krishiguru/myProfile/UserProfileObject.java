package com.ictfa.krishiguru.myProfile;

import java.io.Serializable;

/**
 * Created by Prajeet on 25/07/2017.
 */

public class UserProfileObject implements Serializable {

    public String name;
    public String address;
    public String phone;
    public String marketName;
    public String org_name;
    public String org_address;
    public String org_phone;
    public String toll_free_no;
    public String org_type;
    public String image;
    public String UserType;
    public String marketType;
    public String id;
}