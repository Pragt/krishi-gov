package com.ictfa.krishiguru.myProfile;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.BeATrader.BeATraderActivity;
import com.ictfa.krishiguru.MultipleTraders.object.TraderType;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.editProfile.EditProfileDIalog;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.registerOrganization.view.RegisterOrganizationActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfileActivity extends AppCompatActivity {

    @BindView(R.id.tv_name)
    public TextView tvName;
    public UserProfileObject userProfile;
    @BindView(R.id.iv_profile_pic)
    public CircleImageView ivProfilePic;
    @BindView(R.id.ll_trader_profile)
    LinearLayout llTraderProfile;
    @BindView(R.id.ll_organisation_profile)
    LinearLayout llOrgProfile;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_phone_no)
    TextView tvPhoneNo;
    @BindView(R.id.tv_market_address)
    TextView tvMarketAddress;
    @BindView(R.id.tv_org_name)
    TextView tvOrgName;
    @BindView(R.id.tv_org_type)
    TextView tvOrgType;
    @BindView(R.id.tv_org_address)
    TextView tvOrgAddress;
    @BindView(R.id.tv_org_phone_no)
    TextView tvOrgPhoneNo;
    @BindView(R.id.tv_toll_free_no)
    TextView tvTollFreeNumber;
    @BindView(R.id.iv_edit_profile)
    ImageView ivEditProfile;

    CustomProgressDialog progressDialog;
    Alerts alerts;
    SharedPreference preference;
    private ArrayList<TraderType> traderTypeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        preference = new SharedPreference(this);
        alerts = new Alerts(this);
        progressDialog = new CustomProgressDialog(this);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.my_profile));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            userProfile = (UserProfileObject) b.getSerializable(CommonDef.USER_PROFILE);
            setDetails();
        }
    }

    private void setDetails() {
        if (userProfile.UserType.equalsIgnoreCase(UserType.TYPE_ORGANISATION)) {
            llOrgProfile.setVisibility(View.VISIBLE);
            tvOrgName.setText(userProfile.org_name);
            tvOrgPhoneNo.setText(userProfile.org_phone);
            tvOrgType.setText(userProfile.org_type);
            tvAddress.setText(userProfile.org_address);
            tvTollFreeNumber.setText(userProfile.toll_free_no);

        } else if (userProfile.UserType.equalsIgnoreCase(UserType.TYPE_TRADER)) {
            llTraderProfile.setVisibility(View.VISIBLE);
            tvMarketAddress.setText(userProfile.marketName + ", " + userProfile.marketType);
        }
        tvName.setText(userProfile.name);
        tvAddress.setText(userProfile.address);
        tvPhoneNo.setText(userProfile.phone);
        if (!userProfile.image.equalsIgnoreCase("")) {
            Picasso.with(this).load(userProfile.image).
                    placeholder(getResources().getDrawable(R.drawable.com_facebook_profile_picture_blank_square)).
                    into(ivProfilePic);
        }
    }

    @OnClick(R.id.iv_edit_profile)
    void onEditProfileClick() {
        new EditProfileDIalog().show(getFragmentManager(), "edit_profile");
    }

    @OnClick(R.id.iv_edit_organisation)
    void onEditOrganisation() {
        Intent intent = new Intent(MyProfileActivity.this, RegisterOrganizationActivity.class);
        intent.putExtra(CommonDef.USER_PROFILE, userProfile);
        startActivity(intent);
    }

    @OnClick(R.id.iv_edit_trader)
    void onEditTrader() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = UrlHelper.BASE_URL + "api/v3/trader/type?apikey=" + UrlHelper.API_KEY;
        traderTypeList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        pd.hidepd();
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONArray traderTypeListJson = object.getJSONArray("data");
                            for (int i = 0; i < traderTypeListJson.length(); i++) {
                                JSONObject object1 = (JSONObject) traderTypeListJson.get(i);
                                TraderType traderType = new TraderType();
                                traderType.id = object1.getString("id");
                                traderType.type = object1.getString("type");
                                traderType.nepali_name = object1.getString("nepali_name");
                                traderTypeList.add(traderType);
                            }
                            Intent intent = new Intent(MyProfileActivity.this, BeATraderActivity.class);
                            intent.putExtra(CommonDef.TRADER_TYPE, traderTypeList);
                            intent.putExtra(CommonDef.IS_EDIT_MODE, true);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                pd.hidepd();
//                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void getUserProfile() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = UrlHelper.BASE_URL + "api/v3/user/" + preference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/profile?apikey=" + UrlHelper.API_KEY;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONArray responseJson = object.getJSONArray("data");
//                            String status = jsonObject.getString("status");
//                            String messege = jsonObject.getString("message");
                            if (responseJson != null && responseJson.length() > 0) {
                                JSONObject jsonObject = (JSONObject) responseJson.get(0);
                                userProfile.name = jsonObject.getString("name");
                                userProfile.address = jsonObject.getString("location");
                                userProfile.phone = jsonObject.getString("phone");
                                userProfile.image = jsonObject.getString("image");
                                userProfile.UserType = UserType.TYPE_FARMER;
                                if (jsonObject.getString("user_type").equalsIgnoreCase("farmer")) {
                                    userProfile.UserType = UserType.TYPE_FARMER;
                                } else if (jsonObject.getString("user_type").equalsIgnoreCase("trader")) {
                                    userProfile.UserType = UserType.TYPE_TRADER;
                                    JSONArray marketJsonArray = jsonObject.getJSONArray("market");
                                    if (marketJsonArray.length() > 0) {
                                        JSONObject market = (JSONObject) marketJsonArray.get(0);
                                        userProfile.marketName = market.getString("market_name");
                                        userProfile.marketType = market.getString("market_type");
                                    }

                                } else if (jsonObject.getString("user_type").equalsIgnoreCase("organisation")) //change this to orgajisation
                                {
                                    userProfile.UserType = UserType.TYPE_ORGANISATION;
                                    JSONArray organisationJson = jsonObject.getJSONArray("organisation");
                                    if (organisationJson.length() > 0) {
                                        JSONObject organisation = (JSONObject) organisationJson.get(0);
                                        userProfile.org_name = organisation.getString("organization_name");
                                        userProfile.org_phone = organisation.getString("organization_phone");
                                        userProfile.org_type = organisation.getString("organization_type");
                                        userProfile.org_address = organisation.getString("organization_address");
                                        userProfile.toll_free_no = organisation.getString("organization_toll_free");
                                    }
                                }
                                setDetails();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserProfile();
    }
}
