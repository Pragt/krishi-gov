package com.ictfa.krishiguru.productDetail;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pragt on 3/5/17.
 */

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ITEM_SEND_MESSAGE = 1;
    private static final int ITEM_RECEIVE_MESSAGE = 2;
    ArrayList<Comment> listComment;
    private Context mContext;

    public MessageAdapter(Context mContext, ArrayList<Comment> listNotice) {
        this.mContext = mContext;
        this.listComment = listNotice;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == ITEM_SEND_MESSAGE)
            view = LayoutInflater.from(mContext).inflate(R.layout.item_send_message, parent, false);
        else
            view = LayoutInflater.from(mContext).inflate(R.layout.item_receive_message, parent, false);
        return new myNoticeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myNoticeViewHolder) {
            final myNoticeViewHolder holder1 = (myNoticeViewHolder) holder;
            holder1.tvName.setText(listComment.get(position).commented_by);
            holder1.tvComment.setText(listComment.get(position).comment);
            holder1.tvDateTime.setText(listComment.get(position).date);

        }
    }

    @Override
    public int getItemCount() {
        return listComment.size();
    }

    public void addComment(Comment newComment) {
//        listComment.add(0, newComment);
        notifyItemInserted(getItemCount());

    }

    @Override
    public int getItemViewType(int position) {
        if (listComment.get(position).direction.equalsIgnoreCase("0"))
            return ITEM_SEND_MESSAGE;
        else
            return ITEM_RECEIVE_MESSAGE;
    }

    public class myNoticeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_comment)
        TextView tvComment;
        @BindView(R.id.tv_date_time)
        TextView tvDateTime;

        public myNoticeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
