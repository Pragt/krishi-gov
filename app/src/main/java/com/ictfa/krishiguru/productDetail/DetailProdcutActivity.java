package com.ictfa.krishiguru.productDetail;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;
import com.ictfa.krishiguru.ui.activity.PlayVideoActivity;
import com.ictfa.krishiguru.ui.fragment.ImageShowFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailProdcutActivity extends AppCompatActivity {
    //    Permissions permissions = new Permissions();
    @BindView(R.id.tv_address)
    TextView tvAddress;
    CustomProgressDialog pd;
    @BindView(R.id.ll_call)
    LinearLayout llCall;
    @BindView(R.id.ll_comments)
    LinearLayout llComments;
    @BindView(R.id.ll_buy)
    LinearLayout llBuy;
    private ImageView imageView, video;
    @BindView(R.id.rlVideo)
    RelativeLayout rvVideo;
    private TextView title, detal, intialPrice, finalPrice, seller;
    private String id;
    private String callNum;
    private Alerts alerts;
    private SharedPreference sharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_prodcut);
        ButterKnife.bind(this);
        sharedPreference = new SharedPreference(this);
        imageView = (ImageView) findViewById(R.id.image);
        video = (ImageView) findViewById(R.id.video);
        title = (TextView) findViewById(R.id.title);
        detal = (TextView) findViewById(R.id.detal);
        intialPrice = (TextView) findViewById(R.id.intialPriceValue);
        finalPrice = (TextView) findViewById(R.id.finalPriceValue);
        seller = (TextView) findViewById(R.id.sellerValue);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        permissions.verifyPhonePermissions(DetailProdcutActivity.this);

        Bundle extras = getIntent().getExtras();
        alerts = new Alerts(this);
        id = extras.getString("id");
        pd = new CustomProgressDialog(this);
        setTitle(extras.getString("title"));
        boolean openComment = extras.getBoolean("openComment");
        if (openComment)
            getComments();

        getProduct();
        //Log.d("img", img);
    }

    private void getProduct() {
        pd.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(DetailProdcutActivity.this);
        String url = "http://admin.ict4agri.com/api/v3/featured/" + id + "/view?%20apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
        Log.d("FORMURL", url);
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //commentList.clear();
                        pd.hidepd();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            Log.d("utfstr", response);
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject datavalue = jsonArray.getJSONObject(i);
                                String titlevalue = datavalue.getString("name");
                                setTitle(titlevalue);
                                String desc = datavalue.getString("description");
                                String unit = datavalue.getString("unit");
                                String initlaPrice = datavalue.getString("price");
                                String fianlPrice = datavalue.getString("discounted_price");
                                String name = datavalue.getString("agency_name");
                                String call = datavalue.getString("agency_phone");
                                String video_url = datavalue.getString("video_url");
                                String address = datavalue.getString("agency_address");
                                callNum = call;

                                JSONArray imageArray = datavalue.getJSONArray("images");
                                JSONObject imageValue = imageArray.getJSONObject(0);
                                String imageName = imageValue.getString("name");
                                String imagePath = "http://kinmel.ict4agri.com/assets/images/product/" + imageName;

                                Picasso.with(DetailProdcutActivity.this).load(imagePath).resize(500, 500).into(imageView);
                                title.setText(titlevalue);
                                detal.setText(desc);
                                tvAddress.setText(address);
                                intialPrice.setText(initlaPrice + " प्रति " + (unit.isEmpty() ? " युनिट " : unit));
                                finalPrice.setText(fianlPrice + " प्रति " + (unit.isEmpty() ? " युनिट " : unit));
                                seller.setText(name);
                                intialPrice.setPaintFlags(intialPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                                if (video_url.isEmpty())
                                    rvVideo.setVisibility(View.GONE);
                                else {
                                    rvVideo.setVisibility(View.VISIBLE);
                                    Glide.with(DetailProdcutActivity.this).load("http://img.youtube.com/vi/" + getYoutubeVideoIdFromUrl(video_url) + "/0.jpg").placeholder(R.drawable.img).into(video);

                                    video.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(DetailProdcutActivity.this, PlayVideoActivity.class);
                                            intent.putExtra("videoId", getYoutubeVideoIdFromUrl(video_url));
                                            startActivity(intent);
                                        }
                                    });
                                }

                                imageView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ImageShowFragment imageShowFragment = ImageShowFragment.newInstance();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("image", imagePath);
                                        imageShowFragment.setArguments(bundle);
                                        imageShowFragment.show(getFragmentManager(), "sad");
                                    }
                                });

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.hidepd();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            pd.hidepd();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.hidepd();
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @OnClick(R.id.ll_call)
    void onCall() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(callNum)));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
    }

    @OnClick(R.id.ll_buy)
    void onBuy() {
        Toast.makeText(this, "याे सेवा चाँडै उपलब्ध हनेछ ।", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.ll_comments)
    void onComments() {
        getComments();
//        Toast.makeText(this, "याे सेवा चाँडै उपलब्ध हनेछ ।", Toast.LENGTH_SHORT).show();
    }

    private void getComments() {
        pd.showpd(getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(this);
        String zone = UrlHelper.BASE_URL + "api/v3/featured/" + id + "/comments?apikey=" + UrlHelper.API_KEY + "&user_id=" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
        final ArrayList<Comment> listComment = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //commentList.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray data = jsonObject.getJSONArray("data");
                            Comment comment;
                            if (data != null && data.length() > 0) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject commentObj = data.getJSONObject(i);
                                    comment = new Comment();
                                    comment.comment_id = Integer.parseInt(commentObj.getString("id"));
                                    comment.comment = commentObj.getString("comment");
                                    comment.commented_by = commentObj.getString("commented_by");
                                    comment.date = commentObj.getString("created_at");
                                    comment.direction = commentObj.getString("direction");
                                    listComment.add(comment);
                                }

                            }

                            Intent intent = new Intent(DetailProdcutActivity.this, CommentOnProductDetaiActivity.class);
                            Collections.reverse(listComment);
                            intent.putExtra(CommonDef.COMMENTS, listComment);
                            intent.putExtra(CommonDef.PRODUCT_ID, id);

                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                pd.hidepd();
                // mTextView.setText("That didn't work!");
            }


        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static String getYoutubeVideoIdFromUrl(String inUrl) {
        inUrl = inUrl.replace("&feature=youtu.be", "");
        if (inUrl.toLowerCase().contains("youtu.be")) {
            return inUrl.substring(inUrl.lastIndexOf("/") + 1);
        }
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(inUrl);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }
}
