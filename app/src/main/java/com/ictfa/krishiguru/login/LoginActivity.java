package com.ictfa.krishiguru.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.dashboard.DashBoardActivity;
import com.ictfa.krishiguru.forgetPassword.ForgetPasswordActivity;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.GPSTracker;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.ui.activity.RegistrationActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity implements LocationListener {
    private static final String FACEBOOK_REGISTER_URL = UrlHelper.BASE_URL + "api/v3/update/mobile?apikey=" + UrlHelper.API_KEY;
    SharedPreference sharedPreferences;
    CustomProgressDialog pd;
    private EditText usernameView, passwordView;
    private Button loginbtn;
    private TextView newAccount, passwordChange;
    private String usernameValue, passwordValue;
    private String token = "";
    boolean isFromLogin = false;
    public static String lat = "27.7172";
    public static String lon = "85.3240";
    private GPSTracker gpsTracker;
    private static final int REQUEST_CODE_PERMISSION = 1;
    private GPSTracker gps;
    private LocationManager locationManager;
    private Button fbLoginButton;
    private CallbackManager callbackManager;

    private String KEY_NAME = "name";
    private String KEY_LAT = "lat";
    private String KEY_LON = "lng";
    private String KEY_USERNAME = "username";
    private String KEY_PASSWORD = "password";
    private String KEY_FCM = "fcm_token";
    private String KEY_DEVICE_ID = "device_code";
    private String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        CommonMethods.setupUI(findViewById(R.id.ll_login), this);
        getSupportActionBar().setTitle(getResources().getString(R.string.login));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        usernameView = (EditText) findViewById(R.id.loginUsername);
        passwordView = (EditText) findViewById(R.id.loginPassword);
        loginbtn = (Button) findViewById(R.id.loginBtn);
        newAccount = (TextView) findViewById(R.id.signup);
        passwordChange = (TextView) findViewById(R.id.forgetPassword);
        sharedPreferences = new SharedPreference(this);
        fbLoginButton = findViewById(R.id.login_button);
        pd = new CustomProgressDialog(this);
        String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;

        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        callbackManager = CallbackManager.Factory.create();
//        loginButton.setReadPermissions(Arrays.asList(EMAIL));
        // Set permissions
        fbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("user_photos",
                        "public_profile"));

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                // App code
                                GraphRequest request = GraphRequest.newMeRequest(
                                        loginResult.getAccessToken(),
                                        new GraphRequest.GraphJSONObjectCallback() {
                                            @Override
                                            public void onCompleted(JSONObject object, GraphResponse response) {
                                                Log.v("LoginActivity", response.toString());
                                                try {
                                                    String name = object.getString("name");
//                                                    Toast.makeText(LoginActivity.this, name + " " + email, Toast.LENGTH_SHORT).show();
                                                    doFbLogin(name, loginResult.getAccessToken().getToken());

                                                } catch (JSONException e) {
                                                    pd.hidepd();
//                                            e.printStackTrace();
                                                }
                                            }
                                        });
                                Bundle parameters = new Bundle();
                                parameters.putString("fields", "id,name,picture,email,gender,birthday");
                                request.setParameters(parameters);
                                request.executeAsync();
                            }

                            @Override
                            public void onCancel() {
                                // App code
                                pd.hidepd();
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                // App code
                                pd.hidepd();
                            }
                        });
            }
        });


        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validForm()) {
                    token = FirebaseInstanceId.getInstance().getToken();
//                    if (token == null || token.isEmpty()) {
//                        Toast.makeText(LoginActivity.this, "Token is null", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(LoginActivity.this, token, Toast.LENGTH_SHORT).show();
//                    }
                    getLogin();
                }
            }
        });

        passwordChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);

            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            gps = new GPSTracker(LoginActivity.this);

            // Check if GPS enabled
            if (!gps.canGetLocation()) {
                gps.showSettingsAlert();
            }
        }

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        } catch (Exception exx) {
        }


        newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });

        if (getIntent().getExtras() != null)
            isFromLogin = getIntent().getExtras().getBoolean("is_from_login");
    }

    private void doFbLogin(String name, String accessToken) {
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);

        String url = UrlHelper.BASE_URL + "api/v3/fb/login?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    public String message;

                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject code = jsonObject.getJSONObject("code");
                            String status = code.getString("status");
                            message = code.getString("message");
                            if (status.equalsIgnoreCase("1")) {

                                JSONObject datavalue = code.getJSONObject("user");

                                int id = Integer.parseInt(datavalue.getString("id"));
                                String priorities = datavalue.getString("priorities");
                                String userType = datavalue.getString("user_type");
                                Boolean isNew = datavalue.getBoolean("is_new");
                                String image = datavalue.getString("image");
                                sharedPreferences.setKeyValues(CommonDef.SharedPreference.IS_LOGIN, 1);
                                sharedPreferences.setKeyValues(CommonDef.SharedPreference.USER_ID, id);
                                sharedPreferences.setKeyValues(CommonDef.SharedPreference.LATUTUDE, lat);
                                sharedPreferences.setKeyValues(CommonDef.SharedPreference.LONGITUDE, lon);
                                sharedPreferences.setKeyValues(CommonDef.SharedPreference.USERNAME, datavalue.getString("name"));
                                sharedPreferences.setKeyValues("priorities", priorities);
                                sharedPreferences.setKeyValues(CommonDef.SharedPreference.USERTYPE, userType);
                                if (datavalue.getString("is_number_verified") == "1")
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.IS_NUMBER_VERIFIED, true);
                                if (datavalue.getString("is_otp_send") == "1")
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.IS_OTP_SEND, true);
                                sharedPreferences.setKeyValues(CommonDef.SharedPreference.VERIFIED_NUMBER, datavalue.getString("verified_mobile_number"));
                                Toast.makeText(LoginActivity.this, "succcess", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (status.equalsIgnoreCase("2")) {
                                JSONObject datavalue = code.getJSONObject("user");
                                Boolean isNew = datavalue.getBoolean("is_new");
                                int id = Integer.parseInt(datavalue.getString("id"));
                                String image = datavalue.getString("image");
                                AddMobileNumberDialog dialog = new AddMobileNumberDialog();
                                Bundle args = new Bundle();
                                args.putString("name", name);
                                args.putInt("id", id);
                                args.putString("image", image);
                                dialog.setArguments(args);
                                dialog.show(getFragmentManager(), "add mobile number");
                            } else
                                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                pd.hidepd();
                // mTextView.setText("That didn't work!");
            }


        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lat", lat);
                params.put("lng", lon);
                params.put("fcm_token", FirebaseInstanceId.getInstance().getToken());
                params.put("device_code", deviceId);
                params.put("access_token", accessToken);
                return params;
            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private boolean validForm() {
        usernameValue = usernameView.getText().toString().trim();
        passwordValue = passwordView.getText().toString().trim();

        if (usernameValue.isEmpty()) {
            Toast.makeText(LoginActivity.this, "युजरनाम खाली छ", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (passwordValue.isEmpty()) {
            Toast.makeText(LoginActivity.this, "पासवर्ड खाली छ", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void getLogin() {
        pd.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);

        String LOGIN_URL = "http://admin.ict4agri.com/api/v3/user/login?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA&username=" + usernameValue + "&password=" + passwordValue + "&lat=" + lat + "&lng=" + lon + "&fcm_token=" + token;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonValue = jsonArray.getJSONObject(i);
                                if (jsonValue.has("status")) {
                                    Toast.makeText(LoginActivity.this, "युजरनाम वा पासवर्ड मिलेन", Toast.LENGTH_SHORT).show();
                                } else {
                                    JSONObject value = jsonArray.getJSONObject(0);

                                    int id = Integer.parseInt(value.getString("id"));
                                    String lat = value.getString("lat");
                                    String lon = value.getString("lng");
                                    String username = value.getString("fullname");
                                    String agency = value.getString("agency");
                                    String code = value.getString("code");
                                    String priorities = value.getString("priorities");
                                    String userType = value.getString("user_type");
                                    String userOrgId = value.getString("org_id");
                                    String isNumberVerified = value.getString("is_number_verified");
                                    String verifiedMobileNumber = value.getString("verified_mobile_number");
                                    String isOTPSend = value.getString("is_otp_send");
                                    String hasPackageSubscribe = value.getString("has_package_subscribed");
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.IS_LOGIN, 1);
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.IS_DEVICE_REGISTERED, true);
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.USER_ID, id);
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.LATUTUDE, lat);
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.LONGITUDE, lon);
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.USERNAME, username);
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.AGENCY, agency);
                                    sharedPreferences.setKeyValues("code", code);
                                    sharedPreferences.setKeyValues("priorities", priorities);
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.ORG_ID, userOrgId);
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.USERTYPE, userType);
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.VERIFIED_NUMBER, verifiedMobileNumber);
                                    sharedPreferences.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, hasPackageSubscribe);

                                    if (isNumberVerified.equalsIgnoreCase("1"))
                                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.IS_NUMBER_VERIFIED, true);
                                    if (isOTPSend.equalsIgnoreCase("1"))
                                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.IS_OTP_SEND, true);

                                    if (isFromLogin) {
                                        Intent intent = new Intent();
                                        setResult(RESULT_OK);
                                    }
                                    finish();
//                                    Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
//                                    startActivity(intent);

                                }
                            }

                            int lengthOfArray = jsonArray.length();


                        } catch (JSONException e) {
                            Toast.makeText(sharedPreferences, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
//                Toast.makeText(sharedPreferences, error.toString(), Toast.LENGTH_SHORT).show();
                pd.hidepd();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                    gps = new GPSTracker(LoginActivity.this);

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        lat = String.valueOf(gps.getLatitude());
                        lon = String.valueOf(gps.getLongitude());

                        // \n is for new line
//                        Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + lat + "\nLong: " + lon, Toast.LENGTH_LONG).show();
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
//                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                        ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//
//                    }

                    Toast.makeText(this, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
//        Toast.makeText(this, "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude(), Toast.LENGTH_SHORT).show();
        lat = String.valueOf(location.getLatitude());
        lon = String.valueOf(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            locationManager.removeUpdates(this);
        } catch (Exception exx) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        pd.showpd(getResources().getString(R.string.please_wait));
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }

    public void doFbRegister(String mobile_number, int id, String image) {
        pd.showpd(getResources().getString(R.string.please_wait));
        RequestParams params = new RequestParams();

        if (image != null && !image.equalsIgnoreCase("")) {
            File myFile = new File(image);

            try {
                params.put("image", myFile, "image/jpeg");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
//        }


        params.put("user_id", id);
        params.put("mobile", mobile_number);

        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(10000);
        client.setTimeout(10000);
        client.setResponseTimeout(30000);
        client.post(FACEBOOK_REGISTER_URL, params, new JsonHttpResponseHandler() {

            public String message;

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject jsonObject) {
                pd.hidepd();
                try {
//                    JSONObject jsonObject = new JSONObject(responseBody);
                    JSONObject code = jsonObject.getJSONObject("code");
                    String status = code.getString("status");
                    message = code.getString("message");
                    if (status.equalsIgnoreCase("1")) {
                        JSONObject datavalue = code.getJSONObject("user");

                        int id = Integer.parseInt(datavalue.getString("id"));
                        String priorities = datavalue.getString("priorities");
                        String userType = datavalue.getString("user_type");
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.IS_LOGIN, 1);
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.USER_ID, id);
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.LATUTUDE, lat);
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.LONGITUDE, lon);
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.USERNAME, datavalue.getString("name"));
                        sharedPreferences.setKeyValues("priorities", priorities);
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.USERTYPE, userType);
                        Toast.makeText(LoginActivity.this, "स्वागतम  तपाईंको अकाउन्ट दरत भको छ ।", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (message.equals("User already exist")) {
                        Toast.makeText(LoginActivity.this, "यो युजरनाम पहेला नै प्रयोग भएसक्यो। अर्को युजरनाम चानुहोस्", Toast.LENGTH_LONG).show();
                    } else
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                    pd.hidepd();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                pd.hidepd();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                pd.hidepd();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                pd.hidepd();
            }
        });
    }
}
