package com.ictfa.krishiguru.login;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.FileUtils;
import com.ictfa.krishiguru.helpers.GPSTracker;
import com.ictfa.krishiguru.helpers.ImagePicker;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.PermissionChecker.checkSelfPermission;

/**
 * Created by Prajeet on 10/09/2017.
 */

public class AddMobileNumberDialog extends DialogFragment implements LocationListener {
    SharedPreference sharedPreference;
    CustomProgressDialog pd;
    private ImageView ivProfilePic;
    private EditText etMobileNumber;
    private TextView tvWelcomeMessage;
    private Button btnSave;
    private LoginActivity mContext;
    private String image;
    private String fbImage;
    private Uri imageUri;
    private GPSTracker gps;
    private LocationManager locationManager;
    public String lat = "27.7172";
    public String lon = "85.3240";

    String name = "";
    private int id;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = (LoginActivity) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mContext = (LoginActivity) activity;

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        sharedPreference = new SharedPreference(getActivity());
        pd = new CustomProgressDialog((Activity) getActivity());
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_mobile_number, container, false);
        ivProfilePic = (ImageView) view.findViewById(R.id.iv_profile_pic);
        etMobileNumber = (EditText) view.findViewById(R.id.et_mobile_number);
        btnSave = (Button) view.findViewById(R.id.btn_save);
        tvWelcomeMessage = (TextView) view.findViewById(R.id.tvWelcomeMsg);

        name = getArguments().getString("name");
        id = getArguments().getInt("id");
        fbImage = getArguments().getString("image");

        tvWelcomeMessage.setText("नमस्ते " + name + ". जी, तपाईलाइ कृषि गुरु मा स्वागत छ. कृपया आफ्नो मोबिले नम्बर राख्नुहोस.");

        if (fbImage != null && !fbImage.equalsIgnoreCase(""))
        {
            Picasso.with(mContext).load(fbImage).
                    placeholder(getResources().getDrawable(R.drawable.com_facebook_profile_picture_blank_square)).
                    into(ivProfilePic);
        }

        this.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid())
                doSave(etMobileNumber.getText().toString(), name, id);
            }
        });
//
        ivProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });
        return view;
    }

    private boolean isValid() {
        if (etMobileNumber.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "आफ्नु मोबाईल नम्बर लेखनुहोस् ", Toast.LENGTH_SHORT).show();
            return false;
        }else if (!(etMobileNumber.getText().toString().length() == 10) || !(etMobileNumber.getText().toString().substring(0, 1).matches("9"))) {
            Toast.makeText(getActivity(), "मोबाईल नम्बर मिलेन", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void doSave(String mobileNumber, String name, int id) {
        mContext.doFbRegister(mobileNumber,id, image);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CommonDef.REQUEST_STORAGE_CAMERA);
                return;
            }
        }
        startActivityForResult(ImagePicker.getPickImageChooserIntent((Activity) mContext), CommonDef.PICK_IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonDef.PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                imageUri = data.getData();
                ivProfilePic.setImageURI(imageUri);
                Bitmap imageBmp;
                try {
                    imageBmp = FileUtils.getThumbnail(getActivity(), imageUri, 1080);
                    image = FileUtils.storeBitmapToFile(imageBmp, getActivity());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else // From camera
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    image = ImagePicker.getImage.getAbsolutePath();
                } else
                    image = ImagePicker.outputFileUri.getPath();

                ivProfilePic.setImageURI(Uri.fromFile(new File(image)));
            }

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            startActivityForResult(ImagePicker.getPickImageChooserIntent(getActivity()), CommonDef.PICK_IMAGE_REQUEST);
        }
    }


    @Override
    public void onLocationChanged(Location location) {
//        Toast.makeText(this, "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude(), Toast.LENGTH_SHORT).show();
        lat = String.valueOf(location.getLatitude());
        lon = String.valueOf(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}

