package com.ictfa.krishiguru.malkhadCalculator;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonMethods;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MalkhadCalculator extends AppCompatActivity {

    ArrayList<Fertilizer> fertilizerList;
    @BindView(R.id.tv_malkhad_title)
    TextView tvMalkhadTitle;

    @BindView(R.id.scrollView)
    ScrollView scrollView;

    @BindView(R.id.sp_crop)
    Spinner spCrop;

    @BindView(R.id.sp_system)
    Spinner spSystem;

    @BindView(R.id.edt_ropani)
    EditText edtRopani;

    @BindView(R.id.tv_ropani)
    TextView tvRopani;

    @BindView(R.id.edt_aana)
    EditText edtAana;

    @BindView(R.id.tv_aana)
    TextView tvAana;

    @BindView(R.id.btn_calculate)
    Button btnCalculate;

    @BindView(R.id.tv_result)
    TextView tvResult;

    @BindView(R.id.tv_compost)
    TextView tvCompost;

    @BindView(R.id.tv_dap)
    TextView tvDap;

    @BindView(R.id.tv_urea)
    TextView tvUrea;

    @BindView(R.id.tv_postas)
    TextView tvPotas;

    @BindView(R.id.tv_remarks)
    TextView tvRemarks;

    @BindView(R.id.tv_source)
    TextView tvSource;

    @BindView(R.id.ll_result)
    LinearLayout llResult;

    ArrayList<String> cropList;
    ArrayList<String> systemList;
    boolean isRasayanik = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_malkhad_calculator2);
        ButterKnife.bind(this);
        CommonMethods.setupUI(findViewById(R.id.layout_calculator), this);

        setTitle("कृषि बाली क्यालकुलेटर");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fertilizerList = new ArrayList<>();
        cropList = new ArrayList<>();
        systemList = new ArrayList<>();
        isRasayanik = getIntent().getExtras().getBoolean("is_rasayanic");
        if (isRasayanik) {
            loadChemicalData();
            tvMalkhadTitle.setText("रासायनिक Dose");
        }
        else {
            loadOrganicData();
            tvMalkhadTitle.setText("प्रांगारिक Dose");
        }
        loadSpinners();

        spSystem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    tvRopani.setText("रोपनी");
                    tvAana.setText("आना");
                } else if (i == 1) {
                    tvRopani.setText("बिघा");
                    tvAana.setText("कठ्ठा");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @OnClick(R.id.btn_calculate)
    void doCalculate() {
        if (!edtRopani.getText().toString().isEmpty() || !edtAana.getText().toString().isEmpty()) {
            convertIntoRopani();
            llResult.setVisibility(View.VISIBLE);

            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });
        }
    }

    private void convertIntoRopani() {
        Double totalRopani;
        Double first = edtRopani.getText().toString().isEmpty() ? 0 : Double.parseDouble(edtRopani.getText().toString());
        Double second = edtAana.getText().toString().isEmpty() ? 0 : Double.parseDouble(edtAana.getText().toString());

        String result = "";
        if (first != 0) {
            if (spSystem.getSelectedItemPosition() == 0)
                result = first + " रोपनी ";
            else
                result = first + " बिघा ";

            if (second != 0)
                if (spSystem.getSelectedItemPosition() == 0)
                    result = result + "र " + second + " आना ";
                else
                    result = result + "र " + second + " कठ्ठा ";

        } else {
            if (spSystem.getSelectedItemPosition() == 0)
                result = second + " आना ";
            else
                result = second + " कठ्ठा ";
        }

        tvResult.setText(result + " " + fertilizerList.get(spCrop.getSelectedItemPosition()).name + " को लागि चाहिने" + (isRasayanik? " रासायनिक ":" प्रांगारिक ")  + "मलखादहरु:");

        if (spSystem.getSelectedItemPosition() == 0) {
            totalRopani = first + second / 16;
        } else {
            totalRopani = (first * 13.316) + (second * 0.6656);
        }

        if (isRasayanik) {
            tvCompost.setText("कम्पोस्ट: " + Math.round(totalRopani * fertilizerList.get(spCrop.getSelectedItemPosition()).compost) + " केजी");
            tvUrea.setText("युरिया: " + Math.round(totalRopani * fertilizerList.get(spCrop.getSelectedItemPosition()).urea) + " केजी");
            tvDap.setText("डी. ए. पी.: " + Math.round(totalRopani * fertilizerList.get(spCrop.getSelectedItemPosition()).dap) + " केजी");
            tvPotas.setText("पोटास: " + Math.round(totalRopani * fertilizerList.get(spCrop.getSelectedItemPosition()).potash) + " केजी");
        }else
        {
            tvCompost.setText("कम्प्लेस्क: " + Math.round(totalRopani * fertilizerList.get(spCrop.getSelectedItemPosition()).compost) + " केजी");
            tvUrea.setText("कम्पोस्ट: " + Math.round(totalRopani * fertilizerList.get(spCrop.getSelectedItemPosition()).urea) + " केजी");
            tvDap.setText("कुखुरा मल: " + Math.round(totalRopani * fertilizerList.get(spCrop.getSelectedItemPosition()).dap) + " केजी");
            tvPotas.setText("नीम केक: " + Math.round(totalRopani * fertilizerList.get(spCrop.getSelectedItemPosition()).potash) + " केजी");
        }

        tvRemarks.setText(fertilizerList.get(spCrop.getSelectedItemPosition()).remarks);
        if (fertilizerList.get(spCrop.getSelectedItemPosition()).remarks.equalsIgnoreCase(""))
        {
            tvSource.setVisibility(View.GONE);
        }else
        {
            tvSource.setVisibility(View.VISIBLE);
            if (isRasayanik)
                tvSource.setText("श्रॊत: नेपाल कृषि अनुसन्धान परिषद \n(NARC) खुमालटार");
            else
                tvSource.setText("श्राेत ः उन्नत तरकारी खेती (IDE), नेपाल");
        }
    }

    private void loadSpinners() {
        loadList();

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cropList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCrop.setAdapter(spinnerArrayAdapter);

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, systemList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSystem.setAdapter(spinnerArrayAdapter1);
    }

    private void loadList() {
        for (int i = 0; i < fertilizerList.size(); i++)
            cropList.add(fertilizerList.get(i).name);

        systemList.add("रोपनी/आना");
        systemList.add("बिघा/ कठ्ठा");
    }

    private void loadChemicalData() {
        Fertilizer fertilizer = new Fertilizer();
        fertilizer.name = "आलु";
        fertilizer.compost = 1500;
        fertilizer.urea = 18;
        fertilizer.dap = 15;
        fertilizer.potash = 8;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "गोल्भेडा अग्लो";
        fertilizer.compost = 1500;
        fertilizer.urea = 10;
        fertilizer.dap = 15;
        fertilizer.potash = 7;
        fertilizer.remarks = "बेर्ना सारेको २० दिन पछि, फूल फुल्नु अघि र हरेक पटक बालि लिएपछि डि.ए.पि १५ ग्राम, युरीया १५ ग्राम र पोटास ५ ग्राम प्रति बोट दिने ";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "प्याज";
        fertilizer.compost = 1500;
        fertilizer.urea = 8;
        fertilizer.dap = 12;
        fertilizer.potash = 10;
        fertilizer.remarks = "रोपेको २०, ४० र ५० दिनमा युरीया र डि.ए.पि ५-५ ग्राम प्रति रोपनीको दरले हाल्ने ।";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "सिमि ";
        fertilizer.compost = 600;
        fertilizer.urea = 5;
        fertilizer.dap = 10;
        fertilizer.potash = 5;
        fertilizer.remarks = "बेर्ना सारेको २० दिन पछि, फूल फुल्नु अघि र हरेक पटक बालि लिएपछि डि.ए.पि १५ ग्राम, युरीया १५ ग्राम र पोटास ५ ग्राम प्रति बोट दिने ";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "बन्दा";
        fertilizer.compost = 1000;
        fertilizer.urea = 5;
        fertilizer.dap = 10;
        fertilizer.potash = 7;
        fertilizer.remarks = "बेर्ना सारेको २० दिनमा अर्को १० के.जी, ४० दिनमा अर्को १० के.जी युरीया प्रति रोपनिको दरले हाल्ने ।";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "खुर्सानी";
        fertilizer.compost = 1500;
        fertilizer.urea = 5;
        fertilizer.dap = 10;
        fertilizer.potash = 5;
        fertilizer.remarks = "बेर्ना सारेको २० दिन पछि र हरेक २ पटक बालि लिएपछि युरीया र डि.ए.पि १(२ के.जी प्रति रोपनीको दरले हाल्ने ।";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "काउली";
        fertilizer.compost = 1500;
        fertilizer.urea = 16;
        fertilizer.dap = 13;
        fertilizer.potash = 7;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "बोडि ";
        fertilizer.compost = 1500;
        fertilizer.urea = 5;
        fertilizer.dap = 10;
        fertilizer.potash = 5;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "केराउ ";
        fertilizer.compost = 1500;
        fertilizer.urea = 13;
        fertilizer.dap = 4;
        fertilizer.potash = 10;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "भान्टा ";
        fertilizer.compost = 1500;
        fertilizer.urea = 5;
        fertilizer.dap = 7;
        fertilizer.potash = 5;
        fertilizer.remarks = "रोपेको २०, ४० र ६० दिनमा प्रत्यक पटक १० ग्राम युरीया प्रति बोट दिने";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "करेला ";
        fertilizer.compost = 1500;
        fertilizer.urea = 10;
        fertilizer.dap = 10;
        fertilizer.potash = 5;
        fertilizer.remarks = "रोपेको २०, ४०, ६० र ८० दिनमा युरीया २ के.जी र डि.ए.पि  २ के.जीे प्रति रोपनीको दरले प्रत्यक विरुवा वरीपरि हाल्ने ";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "मुला";
        fertilizer.compost = 1000;
        fertilizer.urea = 14;
        fertilizer.dap = 20;
        fertilizer.potash = 5;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "अदुवा";
        fertilizer.compost = 700;
        fertilizer.urea = 7;
        fertilizer.dap = 5;
        fertilizer.potash = 4;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "काक्रो";
        fertilizer.compost = 1500;
        fertilizer.urea = 13;
        fertilizer.dap = 4;
        fertilizer.potash = 8;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "गाजर";
        fertilizer.compost = 1500;
        fertilizer.urea = 7;
        fertilizer.dap = 11;
        fertilizer.potash = 8;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "चम्सुर";
        fertilizer.compost = 500;
        fertilizer.urea = 3;
        fertilizer.dap = 4;
        fertilizer.potash = 2;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "गोल्भेडा होचो";
        fertilizer.compost = 2000;
        fertilizer.urea = 13;
        fertilizer.dap = 22;
        fertilizer.potash = 12;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "घिरौँला";
        fertilizer.compost = 500;
        fertilizer.urea = 3;
        fertilizer.dap = 4;
        fertilizer.potash = 2;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "पालुङगो";
        fertilizer.compost = 1000;
        fertilizer.urea = 10;
        fertilizer.dap = 9;
        fertilizer.potash = 3;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "फर्सी";
        fertilizer.compost = 1500;
        fertilizer.urea = 18;
        fertilizer.dap = 20;
        fertilizer.potash = 5;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "लौका";
        fertilizer.compost = 1500;
        fertilizer.urea = 4;
        fertilizer.dap = 2;
        fertilizer.potash = 2;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "कुरीलो ";
        fertilizer.compost = 1000;
        fertilizer.urea = 18;
        fertilizer.dap = 20;
        fertilizer.potash = 5;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "तरकारी भटमास";
        fertilizer.compost = 600;
        fertilizer.urea = 5;
        fertilizer.dap = 20;
        fertilizer.potash = 5;
        fertilizer.remarks = "";
        fertilizerList.add(fertilizer);
    }

    private void loadOrganicData() {
        Fertilizer fertilizer = new Fertilizer();
        fertilizer.name = "आलु";
        fertilizer.compost = 2.5;
        fertilizer.urea = 4000;
        fertilizer.dap = 250;
        fertilizer.potash = 2;
        fertilizer.remarks = "रोपेको १ महिना पछि १.२५ के.जी, ३ महिना पछि १.२५ के.जी कम्पलेक्स मल प्रति रोपनी दिने";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "गोल्भेडा अग्लो";
        fertilizer.compost = 2.5;
        fertilizer.urea = 1500;
        fertilizer.dap = 0;
        fertilizer.potash = 2;
        fertilizer.remarks = "रोपेको १ महिना पछि १.२५ के.जी, ३ महिना पछि १.२५ के.जी कम्पलेक्स मल प्रति रोपनी दिने";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "प्याज";
        fertilizer.compost = 2.5;
        fertilizer.urea = 1500;
        fertilizer.dap = 0;
        fertilizer.potash = 2;
        fertilizer.remarks = "रोपेको २ महिना पछि २.५ के.जी कम्पलेक्स मल प्रति रोपनी दिने।";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "सिमि ";
        fertilizer.compost = 2.5;
        fertilizer.urea = 1000;
        fertilizer.dap = 250;
        fertilizer.potash = 0;
        fertilizer.remarks = "रोपेको २ महिना पछि १.२५ के.जी कम्पलेक्स मल प्रति रोपनी दिने";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "बन्दा";
        fertilizer.compost = 2.5;
        fertilizer.urea = 1500;
        fertilizer.dap = 250;
        fertilizer.potash = 0;
        fertilizer.remarks = "रोपेको ३०-४० दिनभित्र १.२५ के.जी कम्पलेक्स मल प्रति रोपनी दिने।";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "खुर्सानी";
        fertilizer.compost = 2.5;
        fertilizer.urea = 1500;
        fertilizer.dap = 0;
        fertilizer.potash = 2;
        fertilizer.remarks = "रोपेको २ महिना पछि १.२५ के.जी कम्पलेक्स मल प्रति रोपनी दिने";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "काउली";
        fertilizer.compost = 0;
        fertilizer.urea = 1500;
        fertilizer.dap = 0;
        fertilizer.potash = 2;
        fertilizer.remarks = "रोपेको २ महिना पछि १.२५ के.जी कम्पलेक्स मल प्रति रोपनी दिने";
        fertilizerList.add(fertilizer);

//        fertilizer = new Fertilizer();
//        fertilizer.name = "बोडि ";
//        fertilizer.compost = 1500;
//        fertilizer.urea = 5;
//        fertilizer.dap = 10;
//        fertilizer.potash = 5;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "केराउ ";
        fertilizer.compost = 2.5;
        fertilizer.urea = 1500;
        fertilizer.dap = 0;
        fertilizer.potash = 0;
        fertilizer.remarks = "रोपेको २ महिना पछि २.५ के.जी, ३ महिना पछि अर्को १.२५ के.जी कम्पलेक्स मल प्रति रोपनी दिने ";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "भान्टा ";
        fertilizer.compost = 1500;
        fertilizer.urea = 5;
        fertilizer.dap = 7;
        fertilizer.potash = 5;
        fertilizer.remarks = "रोपेको २०, ४० र ६० दिनमा प्रत्यक पटक १० ग्राम युरीया प्रति बोट दिने";
        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "करेला ";
        fertilizer.compost = 2.5;
        fertilizer.urea = 1500;
        fertilizer.dap = 0;
        fertilizer.potash = 0;
        fertilizer.remarks = "रोपेको २ महिना पछि २.५ के.जी, ३ महिना पछि अर्को १.२५ के.जी कम्पलेक्स मल प्रति रोपनी दिने ";
        fertilizerList.add(fertilizer);

//        fertilizer = new Fertilizer();
//        fertilizer.name = "मुला";
//        fertilizer.compost = 1000;
//        fertilizer.urea = 14;
//        fertilizer.dap = 20;
//        fertilizer.potash = 5;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);

        fertilizer = new Fertilizer();
        fertilizer.name = "अदुवा";
        fertilizer.compost = 2.5;
        fertilizer.urea = 1500;
        fertilizer.dap = 0;
        fertilizer.potash = 0;
        fertilizer.remarks = "रोपेको २ महिना पछि २.५ के.जी, ३ महिना पछि अर्को १.२५ के.जी कम्पलेक्स मल प्रति रोपनी दिने ";
        fertilizerList.add(fertilizer);

//        fertilizer = new Fertilizer();
//        fertilizer.name = "काक्रो";
//        fertilizer.compost = 1500;
//        fertilizer.urea = 13;
//        fertilizer.dap = 4;
//        fertilizer.potash = 8;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);
//
//        fertilizer = new Fertilizer();
//        fertilizer.name = "गाजर";
//        fertilizer.compost = 1500;
//        fertilizer.urea = 7;
//        fertilizer.dap = 11;
//        fertilizer.potash = 8;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);
//
//        fertilizer = new Fertilizer();
//        fertilizer.name = "चम्सुर";
//        fertilizer.compost = 500;
//        fertilizer.urea = 3;
//        fertilizer.dap = 4;
//        fertilizer.potash = 2;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);
//
//        fertilizer = new Fertilizer();
//        fertilizer.name = "गोल्भेडा होचो";
//        fertilizer.compost = 2000;
//        fertilizer.urea = 13;
//        fertilizer.dap = 22;
//        fertilizer.potash = 12;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);
//
//        fertilizer = new Fertilizer();
//        fertilizer.name = "घिरौँला";
//        fertilizer.compost = 500;
//        fertilizer.urea = 3;
//        fertilizer.dap = 4;
//        fertilizer.potash = 2;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);
//
//        fertilizer = new Fertilizer();
//        fertilizer.name = "पालुङगो";
//        fertilizer.compost = 1000;
//        fertilizer.urea = 10;
//        fertilizer.dap = 9;
//        fertilizer.potash = 3;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);
//
//        fertilizer = new Fertilizer();
//        fertilizer.name = "फर्सी";
//        fertilizer.compost = 1500;
//        fertilizer.urea = 18;
//        fertilizer.dap = 20;
//        fertilizer.potash = 5;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);
//
//        fertilizer = new Fertilizer();
//        fertilizer.name = "लौका";
//        fertilizer.compost = 1500;
//        fertilizer.urea = 4;
//        fertilizer.dap = 2;
//        fertilizer.potash = 2;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);
//
//        fertilizer = new Fertilizer();
//        fertilizer.name = "कुरीलो ";
//        fertilizer.compost = 1000;
//        fertilizer.urea = 18;
//        fertilizer.dap = 20;
//        fertilizer.potash = 5;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);
//
//        fertilizer = new Fertilizer();
//        fertilizer.name = "तरकारी भटमास";
//        fertilizer.compost = 600;
//        fertilizer.urea = 5;
//        fertilizer.dap = 20;
//        fertilizer.potash = 5;
//        fertilizer.remarks = "";
//        fertilizerList.add(fertilizer);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
