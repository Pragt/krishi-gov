package com.ictfa.krishiguru.malkhadCalculator;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.ictfa.krishiguru.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MalkhadCalculatorActivity extends AppCompatActivity {

    @BindView(R.id.btn_rasayanic)
    Button btnRasayanik;

    @BindView(R.id.btn_prangarik)
    Button btnPrangarik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_malkhad_calculator);
        ButterKnife.bind(this);

        setTitle("मलखाद क्याल्कुलेटर ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_rasayanic)
    void onRasayanikClicked() {
        Intent intent = new Intent(this, MalkhadCalculator.class);
        intent.putExtra("is_rasayanic", true);
        startActivity(intent);
    }

    @OnClick(R.id.btn_prangarik)
    void onPrangarikClicked() {
        Intent intent = new Intent(this, MalkhadCalculator.class);
        intent.putExtra("is_rasayanic", false);
        startActivity(intent);
    }

}
