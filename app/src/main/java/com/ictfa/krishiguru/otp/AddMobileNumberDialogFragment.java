package com.ictfa.krishiguru.otp;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.dashboard.DashBoardFragment;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class AddMobileNumberDialogFragment extends DialogFragment {

    private TextView tvVerificationMessage;
    CustomProgressDialog pd;
    Alerts alerts;
    SharedPreference sharedPreference;
    LinearLayout llNumber;
    LinearLayout llOTP;
    private onNumberAddSuccess listener;

    public AddMobileNumberDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.app.DialogFragment.STYLE_NO_TITLE, R.style.dialog);
    }

    public static AddMobileNumberDialogFragment newInstance(String title) {
        AddMobileNumberDialogFragment frag = new AddMobileNumberDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mobile_number_verification, container);
    }

    public void setListener(onNumberAddSuccess listener) {
        this.listener = listener;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        llNumber = view.findViewById(R.id.llNumber);
        llOTP = view.findViewById(R.id.llOTP);
        EditText edtNumber = view.findViewById(R.id.edtMobileNumber);
        EditText edtOTP = view.findViewById(R.id.edtOTP);
        tvVerificationMessage = view.findViewById(R.id.tvMessage);
        Button btnConfirm = view.findViewById(R.id.btn_confirm);
        Button btnSave = view.findViewById(R.id.btn_save);
        TextView tvChangeNumber = view.findViewById(R.id.tv_change);
        TextView tvResend = view.findViewById(R.id.tv_resend);
        pd = new CustomProgressDialog(getActivity());
        alerts = new Alerts(getActivity());
        sharedPreference = new SharedPreference(getActivity());
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().setCancelable(false);

        if (sharedPreference.getBoolValues(CommonDef.SharedPreference.IS_OTP_SEND)) {
            llNumber.setVisibility(View.GONE);
            llOTP.setVisibility(View.VISIBLE);
            tvVerificationMessage.setText("Krishi Guru will send SMS to verify your phone number. Please enter 6 digit code send to you at " + sharedPreference.getStringValues(CommonDef.SharedPreference.VERIFIED_NUMBER));

        }

        btnConfirm.setOnClickListener(v -> {
                    if (isValid(edtNumber.getText().toString())) {
                        verifyNumnber(edtNumber.getText().toString());
                    }
                }
        );

        tvResend.setOnClickListener(v -> {
                    verifyNumnber(sharedPreference.getStringValues(CommonDef.SharedPreference.VERIFIED_NUMBER));
                }
        );

        tvChangeNumber.setOnClickListener(v -> {
                    llNumber.setVisibility(View.VISIBLE);
                    llOTP.setVisibility(View.GONE);
                }
        );

        btnSave.setOnClickListener(v -> {
                    if (isValidOtp(edtOTP.getText().toString())) {
                        verifyOTPCode(edtOTP.getText().toString());
                    }
                }
        );
    }

    private boolean isValid(String value) {
        if (value.isEmpty())
            Toast.makeText(getActivity(), "Enter mobile number", Toast.LENGTH_SHORT).show();
        else if (value.length() < 10)
            Toast.makeText(getActivity(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
        else if (value.startsWith("980") || value.startsWith("981") || value.startsWith("982"))
            return true;
        else
            Toast.makeText(getActivity(), "The number is not Ncell number.", Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean isValidOtp(String value) {
        if (value.isEmpty())
            Toast.makeText(getActivity(), "Enter 6 digit code", Toast.LENGTH_SHORT).show();
        else if (value.length() < 6)
            Toast.makeText(getActivity(), "Enter 6 digit code", Toast.LENGTH_SHORT).show();
        else
            return true;
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    private void verifyOTPCode(String otp) {
        pd.showpd(getResources().getString(R.string.registering));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        String url = UrlHelper.BASE_URL + "api/v3/otp/verify?apikey=" + UrlHelper.API_KEY;
        Log.d("url", url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject code = jsonObject.getJSONObject("code");
                            JSONObject data = jsonObject.getJSONObject("data");
                            String status = code.getString("status");
                            String message = code.getString("message");
                            if (status.equalsIgnoreCase("1")) {
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_NUMBER_VERIFIED, true);
                                alerts.showToastMsg("Number verified");

                                    JSONObject datavalue = data.getJSONObject("login");

                                    int id = Integer.parseInt(datavalue.getString("id"));
                                    String priorities = datavalue.getString("priorities");
                                    String userType = datavalue.getString("user_type");
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_LOGIN, 1);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.USER_ID, id);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.LATUTUDE, DashBoardFragment.lat);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.LONGITUDE, DashBoardFragment.lon);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.USERNAME, datavalue.getString("name"));
                                    sharedPreference.setKeyValues("priorities", priorities);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.USERTYPE, userType);
                                    if (datavalue.getString("is_number_verified") == "1")
                                        sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_NUMBER_VERIFIED, true);
                                    if (datavalue.getString("is_otp_send") == "1")
                                        sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_OTP_SEND, true);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.VERIFIED_NUMBER, datavalue.getString("verified_mobile_number"));

                                listener.onSuccess();
                                dismiss();

                            } else
                                alerts.showErrorAlert(message);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                pd.hidepd();
                // mTextView.setText("That didn't work!");
            }


        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("otp", otp);
                params.put("user_id", String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)));
                return params;
            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    private void verifyNumnber(String mobileNumber) {
        pd.showpd(getResources().getString(R.string.registering));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        String url = UrlHelper.BASE_URL + "api/v3/user/otp?apikey=" + UrlHelper.API_KEY;
        Log.d("url", url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject code = jsonObject.getJSONObject("code");
                            String status = code.getString("status");
                            String message = code.getString("message");
                            String otp = jsonObject.getJSONObject("data").getString("otp");
                            if (status.equalsIgnoreCase("1")) {
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_OTP_SEND, true);
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.VERIFIED_NUMBER, mobileNumber);
                                alerts.showSuccess("SMS sent");
                                Log.d("OTP", otp);
                                llNumber.setVisibility(View.GONE);
                                llOTP.setVisibility(View.VISIBLE);
                                tvVerificationMessage.setText("Krishi Guru will send SMS to verify your phone number. Please enter 6 digit code send to you at " + mobileNumber);
//                                    alerts.showSuccessAlert(message);
                            } else
                                alerts.showErrorAlert(message);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                pd.hidepd();
                // mTextView.setText("That didn't work!");
            }


        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("msisdn", mobileNumber);
                params.put("user_id", String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)));
                return params;
            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public interface onNumberAddSuccess {
        void onSuccess();
    }
}