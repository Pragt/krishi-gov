package com.ictfa.krishiguru.utils;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.stetho.Stetho;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
//        Realm.init(this);
        // initialize Realm
        Realm.init(getApplicationContext());
        InternetAvailabilityChecker.init(this);
        MultiDex.install(MyApplication.this);
        // create your Realm configuration
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder();
//        builder.name("MeroPani");
        builder = builder.deleteRealmIfMigrationNeeded();
        Realm.setDefaultConfiguration(builder.build());

        Log.d("Realm config", "path: " + builder.build().getRealmDirectory());
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .build());

        AppEventsLogger.activateApp(this);
    }
}