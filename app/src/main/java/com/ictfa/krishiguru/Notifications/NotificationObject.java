package com.ictfa.krishiguru.Notifications;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Prajeet on 09/10/2017.
 */

public class NotificationObject extends RealmObject {
    private int ref_id = 0;
    private int type = 0;
    private String msg = "";
    private String userName = "";
    private String time;
    private String isRead;
    @PrimaryKey
    private int id;

    public int getRef_id() {
        return ref_id;
    }

    public void setRef_id(int id) {
        this.ref_id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image = "";

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getIsRead() {
        return isRead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
