package com.ictfa.krishiguru.Notifications;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by sureshlama on 2/24/17.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM = 2;
    private static final int TYPE_PROGRESS = 1;
    private final Alerts alerts;
    private final CustomProgressDialog pd1;
    private final SharedPreference sharedPreference;
    private Activity activity;
    private ArrayList<NotificationObject> listNotificatons;
    private NotificationListner listner;

    // ForumData formData;
    public NotificationsAdapter(Activity activity, ArrayList<NotificationObject> listNotifications) {
        this.activity = activity;
        sharedPreference = new SharedPreference(activity);
        pd1 = new CustomProgressDialog(activity);
        alerts = new Alerts(activity);
        this.listNotificatons = listNotifications;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_ITEM) {
            view = LayoutInflater.from(activity).inflate(R.layout.item_notifications, parent, false);
            return new myViewHolder(view);
        } else
            view = LayoutInflater.from(activity).inflate(R.layout.item_progress_dialog, parent, false);
        return new myProgressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myViewHolder) {
            myViewHolder holder1 = (myViewHolder) holder;
            holder1.tvTitle.setText(listNotificatons.get(position).getMsg());
            holder1.tvCreatedAt.setText(listNotificatons.get(position).getTime());
//            holder1.tvCreatedAt.setText(listNotificatons.get(position).ge());
            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.onNotificationClickListner(listNotificatons.get(position).getRef_id(), listNotificatons.get(position).getType(), position, listNotificatons.get(position).getId());
                }
            });


            if (!listNotificatons.get(position).getImage().equalsIgnoreCase(""))
                Picasso.with(activity).load(listNotificatons.get(position).getImage()).
                        placeholder(activity.getResources().getDrawable(R.drawable.com_facebook_profile_picture_blank_square)).
                        into(holder1.userImage);

//            if (!listNotificatons.get(position).getIsRead().equalsIgnoreCase("1")) {
//                holder1.rlNotifications.setBackgroundColor(activity.getResources().getColor(R.color.light_grey));
//            } else
//                holder1.rlNotifications.setBackgroundColor(Color.WHITE);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listNotificatons.size();
    }

    public void addAll(ArrayList<NotificationObject> list) {
        this.listNotificatons = list;
        notifyDataSetChanged();
    }

    public void setListners(NotificationListner listners) {
        this.listner = listners;
    }

    public interface NotificationListner {
        void onNotificationClickListner(int ref_id, int type, int position, int id);
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        private final ImageView userImage;
        private final TextView tvTitle;
        private final TextView tvCreatedAt;
        private RelativeLayout rlNotifications;

        public myViewHolder(View convertView) {
            super(convertView);

            userImage = (ImageView) convertView.findViewById(R.id.iv_user_image);
            tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            tvCreatedAt = (TextView) convertView.findViewById(R.id.tv_created_at);
            rlNotifications = (RelativeLayout) convertView.findViewById(R.id.rl_notifications);

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (listNotificatons.get(position) == null)
            return TYPE_PROGRESS;
        else
            return TYPE_ITEM;
    }

    private class myProgressViewHolder extends RecyclerView.ViewHolder {
        public myProgressViewHolder(View view) {
            super(view);
        }
    }

}
