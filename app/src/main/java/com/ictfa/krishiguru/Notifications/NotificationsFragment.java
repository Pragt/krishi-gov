package com.ictfa.krishiguru.Notifications;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.ArticalDetail.ArticleDetailActivity;
import com.ictfa.krishiguru.Comment.CommentProductActivity;
import com.ictfa.krishiguru.Comment.FullQuestionActivity;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.chabBot.ChatBotActivity;
import com.ictfa.krishiguru.customViews.EndlessRecyclerOnScrollListener;
import com.ictfa.krishiguru.fullNoticeView.FullNoticeViewActivity;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.krishilibrary.Object.Article;
import com.ictfa.krishiguru.krishilibrary.Object.ArticleImages;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;
import com.ictfa.krishiguru.myOrganization.view.Objects.NoticeObject;
import com.ictfa.krishiguru.news.NewsData;
import com.ictfa.krishiguru.news.NewsDetailActivity;
import com.ictfa.krishiguru.productDetail.DetailProdcutActivity;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.retrofit.ApiClient;
import com.ictfa.krishiguru.retrofit.ApiInterface;
import com.ictfa.krishiguru.ui.adapter.ForumData;
import com.ictfa.krishiguru.utils.TimeAgo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static java.sql.DriverManager.println;

/**
 * Created by Prajeet on 07/10/2017.
 */

public class NotificationsFragment extends Fragment implements NotificationsAdapter.NotificationListner {

    @BindView(R.id.rvNotifications)
    RecyclerView rvNotifications;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.tvNoNotification)
    TextView tvNoNotifiations;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    Alerts alerts;
    CustomProgressDialog pd;
    SharedPreference sharedPreference;
    private ArrayList<NotificationObject> listNotifications;
    private NotificationsAdapter mAdapter;
    private Realm realm;
    private boolean isFromNotification = false;
    private int total_count;
    private boolean isLoading;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notifications_list_activity, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNotifications(true);
            }
        });
        tvTitle.setVisibility(View.GONE);
        listNotifications = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvNotifications.setLayoutManager(layoutManager);
        listNotifications = new ArrayList<>();
        RealmResults<NotificationObject> listNotification = realm.where(NotificationObject.class).findAll();
        for (NotificationObject object : listNotification) {
            listNotifications.add(object);
            tvNoNotifiations.setVisibility(View.GONE);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mAdapter = new NotificationsAdapter(getActivity(), listNotifications);
                rvNotifications.setAdapter(mAdapter);
                mAdapter.setListners(NotificationsFragment.this);
                getNotifications(true);
            }
        }, 50);


        rvNotifications.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (!isLoading && total_count > listNotifications.size())
                    getNotifications(false);
            }
        });


        if (getArguments() != null) {
            int ref_id = getArguments().getInt("ref_id");
            int type = getArguments().getInt("type");

            if (ref_id != 0 && type != 0) {
                onNotificationClickListner(ref_id, type, -1, -1);
            }
        }
    }

    /**
     * --------- Notifications type -----------
     * <p>
     * Notice - 1
     * Forum - 2
     * Library - 3
     * byapari_mulya - 4
     * byawasaik_utpadan - 5
     * one way messaging - 6
     */


    private void getNotifications(boolean clearOld) {
        if (listNotifications.size() > 0) {
            listNotifications.add(null);
            mAdapter.addAll(listNotifications);
            isLoading = true;
        }

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //  String url ="http://www.google.com";

// Request a string response from the provided URL.
        String url = UrlHelper.BASE_URL + "api/v3/user/" + (sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN) == 1 ? sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) : sharedPreference.getIntValues(CommonDef.SharedPreference.ANYNOMOUS_USER_ID)) + "/notifications?apikey=" + UrlHelper.API_KEY + "&length=10&start=" + (int) (!clearOld ? listNotifications.size() : 0);
        Log.d("notification", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        isLoading = false;

                        if (listNotifications.size() > 0)
                            listNotifications.remove(listNotifications.size() - 1);
                        mAdapter.notifyDataSetChanged();
                        pd.hidepd();

                        try {
//                            if (!isFromNotification)
//                                pd.hidepd();
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONObject meta = jsonObject.getJSONObject("meta");
                            total_count = meta.getInt("total_count");


                            JSONObject code = jsonObject.getJSONObject("code");
                            int status = code.getInt("status");
                            NotificationObject notificationObject;
                            if (status == 1) {
                                if (clearOld) {
                                    listNotifications = new ArrayList<>();
                                    mAdapter.addAll(listNotifications);
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.delete(NotificationObject.class);
                                        }
                                    });
                                }

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    tvNoNotifiations.setVisibility(View.GONE);
                                    notificationObject = new NotificationObject();
                                    JSONObject jsonNotification = jsonArray.getJSONObject(i);
                                    notificationObject.setRef_id(jsonNotification.getInt("ref_id"));
                                    notificationObject.setId(jsonNotification.getInt("id"));
                                    notificationObject.setType(jsonNotification.getInt("type"));
                                    notificationObject.setMsg(jsonNotification.getString("title"));
                                    notificationObject.setImage(jsonNotification.getString("image"));
                                    notificationObject.setTime(jsonNotification.getString("created_at"));
                                    notificationObject.setIsRead(jsonNotification.getString("is_read"));
                                    listNotifications.add(notificationObject);

                                    realm.beginTransaction();
                                    realm.copyToRealmOrUpdate(notificationObject);
                                    realm.commitTransaction();
                                }
                                mAdapter.addAll(listNotifications);
                            }
                        } catch (JSONException e) {
                            if (!isFromNotification)
                                pd.hidepd();
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            if (!isFromNotification)
                                pd.hidepd();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
                try {
                    if (!isFromNotification)
                        pd.hidepd();
                    swipeRefreshLayout.setRefreshing(false);
                } catch (Exception exx) {

                }

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void init() {
        sharedPreference = new SharedPreference(getActivity());
        this.realm = RealmController.with(getActivity()).getRealm();
        alerts = new Alerts(getActivity());
        pd = new CustomProgressDialog(getActivity());

    }


    @Override
    public void onNotificationClickListner(int ref_id, int type, int position, int id) {
        RequestQueue queue;
        String url;
        StringRequest stringRequest;
        if (position != -1 && id != -1) {
            markRead(id, position);
        }

        switch (type) {
            case 8:
                startActivity(new Intent(getActivity(), ChatBotActivity.class));
            case 2:
                queue = Volley.newRequestQueue(getActivity());
                pd.showpd(getResources().getString(R.string.please_wait));
                url = UrlHelper.BASE_URL + "api/v3/forum/" + ref_id + "/question?apikey=" + UrlHelper.API_KEY;
                stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                swipeRefreshLayout.setRefreshing(false);
//                        isLoading = false;
                                pd.hidepd();

                                try {
                                    String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");

                                    JSONObject jsonObject = new JSONObject(utfStr);

//                                    JSONObject meta = jsonObject.getJSONObject("code");
//                                    int status = meta.getInt("status");
                                    ForumData forumData = new ForumData();
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject newsValue = jsonArray.getJSONObject(0);

                                        String askBy = newsValue.getString("ask_by");
                                        String address = newsValue.getString("district");
                                        String title = newsValue.getString("question");
                                        String imageresource = newsValue.getString("image_url");
                                        String user_image = newsValue.getString("user_image");
                                        String description = newsValue.getString("description");
                                        String Time = newsValue.getString("created_at");
                                        String commentCount = newsValue.getString("comment_count");
                                        String commentCountNum = newsValue.getString("comment_count");
                                        String noHTMLString = description.replaceAll("\\<.*?>", "");

                                        String likeCount = newsValue.getString("like_count");
                                        String LikeStatus = newsValue.getString("is_liked");
                                        String questionID = newsValue.getString("id");
//                                if(commentCount.equalsIgnoreCase("0")){
//                                    commentCount = commentCount+ " comment";
//                                }
//                                else {
//                                    commentCount = commentCount+ " comments";
//                                }

                                        TimeAgo timeAgo = new TimeAgo();
                                        String stringTimeAgo = "0";
                                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                                        try {

                                            Date date = format.parse(Time);
                                            stringTimeAgo = timeAgo.toDuration(date);

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        //long duration = date.getTime();
                                        // Toast.makeText(DashBoardActivity.this,duration+"",Toast.LENGTH_SHORT).show();

                                        forumData.setNameAddress(askBy + " " + "-" + " " + address);
                                        forumData.setFormTime(Time);
                                        forumData.setProfileImage(imageresource);
                                        forumData.setUserImage(user_image);
                                        forumData.setDesc(description);
                                        forumData.setTitle(title);
                                        forumData.setCommentNums(commentCount);
                                        forumData.setLikeCount(likeCount);
                                        forumData.setIsLiked(LikeStatus);
                                        forumData.setUserId(String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)));
                                        forumData.setQuestionID(questionID);

                                        getComments(questionID, forumData);
                                        break;
                                    }
                                } catch (JSONException e) {
                                    pd.hidepd();
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    pd.hidepd();
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // mTextView.setText("That didn't work!");
                        pd.hidepd();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
// Add the request to the RequestQueue.
                queue.add(stringRequest);
                break;

            case 1:
                queue = Volley.newRequestQueue(getActivity());
                pd.showpd(getResources().getString(R.string.please_wait));
                url = UrlHelper.BASE_URL + "api/v3/organisation/" + ref_id + "/single-notice?apikey=" + UrlHelper.API_KEY;
                stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                swipeRefreshLayout.setRefreshing(false);
                                isLoading = false;
                                pd.hidepd();
                                swipeRefreshLayout.setRefreshing(false);
                                //formDatas.clear();
                                //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                                try {
                                    String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                    JSONObject object = new JSONObject(response);
//
                                    NoticeObject noticeObject;
                                    JSONObject notice = object.getJSONObject("data");
                                    noticeObject = new NoticeObject();
                                    noticeObject.id = Integer.parseInt(notice.getString("id"));
                                    noticeObject.org_name = notice.getString("organisation_name");
                                    noticeObject.notice = notice.getString("notice");
                                    noticeObject.valid_to = notice.getString("valid_to");
                                    noticeObject.phone = notice.getString("phone");

                                    noticeObject.comment_count = notice.getInt("comment_count");

                                    noticeObject.days_remaining = notice.getInt("remaining");
                                    noticeObject.postedDate = notice.getString("created_at");

                                    ArrayList<Comment> listComment = new ArrayList<>();
                                    Comment comment;
                                    JSONArray commentList = notice.getJSONArray("comments");
                                    if (commentList != null) {
                                        for (int j = 0; j < commentList.length(); j++) {
                                            JSONObject commentJson = commentList.getJSONObject(j);
                                            comment = new Comment();
                                            comment.comment_id = commentJson.getInt("id");
                                            comment.comment = commentJson.getString("comment");
                                            comment.commented_by = commentJson.getString("commented_by");
                                            JSONObject created_at = commentJson.getJSONObject("created_at");
                                            comment.date = created_at.getString("date");
                                            comment.timezone_type = created_at.getInt("timezone_type");
                                            comment.timezone = created_at.getString("timezone");
                                            listComment.add(comment);
                                        }
                                        noticeObject.listComment.addAll(realm.copyFromRealm(listComment));

                                        Intent intent = new Intent(getActivity(), FullNoticeViewActivity.class);
                                        try {
                                            intent.putExtra(CommonDef.NOTICE, noticeObject);
                                        } catch (Exception ignored) {
                                        }
                                        startActivity(intent);


                                    }
//                            mAdapter.notifyDataSetChanged();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    alerts.showToastMsg(e.getMessage());
                                } catch (UnsupportedEncodingException e) {
                                    alerts.showToastMsg(e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.hidepd();
                        alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                        // mTextView.setText("That didn't work!");
                    }
                });
// Add the request to the RequestQueue.
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        10000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(stringRequest);
                break;

            case 3:
                pd.showpd(getResources().getString(R.string.please_wait));
                queue = Volley.newRequestQueue(getActivity());
                url = UrlHelper.BASE_URL + "api/v3/library/" + ref_id + "/single?apikey=" + UrlHelper.API_KEY + "&type_id=" + ref_id;
                stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
//                        pd.hidepd();
                                pd.hidepd();
                                //formDatas.clear();
                                //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                                try {
                                    String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                    JSONObject object = new JSONObject(utfStr);
                                    JSONObject jsonResponse = object.getJSONObject("code");
                                    int status = jsonResponse.getInt("status");
                                    String message = jsonResponse.getString("message");
                                    if (!message.equalsIgnoreCase("listed")) {
//                                alerts.showSuccessAlert(message);
                                    } else {
                                        Article article;
                                        article = new Article();
                                        JSONObject type = object.getJSONObject("data");
                                        article.setId(type.getInt("id"));
                                        article.setLibraryType(type.getString("library_type"));
                                        article.setLibraryTitle(type.getString("library_title"));
                                        article.setDescription(type.getString("description"));
                                        article.setCommentCount(type.getInt("comment_count"));
                                        article.setLikeCount(type.getInt("like_count"));
                                        article.setShareCount(type.getInt("share_count"));
                                        article.setLiked(type.getBoolean("is_liked"));
                                        article.setCreatedAt(type.getString("created_at"));
                                        article.setAuthor(type.getString("author"));
                                        JSONArray imageListJson = type.getJSONArray("images");
                                        ArticleImages articleImages;
                                        ArrayList<ArticleImages> listImages = new ArrayList<>();

                                        if (imageListJson != null) {
                                            for (int j = 0; j < imageListJson.length(); j++) {
                                                JSONObject imageObj = imageListJson.getJSONObject(j);
                                                articleImages = new ArticleImages();
                                                articleImages.setName(imageObj.getString("name"));
                                                articleImages.setLocation(imageObj.getString("location"));
//                                            listImages.add(articleImages);
                                                article.setImage("http://admin.ict4agri.com" + imageObj.getString("location"));
                                                break;
                                            }

                                            // Persist your data easily
                                            realm.beginTransaction();
                                            realm.copyToRealmOrUpdate(article);
                                            realm.commitTransaction();


                                            Intent intent = new Intent(getActivity(), ArticleDetailActivity.class);
                                            Bundle b = new Bundle();
                                            b.putSerializable(CommonDef.ARTICLE_ID, (Serializable) article.getId());
                                            intent.putExtras(b);
                                            startActivity(intent);
//                                        article.setListImages(listImages);
                                        }


                                    }
//                            mAdapter.notifyDataSetChanged();

                                } catch (JSONException e) {
                                    pd.hidepd();
                                    e.printStackTrace();
//                            alerts.showToastMsg(e.getMessage());
                                } catch (UnsupportedEncodingException e) {
//                            alerts.showToastMsg(e.getMessage());
                                    pd.hidepd();
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.hidepd();
//                pd.hidepd();
//                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                        // mTextView.setText("That didn't work!");
                    }
                });
// Add the request to the RequestQueue.
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        10000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(stringRequest);
                break;

            case 6:
                Intent intent = new Intent(getActivity(), DetailProdcutActivity.class);
                intent.putExtra("id", String.valueOf(ref_id));
                intent.putExtra("title", "Title");
                intent.putExtra("openComment", true);
                startActivity(intent);
                break;

            case 7:
                queue = Volley.newRequestQueue(getActivity());
                pd.showpd(getResources().getString(R.string.please_wait));
                RequestQueue queue1 = Volley.newRequestQueue(getActivity());
                //  String url ="http://www.google.com";

// Request a string response from the provided URL.
                StringRequest request = new StringRequest(Request.Method.GET, BaseApplication.hostUrl + "news/" + ref_id + "/single?apikey=" + UrlHelper.API_KEY,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                swipeRefreshLayout.setRefreshing(false);
                                pd.hidepd();

                                try {
                                    String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");

                                    JSONObject jsonObject = new JSONObject(utfStr);

//                                    JSONObject meta = jsonObject.getJSONObject("meta");

                                    JSONObject object = jsonObject.getJSONObject("data");


                                    String title = object.getString("title");
                                    String imageresource = object.getString("image");
                                    String description = object.getString("description");
                                    String id = object.getString("id");

                                    NewsData newsData = new NewsData();

                                    newsData.setDesc(description);
                                    newsData.setTitle(title);
                                    newsData.setId(id);
                                    newsData.setNewsImage(imageresource);

                                    Intent intent1 = new Intent(getActivity(), NewsDetailActivity.class);
                                    intent1.putExtra("newsData", newsData);
                                    intent1.putExtra("isFromNotification", true);
                                    startActivity(intent1);


                                } catch (JSONException e) {
                                    pd.hidepd();
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    pd.hidepd();
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // mTextView.setText("That didn't work!");
                        pd.hidepd();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
// Add the request to the RequestQueue.
                queue.add(request);
                break;

            case 5:
                pd.showpd(this.getResources().getString(R.string.please_wait));
                JSONObject jsonObject;
                queue = Volley.newRequestQueue(getActivity());
                String zone = UrlHelper.BASE_URL + "api/v3/production/comments/" + ref_id + "?apikey=" + UrlHelper.API_KEY;
                final ArrayList<Comment> listComment = new ArrayList<>();
                stringRequest = new StringRequest(Request.Method.GET, zone,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                pd.hidepd();
                                //formDatas.clear();
                                //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                                try {
                                    String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                    JSONObject jsonObject = new JSONObject(utfStr);
                                    JSONArray data = jsonObject.getJSONArray("data");
                                    Comment comment;
                                    if (data != null && data.length() > 0) {
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject commentObj = data.getJSONObject(i);
                                            comment = new Comment();
                                            comment.comment_id = Integer.parseInt(commentObj.getString("id"));
                                            comment.comment = commentObj.getString("comment");
                                            comment.commented_by = commentObj.getString("commented_by");
                                            comment.date = commentObj.getString("created_at");
                                            listComment.add(comment);
                                        }

                                    }

                                    Intent intent = new Intent(getActivity(), CommentProductActivity.class);
                                    intent.putExtra(CommonDef.COMMENTS, listComment);
                                    intent.putExtra(CommonDef.PRODUCT_ID, ref_id + "");
                                    startActivityForResult(intent, CommonDef.REQUEST_COMMENT);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        alerts.showToastMsg(NotificationsFragment.this.getResources().getString(R.string.no_internet_connection));
                        pd.hidepd();
                        // mTextView.setText("That didn't work!");
                    }


                });
// Add the request to the RequestQueue.
                queue.add(stringRequest);
                break;

            case 4:
                pd.showpd(this.getResources().getString(R.string.please_wait));
                queue = Volley.newRequestQueue(getActivity());
                String commentUrl = UrlHelper.BASE_URL + "api/v3/production/buy/" + ref_id + "?apikey=" + UrlHelper.API_KEY;
                final ArrayList<Comment> commentList = new ArrayList<>();
                stringRequest = new StringRequest(Request.Method.GET, commentUrl,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                pd.hidepd();
                                //formDatas.clear();
                                //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                                try {
                                    String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                    JSONObject jsonObject = new JSONObject(utfStr);
                                    JSONArray data = jsonObject.getJSONArray("data");
                                    Comment comment;
                                    if (data != null && data.length() > 0) {
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject commentObj = data.getJSONObject(i);
                                            comment = new Comment();
                                            comment.comment_id = Integer.parseInt(commentObj.getString("id"));
                                            comment.comment = commentObj.getString("comment");
                                            comment.commented_by = commentObj.getString("commented_by");
                                            comment.date = commentObj.getString("created_at");
                                            commentList.add(comment);
                                        }

                                    }

                                    Intent intent = new Intent(getActivity(), CommentProductActivity.class);
                                    intent.putExtra(CommonDef.COMMENTS, commentList);
                                    intent.putExtra(CommonDef.PRODUCT_ID, ref_id + "");
                                    startActivityForResult(intent, CommonDef.REQUEST_COMMENT);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        alerts.showToastMsg(NotificationsFragment.this.getResources().getString(R.string.no_internet_connection));
                        pd.hidepd();
                        // mTextView.setText("That didn't work!");
                    }


                });
// Add the request to the RequestQueue.
                queue.add(stringRequest);
                break;
        }


    }

    public void getComments(String questionID, final ForumData forum) {
        pd.showpd(this.getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String zone = UrlHelper.BASE_URL + "api/v3/forum/comments/" + questionID + "?apikey=" + UrlHelper.API_KEY;
        final ArrayList<Comment> listComment = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //forumDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray data = jsonObject.getJSONArray("data");
                            Comment comment;
                            if (data != null && data.length() > 0) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject commentObj = data.getJSONObject(i);
                                    comment = new Comment();
                                    comment.comment_id = Integer.parseInt(commentObj.getString("id"));
                                    comment.comment = commentObj.getString("comment");
                                    comment.commented_by = commentObj.getString("commented_by");
                                    comment.date = commentObj.getString("created_at");
                                    listComment.add(comment);
                                }

                            }

                            Intent intent = new Intent(getActivity(), FullQuestionActivity.class);
                            intent.putExtra(CommonDef.QUESTION, forum);
                            intent.putExtra(CommonDef.COMMENTS, listComment);
                            intent.putExtra("isFromNotification", true);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(NotificationsFragment.this.getResources().getString(R.string.no_internet_connection));
                pd.hidepd();
                // mTextView.setText("That didn't work!");
            }


        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void markRead(int id, final int pos) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //  String url ="http://www.google.com";

// Request a string response from the provided URL.
        String url = UrlHelper.BASE_URL + "api/v3/notification/" + id + "/read?apikey=" + UrlHelper.API_KEY;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");

                            JSONObject jsonObject = new JSONObject(utfStr);

                            JSONObject meta = jsonObject.getJSONObject("code");
                            int status = meta.getInt("status");
                            if (status == 1) {
                                if (pos > listNotifications.size()) {
                                    listNotifications.get(pos).setIsRead("1");
                                    mAdapter.notifyItemChanged(pos);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
                pd.hidepd();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onResume() {
        checkUserPackageSubscribed();
        super.onResume();
    }

    private void checkUserPackageSubscribed() {
        ApiClient.getClient().create(ApiInterface.class).checkUserEligible(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID), "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            if (s.code.status == 1) {
                                // eligible
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, "1");
                            } else if (s.code.status == 0) {
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, "0");
                            }
                        }
                        ,
                        e -> {
//                            alerts.showErrorAlert("Could not connect to server. Please try again later.");
                            e.printStackTrace();
                        }, () ->
                                println("supervisor list"));
    }

}
