package com.ictfa.krishiguru.app;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.ictfa.krishiguru.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class BaseApplication extends Application {

    public final static String hostUrl = "http://admin.ict4agri.com/api/v3/";
    public final static String hostUrlProduct = "http://admin.ict4agri.com/api/v3/production/";
    public final static String Product_HOSTIMG = "http://kinmel.ict4agri.com/assets/images/product/";

    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-RobotoRegular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        MultiDex.install(this);
    }


    public final static String PREFS_NAME = "KrishiPref";
    public final static String Youtube_PlayList_ID= "PL0GmbwjQDfE7aF1VIId6SMzs2h8QhQYg-";
    public final static String Youtube_server_key ="AIzaSyCZe8GINfZx-roI4IkCgREmJ4E-jmVPL74";
    // for news API

    public final static String NEWS_API = hostUrl+"news/list?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    public final static String SINGLE_NEWS_API = hostUrl+"news/list?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    //for image slider
    public final static String IMAGE_SLIDER_API = hostUrl+"images/list?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    //for form
    //loginStatus = mypref.getString("loginStatus", "0");
    public final static String FORM_QUESTION_LIST_API =hostUrl+"forum/questions?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA&start=0&length=10&user_id=";


    // public final static String LOGIN_URL = hostUrl+api/v3/user/login?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA&username=asdddd&password=addd";
    public final static String productCategory= hostUrl+"category/list?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    public final static String productUnit = hostUrl+"unit/list?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    public final static String productBuy = hostUrl+"buy/create?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    public final static String productSell = hostUrl+"sell/create?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    public final static String productTechnology = hostUrl+"production/list?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    public final static String productionCategoryType = hostUrl+"production/type?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    public final static String getProductfeatureList = "/featured?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";

    public final static String wholeSellPrice = "http://admin.ict4agri.com/api/v3/dailyprice/commodity?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    public final static String getMarketName = "http://admin.ict4agri.com/api/v3/agrobiz/markets?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";


}
