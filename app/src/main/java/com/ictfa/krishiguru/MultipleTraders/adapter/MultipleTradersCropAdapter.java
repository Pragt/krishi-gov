package com.ictfa.krishiguru.MultipleTraders.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ictfa.krishiguru.MultipleTraders.object.Crops;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.fragment.BuyFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pragt on 3/5/17.
 */

public class MultipleTradersCropAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_PROGRESS = 1;
    private static final int TYPE_ITEM = 2;
    ArrayList<Crops> cropList;
    boolean showCheckBox;
    boolean viewMode;
    private Context mContext;
    private ArrayList<String> unitList;

    public MultipleTradersCropAdapter(Context mContext, ArrayList<Crops> cropList, boolean showCheckBox, boolean viewMode) {
        this.mContext = mContext;
//        this.cropList = cropList;
        this.showCheckBox = showCheckBox;
        this.viewMode = viewMode;
        this.cropList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_ITEM) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_crop_price, parent, false);
            return new myCategoriesViewHolder(view);
        } else
            view = LayoutInflater.from(mContext).inflate(R.layout.item_progress_dialog, parent, false);
        return new myProgressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int pos) {
        if (holder instanceof myCategoriesViewHolder) {
            final myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;

            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    onItemClickListner.onItemClicked(position);
                }
            });

//            holder1.myCustomEditTextListener.updatePosition(holder.getAdapterPosition());
            holder1.etPrice.setText(cropList.get(pos).price);

            if (!cropList.get(0).price.equalsIgnoreCase("")) {
                holder1.cbCrops.setChecked(true);
            }

//            if (showCheckBox)
//                holder1.etPrice.setFocusable(true);
//            else
//                holder1.etPrice.setFocusable(false);


            if (!cropList.get(pos).unitId.equalsIgnoreCase("")) {
                holder1.spUnit.setSelection(Integer.parseInt(cropList.get(pos).unitId) - 1);
            } else
                holder1.spUnit.setSelection(1);

            holder1.spUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    cropList.get(pos).unitId = BuyFragment.unitListObject.get(i).id;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            holder1.tvCropName.setText(cropList.get(pos).crop_name);
//            holder1.etPrice.setText(cropList.get(position).price);

            holder1.etPrice.setText(cropList.get(pos).price);
//            if (!cropList.get(pos).price.equalsIgnoreCase("")) {
//                holder1.cbCrops.setChecked(true);
//                cropList.get(pos).isChecked = true;
//            } else {
//                holder1.etPrice.setText("");
//                holder1.cbCrops.setChecked(false);
//                cropList.get(pos).isChecked = false;
//            }

            if (showCheckBox) {
                holder1.cbCrops.setVisibility(View.VISIBLE);
                holder1.spUnit.setEnabled(true);

                if (viewMode) {
                    cropList.get(pos).isChecked = true;
                    holder1.cbCrops.setChecked(true);
                } else {
                    cropList.get(pos).isChecked = false;
                    holder1.cbCrops.setChecked(false);
                }
            } else {
                holder1.cbCrops.setVisibility(View.GONE);
                holder1.spUnit.setEnabled(false);
                cropList.get(pos).isChecked = false;
                holder1.cbCrops.setChecked(false);
            }

            holder1.etPrice.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    cropList.get(pos).price = charSequence.toString();
                }

                @Override
                public void afterTextChanged(Editable editable) {
//                    cropList.get(pos).price = holder1.etPrice.getText().toString();

                    if (holder1.etPrice.getText().toString().isEmpty()) {
//                        holder1.cbCrops.setChecked(false);
//                        cropList.get(position).isChecked = false;
                    } else {
//                        holder1.cbCrops.setChecked(true);
//                        cropList.get(position).isChecked = true;
                    }
                }
            });

            if (cropList.get(pos).isChecked)
                holder1.cbCrops.setChecked(true);
            else
                holder1.cbCrops.setChecked(false);

            holder1.cbCrops.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder1.cbCrops.isChecked())
                        cropList.get(pos).isChecked = true;
                    else
                        cropList.get(pos).isChecked = false;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.cropList.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (cropList.get(position) == null)
            return TYPE_PROGRESS;
        else
            return TYPE_ITEM;
    }

    public void addAll(ArrayList<Crops> list) {
        for (int i = 0; i < list.size(); i++) {
            this.cropList.add(list.get(i));
            notifyItemInserted(this.cropList.size());
        }

    }

    public String getCropDatas() {
        String strCropData = "";
        for (int i = 0; i < cropList.size(); i++) {
            if (cropList.get(i).isChecked) {
                if (cropList.get(i).price.equalsIgnoreCase("")) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.add_price), Toast.LENGTH_SHORT).show();
                    return "";
                }
                strCropData = strCropData + "&price[" + cropList.get(i).crop_id + "]=" + cropList.get(i).price + "&unit[" + cropList.get(i).crop_id + "]=" + cropList.get(i).unitId;
            }
        }
        return strCropData;
    }

    public void addLoading() {
        this.cropList.add(null);
        notifyItemInserted(cropList.size());
    }

    public void removeLoading() {
        this.cropList.remove(cropList.size() - 1);
        notifyItemRemoved(cropList.size() - 1);
    }

    public interface OnItemClickListner {
    }

    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {

        //        @BindView(R.id.et_price)
        final EditText etPrice;
        private final ArrayAdapter unitAdapter;
        //        public MyCustomEditTextListener myCustomEditTextListener;
        @BindView(R.id.tv_crop_name)
        TextView tvCropName;
        @BindView(R.id.cb_crops)
        CheckBox cbCrops;
        @BindView(R.id.sp_unit)
        Spinner spUnit;

        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            etPrice = (EditText) itemView.findViewById(R.id.et_price);
//            this.myCustomEditTextListener = myCustomEditTextListener;
//            this.etPrice.addTextChangedListener(myCustomEditTextListener);

            unitList = new ArrayList<>();
            for (int i = 0; i < BuyFragment.unitListObject.size(); i++) {
                unitList.add(BuyFragment.unitListObject.get(i).name);
            }

            unitAdapter = new ArrayAdapter(mContext, R.layout.custome_spiner_text, unitList);
            unitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spUnit.setAdapter(unitAdapter);
        }
    }

    private class myProgressViewHolder extends RecyclerView.ViewHolder {
        public myProgressViewHolder(View view) {
            super(view);
        }
    }

    // we make TextWatcher to be aware of the position it currently works with
    // this way, once a new item is attached in onBindViewHolder, it will
    // update current position MyCustomEditTextListener, reference to which is kept by ViewHolder
//    private class MyCustomEditTextListener implements TextWatcher {
//        private int position;
//
//        public void updatePosition(int position) {
//            this.position = position;
//        }
//
//        @Override
//        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//            // no op
//        }
//
//        @Override
//        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//            cropList.get(position).price = charSequence.toString();
//        }
//
//        @Override
//        public void afterTextChanged(Editable editable) {
//            // no op
//        }
//    }
}
