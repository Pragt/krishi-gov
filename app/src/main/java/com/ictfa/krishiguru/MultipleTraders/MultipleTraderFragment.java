package com.ictfa.krishiguru.MultipleTraders;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.MultipleTraders.adapter.MultipleTradersCropListAdapter;
import com.ictfa.krishiguru.MultipleTraders.object.Crops;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MultipleTraderFragment extends Fragment {

    @BindView(R.id.rv_crops)
    RecyclerView rvCrops;

    @BindView(R.id.et_exp_date)
    EditText etExpiryDate;

    @BindView(R.id.iv_edit)
    ImageView ivEdit;

    boolean isEditMode = false;

    @BindView(R.id.btn_add_crops)
    Button btnAddCrop;

    SharedPreference sharedPreference;
    Alerts alerts;
    CustomProgressDialog pd;
    ProgressBar pbLoading;
    private ArrayList<Crops> cropList;
    private MultipleTradersCropListAdapter mAdapter;
    private String copyDate = "";
//    Alerts


    public MultipleTraderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_multiple_trader, container, false);
        ButterKnife.bind(this, view);
        CommonMethods.setupUI(view.findViewById(R.id.ll_multi_trader), getActivity());
        sharedPreference = new SharedPreference(getActivity());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        rvCrops = (RecyclerView) view.findViewById(R.id.rv_crops);
        pbLoading = (ProgressBar) view.findViewById(R.id.progress_bar);
        init();
        getCropPrice();

    }

    private void getCropPrice() {
        pbLoading.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = UrlHelper.BASE_URL + "api/v3/trader/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/price_list?apikey=" + UrlHelper.API_KEY;
        cropList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        pd.hidepd();
                        pbLoading.setVisibility(View.GONE);
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONArray cropListJson = object.getJSONArray("data");
                            Crops crops;
                            for (int i = 0; i < cropListJson.length(); i++) {
                                JSONObject object1 = (JSONObject) cropListJson.get(i);
                                crops = new Crops();
                                crops.id = object1.getString("id");
                                crops.crop_id = object1.getString("crop_id");
                                crops.crop_name = object1.getString("crop_name");
                                crops.price = object1.getString("price");
                                crops.valid_from = object1.getString("valid_from");
                                crops.valid_to = object1.getString("valid_to");
                                crops.unitId = object1.getString("unit");
                                cropList.add(crops);
                                etExpiryDate.setText(crops.valid_to);
                                copyDate = crops.valid_to;
                            }
                            setViewMode();
                            rvCrops.setItemViewCacheSize(cropList.size());

                        } catch (JSONException e) {
                            e.printStackTrace();
//                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
//                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                pd.hidepd();
//                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void init() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvCrops.setLayoutManager(layoutManager);
        cropList = new ArrayList<>();
        pd = new CustomProgressDialog(getActivity());
        alerts = new Alerts(getActivity());
        mAdapter = new MultipleTradersCropListAdapter(getActivity(), cropList, isEditMode, false);
        rvCrops.setAdapter(mAdapter);
    }

    @OnClick(R.id.btn_add_crops)
    void onAddCropClicked() {
        if (!isEditMode) {
            Intent intent = new Intent(getActivity(), AddCropActivity.class);
            startActivityForResult(intent, CommonDef.REQUEST_ADD_CROP);
        } else {
            String result = mAdapter.getCropDatas();
            if (!result.equalsIgnoreCase("")) {
                result = "&valid_to=" + etExpiryDate.getText().toString() + result;
                addCrops(result);
            } else {
                alerts.showToastMsg(getResources().getString(R.string.please_select_crops));
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonDef.REQUEST_ADD_CROP)
            if (resultCode == RESULT_OK) {
                pbLoading.setVisibility(View.GONE);
//                pd.showpd(getResources().getString(R.string.please_wait));
                getCropPrice();
            }
    }

    @OnClick(R.id.et_exp_date)
    void onExpiryDate() {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        int hour = mcurrentDate.get(Calendar.HOUR_OF_DAY);
        int minutes = mcurrentDate.get(Calendar.MINUTE);
        int second = mcurrentDate.get(Calendar.SECOND);
        String currentDate = mYear + "-" + mMonth + "-" + mDay + " " + hour + ":" + minutes + ":" + second;

        DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                etExpiryDate.setText(selectedyear + "-" + (selectedmonth + 1) + "-" + selectedday);
                //month starts from 0 for janaury so +1 while displaying
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select date");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 6);
        DatePicker dp = mDatePicker.getDatePicker();
        dp.setMinDate(Calendar.getInstance().getTimeInMillis());//get the current day
        mDatePicker.show();
    }

    @OnClick(R.id.iv_edit)
    void onEditClicked() {
        if (isEditMode) {
            setViewMode();
        } else {
            isEditMode = true;
            copyDate = etExpiryDate.getText().toString();
            etExpiryDate.setClickable(true);
            mAdapter = new MultipleTradersCropListAdapter(getActivity(), cropList, true, true);
            rvCrops.setAdapter(mAdapter);
            ivEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_cancel_black_48dp));
            btnAddCrop.setText(getActivity().getResources().getString(R.string.update));
        }
    }

    private void addCrops(String result) {
        pd.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = UrlHelper.BASE_URL + "api/v3/trader/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/update_price?apikey=" + UrlHelper.API_KEY + result;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(response);
                            JSONObject jsonResponse = object.getJSONObject("meta");
                            JSONArray responseJson = object.getJSONArray("data");
                            JSONObject jsonObject = (JSONObject) responseJson.get(0);
                            String status = jsonObject.getString("status");
                            String messege = jsonObject.getString("message");

                            if (status.equalsIgnoreCase("1")) {
                                alerts.showToastMsg(messege);
                                copyDate = etExpiryDate.getText().toString();
                                cropList = mAdapter.getSelectedList();
                                setViewMode();
                            } else
                                alerts.showErrorAlert(messege);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void setViewMode() {
        isEditMode = false;
        etExpiryDate.setClickable(false);
        etExpiryDate.setLongClickable(false);
        etExpiryDate.setText(copyDate);
        mAdapter = new MultipleTradersCropListAdapter(getActivity(), cropList, false, false);
        rvCrops.setAdapter(mAdapter);
        ivEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_black_48dp));
//        mAdapter = new MultipleTradersCropListAdapter(getActivity(), cropList, isEditMode, false);
//        rvCrops.setAdapter(mAdapter);
        btnAddCrop.setText(getActivity().getResources().getString(R.string.add_crops));
    }
}
