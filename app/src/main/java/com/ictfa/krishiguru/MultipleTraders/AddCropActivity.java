package com.ictfa.krishiguru.MultipleTraders;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.MultipleTraders.adapter.MultipleTradersCropAdapter;
import com.ictfa.krishiguru.MultipleTraders.object.Crops;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.customViews.EndlessRecyclerOnScrollListener;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddCropActivity extends AppCompatActivity {


    SharedPreference preference;
    Alerts alerts;
    CustomProgressDialog progressDialog;
    @BindView(R.id.rv_crops)
    RecyclerView rvCrops;
    boolean isLoading = false;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.et_expiry_date)
    EditText etExpiryDate;
    private ArrayList<Crops> cropList;
    private MultipleTradersCropAdapter mAdapter;
    private int total_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_crop);
        ButterKnife.bind(this);
        init();
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        getCropPrice(10);
    }

    private void init() {
        alerts = new Alerts(this);
        progressDialog = new CustomProgressDialog(this);
        preference = new SharedPreference(this);
        setTitle(getResources().getString(R.string.add_crops));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvCrops.setLayoutManager(layoutManager);
        total_count = 0;
        cropList = new ArrayList<>();
        mAdapter = new MultipleTradersCropAdapter(AddCropActivity.this, cropList, true, false);
        rvCrops.setAdapter(mAdapter);

        rvCrops.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (total_count > cropList.size())
                    getCropPrice(cropList.size() + 10);
            }
        });

    }

    private void getCropPrice(int count) {
        if (!isLoading) {
            mAdapter.addLoading();

//            mAdapter.addAll(cropList);
            RequestQueue queue = Volley.newRequestQueue(this);
            isLoading = true;
            String url = UrlHelper.BASE_URL + "api/v3/crop/list?apikey=" + UrlHelper.API_KEY + "&length=" + 10 + "&start=" + (int) (cropList.size() - 1) + "&user_id=" + preference.getIntValues(CommonDef.SharedPreference.USER_ID);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.hidepd();
                            //formDatas.clear();
                            isLoading = false;
                            mAdapter.removeLoading();
//                            cropList.remove(cropList.size() - 1);
                            ArrayList<Crops> newCrops = new ArrayList<>();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                JSONObject object = new JSONObject(utfStr);
                                JSONObject jsonResponse = object.getJSONObject("meta");
                                total_count = jsonResponse.getInt("total_count");
                                JSONArray cropListJson = object.getJSONArray("data");
                                Crops crops;
                                for (int i = 0; i < cropListJson.length(); i++) {
                                    JSONObject object1 = (JSONObject) cropListJson.get(i);
                                    crops = new Crops();
                                    crops.crop_id = object1.getString("id");
//                                crops.crop_id = object1.getString("crop_id");
                                    crops.crop_name = object1.getString("nepali_name");
                                    crops.price = "";
                                    newCrops.add(crops);
//                                crops.valid_from = object1.getString("valid_from");
//                                crops.valid_to = object1.getString("valid_to");
                                    cropList.add(crops);
                                }
                                mAdapter.addAll(newCrops);

//                                mAdapter.addAll(cropList);
                                rvCrops.setItemViewCacheSize(cropList.size());
                            } catch (JSONException e) {
                                e.printStackTrace();
//                            alerts.showToastMsg(e.getMessage());
                            } catch (UnsupportedEncodingException e) {
//                            alerts.showToastMsg(e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                pd.hidepd();
                    progressDialog.hidepd();
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                    // mTextView.setText("That didn't work!");
                }
            });
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_submit)
    void onSubmit() {
        if (etExpiryDate.getText().toString().isEmpty()) {
            alerts.showWarningAlert(getResources().getString(R.string.empty_expiry_date));
        } else {
            String result = mAdapter.getCropDatas();
            if (!result.equalsIgnoreCase("")) {
                result = "&valid_to=" + etExpiryDate.getText().toString() + result;
                addCrops(result);
            } else {
                alerts.showToastMsg(getResources().getString(R.string.please_select_crops));
            }

        }
    }

    private void addCrops(String result) {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(this);
        isLoading = true;
        String url = UrlHelper.BASE_URL + "api/v3/trader/" + preference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/add_price?apikey=" + UrlHelper.API_KEY + result;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        isLoading = false;
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONObject jsonResponse = object.getJSONObject("meta");
                            JSONArray responseJson = object.getJSONArray("data");
                            JSONObject jsonObject = (JSONObject) responseJson.get(0);
                            String status = jsonObject.getString("status");
                            String messege = jsonObject.getString("message");

                            if (status.equalsIgnoreCase("1"))
                                onSuccess(messege);

                            else
                                Toast.makeText(AddCropActivity.this, messege, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void onSuccess(String messege) {
        Intent intent = new Intent();
        Toast.makeText(AddCropActivity.this, messege, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.et_expiry_date)
    void onExpiryDate() {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        int hour = mcurrentDate.get(Calendar.HOUR_OF_DAY);
        int minutes = mcurrentDate.get(Calendar.MINUTE);
        int second = mcurrentDate.get(Calendar.SECOND);
        String currentDate = mYear + "-" + mMonth + "-" + mDay + " " + hour + ":" + minutes + ":" + second;

        DatePickerDialog mDatePicker = new DatePickerDialog(AddCropActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                etExpiryDate.setText(selectedyear + "-" + (selectedmonth + 1) + "-" + selectedday);
                //month starts from 0 for janaury so +1 while displaying
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select date");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 6);
        DatePicker dp = mDatePicker.getDatePicker();
        dp.setMinDate(Calendar.getInstance().getTimeInMillis());//get the current day
        mDatePicker.show();
    }
}
