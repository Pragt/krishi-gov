package com.ictfa.krishiguru.MultipleTraders.object;

import java.io.Serializable;

/**
 * Created by Prajeet on 28/07/2017.
 */

public class TraderType implements Serializable {
    public String id;
    public String nepali_name;
    public String type;
}
