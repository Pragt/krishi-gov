package com.ictfa.krishiguru.kinmel;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class KinmelTitleObject extends RealmObject {
    private String name;

    @PrimaryKey
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}