package com.ictfa.krishiguru.kinmel;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.retrofit.UrlHelpers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import me.relex.circleindicator.CircleIndicator;

public class KinmelActivity extends AppCompatActivity {
    SharedPreference sharedPreference;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CustomProgressDialog progressDialog;
    private ArrayList<KinmelTitleObject> kinmelCategory;
    private CircleIndicator indicator;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_krishi_technology);
        getSupportActionBar().setTitle("किनमेल");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        // viewPager.set
        progressDialog = new CustomProgressDialog(this);
        sharedPreference = new SharedPreference(this);

        //get realm instance
        this.realm = RealmController.with(this).getRealm();

        RealmResults<KinmelTitleObject> realmList = realm.where(KinmelTitleObject.class).findAll();
        KinmelTitleObject type;
        kinmelCategory = new ArrayList<KinmelTitleObject>();
        for (KinmelTitleObject result : realmList) {
            type = new KinmelTitleObject();
            type.setId(result.getId());
            type.setName(result.getName());
            kinmelCategory.add(type);
        }
        setViewPagerAdapter();

        if (!sharedPreference.getBoolValues("isKinmelLoaded")) {
            progressDialog.showpd(getResources().getString(R.string.please_wait));
        }
        getkinmelCategory();
        //   Toast.makeText(KrishiTechnology.this,kinmelCategory.length+"",Toast.LENGTH_SHORT).show();
    }

    private void getkinmelCategory() {
        RequestQueue queue = Volley.newRequestQueue(KinmelActivity.this);
        //  String url ="http://www.google.com";
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlHelpers.BASE_URL + "kinmel/type?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESPONSECATEGORY", response);
                        progressDialog.hidepd();

                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            Log.d("utfstr", utfStr);
                            JSONObject jsonObject = new JSONObject(utfStr);
                            getJson(jsonObject);
                            // JSONObject jsonObject = new JSONObject(response);
//                            progressDialog.dismiss();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                // mTextView.setText("That didn't work!");
                //   progressDialog.dismiss();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void getJson(JSONObject jsonObject) {
        if (jsonObject.has("data")) {
            // Toast.makeText(KrishiTechnology.this,"data cha",Toast.LENGTH_SHORT).show();
            JSONArray jsonArray = null;
            try {
                jsonArray = jsonObject.getJSONArray("data");

                kinmelCategory = new ArrayList<>();
                KinmelTitleObject object;
                //  Toast.makeText(KrishiTechnology.this,kinmelCategory.length+"",Toast.LENGTH_SHORT).show();
                for (int i = 0; i < jsonArray.length(); i++) {
                    object = new KinmelTitleObject();
                    JSONObject value = jsonArray.getJSONObject(i);
                    String name = value.getString("name");
                    object.setName(name);
                    object.setId(value.getString("id"));
                    kinmelCategory.add(object);
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(KinmelTitleObject.class);
                    }
                });

                for (KinmelTitleObject b : kinmelCategory) {
                    // Persist your data easily
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(b);
                    realm.commitTransaction();
                }

                sharedPreference.setKeyValues("isKinmelLoaded", true);

                setViewPagerAdapter();
                //      ProductDataListenerManager.getInstance().callOnlineDataListeners(categoryArray, kinmelCategory[viewPager.getCurrentItem()]);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(KinmelActivity.this, "no data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setViewPagerAdapter() {
        viewPager.setAdapter(new KinmelCategoryTabAdapter(getSupportFragmentManager(),
                KinmelActivity.this, kinmelCategory));
        viewPager.setOffscreenPageLimit(kinmelCategory.size());
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        viewPager.setCurrentItem(0);
        indicator.setViewPager(viewPager);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}


