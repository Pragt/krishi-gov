package com.ictfa.krishiguru.kinmel;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.productDetail.DetailProdcutActivity;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.ui.adapter.ProductTypeAdapter;
import com.ictfa.krishiguru.ui.dtos.ProductTypeListDtos;
import com.ictfa.krishiguru.ui.widget.ExpandableHeightGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class KinmelGridFragment extends Fragment {

    public String typeId;
    public String productionName;
    SharedPreference sharedPreference;
    ProgressBar pbLoading;
    private ExpandableHeightGridView gridView;
    private List<ProductTypeListDtos> productTypeListDtoses = new ArrayList<>();
    private ProductTypeAdapter productTypeAdapter;
    private Realm realm;

    public KinmelGridFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kinmel, container, false);
        gridView = (ExpandableHeightGridView) view.findViewById(R.id.gridview);
        pbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);
        typeId = getArguments().getString(CommonDef.PRODUCT_ID);
        productionName = getArguments().getString(CommonDef.NAME);
        this.realm = RealmController.with(this).getRealm();
        productTypeListDtoses = new ArrayList<>();
        sharedPreference = new SharedPreference(getActivity());
        productTypeAdapter = new ProductTypeAdapter(getActivity(), productTypeListDtoses);
        gridView.setAdapter(productTypeAdapter);
        if (!sharedPreference.getBoolValues(productionName)) {
            pbLoading.setVisibility(View.VISIBLE);
        } else {
            RealmResults<ProductTypeListDtos> realmResults = realm.where(ProductTypeListDtos.class).equalTo("parentTitle", productionName).findAll();
            for (ProductTypeListDtos a : realmResults)
                productTypeListDtoses.add(a);
            populateList();
        }

        getAllProducts();


//        getAllProducts();
        return view;
    }


    private void getAllProducts() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = UrlHelper.BASE_URL + "api/v3/kinmel/products?apikey=" + UrlHelper.API_KEY + "&type_id=" + typeId;


        Log.d("FORMURL", url);
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pbLoading.setVisibility(View.GONE);
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            Log.d("utfstr", response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray cateogiresarray = jsonObject.getJSONArray("data");
                            onDataGot(cateogiresarray);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pbLoading.setVisibility(View.GONE);
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void onDataGot(JSONArray categoryArray) {
        productTypeListDtoses.clear();
        try {
            for (int i = 0; i < categoryArray.length(); i++) {
                JSONObject newsValue = categoryArray.getJSONObject(i);
                ProductTypeListDtos productionTechnologyDtos = new ProductTypeListDtos();
                String id = newsValue.getString("id");
                String name = newsValue.getString("name");
                String unit = newsValue.getString("unit");
                String video_url = newsValue.getString("video_url");
                String discountedPrice = newsValue.getString("price");
                String actualPrice = newsValue.getString("discounted_price");
                String description = newsValue.getString("description");
                String view_count = newsValue.getString("view_count");

                JSONArray images = newsValue.getJSONArray("images");
                if (images != null && images.length() > 0) {
                    JSONObject imageObject = images.getJSONObject(0);
                    String imagevalue = imageObject.getString("name");
                    String img = BaseApplication.Product_HOSTIMG + imagevalue;


                    String dummyImg = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJoAmgMBEQACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAQMEBQYCB//EADoQAAEDAwIEAwYCCAcAAAAAAAEAAgMEBREhMQYSE0EiUXEUMkJhgZEHUhUjQ5KhwdHhFiUzcoKisf/EABsBAQADAQEBAQAAAAAAAAAAAAACAwQBBQYH/8QALREAAgIBBAAGAQIHAQAAAAAAAAECAxEEEiExBRMyQVFhIjOhFCRDUnGR0RX/2gAMAwEAAhEDEQA/APcUAIAQAgBACAEAIAQAgEygFQAgBACAEAIAQAgBACAEAIAQAgBACA5e4MaXOcGtAySdguN4WWBmmraaqc5tPPFKW+8GOyQowshP0vIJCmDJca3qqpay12e1uLaytl53yD9lCzVx+v8AIquxvGEdRqYTzRscdy0FSRwcUgCAEAIAQAgBACAEAIAQAgBAJkIBUBDu9I24Wuro355Z4XMyNCMjcfNRlFSTTB4T+F9/qaLiemFdITHI91JM57icknDf+2NfmsUIxqtSRCL5PfameOngfNK7lYwZcVubSWWTPM7FcorpxLdOK7nK2O3UcRp2F3u/7ADud9ty7RZoT3vc+kdccG24dudFVWmCphknZDM3mjZVOy8N7fTyVsJxayT8qb6RatqoHnDZmH6qakn7nHXNdodyFIgKgBACAEAIAQAgBACAg3m7UdloJa64zCGnjAy46kk6AAbkk7ALjeDqWXgp4LleLhC2qLaey0z9Ym1bOrNI3zLQ4BvpqVDLf0dwjiqn4lpoXT0stNXADwxuo+iXf8jL/JJOS5Qik3gzl0/E2ttcDobnZJLfVHAbNKeeIa7+Hf6FVSumuME3Xh89EkfiLRuhh9irqavlOA9pidCc/LJP/qjLUpe/7EGvc8Nh68lzqw2Xpc1Q8twMn3tFVdJLEkjM/o3/ABvx/LV2mC3ipb7Y9jRJBGNWuwMl3p2Hmc9lNSsnHL6NT2v0oqeHav8AytlrmOYGTuftkkHsPLyysdtnzwjfpdJPUtZ9KPQqC4PdGB0TG0AblTrs46PYnpkvcl+2Sn3TgKzeyPkx9xRfp6Ety84XP4hwOf8Anwu6RoLRxJS12GPe1rytNWqhP3PK1XhtlPKXBeg522Wo80VACAEAIAQAgEOyAyUVI+/XN9xuxa+2Ucp/RzA0t53DQynXXGoB76nYhV492ySTfCJdXcY4CRTBked3huXn6n+6qlbjo3U6TPMipnq2SkmaR7/m95Kq35Nqpa6S/wBFHeq6hETofFI9+nSaOYO9cqmzUQhx7mmnSWT5fX2Y53AwrpOv1hQ6ktbEMkfJRrsm+1wRt0FWeGzqPg+59J0Dbl02u0MvIOfHlkKMW92dv7lT8LpXKl+xXT/h7UURMwrjP5kNwVO3UTUfSXUeGUyeJSZp+FrdFBTOEkeX5yHFuqzUtWcyRusoVCUYdGiL2saBotGUitRbYw+vY3RQdiRcqJMr7hVMljwCs91kXFmqipxkUsc8tPIHxPLSDnRYYTcemehKuM1ho9G4H4qFaG0NY7EwHgcTuvd0Ws8z8J9nyPi/hfkvza+jbr0j54EAIAQAgBANzOYInGQ4bjVcfCOpNvCKCod+obDABHBG0MjYNmtGgCzzk2ehTXGHfZnbiTG7VyyzPVpaZQ11W4Mdh2T2VE5YRuqryxq1xxtBlkIdI7uVXVFL8n2XXNvhFzE9pG4WhNGVxY7zAdxhMnMHEj2EYOy42iSixrrRxjTAXMpInskyurK0ZIaf4rPZcka6qPkqpZ3OOQVhla2bo1oaLnEqonhHJBI1TJ1PA0yWSknbPAS2RhyCFfCWHlHZ1xsjtke08KXqO82qObP61ujx819HpbvNrT9z888R0b0tzj7exdrSYAQAgBACApLtWB0wgafC0647lUWT5wbtPViO5lLdLrFSxnJGfJZ52KJtp08pMxN1vgcXa51WKy/4PYp0+DMVl1LnYGTn5qjbKfZ6dVKHKW5u7PKg1OPRN1ItKe6PGMuyiukuyt0pk6O6gjVymtQiv+HOnXDI3R6hElRgjyVb3e6qpXN9FsakhkxyPOSqnCcuSxSig9mf5LnlSO+ZEOiR2Kr2yG9CmIhqi012c3EaVm+ilFl0WXv4f3Z1tvTYHuPRnPKfLK9LRXbLEvZnk+N6Tz6HNdo9iC+gPgxUAIAQDVVL0YJJPytJXJPCyShHdJIwNfcOmXyOIzuvPlPB79dWVgwF6vDpZnEu0z5rz5zlN8Hs01KK4MzUVb5CdVZGtI2QikMBxJ1KnguUh2OQt2Ki1ktXJNgqDtlZ5QDiTY5jjdUuJzBPpf1hAOVWo5lgjJ4RdU1G0gaLbCpGOy1osGUTcbLQq0ZXcx5tC3HuqXlor89jclvA+FVyqTJLUESppQxp00Wa6pJF9duWVEjNcLzT0IsjRZhq45W5Ba4HK0Qlgsmt9bR7pap/abdTy5yXMGV9VVLdBM/NdTDy7pR+yYrCgEAICuv7+W2yfMgKu14iaNKs2I8rv1QQ14BXl2vg+loiee3GUmQjKpqXB6sOiFlXk8igrhJMcaVFl8WPRuwVBouROhkHdUSRxotrfIMhUdSyVTXBqqAhzcr0K+jy7uC0YBhXoxsda4BSK2jpzmlDiTRXXDHIVlu6NdHZnZxh5wvGl6merDoY6fNI0AblShy8Fu7CPYeEyf0LCCc8pwvq9N+mj4DxJfzMi5WgwAgBAVfEYza3/IhVXek06T9VHk98BJK8yxZPpaXwYa5wkPLgPVVVvHB6MJcFcriw6C4SQ41RZdEdaos0RHWOwVBosLagccDVZbUVTRqbbN4N1fTPKPNvgW8UwPda0zFKA6JPmpZIbRHTY7rjZ1QK6tqA5pAKyXz4NdNeCof4nFeU+WblwiRRw5kaSO610Vc5ZVbP8WercMs6dpiGMZJK+k06xWj4fxCWdQy2V5iBACAhXiPq26oaN+TP2ULFmLLaZbbEzye8xnmPqvNkj6al8GWrKfmJOFllH3RsjIqJ6LXLThdjY12aIz+SM6ne3torFNMuTTEaw+SNouiPMYSoNlyeBzplRyWKRY0IIaMrNayE2XdHP09yq657XgzzhksY6wALUrUZ3TkV1wI2R3nFp0MSVMkh3ICqlbJlsaoobBJ3KqeWTO2MyUjDki2WVDFmVoHmttceUY7p4TPVLfF0aKBnkwL3K1iKR8TfLdZJ/ZJUyoEAIDlzQ4EHYjBCDODzHiWhdT1UkZGjTp6dl51scPB9FpLFOCZkqqPdZpI9GLK2WJUTiXRYw6EFVZaLUzjoDK7vLoyHGQBQcyxSHBA1c3smpD8UYAVcpEtxKjCqYH26BTicF5gpnDoFBg7aclDjJVOzOFfCJROWDTcOUJqK6NuNAcn0C36evMkeN4hfsqZ6GNl6x8qCAEAIAQGd4ttvtNN7SweKMeLHceaouhlZNuiv3S2s8zroS17gQsEkfQwllFVNHhUyRoiRnhZpIuRxhVFqO2hRZajoLhNDrFBkx5pwogXnXUdELl3IwONciZ1kiHVyugVz6Ligh5sLZXE8+6eD0Phy3+yUnUeMSSa+gXr0V7Vk+T12o82eF0i4V5hBACAEAIDh2CCD3QHnnGFk9jcaiFpNO4/unyWG+rbz7HuaHVb/AMX2YicakLHI9eDITxqVnmaIjfdZ2XIXK4WJgHarhNDzDooMmd5XDoZTB0AdUwdHWFEMlhSMLngAarVXEz2SSRvuF7IS1lTUtwwatafi/svY09HG6R8t4jruXCBsAtx4QqAEAIAQCHZAcOKAhVoZJC+OVrXMcMFpGQQuNZWGdTcXlHmPE1k9llfLSEvh/L8Tf6hYLtO1zHo93Sa+M/xnwzJSuIJBXnTR7UJZGC9UYL0w5kwWJic+qYJpj0b1BonkdDlHBLIFyYO5OmZPZcxkZJtJSyzPa1jHEk4AAV1dEmyiy+MFls3/AAzwyIOWouAy7dsW/wB/6L2dPpNvMj5jX+LeZ+FPXz/w2segAAAHkFvPBHAgOkAIAQAgEKA4cEBDqY8tIQGdutA6TmQGGu/D73lzmNLT5hZ7dNCz6Zu0+vtp47X2Zeqt9wpnYdTOkb+Zmv8ADdYJ6Kcfs9ujxSifbw/shGpDDyyAsI7OBCzSomu0elC6EuYvJ02piJ/1WfdR8uXwW7yVFIw7Pb91B1tkt7JMYDj4Tn0Ulp2yLux2WNNbKicjkgkcPTCujo5P2MtniNNfqmi9t/DMz3DqkRjyGpWuvQ/3Hm3+Nx/prP8Ak19ptMFEB0YvF+Y6lba6YV+lHiX6u69/m+PgvoWYAVpmJTAgHAgOkAIAQAgBAckIDhzMoCPJTh24QEOa3Mf8KAgzWOF+7QgIz+HIT8IQDf8AhmnO8Y+yAVvDNKP2Mf7q5hHdz+SVFY4o/dYB6Bd4OPnslR2uNvwplgkx0bWjRqAkshA7IB5rEA4AgOgEAqAEAIAQAgBAJhAIQgOS0FAIWBAJ00AnTQB00AvTCABGPJAdciAUNQHWEAIBUAIAQAgBACAEAIAQAgBAJhAGEAYQBhAGEAqAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQH//2Q==";
                    //Toast.makeText(ProductTypeListActivity.this,image.toString(),Toast.LENGTH_SHORT).show();
                    productionTechnologyDtos.setImg(img);

                    Log.d("imageLink", img);
                }
                productionTechnologyDtos.setId(id);
                productionTechnologyDtos.setTitle(name);
                productionTechnologyDtos.setVideoUrl(video_url);
                productionTechnologyDtos.setParentTitle(productionName);
                productionTechnologyDtos.setDiscountPrice(discountedPrice + " per " + unit);
                productionTechnologyDtos.setActualPrice(actualPrice + " per " + unit);
                productionTechnologyDtos.setViewdCounter(view_count);
                productionTechnologyDtos.setDescription(description);
                productTypeListDtoses.add(productionTechnologyDtos);
            }
            productTypeAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (ProductTypeListDtos b : productTypeListDtoses) {
            // Persist your data easily
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(b);
            realm.commitTransaction();
        }
        sharedPreference.setKeyValues(productionName, true);
        populateList();
    }

    private void populateList() {
//        productTypeAdapter.notifyDataSetChanged();
//        if (getActivity() != null) {
//            productTypeAdapter = new ProductTypeAdapter(getActivity(), productTypeListDtoses);
//            gridView.setAdapter(productTypeAdapter);
//        }

        if (getActivity() != null) {
            productTypeAdapter = new ProductTypeAdapter(getActivity(), productTypeListDtoses);
            gridView.setAdapter(productTypeAdapter);
        }

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long ids) {
                ProductTypeListDtos object = (ProductTypeListDtos) parent.getItemAtPosition(position);
                String image = object.getImg();
                String description = object.getDescription();
                String id = object.getId();

                if (description.isEmpty()) {
                    description = "this is test only no data got.....";
                }
                String title = object.getTitle();
                String actualPrice = object.getActualPrice();
                String discountPrice = object.getDiscountPrice();
                // String name = object.ge
                Intent intent = new Intent(getActivity(), DetailProdcutActivity.class);
                intent.putExtra("image", image);
                intent.putExtra("desc", description);
                intent.putExtra("title", title);
                intent.putExtra("actualPrice", actualPrice);
                intent.putExtra("discountPrice", discountPrice);
                intent.putExtra("video_url", object.getVideoUrl());
                intent.putExtra("view_count", object.getViewdCounter());
                intent.putExtra("id", id);

                startActivity(intent);
            }
        });
    }
}
