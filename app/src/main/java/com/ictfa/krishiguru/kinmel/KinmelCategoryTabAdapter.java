package com.ictfa.krishiguru.kinmel;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ictfa.krishiguru.helpers.CommonDef;

import java.util.ArrayList;

/**
 * Created by sureshlama on 5/19/17.
 */

public class KinmelCategoryTabAdapter extends FragmentPagerAdapter {
    int PAGE_COUNT;
    private ArrayList<KinmelTitleObject> tabTitles;

    public KinmelCategoryTabAdapter(FragmentManager fm, Context context, ArrayList<KinmelTitleObject> tabTitles) {
        super(fm);
        PAGE_COUNT = tabTitles.size();
//        Log.d("PAGECOUNT", tabTitles.length+"");
        this.tabTitles = tabTitles;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        KinmelGridFragment kinmelGridFragment = new KinmelGridFragment();
        Bundle b = new Bundle();
        b.putString(CommonDef.PRODUCT_ID, tabTitles.get(position).getId());
        b.putString(CommonDef.NAME, tabTitles.get(position).getName());
        kinmelGridFragment.setArguments(b);
        return kinmelGridFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles.get(position).getName();
    }


}
