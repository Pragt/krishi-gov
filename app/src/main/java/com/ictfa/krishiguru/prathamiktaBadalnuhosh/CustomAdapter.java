package com.ictfa.krishiguru.prathamiktaBadalnuhosh;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.ictfa.krishiguru.R;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    Context context;
    int[] imageId;
    ArrayList<Crop> cropList;

    private static LayoutInflater inflater = null;

    public CustomAdapter(Activity mainActivity, ArrayList<Crop> cropList) {
        // TODO Auto-generated constructor stub
        context = mainActivity;
        this.cropList = cropList;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.cropList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View rowView;

        rowView = inflater.inflate(R.layout.item_crops, null);
        final CheckBox cbCrops = (CheckBox) rowView.findViewById(R.id.cb_crops);
        if (cropList.get(position).isSelected)
            cbCrops.setChecked(true);
        cbCrops.setText(cropList.get(position).nepali_name);
        cbCrops.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbCrops.isChecked()) {
                    cropList.get(position).isSelected = true;
                } else
                    cropList.get(position).isSelected = false;
            }
        });

        return rowView;
    }

    public String getItems() {
        String crop = "";
        for (int i = 0; i < cropList.size(); i++) {
            if (cropList.get(i).isSelected) {
                if (crop.equalsIgnoreCase(""))
                    crop = cropList.get(i).id;
                else
                    crop = crop + "," + cropList.get(i).id;
            }
        }
        return crop;

    }

} 