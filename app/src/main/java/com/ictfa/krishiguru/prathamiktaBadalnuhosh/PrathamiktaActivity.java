package com.ictfa.krishiguru.prathamiktaBadalnuhosh;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.MultipleTraders.object.TraderType;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.login.LoginActivity;
import com.ictfa.krishiguru.registerOrganization.object.Zone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by prajit on 11/3/17.
 */

public class PrathamiktaActivity extends AppCompatActivity {


    ArrayList<TraderType> listTraderType;
    ArrayList<Crop> cropList;
    @BindView(R.id.sp_trader_type)
    Spinner spTraderType;
    @BindView(R.id.sp_market_name)
    Spinner spMarketName;
    @BindView(R.id.sp_disctict)
    Spinner spDistrict;
    @BindView(R.id.gv_crops)
    GridView gvCrops;
    @BindView(R.id.btn_update)
    Button btnUpdate;
    CustomProgressDialog progressDialog;
    Alerts alerts;
    SharedPreference sharedPreference;
    boolean isEditMode = false;
    private ArrayList<Zone> zoneList;
    private ArrayList<String> zoneStringList;
    private ArrayList<Zone> districtList;
    private ArrayList<String> districtStringList;
    private ArrayList<TraderType> marketTypeList;
    private ArrayList<String> countryList;
    @BindView(R.id.tv_croplist_title)
    TextView tvCropListTitle;
    private CustomAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prathamikta);
        ButterKnife.bind(this);
        setTitle("प्राथमिकता बदल्नुहोस");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle b = getIntent().getExtras();
        progressDialog = new CustomProgressDialog(this);
        sharedPreference = new SharedPreference(this);
        zoneList = new ArrayList<>();
        zoneStringList = new ArrayList<>();
        alerts = new Alerts(this);
        districtList = new ArrayList<>();
        districtStringList = new ArrayList<>();
        alerts = new Alerts(this);
        cropList = new ArrayList<>();
        mAdapter = new CustomAdapter(PrathamiktaActivity.this, cropList);
        if (b != null) {
            listTraderType = (ArrayList<TraderType>) getIntent().getSerializableExtra(CommonDef.TRADER_TYPE);
            loadTraderType();
        }

        if (isEditMode) {
            setTitle(getResources().getString(R.string.change_trader));
        }

        spTraderType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
//                    case 0:
//                        marketTypeList = new ArrayList<TraderType>();
//                        loadMarketSpinner();
//                        spDistrict.setVisibility(View.GONE);
//                        break;
//                    case 0:
//                        getZone();
//                        spDistrict.setVisibility(View.VISIBLE);
//                        loadZoneSpinner();
//                        loadDistrictSpinner();
//                        break;
                    case 0:
                        spDistrict.setVisibility(View.GONE);
                        loadMarketList();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    private void loadMarketList() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = UrlHelper.BASE_URL + "api/v3/market/list?apikey=" + UrlHelper.API_KEY;
        marketTypeList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        pd.hidepd();
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONArray traderTypeListJson = object.getJSONArray("data");
                            for (int i = 0; i < traderTypeListJson.length(); i++) {
                                JSONObject object1 = (JSONObject) traderTypeListJson.get(i);
                                TraderType traderType = new TraderType();
                                traderType.id = object1.getString("id");
                                traderType.type = object1.getString("name");
                                traderType.nepali_name = object1.getString("nepali_name");
                                marketTypeList.add(traderType);
                            }

                            loadMarketSpinner();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                pd.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadMarketSpinner() {
        ArrayList<String> listMarketString = new ArrayList<>();
//        listMarketString.add("बजार को नाम छान्नुहोस्");
        for (int i = 0; i < marketTypeList.size(); i++) {
            listMarketString.add(marketTypeList.get(i).nepali_name);
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listMarketString); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMarketName.setAdapter(spinnerArrayAdapter);

        spMarketName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                onMarketSelected();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void loadTraderType() {
        ArrayList<String> listZoneString = new ArrayList<>();
//        listZoneString.add("ब्यापारीको किसिम छान्नुहोस्");
        for (int i = 0; i < listTraderType.size(); i++) {
            listZoneString.add(listTraderType.get(i).nepali_name);
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listZoneString); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTraderType.setAdapter(spinnerArrayAdapter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getDistrict(int id) {
        progressDialog.showpd(getResources().getString(R.string.loading_district));
        RequestQueue queue = Volley.newRequestQueue(PrathamiktaActivity.this);
        districtList = new ArrayList<>();
        districtStringList = new ArrayList<>();
        String zone = UrlHelper.BASE_URL + "api/v3/district/list/" + id + "?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(response);
                            JSONArray listObject = object.getJSONArray("data");
//                             String id = object.getString(object.getString("id"));
                            JSONObject zoneObject;
                            Zone zone;
                            for (int i = 0; i < listObject.length(); i++) {
                                zone = new Zone();
                                zoneObject = (JSONObject) listObject.get(i);
                                zone.id = zoneObject.getInt("id");
                                zone.name = zoneObject.getString("name");
                                districtList.add(zone);
                                districtStringList.add(zoneObject.getString("name"));
                                Log.i("Prajit", "onResponse: " + zone.name);
                            }
                            loadDistrictSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    private void getZone() {
        progressDialog.showpd(getResources().getString(R.string.loading_zone));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(PrathamiktaActivity.this);
        zoneList = new ArrayList<>();
        String zone = UrlHelper.BASE_URL + "api/v3/zone/list?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(response);
                            JSONArray listObject = object.getJSONArray("data");
//                             String id = object.getString(object.getString("id"));
                            JSONObject zoneObject;
                            Zone zone;
                            zoneStringList = new ArrayList<>();
                            for (int i = 0; i < listObject.length(); i++) {
                                zone = new Zone();
                                zoneObject = (JSONObject) listObject.get(i);
                                zone.id = zoneObject.getInt("id");
                                zone.name = zoneObject.getString("name");
                                zoneList.add(zone);
                                zoneStringList.add(zoneObject.getString("name"));
                                Log.i("Prajit", "onResponse: " + zone.name);
                            }
                            loadZoneSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                progressDialog.hidepd();
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadZoneSpinner() {
//        zoneStringList.add(0, getResources().getString(R.string.choose_zone));
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, zoneStringList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMarketName.setAdapter(spinnerArrayAdapter);

        spMarketName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spTraderType.getSelectedItemPosition() == 0) {
//                    if (i == 0) {
//                        districtStringList = new ArrayList<String>();
//                        loadDistrictSpinner();
//                    } else
                    getDistrict(zoneList.get(i).id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void loadDistrictSpinner() {
//        districtStringList.add(0, getResources().getString(R.string.choose_district));
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, districtStringList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDistrict.setAdapter(spinnerArrayAdapter);

        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    onMarketSelected();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    void onMarketSelected() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(PrathamiktaActivity.this);

        String url;
        url = UrlHelper.BASE_URL + "api/v3/crop/traders?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONObject code = jsonObject.getJSONObject("code");
                            String status = code.getString("status");
                            String message = code.getString("message");
                            cropList = new ArrayList<>();
                            Crop crop = new Crop();
                            if (status.equalsIgnoreCase("1")) {
                                JSONArray array = jsonObject.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject1 = array.getJSONObject(i);
                                    crop = new Crop();
                                    crop.id = jsonObject1.getString("id");
                                    crop.name = jsonObject1.getString("name");
                                    crop.nepali_name = jsonObject1.getString("nepali_name");
                                    crop.description = jsonObject1.getString("description");
                                    cropList.add(crop);
                                }
//                                if (cropList.size() > 0)
//                                    tvCropListTitle.setVisibility(View.VISIBLE);
//                                else
//                                    tvCropListTitle.setVisibility(View.GONE);

                                mAdapter = new CustomAdapter(PrathamiktaActivity.this, cropList);
                                gvCrops.setAdapter(mAdapter);

//                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.ORG_ID, data.getInt("id"));
//                                    alerts.showSuccessAlert(message);
                            } else
                                alerts.showErrorAlert(message);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                progressDialog.hidepd();
                // mTextView.setText("That didn't work!");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                if (spTraderType.getSelectedItemPosition() == 1) {
//                    params.put("type_id", listTraderType.get(spTraderType.getSelectedItemPosition() - 1).id);
//                    params.put("district_id", String.valueOf(districtList.get(spDistrict.getSelectedItemPosition() - 1).id));
//                } else if (spTraderType.getSelectedItemPosition() == 2) {
                    params.put("tag", "login");
                    params.put("type_id", listTraderType.get(spTraderType.getSelectedItemPosition()).id);
                    params.put("market_id", marketTypeList.get(spMarketName.getSelectedItemPosition()).id);
//                }

                return params;
            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @OnClick(R.id.btn_update)
    void onUpdate() {
        if (isValid()) {
            progressDialog.showpd(getResources().getString(R.string.please_wait));
            RequestQueue queue = Volley.newRequestQueue(this);
            String url = UrlHelper.BASE_URL + "api/v3/user/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/priority?apikey=" + UrlHelper.API_KEY;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                JSONObject object = new JSONObject(utfStr);
                                JSONObject jsonObject = (JSONObject) object.getJSONObject("code");
                                String status = jsonObject.getString("status");
                                String messege = jsonObject.getString("message");

                                if (status.equalsIgnoreCase("1"))
                                    alerts.showSuccessAlert(getResources().getString(R.string.success));
                                else
                                    Toast.makeText(PrathamiktaActivity.this, messege, Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                alerts.showToastMsg(e.getMessage());
                            } catch (UnsupportedEncodingException e) {
                                alerts.showToastMsg(e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    }, error -> {
                        progressDialog.hidepd();
                        alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                        // mTextView.setText("That didn't work!");
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
//                    if (spTraderType.getSelectedItemPosition() == 1) {
//                        params.put("zone_id", String.valueOf(zoneList.get(spMarketName.getSelectedItemPosition() - 1).id));
//                        params.put("district_id", String.valueOf(districtList.get(spDistrict.getSelectedItemPosition() - 1).id));
//                    } else if (spTraderType.getSelectedItemPosition() == 2) {
                        params.put("market_id", marketTypeList.get(spMarketName.getSelectedItemPosition()).id);
//                    }
                    params.put("priority", mAdapter.getItems());
                    params.put("type", listTraderType.get(spTraderType.getSelectedItemPosition()).id);

                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    private boolean isValid() {
        if (mAdapter.getItems().equalsIgnoreCase("")) {
            alerts.showToastMsg("कृपया बस्तुहरु छान्नुहोस्");
            return false;
        } else if (sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN) == 1)
            return true;
        else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return false;
        }
    }
}
