package com.ictfa.krishiguru.utilities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonMethods;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WeightMeasurement extends AppCompatActivity {

    @BindView(R.id.sp_option)
    Spinner spOptions;

    @BindView(R.id.et_length)
    EditText etLength;

    @BindView(R.id.et_width)
    EditText etWidht;

    @BindView(R.id.tv_result)
    TextView tvResult;

    @BindView(R.id.btn_measure)
    Button btnMeasure;

    ArrayList<String> listOptions;
    Alerts alerts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight_measurement);
        CommonMethods.setupUI(findViewById(R.id.cl_weight_measurement), this);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.pasu_taul_mapan));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        alerts = new Alerts(this);

        listOptions = new ArrayList<>();
        listOptions.add("पशु छान्नुहोस्");
        listOptions.add("गाइ/ भैसी");
        listOptions.add("खसी/ बाख्रा");
        listOptions.add("भेडा");

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listOptions); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spOptions.setAdapter(spinnerArrayAdapter);
    }

    @OnClick(R.id.btn_measure)
    void onMeasure() {
        if (isAllValid()) {
            tvResult.setVisibility(View.VISIBLE);
            switch (spOptions.getSelectedItemPosition()) {
                case 1:
                    double result = (1.74 * Float.parseFloat(etLength.getText().toString())) + (1.05 * Float.parseFloat(etWidht.getText().toString())) - 71.1;
                    tvResult.setText(etLength.getText().toString() + " से. मि. लम्बाई र " + etWidht.getText().toString() + " से. मि. चौडाई " + listOptions.get(spOptions.getSelectedItemPosition()) + " को तौल " + new DecimalFormat("##.##").format(result) + " किलो  हुन्छ |");
                    break;
                case 2:
                    double result1 = (Float.parseFloat(etWidht.getText().toString()) * Float.parseFloat(etWidht.getText().toString()) * Float.parseFloat(etLength.getText().toString())) / 10500;
                    tvResult.setText(etLength.getText().toString() + " से. मि. लम्बाई र " + etWidht.getText().toString() + " से. मि. चौडाई " + listOptions.get(spOptions.getSelectedItemPosition()) + " को तौल " +  new DecimalFormat("##.##").format(result1) + " किलो हुन्छ |");
                    break;
                case 3:
                    double result2 = (Float.parseFloat(etWidht.getText().toString()) * Float.parseFloat(etWidht.getText().toString()) * Float.parseFloat(etLength.getText().toString())) / 12000;
                    tvResult.setText(etLength.getText().toString() + " से. मि. लम्बाई र " + etWidht.getText().toString() + " से. मि. चौडाई " + listOptions.get(spOptions.getSelectedItemPosition()) + " को तौल " +  new DecimalFormat("##.##").format(result2) + " किलो  हुन्छ |");
                    break;
            }
        }
    }

    public boolean isAllValid() {
        if (spOptions.getSelectedItemPosition() == 0) {
            alerts.showWarningAlert(getResources().getString(R.string.choose_pasu_type));
            return false;
        } else if (etLength.getText().toString().isEmpty()) {
            alerts.showWarningAlert(getResources().getString(R.string.provide_length));
            return false;
        } else if (etWidht.getText().toString().isEmpty()) {
            alerts.showWarningAlert(getResources().getString(R.string.provide_width));
            return false;
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
