package com.ictfa.krishiguru.utilities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonMethods;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LandMeasurement extends AppCompatActivity {
    @BindView(R.id.sp_from)
    Spinner spFrom;

    @BindView(R.id.sp_to)
    Spinner spTo;

    @BindView(R.id.et_from)
    EditText etFrom;

    @BindView(R.id.et_to)
    EditText etTo;

    ArrayList<String> listMeasurement;
    ArrayList<Double> listRatio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land_measurement);
        CommonMethods.setupUI(findViewById(R.id.rl_land_measurement), this);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.jagga_mapak));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        listMeasurement = new ArrayList<>();
        listMeasurement.add("रोपनी");
        listMeasurement.add("आना");
        listMeasurement.add("पैसा");
        listMeasurement.add("दाम");
        listMeasurement.add("बिघा");
        listMeasurement.add("कत्था");
        listMeasurement.add("धुर");
        listMeasurement.add("acre");
        listMeasurement.add("sq. ft");
        listMeasurement.add("sq. m");
        listMeasurement.add("hectare");

        listRatio = new ArrayList<>();
        listRatio.add(1.0);
        listRatio.add(16.0);
        listRatio.add(64.0);
        listRatio.add(256.0);
        listRatio.add(0.0751);
        listRatio.add(1.502);
        listRatio.add(30.047);
        listRatio.add(0.12571);
        listRatio.add(5476.0);
        listRatio.add(508.738);
        listRatio.add(0.05087);


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listMeasurement); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFrom.setAdapter(spinnerArrayAdapter);
        spTo.setAdapter(spinnerArrayAdapter);

        spFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                etTo.setText("");
                etFrom.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                etTo.setText("");
                etFrom.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        etTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etTo.isFocused())
                    etFrom.setText("");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if (etTo.isFocused() && !etTo.getText().toString().isEmpty()) {
                        double result = (listRatio.get(spFrom.getSelectedItemPosition()) / listRatio.get(spTo.getSelectedItemPosition())) * Double.parseDouble(etTo.getText().toString());
                        etFrom.setText(result + "");
                    }
                }catch (Exception exx){}

            }
        });

        etFrom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etFrom.isFocused())
                    etTo.setText("");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if (etFrom.isFocused() && !etFrom.getText().toString().isEmpty()) {
                        double result = (listRatio.get(spTo.getSelectedItemPosition()) / listRatio.get(spFrom.getSelectedItemPosition())) * Double.parseDouble(etFrom.getText().toString());
                        etTo.setText(result + "");
                    }
                }catch (Exception exx)
                {}
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
