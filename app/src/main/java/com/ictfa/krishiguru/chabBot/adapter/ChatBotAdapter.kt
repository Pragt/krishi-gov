package com.ictfa.krishiguru.chabBot.adapter

import android.app.Activity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.ictfa.krishiguru.R
import com.ictfa.krishiguru.chabBot.model.ChatHistory
import com.ictfa.krishiguru.chabBot.model.NormalAnswer
import com.ictfa.krishiguru.chabBot.model.ProductionListResponse
import com.ictfa.krishiguru.chabBot.model.ProductionTechnologyResponse
import com.ictfa.krishiguru.ui.fragment.ImageShowFragment
import com.squareup.picasso.Picasso
import java.io.File
import java.util.*

/**
Created by Prajeet Naga on 1/20/19, 9:48 AM.
 **/

class ChatBotAdapter(private val mContext: Activity, var listItem: ArrayList<ChatHistory> = arrayListOf()) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SimpleTextReplyViewHolder) {
            val message = (listItem[position].object1 as NormalAnswer).answer.replace("\\n", "\n")
            holder.tvReply.text = message

//            holder.tvReply.text="asdfds\nasdfdsf"

            holder.tvTime.text = getFormattedDateTime(listItem[position].timestamp)
            if (!TextUtils.isEmpty((listItem[position].object1 as NormalAnswer).imagePath)) {
                Picasso.with(mContext).load(File((listItem[position].object1 as NormalAnswer).imagePath)).resize(500, 500).into(holder.ivImage)
                holder.ivImage.visibility = View.VISIBLE

                holder.ivImage.setOnClickListener {
                    val imageShowFragment = ImageShowFragment.newInstance()
                    val bundle = Bundle()
                    bundle.putString("image", (listItem[position].object1 as NormalAnswer).imagePath)
                    imageShowFragment.arguments = bundle
                    imageShowFragment.show(mContext.fragmentManager, "sad")
                }
            } else if (!TextUtils.isEmpty((listItem[position].object1 as NormalAnswer).imageUrl)) {
                Picasso.with(mContext).load((listItem[position].object1 as NormalAnswer).imageUrl).resize(500, 500).into(holder.ivImage)
                holder.ivImage.visibility = View.VISIBLE

                holder.ivImage.setOnClickListener {
                    val imageShowFragment = ImageShowFragment.newInstance()
                    val bundle = Bundle()
                    bundle.putString("image", (listItem[position].object1 as NormalAnswer).imageUrl)
                    imageShowFragment.arguments = bundle
                    imageShowFragment.show(mContext.fragmentManager, "sad")
                }
            } else
                holder.ivImage.visibility = View.GONE
//            if (position == listItem.size - 1)
//                holder.ivLogo.visibility = View.VISIBLE
//            else
//                holder.ivLogo.visibility = View.INVISIBLE
        } else if (holder is ProductionViewHolder) {
            holder.tvTime.text = getFormattedDateTime(listItem[position].timestamp)
            if (position == listItem.size - 1)
                isClickedEnabled = true
            else
                isClickedEnabled = false
            val adapter = ProductionListAdapter(mContext, listItem.get(position).object1 as MutableList<Any>, isClickedEnabled)
            holder.recyclerView.adapter = adapter
            if ((listItem.get(position).object1 as MutableList<Any>).size > 0) {
                if ((listItem.get(position).object1 as MutableList<Any>).get(0) is ProductionListResponse.Datum) {
                    holder.tvTitle.setText("तपाइ कुन विषयमा प्रश्न सोध्न चाहनुहुन्छ?")
                } else if ((listItem.get(position).object1 as MutableList<Any>).get(0) is ProductionTechnologyResponse.Datum) {
                    holder.tvTitle.setText("व्यवसायिक उत्पादन छान्नुहोश")
                } else if ((listItem.get(position).object1 as MutableList<Any>).get(0) is TitleResponse.Datum) {
                    val data: MutableList<TitleResponse.Datum> = listItem.get(position).object1 as MutableList<TitleResponse.Datum>
                    holder.tvTitle.setText(data.get(0).mainTitle)
                }
            }

        } else if (holder is ConfirmationViewHolder) {
            holder.tvTime.text = getFormattedDateTime(listItem[position].timestamp)
            holder.btnYes.setOnClickListener({
                listener.onYesClicked(listItem[position].object1 as NormalAnswer)
            })

            val message = (listItem[position].object1 as NormalAnswer).answer.replace("\\n", "\n")
            holder.tvTitle.text = message
            if (!TextUtils.isEmpty((listItem[position].object1 as NormalAnswer).imageUrl)) {
                Picasso.with(mContext).load((listItem[position].object1 as NormalAnswer).imageUrl).resize(500, 500).into(holder.ivImage)
                holder.ivImage.visibility = View.VISIBLE

                holder.ivImage.setOnClickListener {
                    val imageShowFragment = ImageShowFragment.newInstance()
                    val bundle = Bundle()
                    bundle.putString("image", (listItem[position].object1 as NormalAnswer).imageUrl)
                    imageShowFragment.arguments = bundle
                    imageShowFragment.show(mContext.fragmentManager, "sad")
                }
            } else
                holder.ivImage.visibility = View.GONE
//            if (Build.VERSION.SDK_INT >= 24) {
//                holder.tvTitle.text = Html.fromHtml(message, Html.FROM_HTML_MODE_LEGACY) // for 24 api and more
//            } else {
//                holder.tvTitle.text = Html.fromHtml(message) // or for older api
//            }
            holder.btnNo.setOnClickListener({
                listener.onNoClicked(listItem[position].object1 as NormalAnswer)
            })

            if ((listItem[position].object1 as NormalAnswer).userInteraction == 2) {
                holder.btnNo.visibility = View.GONE
                holder.btnYes.visibility = View.VISIBLE
                holder.btnNo.isEnabled = false
                holder.btnYes.isEnabled = false
            } else if ((listItem[position].object1 as NormalAnswer).userInteraction == 3) {
                holder.btnYes.visibility = View.GONE
                holder.btnNo.visibility = View.VISIBLE
                holder.btnNo.isEnabled = false
                holder.btnYes.isEnabled = false
            } else if ((listItem[position].object1 as NormalAnswer).userInteraction == 1 && position != listItem.size - 1) {
                holder.btnYes.visibility = View.GONE
                holder.btnNo.visibility = View.GONE
            } else {
                holder.btnYes.visibility = View.VISIBLE
                holder.btnNo.visibility = View.VISIBLE
                holder.btnNo.isEnabled = true
                holder.btnYes.isEnabled = true
            }

            if ((listItem[position].object1 as NormalAnswer).type == 6) {
                holder.btnYes.text = "भयाे"
                holder.btnNo.text = "भएन"
            } else {
                holder.btnYes.text = "चाहन्छु"
                holder.btnNo.text = "चाहदिन"
            }
        }
    }

    private val TYPE_HEADER = 1
    private val TYPE_REPLY = 2
    private val TYPE_QUESTION = 3
    private val TYPE_TYPING = 4
    private val TYPE_LIST = 5
    private val TYPE_CONFIRMATION = 6
    private var isClickedEnabled = true


    private lateinit var listener: SaveListPreferences

    fun setListener(mLisetner: SaveListPreferences, packageProductName: String) {
        this.listener = mLisetner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_REPLY) {
            val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_simple_text_reply, parent, false)
            return SimpleTextReplyViewHolder(view)
        } else if (viewType == TYPE_QUESTION) {
            val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_question, parent, false)
            return SimpleTextReplyViewHolder(view)
        } else if (viewType == TYPE_TYPING) {
            val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_bot_typing, parent, false)
            return HeaderViewHolder(view)
        } else if (viewType == TYPE_LIST) {
            val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_production_list, parent, false)
            return ProductionViewHolder(view)
        } else if (viewType == TYPE_CONFIRMATION) {
            val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_confirmation_layout, parent, false)
            return ConfirmationViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_chat_bot_header, parent, false)
            return HeaderViewHolder(view)
        }
    }

    override fun getItemCount() = this.listItem.size


    inner class SimpleTextReplyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivLogo: ImageView
        var tvReply: TextView
        var tvTime: TextView
        var ivImage: ImageView

        init {
            ivLogo = itemView.findViewById(R.id.ivLogo)
            tvReply = itemView.findViewById(R.id.tvReply)
            tvTime = itemView.findViewById(R.id.tvTime)
            ivImage = itemView.findViewById(R.id.ivImage)
        }

    }

    inner class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

        }

    }

    inner class ConfirmationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var btnYes: Button
        var btnNo: Button
        var tvTitle: TextView
        var tvTime: TextView
        var ivImage: ImageView

        init {
            btnYes = itemView.findViewById(R.id.btnYes)
            btnNo = itemView.findViewById(R.id.btnNo)
            tvTitle = itemView.findViewById(R.id.tvTitle)
            tvTime = itemView.findViewById(R.id.tvTime)
            ivImage = itemView.findViewById(R.id.ivImage)
        }

    }

    inner class ProductionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var recyclerView: RecyclerView
        var tvTitle: TextView
        var tvTime: TextView

        init {
            recyclerView = itemView.findViewById(R.id.rvProductionList)
            tvTitle = itemView.findViewById(R.id.tvTitle)
            tvTime = itemView.findViewById(R.id.tvTime)
            recyclerView.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)

        }
    }

    fun add(item: NormalAnswer) {
        this.listItem.add(ChatHistory(object1 = item))
        notifyItemInserted(listItem.size - 1)
    }

    fun add(list: List<Any>) {
        this.listItem.add(ChatHistory(object1 = list))
//        if (listItem.size > 1) {
//            notifyItemChanged(listItem.size - 1)
//        }
        notifyItemInserted(listItem.size - 1)
//        listener.save(listItem)

    }

    fun addLoading() {
        this.listItem.add(ChatHistory(object1 = null))
        notifyItemInserted(listItem.size - 1)
    }

    fun removeLoading() {
        if (listItem.size > 0 && listItem.get(listItem.size - 1).object1 == null) {
            this.listItem.remove(listItem[listItem.size - 1])
            notifyItemRemoved(listItem.size - 1)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (listItem.get(position).object1 == null) {
            return TYPE_TYPING
        } else if (listItem.get(position).object1 is NormalAnswer) {
            if ((listItem.get(position).object1 as NormalAnswer).type == 1) {
                return TYPE_REPLY
            } else if ((listItem.get(position).object1 as NormalAnswer).type == 2) {
                return TYPE_QUESTION
            } else if ((listItem.get(position).object1 as NormalAnswer).type == 3) {
                return TYPE_CONFIRMATION
            } else if ((listItem.get(position).object1 as NormalAnswer).type == 4) {
                return TYPE_CONFIRMATION
            } else if ((listItem.get(position).object1 as NormalAnswer).type == 5) {
                return TYPE_CONFIRMATION
            } else if ((listItem.get(position).object1 as NormalAnswer).type == 6) {
                return TYPE_CONFIRMATION
            } else
                return TYPE_HEADER
        } else if (listItem.get(position).object1 is List<*>) {
            return TYPE_LIST
        } else
            return TYPE_REPLY
    }

    fun setClickedDisabled() {
        isClickedEnabled = false
        notifyDataSetChanged()
    }

    interface SaveListPreferences {
        fun save(list: ArrayList<Any?>)
        fun onYesClicked(normalAnswer: NormalAnswer)
        fun onNoClicked(normalAnswer: NormalAnswer)
    }

    fun getFormattedDateTime(msgTimeMillis: Long): String {

        val messageTime = Calendar.getInstance()
        messageTime.setTimeInMillis(msgTimeMillis)
        // get Currunt time
        val now = Calendar.getInstance()

        val strTimeFormate = "h:mm aa"
        val strDateFormate = "dd/MM/yyyy h:mm aa"

        return if (now.get(Calendar.DATE) === messageTime.get(Calendar.DATE)
                &&
                now.get(Calendar.MONTH) === messageTime.get(Calendar.MONTH)
                &&
                now.get(Calendar.YEAR) === messageTime.get(Calendar.YEAR)) {

            "Today at " + DateFormat.format(strTimeFormate, messageTime)

        } else if (now.get(Calendar.DATE) - messageTime.get(Calendar.DATE) === 1
                &&
                now.get(Calendar.MONTH) === messageTime.get(Calendar.MONTH)
                &&
                now.get(Calendar.YEAR) === messageTime.get(Calendar.YEAR)) {
            "Yesterday at " + DateFormat.format(strTimeFormate, messageTime)
        } else {
            "" + DateFormat.format(strDateFormate, messageTime)
        }
    }
}