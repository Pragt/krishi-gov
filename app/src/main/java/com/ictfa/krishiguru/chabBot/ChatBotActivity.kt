package com.ictfa.krishiguru.chabBot

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ictfa.krishiguru.R
import com.ictfa.krishiguru.chabBot.adapter.*
import com.ictfa.krishiguru.chabBot.model.ChatHistory
import com.ictfa.krishiguru.chabBot.model.NormalAnswer
import com.ictfa.krishiguru.chabBot.model.ProductionListResponse
import com.ictfa.krishiguru.chabBot.model.ProductionTechnologyResponse
import com.ictfa.krishiguru.helpers.*
import com.ictfa.krishiguru.realm.RealmController
import com.ictfa.krishiguru.retrofit.ApiClient
import com.ictfa.krishiguru.retrofit.ApiInterface
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker
import com.treebo.internetavailabilitychecker.InternetConnectivityListener
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_chat_bot.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.File
import java.io.IOException


class ChatBotActivity : AppCompatActivity(), ProductionListAdapter.onProductionOptionChooseListener, ChatBotAdapter.SaveListPreferences, InternetConnectivityListener {

    val customProgressDialog: CustomProgressDialog by lazy {
        CustomProgressDialog(this@ChatBotActivity)
    }

    override fun onInternetConnectivityChanged(isIntConnected: Boolean) {
        if (!isIntConnected) {
            tvStatus.text = "Offline"
            ivStatus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.offline_indicator))
            tvNoInternet.visibility = View.VISIBLE
            isConnected = false
        } else {
            tvStatus.text = "Online"
            ivStatus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.online_indicator))
            tvNoInternet.visibility = View.GONE
            isConnected = true
        }
    }

    var subscribedPackageId = 0
    var mainPackageId = 0
    var isAnother = false
    var isConnected: Boolean = false
    lateinit var mInternetAvailabilityChecker: InternetAvailabilityChecker

    override fun save(list: ArrayList<Any?>) {
    }

    override fun onYesClicked(normalAnswer: NormalAnswer) {
        if (isConnected) {
            if (normalAnswer.type == TYPE_CONSIRMATION_ASK_QUESTION) {
                var json = gson.toJson(NormalAnswer(" के तपाइ कृषि विज्ञलाइ प्रश्न सोध्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION, 2))
                val lastItem = realm.where(ChatData::class.java).findAll()
                var chatData = ChatData(lastItem.last()!!.id, TYPE_NORMAL_ANSWER, json, lastItem.last()!!.is_synced)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()

                chatBotAdapter.listItem.set(chatBotAdapter.listItem.size - 1, ChatHistory(object1 = NormalAnswer(" के तपाइ कृषि विज्ञलाइ प्रश्न सोध्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION, 2)))
                chatBotAdapter.notifyDataSetChanged()
                chatBotAdapter.addLoading()
                Handler().postDelayed(Runnable {
                    getProductionType()
                }, 1000)
            } else if (normalAnswer.type == TYPE_REASK_QUESTION) {


                var json = gson.toJson(NormalAnswer("के तपाइ ${normalAnswer.selectedTitle} सम्बन्धि थप प्रश्न गर्न चाहनुहुन्छ ?", TYPE_REASK_QUESTION, 2, subTypeId = normalAnswer.subTypeId))
                val lastItem = realm.where(ChatData::class.java).findAll()
                var chatData = ChatData(lastItem.last()!!.id, TYPE_NORMAL_ANSWER, json, lastItem.last()!!.is_synced)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()
                normalAnswer.subTypeId?.let {
                    production_type_id = it
                }


                chatBotAdapter.listItem.set(chatBotAdapter.listItem.size - 1, ChatHistory(object1 = NormalAnswer("के तपाइ ${normalAnswer.selectedTitle} सम्बन्धि थप प्रश्न गर्न चाहनुहुन्छ ?", TYPE_REASK_QUESTION, 2, normalAnswer.subTypeId)))
                chatBotAdapter.notifyDataSetChanged()

                editText.isFocusableInTouchMode = true
                editText.isFocusable = true
                rlEdittext.visibility = View.VISIBLE

                chatBotAdapter.add(NormalAnswer("तपाई अब आफ्नो प्रश्न राख्न सक्नुहुन्छ सक्नु हुन्छ |तपाइ अब आफ्नो जिज्ञासा तल देखाइएको म्यासेज बक्समा लेखेर पठाउनुहोस । आवश्यक्ता अनुसार जिज्ञासा सम्बन्धित फोटो पनि पठाउनु होला ।", 1))
                chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                chatBotAdapter.setClickedDisabled()

                json = gson.toJson(NormalAnswer("तपाइ अब आफ्नो जिज्ञासा तल देखाइएको म्यासेज बक्समा लेखेर पठाउनुहोस । आवश्यक्ता अनुसार जिज्ञासा सम्बन्धित फोटो पनि पठाउनु होला ।", 1))
                var maxId = realm.where(ChatData::class.java).max("id")
                var nextId = if (maxId == null) 1 else maxId.toInt() + 1
                chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, lastItem.last()!!.is_synced)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()
            } else if (normalAnswer.type == TYPE_ANSWER_SATISFIED) {
                var json = gson.toJson(NormalAnswer("कृषि गुरूकाे याे उत्तरबाट तपाइकाे जिज्ञासा समाधान भयाे ?", TYPE_ANSWER_SATISFIED, 2, normalAnswer.subTypeId))
                val lastItem = realm.where(ChatData::class.java).findAll()
                var chatData = ChatData(lastItem.last()!!.id, TYPE_NORMAL_ANSWER, json, lastItem.last()!!.is_synced)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()

                chatBotAdapter.listItem.set(chatBotAdapter.listItem.size - 1, ChatHistory(object1 = NormalAnswer(" कृषि गुरूकाे याे उत्तरबाट तपाइकाे जिज्ञासा समाधान भयाे ?", TYPE_ANSWER_SATISFIED, 2, normalAnswer.subTypeId)))
                chatBotAdapter.notifyDataSetChanged()
                chatBotAdapter.addLoading()
                chatBotAdapter.setClickedDisabled()
                saveAndClose()
            } else {
                var json = gson.toJson(NormalAnswer("के तपाई यो प्याकेज किन्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION, 2))
                val lastItem = realm.where(ChatData::class.java).findAll()
                var chatData = ChatData(lastItem.last()!!.id, TYPE_NORMAL_ANSWER, json, lastItem.last()!!.is_synced)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()

                chatBotAdapter.listItem.set(chatBotAdapter.listItem.size - 1, ChatHistory(object1 = NormalAnswer("के तपाई यो प्याकेज किन्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION, 2)))
                chatBotAdapter.notifyDataSetChanged()
                chatBotAdapter.addLoading()
                Handler().postDelayed(Runnable {
                    getSubscriptionTitles(subscribedPackageId)
                }, 1000)

            }
        }

    }

    override fun onNoClicked(normalAnswer: NormalAnswer) {
        if (isConnected) {
            if (normalAnswer.type == TYPE_CONSIRMATION_ASK_QUESTION) {
                var json = gson.toJson(NormalAnswer(" के तपाइ कृषि विज्ञलाइ प्रश्न सोध्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION, 3))
                val lastItem = realm.where(ChatData::class.java).findAll()
                var chatData = ChatData(lastItem.last()!!.id, TYPE_NORMAL_ANSWER, json, lastItem.last()!!.is_synced)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()
                chatBotAdapter.listItem.set(chatBotAdapter.listItem.size - 1, ChatHistory(object1 = NormalAnswer(" के तपाइ कृषि विज्ञलाइ प्रश्न सोध्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION, 3)))
                chatBotAdapter.notifyDataSetChanged()
                saveAndClose()
            } else if (normalAnswer.type == TYPE_CONSIRMATION_ASK_QUESTION) {
                var json = gson.toJson(NormalAnswer("के तपाइ ${normalAnswer.selectedTitle} सम्बन्धि थप प्रश्न गर्न चाहनुहुन्छ ?", TYPE_REASK_QUESTION, 3))
                val lastItem = realm.where(ChatData::class.java).findAll()
                var chatData = ChatData(lastItem.last()!!.id, TYPE_NORMAL_ANSWER, json, lastItem.last()!!.is_synced)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()
                chatBotAdapter.listItem.set(chatBotAdapter.listItem.size - 1, ChatHistory(object1 = NormalAnswer("के तपाइ ${normalAnswer.selectedTitle} सम्बन्धि थप प्रश्न गर्न चाहनुहुन्छ ?", TYPE_REASK_QUESTION, 3)))
                chatBotAdapter.notifyDataSetChanged()
                saveAndClose()
            } else if (normalAnswer.type == TYPE_ANSWER_SATISFIED) {
                val answer = NormalAnswer("कृषि गुरूकाे याे उत्तरबाट तपाइकाे जिज्ञासा समाधान भयाे ?", TYPE_ANSWER_SATISFIED, 3, subTypeId = normalAnswer.subTypeId, selectedTitle = normalAnswer.selectedTitle)
                var json = gson.toJson(answer)
                val lastItem = realm.where(ChatData::class.java).findAll()
                var chatData = ChatData(lastItem.last()!!.id, TYPE_NORMAL_ANSWER, json, lastItem.last()!!.is_synced)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()

                chatBotAdapter.listItem.set(chatBotAdapter.listItem.size - 1, ChatHistory(object1 = NormalAnswer(" कृषि गुरूकाे याे उत्तरबाट तपाइकाे जिज्ञासा समाधान भयाे ?", TYPE_ANSWER_SATISFIED, 3, subTypeId = normalAnswer.subTypeId, selectedTitle = normalAnswer.selectedTitle)))
                chatBotAdapter.notifyDataSetChanged()
                chatBotAdapter.addLoading()
                Handler().postDelayed(Runnable {
                    var json = gson.toJson(NormalAnswer("के तपाइ ${normalAnswer.selectedTitle} सम्बन्धि थप प्रश्न गर्न चाहनुहुन्छ ?", TYPE_REASK_QUESTION, 1, subTypeId = normalAnswer.subTypeId, selectedTitle = normalAnswer.selectedTitle))
                    val lastItem = realm.where(ChatData::class.java).findAll()
                    var maxId = realm.where(ChatData::class.java).max("id")
                    var nextId = if (maxId == null) 1 else maxId.toInt() + 1
                    var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, lastItem.last()!!.is_synced)
                    production_type_id = normalAnswer.subTypeId!!
                    realm.beginTransaction()
                    realm.copyToRealmOrUpdate(chatData)
                    realm.commitTransaction()
                    chatBotAdapter.listItem.set(chatBotAdapter.listItem.size - 1, ChatHistory(object1 = NormalAnswer("के तपाइ ${normalAnswer.selectedTitle} सम्बन्धि थप प्रश्न गर्न चाहनुहुन्छ ?", TYPE_REASK_QUESTION, 1, subTypeId = normalAnswer.subTypeId, selectedTitle = normalAnswer.selectedTitle)))
                    chatBotAdapter.notifyDataSetChanged()
                    chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                }, 1000)
            } else {
                var json = gson.toJson(NormalAnswer("के तपाई यो प्याकेज किन्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION, 3))
                val lastItem = realm.where(ChatData::class.java).findAll()
                var chatData = ChatData(lastItem.last()!!.id, TYPE_NORMAL_ANSWER, json, lastItem.last()!!.is_synced)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()
                chatBotAdapter.listItem.set(chatBotAdapter.listItem.size - 1, ChatHistory(object1 = NormalAnswer("के तपाई यो प्याकेज किन्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION, 3)))
                chatBotAdapter.notifyDataSetChanged()

                saveAndClose()
            }

        }
    }

    private fun saveAndClose() {
        chatBotAdapter.add(NormalAnswer("कृषि गुरु चलाउनु भाकोमा धन्यवाद ", 1))
        chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
        chatBotAdapter.setClickedDisabled()
        var json = gson.toJson(NormalAnswer("कृषि गुरु चलाउनु भाकोमा धन्यवाद  |", 1))
        var maxId = realm.where(ChatData::class.java).max("id")
        var nextId = if (maxId == null) 1 else maxId.toInt() + 1
        var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
        realm.beginTransaction()
        realm.copyToRealmOrUpdate(chatData)
        realm.commitTransaction()
        syncToServerChatHistory()

        Handler().postDelayed(object : Runnable {
            override fun run() {
                finish()
            }

        }, 3000)
    }

    private val gson: Gson by lazy {
        Gson()
    }

    private var production_type_id = 0
    private var technology_id = 0
    private val realm: Realm by lazy {
        RealmController.with(this).realm
    }

    companion object {
        const val TYPE_NORMAL_ANSWER = "1"
        const val TYPE_PRODUCTION_TYPE = "2"
        const val TYPE_PRODUCTION_TECHNOLOGY = "3"
        const val TYPE_SUBSCRIPTION_TITLE = "4"
        const val TYPE_CONSIRMATION_ASK_QUESTION = 3
        const val TYPE_CONFIRMATION_BUY_PACKAGE = 4
        const val TYPE_REASK_QUESTION = 5
        const val TYPE_ANSWER_SATISFIED = 6


        //Normal Answer
    }


    override fun onSubscritionClicked(id: Int, title: String, mainPackageId: Int, productionId: Int) {
        if (isConnected) {
            chatBotAdapter.add(NormalAnswer(title, 2))
            chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
            chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)

            chatBotAdapter.addLoading()
            this.mainPackageId = mainPackageId
            production_type_id = productionId
            Handler().postDelayed(Runnable {
                getSubscriptionType(id, title)
                chatBotAdapter.notifyDataSetChanged()
            }, 1000)
            val json = gson.toJson(NormalAnswer(title, 2))
            val maxId = realm.where(ChatData::class.java).max("id")
            val nextId = if (maxId == null) 1 else maxId.toInt() + 1
            var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
            realm.beginTransaction()
            realm.copyToRealmOrUpdate(chatData)
            realm.commitTransaction()
        }
    }

    override fun onProductionTitleChoose(id: Int, title: String) {
        if (isConnected) {
            chatBotAdapter.add(NormalAnswer(title, 2))
            var json = gson.toJson(NormalAnswer(title, 2))
            var maxId = realm.where(ChatData::class.java).max("id")
            var nextId = if (maxId == null) 1 else maxId.toInt() + 1
            var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
            realm.beginTransaction()
            realm.copyToRealmOrUpdate(chatData)
            realm.commitTransaction()
            technology_id = id
            chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
            if (sharedPreference.getStringValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED) == "1" && isAnother == false) {
                editText.isFocusableInTouchMode = true
                editText.isFocusable = true
                rlEdittext.visibility = View.VISIBLE

                chatBotAdapter.add(NormalAnswer("तपाइ अब आफ्नो जिज्ञासा तल देखाइएको म्यासेज बक्समा लेखेर पठाउनुहोस । आवश्यक्ता अनुसार जिज्ञासा सम्बन्धित फोटो पनि पठाउनु होला ।", 1))
                chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                chatBotAdapter.setClickedDisabled()

                var json = gson.toJson(NormalAnswer("तपाइ अब आफ्नो जिज्ञासा तल देखाइएको म्यासेज बक्समा लेखेर पठाउनुहोस । आवश्यक्ता अनुसार जिज्ञासा सम्बन्धित फोटो पनि पठाउनु होला ।", 1))
                var maxId = realm.where(ChatData::class.java).max("id")
                var nextId = if (maxId == null) 1 else maxId.toInt() + 1
                var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()
            } else {
                chatBotAdapter.add(NormalAnswer("के तपाई यो प्याकेज किन्न चाहनुहुन्छ?", TYPE_CONFIRMATION_BUY_PACKAGE, 1))
                chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                subscribedPackageId = id

                json = gson.toJson(NormalAnswer("के तपाई यो प्याकेज किन्न चाहनुहुन्छ?", TYPE_CONFIRMATION_BUY_PACKAGE, 1))
                maxId = realm.where(ChatData::class.java).max("id")
                nextId = if (maxId == null) 1 else maxId.toInt() + 1
                chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(chatData)
                realm.commitTransaction()
            }

            syncToServerChatHistory()
        }
    }

    override fun onProductionOptionChoose(id: Int, title: String) {
        if (isConnected) {
            chatBotAdapter.add(NormalAnswer(title, 2))
            val json = gson.toJson(NormalAnswer(title, 2))
            val maxId = realm.where(ChatData::class.java).max("id")
            val nextId = if (maxId == null) 1 else maxId.toInt() + 1
            var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
            realm.beginTransaction()
            realm.copyToRealmOrUpdate(chatData)
            realm.commitTransaction()
            production_type_id = id
            chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
            Handler().postDelayed(Runnable {
                chatBotAdapter.notifyDataSetChanged()
                if (sharedPreference.getStringValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED) == "1" && isAnother == false) {
                    editText.isFocusableInTouchMode = true
                    editText.isFocusable = true
                    rlEdittext.visibility = View.VISIBLE

                    chatBotAdapter.add(NormalAnswer("तपाइ अब आफ्नो जिज्ञासा तल देखाइएको म्यासेज बक्समा लेखेर पठाउनुहोस । आवश्यक्ता अनुसार जिज्ञासा सम्बन्धित फोटो पनि पठाउनु होला ।", 1))
                    chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                    chatBotAdapter.setClickedDisabled()

                    var json = gson.toJson(NormalAnswer("तपाइ अब आफ्नो जिज्ञासा तल देखाइएको म्यासेज बक्समा लेखेर पठाउनुहोस । आवश्यक्ता अनुसार जिज्ञासा सम्बन्धित फोटो पनि पठाउनु होला ।", 1))
                    var maxId = realm.where(ChatData::class.java).max("id")
                    var nextId = if (maxId == null) 1 else maxId.toInt() + 1
                    var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
                    realm.beginTransaction()
                    realm.copyToRealmOrUpdate(chatData)
                    realm.commitTransaction()
                } else
                    getTechnologyList(id)
            }, 1000)
        }
    }

    val alerts: Alerts by lazy {
        Alerts(this@ChatBotActivity)
    }
    lateinit var sharedPreference: SharedPreference

    private val chatBotAdapter: ChatBotAdapter by lazy {
        ChatBotAdapter(this@ChatBotActivity)
    }

    private var realmList: RealmResults<ChatData>? = null

    private var packageProductName: String = ""

    private var validity: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_bot)
        sharedPreference = SharedPreference(this)
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance()
        mInternetAvailabilityChecker.addInternetConnectivityListener(this)
        mainPackageId = sharedPreference.getIntValues(CommonDef.SharedPreference.PACKAGE_ID)
        if (intent.extras != null) {
            isAnother = intent.extras.getBoolean("is_another")
            mainPackageId = intent.extras.getInt("package_id")
            packageProductName = intent!!.getStringExtra("packageProductName")
            validity = intent!!.getIntExtra("validity", 0)
        }

        rvBot.layoutManager = LinearLayoutManager(this)
        rvBot.adapter = chatBotAdapter
        rvBot.itemAnimator?.addDuration = 100
        rvBot.itemAnimator?.changeDuration = 50
        chatBotAdapter.setListener(this, packageProductName)
//        chatBotAdapter.add(NormalAnswer("", 0))
//        Toast.makeText(this, sharedPreference.getIntValues(CommonDef.SharedPreference.PACKAGE_ID).toString(), Toast.LENGTH_SHORT).show()
        chatBotAdapter.add(NormalAnswer("नमस्ते ${sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME)}जी, कृषि गुरुमा तपाइलाई स्वागत छ.", 1))

        try {
            realmList = realm.where(ChatData::class.java).findAll()
            if (realmList != null) {
                for (chatData in realmList!!) {
                    if (chatData.type.equals(TYPE_NORMAL_ANSWER)) {
                        val myClass = gson.fromJson(chatData.data, NormalAnswer::class.java)
//                        Toast.makeText(this@ChatBotActivity, myClass.answer, Toast.LENGTH_SHORT).show()
//                        if (myClass.answer.equals("तपाई अब कृषि विशेषज्ञ सित कुराकानी गर्न सक्नु हुन्छ |")) {
//                            chatBotAdapter.setClickedDisabled()
//                            editText.setFocusableInTouchMode(true);
//                            editText.setFocusable(true)
//                        }
                        chatBotAdapter.listItem.add(ChatHistory(timestamp = chatData.timestamp, object1 = myClass))
                    } else if (chatData.type == TYPE_PRODUCTION_TYPE) {
                        val token = object : TypeToken<ArrayList<ProductionListResponse.Datum>>() {}
                        val publicationListing = gson.fromJson(chatData.data, token.type) as ArrayList<ProductionListResponse.Datum>
//                        Toast.makeText(this@ChatBotActivity, publicationListing[0].type, Toast.LENGTH_SHORT).show()
                        chatBotAdapter.listItem.add(ChatHistory(timestamp = chatData.timestamp, object1 = publicationListing))
                    } else if (chatData.type == TYPE_PRODUCTION_TECHNOLOGY) {
                        val token = object : TypeToken<ArrayList<ProductionTechnologyResponse.Datum>>() {}
                        val publicationListing = gson.fromJson(chatData.data, token.type) as ArrayList<ProductionTechnologyResponse.Datum>
                        chatBotAdapter.listItem.add(ChatHistory(timestamp = chatData.timestamp, object1 = publicationListing))
                    } else if (chatData.type == TYPE_SUBSCRIPTION_TITLE) {
                        val token = object : TypeToken<ArrayList<TitleResponse.Datum>>() {}
                        val publicationListing = gson.fromJson(chatData.data, token.type) as ArrayList<TitleResponse.Datum>
                        chatBotAdapter.listItem.add(ChatHistory(timestamp = chatData.timestamp, object1 = publicationListing))
                    }
                }
                rvBot.scrollToPosition(chatBotAdapter.listItem.size - 1)
            }
        } catch (exx: Exception) {

        }
//        Toast.makeText(this, realmList.size, Toast.LENGTH_SHORT).show()
        getChatHistory()

        iv_back.setOnClickListener {
            onBackPressed()
        }

        editText.addTextChangedListener(
                object : TextWatcher {
                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                    }

                    override fun afterTextChanged(s: Editable?) {
                        if (editText.text.isNullOrBlank())
                            img_send.visibility = View.GONE
                        else
                            img_send.visibility = View.VISIBLE
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                    }
                })


        img_send.setOnClickListener {
            chatBotAdapter.add(NormalAnswer(editText.text.toString(), 2, imagePath = imageFile))
            chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
            chatBotAdapter.addLoading()
            Handler().postDelayed(Runnable {
                if (TextUtils.isEmpty(imageFile)) {
                    askToExpert(editText.text.toString())
                } else
                    askToExpertWithImage(editText.text.toString())
                editText.setText("")
                ivImage.setImageDrawable(ContextCompat.getDrawable(this@ChatBotActivity, R.drawable.ic_camera_kirsi))
                imageFile = ""
            }, 1000)
        }

        chatBotAdapter.registerAdapterDataObserver(
                object : RecyclerView.AdapterDataObserver() {
                    override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                        rvBot.smoothScrollToPosition(chatBotAdapter.itemCount)
                    }
                })


        ivImage.setOnClickListener {
            showFileChooser()
        }
    }

    private fun showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this@ChatBotActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA), CommonDef.REQUEST_STORAGE_CAMERA)
                return
            }
        }
        startActivityForResult(ImagePicker.getPickImageChooserIntent(this), CommonDef.PICK_IMAGE_REQUEST)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            startActivityForResult(ImagePicker.getPickImageChooserIntent(this), CommonDef.PICK_IMAGE_REQUEST)
        }
    }

    private var imageUri: Uri? = null

    private var imageFile: String? = null

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CommonDef.PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data != null && data.data != null) {
                imageUri = data.data
                ivImage.setImageURI(imageUri)
                val imageBmp: Bitmap?
                try {
                    imageBmp = FileUtils.getThumbnail(this@ChatBotActivity, imageUri, 1080)
                    imageFile = FileUtils.storeBitmapToFile(imageBmp, this@ChatBotActivity)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            } else
            // From camera
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    imageFile = ImagePicker.getImage.absolutePath
                } else
                    imageFile = ImagePicker.outputFileUri.path

                val imageBmp = FileUtils.getThumbnail(this@ChatBotActivity, Uri.fromFile(File(imageFile)), 1080)
                imageFile = FileUtils.storeBitmapToFile(imageBmp, this@ChatBotActivity)

                ivImage.setImageURI(Uri.fromFile(File(imageFile)))
            }

        }
    }


    private fun getChatHistory() {
        customProgressDialog.showpd(resources.getString(R.string.please_wait))
        var timestamp: Long = 1
        realmList?.let {
            if (it.size > 0)
                timestamp = it.get(it.size - 1)!!.timestamp
        }


        ApiClient.getClient().create(ApiInterface::class.java).getChatHistory(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID), "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA", timestamp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    if (s.code === 200) {
//                        alerts.showToastMsg("Chat history listed")
                        customProgressDialog.hidepd()
                        for (chatData in s.data.histories) {
                            if (chatData.type.equals(TYPE_NORMAL_ANSWER)) {
                                val myClass = gson.fromJson(chatData.data, NormalAnswer::class.java)
//                        Toast.makeText(this@ChatBotActivity, myClass.answer, Toast.LENGTH_SHORT).show()
//                        if (myClass.answer.equals("तपाई अब कृषि विशेषज्ञ सित कुराकानी गर्न सक्नु हुन्छ |")) {
//                            chatBotAdapter.setClickedDisabled()
//                            editText.setFocusableInTouchMode(true);
//                            editText.setFocusable(true)
//                        }
                                chatBotAdapter.listItem.add(ChatHistory(timestamp = chatData.timestamp, object1 = myClass))
                            } else if (chatData.type == TYPE_PRODUCTION_TYPE) {
                                val token = object : TypeToken<ArrayList<ProductionListResponse.Datum>>() {}
                                val publicationListing = gson.fromJson(chatData.data, token.type) as ArrayList<ProductionListResponse.Datum>
//                        Toast.makeText(this@ChatBotActivity, publicationListing[0].type, Toast.LENGTH_SHORT).show()
                                chatBotAdapter.listItem.add(ChatHistory(timestamp = chatData.timestamp, object1 = publicationListing))
                            } else if (chatData.type == TYPE_PRODUCTION_TECHNOLOGY) {
                                val token = object : TypeToken<ArrayList<ProductionTechnologyResponse.Datum>>() {}
                                val publicationListing = gson.fromJson(chatData.data, token.type) as ArrayList<ProductionTechnologyResponse.Datum>
                                chatBotAdapter.listItem.add(ChatHistory(timestamp = chatData.timestamp, object1 = publicationListing))
                            } else if (chatData.type == TYPE_SUBSCRIPTION_TITLE) {
                                val token = object : TypeToken<ArrayList<TitleResponse.Datum>>() {}
                                val publicationListing = gson.fromJson(chatData.data, token.type) as ArrayList<TitleResponse.Datum>
                                chatBotAdapter.listItem.add(ChatHistory(timestamp = chatData.timestamp, object1 = publicationListing))
                            }
                            realm.beginTransaction()
                            realm.copyToRealmOrUpdate(chatData)
                            realm.commitTransaction()
                        }

                        if (chatBotAdapter.listItem != null && chatBotAdapter.listItem.size > 2) {
                            chatBotAdapter.notifyDataSetChanged()
                            rvBot.scrollToPosition(chatBotAdapter.listItem.size - 1)
                            //if last item is title choose response
                            if (chatBotAdapter.listItem.get(chatBotAdapter.listItem.size - 1).object1 is List<*>) {
                                //&& (chatBotAdapter.listItem.get(chatBotAdapter.listItem.size - 1).object1 as MutableList<Any>).get(0) is TitleResponse.Datum)
                                // do nothing
                                if (sharedPreference.getStringValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED) == "1" && isAnother == false) {

                                } else {
                                    chatBotAdapter.addLoading()
                                    Handler().postDelayed(Runnable {
                                        getSubscriptionTitles(mainPackageId)
                                    }, 1000)

                                }
                            } else if (chatBotAdapter.listItem.get(chatBotAdapter.listItem.size - 1).object1 is NormalAnswer) {
                                val myClass = chatBotAdapter.listItem.get(chatBotAdapter.listItem.size - 1).object1 as NormalAnswer
                                if (myClass.type == 3 && myClass.subTypeId != null) {
                                    chatBotAdapter.add(NormalAnswer("कृषि गुरूकाे याे उत्तरबाट तपाइकाे जिज्ञासा समाधान भयाे ?", TYPE_ANSWER_SATISFIED, 1, subTypeId = myClass.subTypeId, selectedTitle = myClass.selectedTitle))
                                    chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                                    technology_id = myClass.subTypeId!!
                                } else if (myClass.type == 4 && myClass.subTypeId != null) {
//                                    chatBotAdapter.add(NormalAnswer("${sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME)}जी तपाइले माथि साेधएकाे विकल्प नछान्नु भएकाले ${myClass.selectedTitle} बिषय Automatically छानिएकाे छ । अब तपाईंले ${myClass.selectedTitle} बिषयमा त्यो अबधिभर SMS प्राप्त गर्नु का साथै कृषि गुरु संग प्रश्न सोधी परामर्श लिन सक्नुहुनेछ "))
//                                    chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                                    chatBotAdapter.addLoading()
                                    Handler().postDelayed(Runnable {
                                        getProductionType()
                                    }, 1000)
                                } else {
                                    if (sharedPreference.getStringValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED) == "1" && isAnother == false) {
                                        chatBotAdapter.add(NormalAnswer("Welcome back ${sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME)}जी!  के तपाइ कृषि विज्ञलाइ प्रश्न सोध्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION))
                                        chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                                    } else {
                                        chatBotAdapter.addLoading()
                                        Handler().postDelayed(Runnable {
                                            getSubscriptionTitles(mainPackageId)
                                        }, 1000)

                                    }
                                }
                            } else {
                                if (sharedPreference.getStringValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED) == "1" && isAnother == false) {
                                    chatBotAdapter.add(NormalAnswer("Welcome back ${sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME)}जी! के तपाइ कृषि विज्ञलाइ प्रश्न सोध्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION))
                                    chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                                } else {
                                    chatBotAdapter.addLoading()
                                    Handler().postDelayed(Runnable {
                                        getSubscriptionTitles(mainPackageId)
                                    }, 1000)

                                }
                            }
                        } else {
                            Handler().postDelayed(
                                    object : Runnable {
                                        override fun run() {
                                            if (sharedPreference.getStringValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED) == "1" && isAnother == false) {
                                                chatBotAdapter.addLoading()
                                                getProductionType()
                                            } else {
                                                chatBotAdapter.addLoading()
                                                getSubscriptionTitles(mainPackageId)
                                            }
                                        }
                                    }, 1000)
                        }
                    } else {
                        alerts.showErrorAlert(s.message)
                    }
                },
                        { e ->
                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
                            customProgressDialog.hidepd()
                            e.printStackTrace()
                        },
                        { println("supervisor list") })
        syncToServerChatHistory()
    }

    private fun askToExpert(question: String) {
        ApiClient.getClient().create(ApiInterface::class.java).askToExpert(subscribedPackageId, "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA", sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID), production_type_id, question)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    chatBotAdapter.removeLoading()
                    if (s.code.status === 1) {

                        var json = gson.toJson(NormalAnswer(question, 2, imagePath = imageFile))
                        var maxId = realm.where(ChatData::class.java).max("id")
                        var nextId = if (maxId == null) 1 else maxId.toInt() + 1
                        var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(chatData)
                        realm.commitTransaction()

                        chatBotAdapter.add(NormalAnswer("तपाइकाे प्रश्नकाे उत्तर ढिलाेमा २४ घण्टाभित्र प्राप्त हुनेछ, धन्यवाद ।"))
                        chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)

                        json = gson.toJson(NormalAnswer("तपाइकाे प्रश्नकाे उत्तर ढिलाेमा २४ घण्टाभित्र प्राप्त हुनेछ, धन्यवाद ।"))
                        maxId = realm.where(ChatData::class.java).max("id")
                        nextId = if (maxId == null) 1 else maxId.toInt() + 1
                        chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(chatData)
                        realm.commitTransaction()
                    } else {
                        alerts.showErrorAlert(s.code.message)
                    }
                },
                        { e ->
                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
                            chatBotAdapter.removeLoading()
                            chatBotAdapter.removeLoading()
                            e.printStackTrace()
                        },
                        { println("supervisor list") })

        syncToServerChatHistory()
    }


    private fun askToExpertWithImage(ques: String) {
        val question = RequestBody.create(MediaType.parse("multipart/form-data"), ques)
        val user_id = RequestBody.create(MediaType.parse("multipart/form-data"), sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID).toString())
        val production_type_id = RequestBody.create(MediaType.parse("multipart/form-data"), production_type_id.toString())
        val technology_id = RequestBody.create(MediaType.parse("multipart/form-data"), technology_id.toString())

        val imageFile = File(imageFile)
        val imageBody = RequestBody.create(MediaType.parse("video/*"), imageFile)
        val vFile = MultipartBody.Part.createFormData("image", imageFile.name, imageBody)
        ApiClient.getClient().create(ApiInterface::class.java).askToExpertWithImage(subscribedPackageId, "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA", user_id, production_type_id, technology_id, question, vFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    chatBotAdapter.removeLoading()
                    if (s.code.status === 1) {
                        var json = gson.toJson(NormalAnswer(ques, 2, imageUrl = s.data?.image))
                        var maxId = realm.where(ChatData::class.java).max("id")
                        var nextId = if (maxId == null) 1 else maxId.toInt() + 1
                        var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(chatData)
                        realm.commitTransaction()

                        chatBotAdapter.add(NormalAnswer("तपाइकाे प्रश्नकाे उत्तर ढिलाेमा २४ घण्टाभित्र प्राप्त हुनेछ, धन्यवाद ।"))
                        chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)

                        json = gson.toJson(NormalAnswer("तपाइकाे प्रश्नकाे उत्तर ढिलाेमा २४ घण्टाभित्र प्राप्त हुनेछ, धन्यवाद ।"))
                        maxId = realm.where(ChatData::class.java).max("id")
                        nextId = if (maxId == null) 1 else maxId.toInt() + 1
                        chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(chatData)
                        realm.commitTransaction()
                    } else {
                        alerts.showErrorAlert(s.code.message)
                    }
                },
                        { e ->
                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
                            chatBotAdapter.removeLoading()
                            chatBotAdapter.removeLoading()
                            e.printStackTrace()
                        },
                        { println("supervisor list") })

        syncToServerChatHistory()
    }


    fun getProductionType() {
        ApiClient.getClient().create(ApiInterface::class.java).getProductionTechnology("O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    chatBotAdapter.removeLoading()
                    if (s.code.status === 1) {
                        chatBotAdapter.add(s.data)

                        val json = gson.toJson(s.data)
                        val maxId = realm.where(ChatData::class.java).max("id")
                        val nextId = if (maxId == null) 1 else maxId.toInt() + 1
                        var chatData = ChatData(nextId, TYPE_PRODUCTION_TYPE, json, false)
                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(chatData)
                        realm.commitTransaction()
                    } else {
                        alerts.showErrorAlert(s.code.message)
                    }
                },
                        { e ->
                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
                            chatBotAdapter.removeLoading()
                            e.printStackTrace()
                        },
                        { println("supervisor list") })

        syncToServerChatHistory()
    }


//    fun getProductionForBuyTechnology() {
//        ApiClient.getClient().create(ApiInterface::class.java).getProductionForBuyTechnology("O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({ s ->
//                    chatBotAdapter.removeLoading()
//                    if (s.code.status === 1) {
//                        chatBotAdapter.add(s.data)
//
//                        val json = gson.toJson(s.data)
//                        val maxId = realm.where(ChatData::class.java).max("id")
//                        val nextId = if (maxId == null) 1 else maxId!!.toInt() + 1
//                        var chatData = ChatData(nextId, TYPE_PRODUCTION_TYPE, json, false)
//                        realm.beginTransaction()
//                        realm.copyToRealmOrUpdate(chatData)
//                        realm.commitTransaction()
//                    } else {
//                        alerts.showErrorAlert(s.code.message)
//                    }
//                },
//                        { e ->
//                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
//                            chatBotAdapter.removeLoading()
//                            e.printStackTrace()
//                        },
//                        { println("supervisor list") })
//
//        syncToServerChatHistory()
//    }


    fun getSubscriptionTitles(id: Int) {
        ApiClient.getClient().create(ApiInterface::class.java).getTitles(mainPackageId, "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA", sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    chatBotAdapter.removeLoading()
                    if (s.code.status === 1) {
                        if (s.data.size > 0) {
                            for (titles in s.data) {
                                titles.mainTitle = "तपाइले $validity दिने $packageProductName को प्याकेज छान्नुभएको छ । यो सम्बन्धि कुन विषयको SMS प्राप्त गर्न चाहनुहुन्छ ?"
                                titles.productionId = id
                                titles.mainPackageId = mainPackageId
                            }
                            chatBotAdapter.add(s.data)
                            production_type_id = id
                            chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)

                            val json = gson.toJson(s.data)
                            val maxId = realm.where(ChatData::class.java).max("id")
                            val nextId = if (maxId == null) 1 else maxId.toInt() + 1
                            var chatData = ChatData(nextId, TYPE_SUBSCRIPTION_TITLE, json, false)
                            realm.beginTransaction()
                            realm.copyToRealmOrUpdate(chatData)
                            realm.commitTransaction()
                        } else {
                            chatBotAdapter.add(NormalAnswer(" के तपाइ कृषि विज्ञलाइ प्रश्न सोध्न चाहनुहुन्छ?", TYPE_CONSIRMATION_ASK_QUESTION))
                            chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                        }

                    } else {
                        alerts.showErrorAlert(s.code.message)
                    }
                },
                        { e ->
                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
                            chatBotAdapter.removeLoading()
                            e.printStackTrace()
                        },
                        { println("supervisor list") })

        syncToServerChatHistory()
    }


    fun getSubscriptionType(id: Int, title: String) {
        ApiClient.getClient().create(ApiInterface::class.java).getSubscriptionType(id, "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA", sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID), mainPackageId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    chatBotAdapter.removeLoading()
                    if (s.code.status === 1) {
                        technology_id = id

                        chatBotAdapter.add(NormalAnswer("धन्यबाद अब तपाइले $title विषयमा त्यो अबधिभर SMS प्राप्त गर्नुहुनेछ । त्यो अवधिभर तपाइले कृषि गुरू सग प्रश्न सोधी परामर्श लिन सक्नुहुनेछ ।", 1))
                        chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)

                        var json = gson.toJson(NormalAnswer("धन्यबाद अब तपाइले $title विषयमा त्यो अबधिभर SMS प्राप्त गर्नुहुनेछ । त्यो अवधिभर तपाइले कृषि गुरू सग प्रश्न सोधी परामर्श लिन सक्नुहुनेछ ।", 1))
                        var maxId = realm.where(ChatData::class.java).max("id")
                        var nextId = if (maxId == null) 1 else maxId.toInt() + 1
                        var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(chatData)
                        realm.commitTransaction()
                        chatBotAdapter.addLoading()
                        Handler().postDelayed(Runnable {
                            chatBotAdapter.removeLoading()
                            chatBotAdapter.add(NormalAnswer("तपाई अब कृषि विशेषज्ञ सित कुराकानी गर्न सक्नु हुन्छ |", 1))
                            chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
                            chatBotAdapter.addLoading()
                            Handler().postDelayed(Runnable {
                                json = gson.toJson(NormalAnswer("तपाई अब कृषि विशेषज्ञ सित कुराकानी गर्न सक्नु हुन्छ |", 1))
                                maxId = realm.where(ChatData::class.java).max("id")
                                nextId = if (maxId == null) 1 else maxId!!.toInt() + 1
                                chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
                                realm.beginTransaction()
                                realm.copyToRealmOrUpdate(chatData)
                                realm.commitTransaction()
                                chatBotAdapter.removeLoading()
                                chatBotAdapter.addLoading()
                                Handler().postDelayed(Runnable {
                                    //                                    chatBotAdapter.removeLoading()
//                                    chatBotAdapter.add(NormalAnswer("तपाई के सम्बन्धि प्रश्न सोध्न चाहनुहुन्छ?", 1))
//                                    chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)
//
//                                    json = gson.toJson(NormalAnswer("तपाई के सम्बन्धि प्रश्न सोध्न चाहनुहुन्छ? ", 1))
//                                    maxId = realm.where(ChatData::class.java).max("id")
//                                    nextId = if (maxId == null) 1 else maxId!!.toInt() + 1
//                                    chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
//                                    realm.beginTransaction()
//                                    realm.copyToRealmOrUpdate(chatData)
//                                    realm.commitTransaction()
                                    getProductionType()
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, "1")
                                    isAnother = false
                                }, 1000)
                            }, 2000)
                        }, 2000)


                    } else {
//                        alerts.showErrorAlert(s.code.message)
                        chatBotAdapter.add(NormalAnswer(s.code.message, 1))
                        chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)

                        var json = gson.toJson(NormalAnswer(s.code.message, 1))
                        var maxId = realm.where(ChatData::class.java).max("id")
                        var nextId = if (maxId == null) 1 else maxId!!.toInt() + 1
                        var chatData = ChatData(nextId, TYPE_NORMAL_ANSWER, json, false)
                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(chatData)
                        realm.commitTransaction()

//                        Handler().postDelayed(object : Runnable {
//                            override fun run() {
//                                finish()
//                            }
//
//                        }, 1000)
                    }
                },
                        { e ->
                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
                            chatBotAdapter.removeLoading()
                            e.printStackTrace()
                        },
                        { println("supervisor list") })

        syncToServerChatHistory()
    }

    fun getTechnologyList(id: Int) {
        ApiClient.getClient().create(ApiInterface::class.java).getTechnology(id, "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    chatBotAdapter.removeLoading()
                    if (s.code.status === 1) {
                        chatBotAdapter.add(s.data)
                        chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)

                        val json = gson.toJson(s.data)
                        val maxId = realm.where(ChatData::class.java).max("id")
                        val nextId = if (maxId == null) 1 else maxId.toInt() + 1
                        var chatData = ChatData(nextId, TYPE_PRODUCTION_TECHNOLOGY, json, false)
                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(chatData)
                        realm.commitTransaction()
                    } else {
                        alerts.showErrorAlert(s.code.message)
                    }
                },
                        { e ->
                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
                            chatBotAdapter.removeLoading()
                            e.printStackTrace()
                        },
                        { println("supervisor list") })

        syncToServerChatHistory()
    }

    fun getTechnologyAskList(id: Int) {
        ApiClient.getClient().create(ApiInterface::class.java).getTechnologyAsk(id, "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    chatBotAdapter.removeLoading()
                    if (s.code.status === 1) {
                        chatBotAdapter.add(s.data)
                        chatBotAdapter.notifyItemInserted(chatBotAdapter.listItem.size - 1)

                        val json = gson.toJson(s.data)
                        val maxId = realm.where(ChatData::class.java).max("id")
                        val nextId = if (maxId == null) 1 else maxId.toInt() + 1
                        var chatData = ChatData(nextId, TYPE_PRODUCTION_TECHNOLOGY, json, false)
                        realm.beginTransaction()
                        realm.copyToRealmOrUpdate(chatData)
                        realm.commitTransaction()
                    } else {
                        alerts.showErrorAlert(s.code.message)
                    }
                },
                        { e ->
                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
                            chatBotAdapter.removeLoading()
                            e.printStackTrace()
                        },
                        { println("supervisor list") })

        syncToServerChatHistory()
    }

    fun syncToServerChatHistory() {
        var list = arrayListOf<DataToServer>()
        val realmList = realm.where(ChatData::class.java).findAll()
        if (realmList != null) {
            for (chatData in realmList) {
                if (!chatData.is_synced) {
                    val data = DataToServer(chatData.id, chatData.type, chatData.data, chatData.is_synced, chatData.timestamp)
                    list.add(data)
                }
            }
        }

        if (list.isNotEmpty()) {
            ApiClient.getClient().create(ApiInterface::class.java).syncToServer(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID), "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA", list)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ s ->
                        //                        chatBotAdapter.removeLoading()
                        if (s.code.status === 1) {
                            val realmList = realm.where(ChatData::class.java).findAll()
                            if (realmList != null) {
                                realm.beginTransaction()
                                for (chatData in realmList) {
                                    if (!chatData.is_synced) {
                                        chatData.is_synced = true
                                        realm.insertOrUpdate(chatData)
                                    }
                                }
                                realm.commitTransaction()
                            }
                        } else {
//                            alerts.showErrorAlert(s.code.message)
                        }
                    },
                            { e ->
                                //                                alerts.showErrorAlert("Could not connect to server. Please try again later.")
//                                chatBotAdapter.removeLoading()
//                                chatBotAdapter.removeLoading()
                                e.printStackTrace()
                            },
                            { println("supervisor list") })
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        syncToServerChatHistory()
        mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this)
    }
}
