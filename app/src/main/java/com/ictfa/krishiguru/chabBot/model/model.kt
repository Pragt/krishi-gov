package com.ictfa.krishiguru.chabBot.model

import com.ictfa.krishiguru.chabBot.adapter.ChatData

/**
Created by Prajeet Naga on 3/3/19, 4:19 PM.
 **/
data class ChatHistoryResponse(
    val code: Int,
    val data: Data,
    val message: String
)

data class Data(
    val histories: List<ChatData>
)

data class History(
    val data: String,
    val id: Int,
    val is_synced: Boolean,
    val timestamp: String,
    val type: Int
)

data class ChatHistory(val timestamp: Long = System.currentTimeMillis() ,val object1 : Any?)

data class AskQuestionResponse(
    val data: Answer?,
    val code: Code,
    val meta: List<Any>
)

data class Code(
    val message: String,
    val status: Int
)

data class Answer(
    val image: String
)