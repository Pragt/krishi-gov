package com.ictfa.krishiguru.chabBot.adapter;

/**
 * Created by Prajeet Naga on 3/3/19, 11:58 AM.
 **/
public class DataToServer {
    public String type;
    public String element;
    public long timeStamp;

    public DataToServer() {

    }

    public DataToServer(int id, String type, String data, boolean isSynced, long timeStamp) {
        this.type = type;
        this.element = data;
        this.timeStamp = timeStamp;
    }
}
