package com.ictfa.krishiguru.chabBot.adapter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ictfa.krishiguru.chabBot.model.ProductionListResponse;

import java.util.List;

public class TitleResponse {

    @SerializedName("meta")
    @Expose
    public List<Object> meta = null;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
    @SerializedName("code")
    @Expose
    public ProductionListResponse.Code code;

    public class Image {

        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("location")
        @Expose
        public String location;

    }

    public class Datum {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("title")
        @Expose
        public String title;
        public String mainTitle;
        public int productionId;
        public int mainPackageId;
        @SerializedName("image")
        @Expose
        public String image = null;

    }
}