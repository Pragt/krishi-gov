package com.ictfa.krishiguru.chabBot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductionTechnologyResponse {

    @SerializedName("meta")
    @Expose
    public List<Object> meta = null;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
    @SerializedName("code")
    @Expose
    public ProductionListResponse.Code code;

    public class Image implements Serializable {

        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("location")
        @Expose
        public String location;

    }

    public class Datum implements Serializable{

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("image")
        @Expose
        public List<Image> image = null;

    }
}