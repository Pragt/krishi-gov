package com.ictfa.krishiguru.chabBot.model

import java.io.Serializable

/**
Created by Prajeet Naga on 2/10/19, 3:01 PM.
 **/

// 0 -> welcome message, 1 -> answer, 2-> question, 3 -> expert answer
// 1 - > confirmation, 2-> yes, 3 -> no
data class NormalAnswer(var answer: String,  var type: Int = 1, var userInteraction: Int = 1, var subTypeId : Int? = null, var imagePath : String ? = null, var imageUrl : String ? = null,var selectedTitle : String?= null) : Serializable