package com.ictfa.krishiguru.chabBot.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Prajeet Naga on 2/10/19, 7:28 PM.
 */
class SubscriptionTypeResponseBot : Serializable {
    @SerializedName("meta")
    @Expose
    var meta: List<Any>? = null
    @SerializedName("code")
    @Expose
    lateinit var code: ProductionListResponse.Code
}