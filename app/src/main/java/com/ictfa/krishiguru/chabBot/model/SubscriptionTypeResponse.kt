package com.ictfa.krishiguru.chabBot.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Prajeet Naga on 2/10/19, 7:28 PM.
 */
class SubscriptionTypeResponse : Serializable {
    @SerializedName("meta")
    @Expose
    var meta: List<Any>? = null
    @SerializedName("code")
    @Expose
    lateinit var code: ProductionListResponse.Code
    @SerializedName("data")
    @Expose
    val data: Data? = null

    data class Data(
            val consent: Consent,
            val consent_ok: Boolean,
            val redirect_url: String
    )

    data class Consent(
            val accessToken: String,
            val consentChannel: String,
            val consentText: String,
            val cpTransactionID: String,
            val redirectionURL: String,
            val status: Int,
            val transactionID: String
    )
}