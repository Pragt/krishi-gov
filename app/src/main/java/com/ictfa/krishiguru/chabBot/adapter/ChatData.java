package com.ictfa.krishiguru.chabBot.adapter;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Prajeet Naga on 2/13/19, 8:26 PM.
 **/
public class ChatData extends RealmObject {
    @PrimaryKey
    public int id;
    public int packageId = 0;
    public String type;
    public String data;
    public boolean is_synced;
    public long timestamp;

    public ChatData() {

    }

    public ChatData(int id, String type, String data, boolean isSynced) {
        this.id = id;
        this.type = type;
        this.data = data;
        this.is_synced = isSynced;
        timestamp = System.currentTimeMillis();
    }
}
