package com.ictfa.krishiguru.chabBot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductionListResponse {

    @SerializedName("meta")
    @Expose
    public List<Object> meta = null;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
    @SerializedName("code")
    @Expose
    public Code code;


    public class Datum implements Serializable {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("type")
        @Expose
        public String type;

    }

    public class Code {

        @SerializedName("status")
        @Expose
        public Integer status;
        @SerializedName("message")
        @Expose
        public String message;

    }
}