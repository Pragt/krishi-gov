package com.ictfa.krishiguru.chabBot.adapter

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.ictfa.krishiguru.R
import com.ictfa.krishiguru.chabBot.model.ProductionListResponse
import com.ictfa.krishiguru.chabBot.model.ProductionTechnologyResponse
import com.squareup.picasso.Picasso

/**
 * Created by sureshlama on 2/24/17.
 */

class ProductionListAdapter(private val activity: Activity, private val list: List<Any>, private val isClickedEnabled: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal var listener: onProductionOptionChooseListener

    init {
        this.listener = activity as onProductionOptionChooseListener

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        view = LayoutInflater.from(activity).inflate(R.layout.item_bot_list_options, null)
        return myViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is myViewHolder) {
            var imageUrl: String = ""
            if (list[position] is ProductionListResponse.Datum) {
                val item = list[position] as ProductionListResponse.Datum
                holder.btnName.text = item.type
                holder.btnName.setOnClickListener {
                    if (isClickedEnabled)
                        listener.onProductionOptionChoose(item.id, item.type)
                }
            } else if (list[position] is ProductionTechnologyResponse.Datum) {
                val item = list[position] as ProductionTechnologyResponse.Datum
                holder.btnName.text = item.title
                holder.btnName.setOnClickListener {
                    if (isClickedEnabled)
                        listener.onProductionTitleChoose(item.id, item.title)
                }
                if (item.image.size > 0)
                    imageUrl = item.image[0].location
            } else if (list[position] is TitleResponse.Datum) {
                val item = list[position] as TitleResponse.Datum
                holder.btnName.text = item.title
                holder.btnName.setOnClickListener {
                    if (isClickedEnabled)
                        listener.onSubscritionClicked(item.id, item.title, item.mainPackageId, item.productionId)
                }
                if (item.image != null) {
                    imageUrl = item.image
                }
            }

            if (!imageUrl.isEmpty()) {
                val image = "http://admin.ict4agri.com/" + imageUrl
                Picasso.with(activity).load(image).resize(500, 500).into(holder.imgProduct)

            }
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }


    inner class myViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var btnName: Button
        var imgProduct: ImageView


        init {
            btnName = itemView.findViewById(R.id.btn_name)
            imgProduct = itemView.findViewById(R.id.imgProduct)
        }
    }

    interface onProductionOptionChooseListener {
        fun onProductionOptionChoose(id: Int, title: String)
        fun onProductionTitleChoose(id: Int, title: String)
        fun onSubscritionClicked(id: Int, title: String, mainPackageId: Int, productionId: Int)
    }

}
