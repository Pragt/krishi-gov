package com.ictfa.krishiguru.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.dtos.MarketNameList;

import java.util.List;

/**
 * Created by sureshlama on 6/6/17.
 */

public class MarketNameAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<MarketNameList> MarketNameList;
    private MarketClickListner listner;

    public  MarketNameAdapter(Activity activity, List<MarketNameList> MarketNameList) {
        this.MarketNameList = MarketNameList;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return this.MarketNameList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.MarketNameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView textname;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        View convertedView = convertView;
        ViewHolder viewHolder;
        if (convertedView == null) {
            convertedView = inflater.inflate(R.layout.custome_marketname_list, parent, false);
            viewHolder = new ViewHolder();
            // viewHolder.textViewNumber = (CustomTextView) convertedView.findViewById(R.id.textViewNumberCustomAss);
            viewHolder.textname = (TextView) convertedView.findViewById(R.id.marketName);



            convertedView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertedView.getTag();
        MarketNameList tempDto = MarketNameList.get(position);
        // viewHolder.textViewNumber.setText(Integer.toString(tempDto.getNumber()) + ".");
        viewHolder.textname.setText(tempDto.getMarketName());

        convertedView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onMarketClicked(MarketNameList.get(position).getId(), MarketNameList.get(position).getMarketName());
            }
        });

        return convertedView;
    }

    public void setListner(MarketClickListner listner)
    {
        this.listner = listner;
    }

    public interface MarketClickListner{
        void onMarketClicked(String market_id, String marketName);
    }
}


