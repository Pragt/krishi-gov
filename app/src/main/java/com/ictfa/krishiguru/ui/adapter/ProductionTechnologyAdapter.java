package com.ictfa.krishiguru.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.dtos.ProductionTechnologyDtos;

import java.util.List;

/**
 * Created by sureshlama on 5/18/17.
 */

public class ProductionTechnologyAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<ProductionTechnologyDtos> productionTechnologyDtoses;


    public  ProductionTechnologyAdapter (Activity activity, List<ProductionTechnologyDtos> productionTechnologyDtoses) {
        this.activity = activity;
        this.productionTechnologyDtoses = productionTechnologyDtoses;
    }
    public int getCount() {
        return this.productionTechnologyDtoses.size();
    }

    @Override
    public Object getItem(int position) {
        return this.productionTechnologyDtoses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.custome_product_technology, null);

        TextView productionType = (TextView)convertView.findViewById(R.id.productionType);
        TextView productionName = (TextView)convertView.findViewById(R.id.productionName);
        TextView subtitle = (TextView)convertView.findViewById(R.id.subtitle);
        TextView productDesc = (TextView)convertView.findViewById(R.id.productDesc);
        TextView createDate = (TextView)convertView.findViewById(R.id.createdDate);
        ImageView images = (ImageView)convertView.findViewById(R.id.productImageList);

        ProductionTechnologyDtos productionTechnologyDtos = productionTechnologyDtoses.get(position);
        if(!(productionTechnologyDtos.getProductionType().isEmpty())){
            productionType.setText(productionTechnologyDtos.getProductionType());
        }

        productionName.setText(productionTechnologyDtos.getProductionName());
        subtitle.setText(productionTechnologyDtos.getProductionSubtitle());
        productDesc.setText(productionTechnologyDtos.getProductionDescription());
        createDate.setText(productionTechnologyDtos.getCreatedDate());
        //images.setImageDrawable(pr);
        return convertView;
    }
}
