package com.ictfa.krishiguru.ui.dtos;

/**
 * Created by sureshlama on 6/12/17.
 */

public class CommentOrganizationDtos {

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    String id, name, comment;
}
