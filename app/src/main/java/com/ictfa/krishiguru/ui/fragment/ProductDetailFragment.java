package com.ictfa.krishiguru.ui.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ictfa.krishiguru.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetailFragment extends Fragment {
    private String name;
    private String image;

    public ProductDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        image = getArguments().getString("image");
        name = getArguments().getString("name");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setSubtitle(name);


        return view;
    }

}
