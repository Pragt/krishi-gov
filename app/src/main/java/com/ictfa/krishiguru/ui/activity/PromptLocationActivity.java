package com.ictfa.krishiguru.ui.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.ictfa.krishiguru.R;

public class PromptLocationActivity extends AppCompatActivity {
    private Spinner zone;
    private Spinner district;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promptlocation);
        zone = (Spinner) findViewById(R.id.zone);
        district = (Spinner)findViewById(R.id.district);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,getResources()
                .getStringArray(R.array.zone));

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        zone.setAdapter(adapter);
    }
}
