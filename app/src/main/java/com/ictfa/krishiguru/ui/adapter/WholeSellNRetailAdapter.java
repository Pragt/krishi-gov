package com.ictfa.krishiguru.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.dtos.wholeSellRetailPriceDtos;

import java.util.List;

/**
 * Created by sureshlama on 5/26/17.
 */

public class WholeSellNRetailAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<wholeSellRetailPriceDtos> wholesellretaildatas;

    public  WholeSellNRetailAdapter(Activity activity, List<wholeSellRetailPriceDtos> wholesellretaildatas) {
        this.wholesellretaildatas = wholesellretaildatas;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return this.wholesellretaildatas.size();
    }

    @Override
    public Object getItem(int position) {
        return this.wholesellretaildatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView textsnum, textproduct, textquantity, textretail, textholesellprice;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


          View convertedView = convertView;
         ViewHolder viewHolder;
         if (convertedView == null) {
         convertedView = inflater.inflate(R.layout.wholesell_retail_price_list, parent, false);
         viewHolder = new ViewHolder();
         // viewHolder.textViewNumber = (CustomTextView) convertedView.findViewById(R.id.textViewNumberCustomAss);
         viewHolder.textsnum = (TextView) convertedView.findViewById(R.id.snum);
         viewHolder.textproduct = (TextView)convertedView.findViewById(R.id.product);
         viewHolder.textquantity = (TextView)convertedView.findViewById(R.id.quantity);
         viewHolder.textretail = (TextView)convertedView.findViewById(R.id.retailPrice);
         viewHolder.textholesellprice = (TextView)convertedView.findViewById(R.id.holsellPriceholsellPrice);


         convertedView.setTag(viewHolder);
         } else
         viewHolder = (ViewHolder) convertedView.getTag();
        wholeSellRetailPriceDtos tempDto = wholesellretaildatas.get(position);
         // viewHolder.textViewNumber.setText(Integer.toString(tempDto.getNumber()) + ".");
         viewHolder.textsnum.setText(tempDto.getsNum());
         viewHolder.textproduct.setText(tempDto.getProduct());
         viewHolder.textquantity.setText(tempDto.getQuantity());
         viewHolder.textretail.setText(tempDto.getRetailPrice());
         viewHolder.textholesellprice.setText(tempDto.getWholeSellPrice());

         if (position % 2 == 0) {
         convertedView.setBackgroundResource(R.drawable.list_row_effect_odd);
         } else if (position % 2 == 1) {
         convertedView.setBackgroundResource(R.drawable.list_row_effect_even);
         }
         return convertedView;




    }



}

