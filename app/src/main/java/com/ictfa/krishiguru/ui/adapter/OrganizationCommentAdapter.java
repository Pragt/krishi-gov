package com.ictfa.krishiguru.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.dtos.CommentOrganizationDtos;

import java.util.List;

/**
 * Created by sureshlama on 6/12/17.
 */

public class OrganizationCommentAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<CommentOrganizationDtos> commentDatas;

    public  OrganizationCommentAdapter(Activity activity, List<CommentOrganizationDtos> commentDatas) {
        this.commentDatas = commentDatas;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return this.commentDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return this.commentDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView textName, textComment;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View convertedView = convertView;
        ViewHolder viewHolder;
        if (convertedView == null) {
            convertedView = inflater.inflate(R.layout.organization_comment_list_view, parent, false);
            viewHolder = new ViewHolder();
            // viewHolder.textViewNumber = (CustomTextView) convertedView.findViewById(R.id.textViewNumberCustomAss);
            viewHolder.textName = (TextView) convertedView.findViewById(R.id.commentName);
            viewHolder.textComment = (TextView) convertedView.findViewById(R.id.CommentContent);


            convertedView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertedView.getTag();
        CommentOrganizationDtos tempDto = commentDatas.get(position);
        // viewHolder.textViewNumber.setText(Integer.toString(tempDto.getNumber()) + ".");
        viewHolder.textName.setText(tempDto.getName());
        viewHolder.textComment.setText(tempDto.getComment());


        if (position % 2 == 0) {
            convertedView.setBackgroundResource(R.drawable.list_row_odd_organiation);
        } else if (position % 2 == 1) {
            convertedView.setBackgroundResource(R.drawable.list_row_effect_even_organization);
        }
        return convertedView;


    }
    }

