package com.ictfa.krishiguru.ui.dtos;

/**
 * Created by sureshlama on 5/18/17.
 */

public class ProductionTechnologyDtos  {
    public String getProductionType() {
        return productionType;
    }

    public void setProductionType(String productionType) {
        this.productionType = productionType;
    }

    public String getProductionName() {
        return productionName;
    }

    public void setProductionName(String productionName) {
        this.productionName = productionName;
    }

    public String getProductionDescription() {
        return productionDescription;
    }

    public void setProductionDescription(String productionDescription) {
        this.productionDescription = productionDescription;
    }

    public String getProductionSubtitle() {
        return productionSubtitle;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }



    String productionType, productionName, productionDescription, productionImages, productionSubtitle, createdDate;
}
