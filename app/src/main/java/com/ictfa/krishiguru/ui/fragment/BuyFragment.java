package com.ictfa.krishiguru.ui.fragment;


import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.FileUtils;
import com.ictfa.krishiguru.helpers.ImagePicker;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.login.LoginActivity;
import com.ictfa.krishiguru.task.DataGotListener;
import com.ictfa.krishiguru.task.DataListenerManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;

import static android.app.Activity.RESULT_OK;


public class BuyFragment extends Fragment implements DataGotListener {
    ArrayList<String> categoryList = new ArrayList<>();
    ArrayList<String> unitList = new ArrayList<>();
    ArrayList<String> buySellList = new ArrayList<>();
    public static ArrayList<Unit> unitListObject = new ArrayList<>();
    ArrayList<String> categoryId = new ArrayList<>();
    ArrayAdapter categoryAdapter;
    ArrayAdapter unitAdapter;
    CustomProgressDialog pd;
    Alerts alerts;
    private Spinner categorySpinner, quantitySpinner, spinnerBuySell;
    private EditText spinnerProduct, spinnerProductType;
    private int PICK_IMAGE_REQUEST = 1;
    private ImageView selectImage;
    private TextView startDate;
    private TextView endDate;
    private Button sumbit;
    private SharedPreference mypref;
    private String categoryIdValue;
    private String fromDate, toDate;
    private String minPrice, maxPrice;
    private String quality, unitValue;
    //private
    private String quantityValue;
    private TextView amountUnit;
    private TextView tvTo;
    private EditText minAmount, maxAmount;
    private EditText qualityView, quantity;
    private String KEY_USERID = "user_id";
    private String KEY_CATEGORYID = "category_id";
    private String KEY_PRODUCTNAME = "product_name";
    private String KEY_PRODUCTTYPE = "product_type";
    private String KEY_PRICEMIN = "price_min";
    private String KEY_PRICEMAX = "price_max";
    private String PRICE = "price";
    private String KEY_QUNATITY = "quantity";
    private String KEY_VALID_FRPM = "valid_from";
    private String KEY_VALID_TO = "valid_to";
    private String KEY_UNIT = "unit";
    private String KEY_QUALITY = "quality";
    private String product, productTypeValue;
    private String image;
    private TextView tasbir;
    private int loginStatus;

    // private String productEditText, p

    /***
     *
     *
     *user_id, category_id, product_name, product_type, price_min, price_max,
     quantity, valid_from, valid_to, quality, unit
     */

    public BuyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        DataListenerManager.getInstance().addDataGotListeners(this);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_buy, container, false);
        mypref = new SharedPreference(getActivity());
        alerts = new Alerts(getActivity());
        pd = new CustomProgressDialog(getActivity());
        loginStatus = mypref.getIntValues(CommonDef.SharedPreference.IS_LOGIN);
        getCategory();
        getUnit();
        categorySpinner = (Spinner) view.findViewById(R.id.spinnercategory);
        quantitySpinner = (Spinner) view.findViewById(R.id.spinnerQuantity);
        spinnerProduct = (EditText) view.findViewById(R.id.productcategory);
        spinnerProductType = (EditText) view.findViewById(R.id.producttypecategory);
        spinnerBuySell = (Spinner) view.findViewById(R.id.spinnerBuySell);

        minAmount = (EditText) view.findViewById(R.id.editStartPrice);
        maxAmount = (EditText) view.findViewById(R.id.editEndPrice);
        qualityView = (EditText) view.findViewById(R.id.quality);
        amountUnit = (TextView) view.findViewById(R.id.amount);
        tvTo = (TextView) view.findViewById(R.id.to);

        quantity = (EditText) view.findViewById(R.id.quantityEdit);
        unitList = new ArrayList<>();

        categoryAdapter = new ArrayAdapter(getActivity(), R.layout.custome_spiner_text, categoryList);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        categorySpinner.setAdapter(categoryAdapter);


        unitAdapter = new ArrayAdapter(getActivity(), R.layout.custome_spiner_text, unitList);
        unitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quantitySpinner.setAdapter(unitAdapter);


        startDate = (TextView) view.findViewById(R.id.startdate);
        endDate = (TextView) view.findViewById(R.id.enddate);
        sumbit = (Button) view.findViewById(R.id.sumbitbuy);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoryIdValue = categoryId.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        quantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getItemAtPosition(position).toString();
                amountUnit.setText(selected);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerBuySell.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    maxAmount.setVisibility(View.VISIBLE);
                    tvTo.setVisibility(View.VISIBLE);
                } else {
                    maxAmount.setVisibility(View.INVISIBLE);
                    tvTo.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validform()) {
                    if (spinnerBuySell.getSelectedItemPosition() == 0)
                        sendBuy();
                    else
                        sendsell();
                }
            }
        });

        unitListObject = new ArrayList<>();
        Unit unit = new Unit();
        unit.id = "1";
        unit.name = "मुठा";
        unitListObject.add(unit);

        Unit unit1 = new Unit();
        unit1.id = "2";
        unit1.name = "के.जी";
        unitListObject.add(unit1);

        Unit unit2 = new Unit();
        unit2.id = "3";
        unit2.name = "मन";
        unitListObject.add(unit2);

        buySellList = new ArrayList<>();
        buySellList.add("खरीद");
        buySellList.add("बिक्री");


        ArrayAdapter buySellAdapter = new ArrayAdapter(getActivity(), R.layout.custome_spiner_text, buySellList);
        buySellAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBuySell.setAdapter(buySellAdapter);


        tasbir = (TextView) view.findViewById(R.id.tasbierSelect);
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                int hour = mcurrentDate.get(Calendar.HOUR_OF_DAY);
                int minutes = mcurrentDate.get(Calendar.MINUTE);
                int second = mcurrentDate.get(Calendar.SECOND);
                String currentDate = mYear + "-" + mMonth + "-" + mDay + " " + hour + ":" + minutes + ":" + second;

                DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        startDate.setText(selectedyear + "-" + (selectedmonth + 1) + "-" + selectedday);
                        //month starts from 0 for janaury so +1 while displaying
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, 6);
                DatePicker dp = mDatePicker.getDatePicker();
                try {
                    dp.setMinDate(Calendar.getInstance().getTimeInMillis());//get the current day
                } catch (Exception exx) {
                }

                mDatePicker.show();
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                int hour = mcurrentDate.get(Calendar.HOUR_OF_DAY);
                int minutes = mcurrentDate.get(Calendar.MINUTE);
                int second = mcurrentDate.get(Calendar.SECOND);
                String currentDate = mYear + "-" + mMonth + "-" + mDay + " " + hour + ":" + minutes + ":" + second;

                DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        endDate.setText(selectedyear + "-" + (selectedmonth + 1) + "-" + selectedday);
                        //month starts from 0 for janaury so +1 while displaying
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, 6);
                DatePicker dp = mDatePicker.getDatePicker();
                try {
                    dp.setMinDate(Calendar.getInstance().getTimeInMillis());//get the current day
                } catch (Exception exx) {
                }
                mDatePicker.show();

            }
        });

        selectImage = (ImageView) view.findViewById(R.id.camerapic);

        selectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });


        return view;
    }

    private boolean validform() {
        product = spinnerProduct.getText().toString().trim();
        productTypeValue = spinnerProductType.getText().toString().trim();
        fromDate = startDate.getText().toString().trim();
        toDate = endDate.getText().toString().trim();
        minPrice = minAmount.getText().toString().trim();
        maxPrice = maxAmount.getText().toString().trim();
        quality = qualityView.getText().toString().trim();
        unitValue = amountUnit.getText().toString().trim();
        quantityValue = quantity.getText().toString().trim();

        boolean isvalid = true;
        if ((mypref.getIntValues(CommonDef.SharedPreference.USER_ID) == 0)) {

            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
            return false;
        }
        if (categoryIdValue.equals("nothing")) {
            Toast.makeText(getActivity(), "please select product category", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (product.isEmpty()) {
            Toast.makeText(getActivity(), "please enter product", Toast.LENGTH_SHORT).show();
            return false;
        }

//        if(productTypeValue.isEmpty()){
//            Toast.makeText(getActivity(),"please enter product type", Toast.LENGTH_SHORT).show();
//            return false;
//        }
        if (fromDate.isEmpty()) {
            Toast.makeText(getActivity(), "please enter start date", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (toDate.isEmpty()) {
            Toast.makeText(getActivity(), "please enter end date", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (minPrice.isEmpty()) {
            Toast.makeText(getActivity(), "please select minimun price", Toast.LENGTH_SHORT).show();
            return false;

        }

        if (maxPrice.isEmpty() && spinnerBuySell.getSelectedItemPosition() == 0) {
            Toast.makeText(getActivity(), "please select max price", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (quality.isEmpty()) {
            Toast.makeText(getActivity(), "please enter quality", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (quantityValue.isEmpty()) {
            Toast.makeText(getActivity(), "please select quantity", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(image)) {
            Toast.makeText(getActivity(), "please provide image", Toast.LENGTH_SHORT).show();
            return false;
        }
        return isvalid;
    }

    private void getCategory() {
        categoryList.clear();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //  String url ="http://www.google.com";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BaseApplication.productCategory,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //videoDatas.clear();
                        // Log.d("YOUTUBEURL", youtubeUrl);
                        categoryList.add(0, "बर्ग छनुहोस् ");
                        categoryId.add(0, "nothing");
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            Log.d("UTFSTR", utfStr);
                            JSONObject categoryData = new JSONObject(utfStr);
                            Log.d("DATATODAY", categoryData.toString());
                            JSONArray categoryArray = categoryData.getJSONArray("data");
                            Log.d("categoryArray", categoryArray.toString());
                            for (int i = 0; i < categoryArray.length(); i++) {
                                JSONObject object = categoryArray.getJSONObject(i);
                                String name = object.getString("name");
                                String id = object.getString("id");
                                //      Toast.makeText(getActivity(),name,Toast.LENGTH_SHORT).show();
                                categoryList.add(i + 1, name);
                                categoryId.add(i + 1, id);
                                //     categoryArrays[i]=name;

                            }
                            categoryAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void getUnit() {
        unitList.clear();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //  String url ="http://www.google.com";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BaseApplication.productUnit,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //videoDatas.clear();
                        // Log.d("YOUTUBEURL", youtubeUrl);

                        // categoryList.add(0,"बर्ग छनुहोस् ");
                        //categoryArrays[0]= "बर्ग छनुहोस्";
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            Log.d("UTFSTR", utfStr);
                            JSONObject categoryData = new JSONObject(utfStr);
                            Log.d("DATATODAY", categoryData.toString());
                            JSONArray categoryArray = categoryData.getJSONArray("data");
                            Log.d("categoryArray", categoryArray.toString());
                            unitListObject = new ArrayList<>();
                            Unit unit;
                            for (int i = 0; i < categoryArray.length(); i++) {
                                JSONObject object = categoryArray.getJSONObject(i);
                                String name = object.getString("name");
                                unit = new Unit();
                                unit.id = object.getString("id");
                                unit.name = object.getString("name");
                                unitListObject.add(unit);
                                //      Toast.makeText(getActivity(),name,Toast.LENGTH_SHORT).show();
                                unitList.add(i, name);
                                //     categoryArrays[i]=name;

                            }
                            unitAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CommonDef.REQUEST_STORAGE_CAMERA);
                return;
            }
        }
        startActivityForResult(ImagePicker.getPickImageChooserIntent(getActivity()), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            Uri imageUri;
            if (data != null && data.getData() != null) {
                imageUri = data.getData();
                selectImage.setImageURI(imageUri);
                Bitmap imageBmp;
                try {
                    imageBmp = FileUtils.getThumbnail(getActivity(), imageUri, 1080);
                    image = FileUtils.storeBitmapToFile(imageBmp, getActivity());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else // From camera
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    image = ImagePicker.getImage.getAbsolutePath();
                } else
                    image = ImagePicker.outputFileUri.getPath();

                selectImage.setImageURI(Uri.fromFile(new File(image)));
            }

        }
    }

    @Override
    public void onDataGot(int position, String date) {
        switch (position) {
            case 0:
                startDate.setText(date);
                break;
            case 1:
                endDate.setText(date);
                break;

            default:
                break;

        }
    }


    void sendBuy() {
        pd.showpd(getActivity().getResources().getString(R.string.please_wait));
        RequestParams params = new RequestParams();
        params.put(KEY_USERID, mypref.getIntValues(CommonDef.SharedPreference.USER_ID));
        params.put(KEY_CATEGORYID, categoryIdValue);
        params.put(KEY_PRODUCTNAME, product);
        params.put(KEY_PRODUCTTYPE, productTypeValue);
        params.put(KEY_VALID_FRPM, fromDate);
        params.put(KEY_VALID_TO, toDate);
        params.put(KEY_PRICEMIN, minPrice);
        params.put(KEY_PRICEMAX, maxPrice);
        params.put(KEY_QUNATITY, quantityValue);
        params.put(KEY_UNIT, unitValue);
        params.put(KEY_QUALITY, quality);

        if (image != null && !image.equalsIgnoreCase("")) {
            final File myFile = new File(image);
            try {
                params.put("image", myFile, "image/jpeg");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        Log.d("quantity", String.valueOf(quantity));
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(BaseApplication.productBuy, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                Log.d("responseBuy", responseBody.toString());
                Toast.makeText(getActivity(), responseBody.toString(), Toast.LENGTH_SHORT).show();
                pd.hidepd();
                alerts.showSuccessAlert(responseBody.toString());
            }

        });

    }

    void sendsell() {
        pd.showpd(getActivity().getResources().getString(R.string.please_wait));
        RequestParams params = new RequestParams();
        // RequestParams params = new RequestParams();
        if (image != null && !image.equalsIgnoreCase("")) {
            final File myFile = new File(image);
            try {
                params.put("image", myFile, "image/jpeg");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        params.put(KEY_USERID, mypref.getIntValues(CommonDef.SharedPreference.USER_ID));
        params.put(KEY_CATEGORYID, categoryIdValue);
        params.put(KEY_PRODUCTNAME, product);
        params.put(KEY_PRODUCTTYPE, productTypeValue);
        params.put(KEY_VALID_FRPM, fromDate);
        params.put(KEY_VALID_TO, toDate);
        params.put(PRICE, minPrice);
        params.put(KEY_QUNATITY, quantityValue);
        params.put(KEY_UNIT, unitValue);
        params.put(KEY_QUALITY, quality);
        Log.d("price", minPrice);

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(BaseApplication.productSell, params, new JsonHttpResponseHandler() {
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                Log.d("responsesell", responseBody.toString());
                pd.hidepd();
                alerts.showSuccessAlert(responseBody.toString());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            startActivityForResult(ImagePicker.getPickImageChooserIntent(getActivity()), CommonDef.PICK_IMAGE_REQUEST);
        }
    }
}
