package com.ictfa.krishiguru.ui.dtos;

/**
 * Created by sureshlama on 5/21/17.
 */

public class AccorDetailDtos {
    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    String subtitle, description;
}
