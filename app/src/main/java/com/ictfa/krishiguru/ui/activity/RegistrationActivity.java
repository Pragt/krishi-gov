package com.ictfa.krishiguru.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.dashboard.DashBoardActivity;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.FileUtils;
import com.ictfa.krishiguru.helpers.GPSTracker;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.library.task.Permissions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class RegistrationActivity extends AppCompatActivity implements LocationListener {
    CustomProgressDialog pd;
    SharedPreference sharedPreferences;
    private EditText fullname, username, password, confirmPassword;
    private ImageView SelectProfilePic;
    private Button register;
    private CircleImageView profileImage;
    private int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap;
    private String REGESTRAION_URL = "http://admin.ict4agri.com/api/v3/user/register?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    private String KEY_NAME = "name";
    private String KEY_LAT = "lat";
    private String KEY_LON = "lng";
    private String KEY_USERNAME = "username";
    private String KEY_PASSWORD = "password";
    private String KEY_FCM = "fcm_token";
    private String KEY_DEVICE_ID = "device_code";
    private Uri filePath;
    private String image;
    private String deviceId;
    public String lat = "27.7172";
    public String lon = "85.3240";
    private String nameValue, usernameValue, passwordValue;
    private String firebaseId;
    private GPSTracker gps;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        CommonMethods.setupUI(findViewById(R.id.ll_register), this);
        getSupportActionBar().setTitle(getResources().getString(R.string.register));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Permissions permissions = new Permissions();
        permissions.verifyMapPermissions(this);
        permissions.verifyExternalStorage(this);
        pd = new CustomProgressDialog(this);
        sharedPreferences = new SharedPreference(this);


//       TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegistrationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
//            Toast.makeText(this,"You need have granted permission",Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(RegistrationActivity.this);

            // Check if GPS enabled
            if (!gps.canGetLocation()) {
                gps.showSettingsAlert();
            }
        }

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        } catch (Exception exx) {
        }
        // Toast.makeText(RegistrationActivity.this,"lat: "+lat+" lon:"+lon+"",Toast.LENGTH_LONG).show();


        fullname = (EditText) findViewById(R.id.fullname);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirmPass);
        SelectProfilePic = (ImageView) findViewById(R.id.proPic);
        profileImage = (CircleImageView) findViewById(R.id.profile_image);
        register = (Button) findViewById(R.id.buttonRegister);


        SelectProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validForm()) {
                    nameValue = fullname.getText().toString().trim();
                    usernameValue = username.getText().toString().trim();
                    passwordValue = password.getText().toString().trim();
//                    image = "null";
                    //   final String instanceId =
                    firebaseId = FirebaseInstanceId.getInstance().getToken();
//                    if (firebaseId == null || firebaseId.isEmpty()) {
//                        Toast.makeText(RegistrationActivity.this, "Error Occured", Toast.LENGTH_SHORT).show();
//                    } else {
                    registerDAta();
//                    }
                }
            }
        });


    }

    private boolean validForm() {
        usernameValue = username.getText().toString().trim();
        if (fullname.getText().toString().isEmpty()) {
            Toast.makeText(RegistrationActivity.this, "पुरा नाम लेखनुहोस्", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (usernameValue.isEmpty()) {
            Toast.makeText(RegistrationActivity.this, "आफ्नु मोबाईल नम्बर लेखनुहोस् ", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.getText().toString().isEmpty()) {
            Toast.makeText(RegistrationActivity.this, "पासवर्ड छानुस्", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (confirmPassword.getText().toString().isEmpty()) {
            Toast.makeText(RegistrationActivity.this, "पासवर्ड यकिन गर्नुहोस", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!(password.getText().toString().equals(confirmPassword.getText().toString()))) {
            Toast.makeText(RegistrationActivity.this, "पासवर्ड मिलेन", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!(usernameValue.length() == 10) || !(usernameValue.substring(0, 1).matches("9"))) {
            Toast.makeText(RegistrationActivity.this, "मोबाईल नम्बर मिलेन", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            //  Toast.makeText(RegistrationActivity.this,filePath.toString(),Toast.LENGTH_SHORT).show();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                SelectProfilePic.setVisibility(View.GONE);
                profileImage.setVisibility(View.VISIBLE);
                profileImage.setImageBitmap(bitmap);
                image = getPath(filePath);
                if (!image.equalsIgnoreCase(""))
                    image = FileUtils.resizeAndCompressImageBeforeSend(this, image);

                Log.d("ImageView", image);

                //   Toast.makeText(RegistrationActivity.this,image,Toast.LENGTH_SHORT).show();
                //   username.setText(getStringImage(bitmap));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    void registerDAta() {
        pd.showpd(getResources().getString(R.string.please_wait));
        RequestParams params = new RequestParams();

        if (image != null && !image.equalsIgnoreCase("")) {
            File myFile = new File(image);

            try {
                params.put("image", myFile, "image/jpeg");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
//        }


        params.put(KEY_NAME, nameValue);
        params.put(KEY_LAT, lat);
        params.put(KEY_LON, lon);
        params.put(KEY_USERNAME, usernameValue);
        params.put(KEY_PASSWORD, passwordValue);
        params.put(KEY_DEVICE_ID, deviceId);
        params.put(KEY_FCM, firebaseId);

//        Log.d("FIREBASEID", firebaseId);
        //params.put();

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(REGESTRAION_URL, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                pd.hidepd();
                try {
                    Log.d("responseRegestration", responseBody.toString());
                    String message = responseBody.getString("message");
                    //Toast.makeText(RegistrationActivity.this, message,Toast.LENGTH_SHORT).show();

                    if (message.equals("User was successfully added")) {
                        JSONArray data = responseBody.getJSONArray("data");
                        JSONObject datavalue = data.getJSONObject(0);

                        int id = Integer.parseInt(datavalue.getString("id"));
                        String priorities = datavalue.getString("priorities");
                        String userType = datavalue.getString("user_type");
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.IS_LOGIN, 1);
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.USER_ID, id);
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.LATUTUDE, lat);
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.LONGITUDE, lon);
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.USERNAME, datavalue.getString("name"));
                        sharedPreferences.setKeyValues("priorities", priorities);
                        sharedPreferences.setKeyValues(CommonDef.SharedPreference.USERTYPE, userType);
                        Toast.makeText(RegistrationActivity.this, "स्वागतम  तपाईंको अकाउन्ट दरत भको छ ।", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(RegistrationActivity.this, DashBoardActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (message.equals("User already exist")) {
                        Toast.makeText(RegistrationActivity.this, "यो युजरनाम पहेला नै प्रयोग भएसक्यो। अर्को युजरनाम चानुहोस्", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    pd.hidepd();
                }

            }


        });
    }


    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();

            cursor = getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();

            return path;
        } else
            return "";
    }

    @Override
    protected void onResume() {
        super.onResume();


    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                    gps = new GPSTracker(RegistrationActivity.this);

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        lat = String.valueOf(gps.getLatitude());
                        lon = String.valueOf(gps.getLongitude());

                        // \n is for new line
//                        Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + lat + "\nLong: " + lon, Toast.LENGTH_LONG).show();
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(RegistrationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

                    }

                    Toast.makeText(this, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
//        Toast.makeText(this, "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude(), Toast.LENGTH_SHORT).show();
        lat = String.valueOf(location.getLatitude());
        lon = String.valueOf(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            locationManager.removeUpdates(this);
        } catch (Exception exx) {
        }


    }
}