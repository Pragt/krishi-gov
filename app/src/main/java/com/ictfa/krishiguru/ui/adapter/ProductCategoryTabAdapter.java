package com.ictfa.krishiguru.ui.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ictfa.krishiguru.byawasaikUtpadanPrabidhi.CommonObject;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.ui.fragment.CategoryGridFragment;

import java.util.ArrayList;

/**
 * Created by sureshlama on 5/19/17.
 */

public class ProductCategoryTabAdapter extends FragmentPagerAdapter {
    int PAGE_COUNT;
    private ArrayList<CommonObject> tabTitles;

    public ProductCategoryTabAdapter(FragmentManager fm, Context context, ArrayList<CommonObject> tabTitles) {
        super(fm);
        PAGE_COUNT = tabTitles.size();
//        Log.d("PAGECOUNT", tabTitles.length+"");
        this.tabTitles = tabTitles;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        CategoryGridFragment categoryGridFragment = new CategoryGridFragment();
        Bundle b = new Bundle();
        b.putString(CommonDef.PRODUCT_ID, tabTitles.get(position).getId());
        b.putString(CommonDef.NAME, tabTitles.get(position).getName());
        categoryGridFragment.setArguments(b);
        return categoryGridFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles.get(position).getName();
    }


}
