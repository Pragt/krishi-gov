package com.ictfa.krishiguru.ui.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ictfa.krishiguru.Notifications.NotificationsFragment;
import com.ictfa.krishiguru.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

/**
 * Created by 3421 on 9/6/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    Random random = new Random();
    int NOTIFICATION_ID = random.nextInt(9999 - 1000) + 1000;

    int ref_id = 0;
    int type = 0;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
       String body = remoteMessage.getNotification().getTitle();
        String title = remoteMessage.getNotification().getBody();

//        sendNotification(body,title);
        if (remoteMessage.getData() != null){
            try {
                ref_id = Integer.parseInt(remoteMessage.getData().get("ref_id"));
                type = Integer.parseInt(remoteMessage.getData().get("type"));
            }catch (Exception exx){

            }
        }

        if (remoteMessage.getNotification() != null && remoteMessage.getNotification().getBody() !=null) {
            sendNotification(remoteMessage.getNotification().getBody(),remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getIcon());
        }else
            sendNotification("You have a new notification", "कृषि गुरू", "");
    }

    private void sendNotification(String body, String title, String icon) {
        Intent intent = new Intent(this, NotificationsFragment.class);
        intent.putExtra("ref_id", ref_id);
        intent.putExtra("type", type);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.splash_icon)
                .setLargeIcon(!icon.equalsIgnoreCase("") ? getBitmapFromURL(icon) : BitmapFactory.decodeResource(getResources(),
                        R.drawable.splash_icon))
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(getBitmapFromURL(icon)))
                .setContentTitle(title)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                 .setContentText(body)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection)
                    url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}