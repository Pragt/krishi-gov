package com.ictfa.krishiguru.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.ui.adapter.WholeSellNRetailAdapter;
import com.ictfa.krishiguru.ui.dtos.wholeSellRetailPriceDtos;
import com.ictfa.krishiguru.ui.fragment.MarketNameDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class WholesellRetailerPriceActivity extends AppCompatActivity implements MarketNameDialogFragment.MarketFilterListner {

    private List<wholeSellRetailPriceDtos> wholeSellRetailPriceDtoses = new ArrayList<>();
    private WholeSellNRetailAdapter wholeSellNRetailAdapter;
    private ListView retalHolsellListl;
    private ProgressDialog progressDialog;
    private ImageView refreshIcon, balanceIcon;
    TextView udapteValue;
    TextView tvMarketName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wholesell_retailer_price);
        retalHolsellListl = (ListView) findViewById(R.id.wholesellRetailList);
        progressDialog = new ProgressDialog(this);
        tvMarketName = (TextView) findViewById(R.id.tv_market_name);
        udapteValue = (TextView) findViewById(R.id.udapteValue);
        wholeSellNRetailAdapter = new WholeSellNRetailAdapter(WholesellRetailerPriceActivity.this, wholeSellRetailPriceDtoses);
        retalHolsellListl.setAdapter(wholeSellNRetailAdapter);
//        Marketname = "Kalimati";

        getMarketPrice("2");


    }

    private void getMarketPrice(String market_id) {
        //
        wholeSellRetailPriceDtoses.clear();
        RequestQueue queue = Volley.newRequestQueue(WholesellRetailerPriceActivity.this);
        progressDialog.setMessage(" लोड हुँदै... ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        String url = BaseApplication.wholeSellPrice;

        if (!market_id.equalsIgnoreCase(""))
            url = url + "&market_id=" + market_id;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            String last_updated_date = jsonObject.getString("last_update");
                            udapteValue.setText(last_updated_date);
                            int count = 0;
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject value = jsonArray.getJSONObject(i);
                                wholeSellRetailPriceDtos wholeSellRetailPriceDtoes = new wholeSellRetailPriceDtos();
                                String markentname = value.getString("market_name");

//                                    if(markentname.equals(Marketname)){

                                count = count + 1;
                                String id = value.getString("id");
                                String productName = value.getString("commodity_name_nepali");
                                String quantity = value.getString("factor");
                                double wholeSellPriceMax = value.getDouble("max_price");
                                double woleSellPirceMin = value.getDouble("min_price");
                                double avgWholeSell = (wholeSellPriceMax + woleSellPirceMin) / 2;
                                double retailPRiceMax = value.getDouble("rmax_price");
                                double retailPRiceMin = value.getDouble("rmin_price");
                                double retailPRiceAvg = (retailPRiceMax + retailPRiceMin) / 2;
                                String unit = value.getString("unit_code");
                                String snum = Integer.toString(count);
                                wholeSellRetailPriceDtoes.setId(id);
                                wholeSellRetailPriceDtoes.setProduct(productName);
                                wholeSellRetailPriceDtoes.setQuantity(quantity + " " + unit);
                                wholeSellRetailPriceDtoes.setRetailPrice(retailPRiceAvg + "");
                                wholeSellRetailPriceDtoes.setWholeSellPrice(avgWholeSell + "");
                                wholeSellRetailPriceDtoes.setsNum(snum);
                                wholeSellRetailPriceDtoses.add(wholeSellRetailPriceDtoes);
                            }

//                                }

                            wholeSellNRetailAdapter.notifyDataSetChanged();
                            Log.d("JSONRESPONSE=>>>>>>>>", jsonArray.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                // mTextView.setText("That didn't work!");

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the actiun bar if it is present.
        getMenuInflater().inflate(R.menu.whole_retail_menu, menu);

        getSupportActionBar().setTitle("खुद्र होल्सेल मुल्य");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            case R.id.market:
                FragmentManager fragmentManager = getSupportFragmentManager();
                MarketNameDialogFragment dialogChooseDistrict = new MarketNameDialogFragment();
                dialogChooseDistrict.setListner(WholesellRetailerPriceActivity.this);
                dialogChooseDistrict.show(fragmentManager, "alert");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void doFilter(String market_id, String marketname) {
        getMarketPrice(market_id);
        tvMarketName.setText(marketname);
    }
}
