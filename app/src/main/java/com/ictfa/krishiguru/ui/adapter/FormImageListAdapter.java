package com.ictfa.krishiguru.ui.adapter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ictfa.krishiguru.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sureshlama on 3/14/17.
 */

public class FormImageListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<FormImageList> formImageLists = new ArrayList<>();


    public  FormImageListAdapter(Activity activity,@NonNull List<FormImageList> formImageLists) {
        this.activity = activity;
        this.formImageLists = formImageLists;
    }


    @Override
    public int getCount() {
        return this.formImageLists.size();
    }

    @Override
    public Object getItem(int location) {
        return this.formImageLists.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.horizontal_list_image_form, null);

        ImageView icon=(ImageView) convertView.findViewById(R.id.imageFromForm);

        FormImageList view = (FormImageList) getItem(position);
        Picasso.with(activity).load(view.getFormImages()).into(icon);
        return convertView;
    }
}

