package com.ictfa.krishiguru.ui.dtos;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sureshlama on 5/22/17.
 */

public class ProductTypeListDtos extends RealmObject {
    private String videoUrl;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @PrimaryKey
    String id;
    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParentTitle() {
        return title;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(String actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getViewdCounter() {
        return viewdCounter;
    }

    public void setViewdCounter(String viewdCounter) {
        this.viewdCounter = viewdCounter;
    }

    String img, title, discountPrice, actualPrice, viewdCounter, parentTitle;

    public void setVideoUrl(String video_url) {
        this.videoUrl = video_url;
    }

    public String getVideoUrl() {
        return videoUrl;
    }
}
