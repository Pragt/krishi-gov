package com.ictfa.krishiguru.ui.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ictfa.krishiguru.MultipleTraders.MultipleTraderFragment;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.ui.fragment.BuyFragment;

/**
 * Created by sureshlama on 5/7/17.
 */

public class MarketTabAdapter extends FragmentPagerAdapter {
    private String tabTitles[] = new String[]{"खरीद/बिक्री दर्ता ", "Traders Multiple Entry"};
    private SharedPreference sharedPreference;

    public MarketTabAdapter(FragmentManager fm, Context context) {
        super(fm);
        sharedPreference = new SharedPreference(context);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                BuyFragment buyFragment1 = new BuyFragment();
                //  buyFragment1.setB
                return new BuyFragment();

            case 1:
                return new MultipleTraderFragment();

            default:
                return null;
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }


}
