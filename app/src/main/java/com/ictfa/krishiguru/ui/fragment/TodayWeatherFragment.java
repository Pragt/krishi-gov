package com.ictfa.krishiguru.ui.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodayWeatherFragment extends Fragment {
    TextView min, max, daydesc, dayRainChance, dayRainDuration, dayRainAmount, dayThunderStorm, nightDesc,
            nightRainChance,nightRainDuration, nightRainAmount, nightThunderStorm;
    ImageView dayIcon, nightIcon;

    //WeatherActivity.TodayWheater weatherActivity;

    public TodayWeatherFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View view = inflater.inflate(R.layout.fragment_today_weather, container, false);
       // ProductDataListenerManager.getInstance().addDataGotTodayWeather(this);
        min = (TextView)view.findViewById(R.id.todayminTemp);
        max = (TextView)view.findViewById(R.id.todaymaxTemp);
        daydesc = (TextView)view.findViewById(R.id.todaydesc);
        dayRainChance = (TextView)view.findViewById(R.id.todayRainChance);
        dayRainDuration = (TextView)view.findViewById(R.id.todayRainDuration);
        dayRainAmount = (TextView)view.findViewById(R.id.todayRainAmount);
        dayThunderStorm = (TextView)view.findViewById(R.id.todayThunder);
        nightDesc = (TextView)view.findViewById(R.id.todayNightdesc);
        nightRainChance = (TextView)view.findViewById(R.id.todayNightRainChance);
        nightRainDuration = (TextView)view.findViewById(R.id.todayNightRainDuration);
        nightRainAmount = (TextView)view.findViewById(R.id.todayNightRainAmount);
        nightThunderStorm = (TextView)view.findViewById(R.id.todayNightThunder);

        dayIcon = (ImageView)view.findViewById(R.id.todayImg);
        nightIcon = (ImageView)view.findViewById(R.id.todayNightImg);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
      //  ProductDataListenerManager.getInstance().addDataGotTodayWeather(this);
    }


}
