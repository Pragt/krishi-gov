package com.ictfa.krishiguru.ui.dtos;

/**
 * Created by sureshlama on 6/6/17.
 */

public class MarketNameList {
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    String marketName;

}
