package com.ictfa.krishiguru.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.ui.activity.ProductDetailActivity;
import com.ictfa.krishiguru.ui.adapter.GridProductAdapter;
import com.ictfa.krishiguru.ui.dtos.GridProductDtos;
import com.ictfa.krishiguru.ui.widget.ExpandableHeightGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryGridFragment extends Fragment {

    public String productionId;
    public String productionName;
    SharedPreference sharedPreference;
    ProgressBar pbLoading;
    private ExpandableHeightGridView gridView;
    private List<GridProductDtos> gridProductDtosList = new ArrayList<>();
    private GridProductAdapter gridProductAdapter;
    private Realm realm;

    public CategoryGridFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category_grid, container, false);
        gridView = (ExpandableHeightGridView) view.findViewById(R.id.gridview);
        pbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);
        productionId = getArguments().getString(CommonDef.PRODUCT_ID);
        productionName = getArguments().getString(CommonDef.NAME);
        this.realm = RealmController.with(this).getRealm();

        sharedPreference = new SharedPreference(getActivity());
        if (!sharedPreference.getBoolValues(productionName)) {
            pbLoading.setVisibility(View.VISIBLE);
        } else {
            gridProductDtosList = new ArrayList<>();
            RealmResults<GridProductDtos> realmResults = realm.where(GridProductDtos.class).equalTo("title", productionName).findAll();
            for (GridProductDtos a : realmResults)
                gridProductDtosList.add(a);
            populateList();
        }
        getAllProducts();


//        getAllProducts();
        return view;
    }


    private void getAllProducts() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = UrlHelper.BASE_URL + "api/v3/production/list1?apikey=" + UrlHelper.API_KEY + "&production_id=" + productionId;


        Log.d("FORMURL", url);
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pbLoading.setVisibility(View.GONE);
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            Log.d("utfstr", response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray cateogiresarray = jsonObject.getJSONArray("data");
                            onDataGot(cateogiresarray);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pbLoading.setVisibility(View.GONE);
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void onDataGot(JSONArray categoryArray) {
        gridProductDtosList.clear();
        for (int i = 0; i < categoryArray.length(); i++) {
            try {
                final JSONObject jsonValue = categoryArray.getJSONObject(i);
                GridProductDtos gridProductDtos = new GridProductDtos();
                String name = jsonValue.getString("production_name");
                JSONArray images = jsonValue.getJSONArray("images");
                JSONObject imageobj = images.getJSONObject(0);
                int comment_count = jsonValue.getInt("comment_count");
                int like_count = jsonValue.getInt("like_count");
                int share_count = jsonValue.getInt("share_count");
                String isLiked = jsonValue.getString("is_liked");

                JSONArray subtitleArray = jsonValue.getJSONArray("subtitles");


                String image = imageobj.getString("name");
                gridProductDtos.setName(name);
                String id = jsonValue.getString("id");
                String imagelocation = "http://admin.ict4agri.com/assets/images/production/" + image;

                gridProductDtos.setImage(imagelocation);
                gridProductDtos.setId(id);
                gridProductDtos.setDetail(subtitleArray.toString());
                gridProductDtos.setCommentCounts(comment_count);
                gridProductDtos.setLike_count(like_count);
                gridProductDtos.setShare_count(share_count);
                gridProductDtos.setIsLiked(isLiked);
                gridProductDtos.setTitle(productionName);
                gridProductDtosList.add(gridProductDtos);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for (GridProductDtos b : gridProductDtosList) {
            // Persist your data easily
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(b);
            realm.commitTransaction();
        }
        sharedPreference.setKeyValues(productionName, true);
        populateList();
    }

    private void populateList() {
//        gridProductAdapter.notifyDataSetChanged();
        if (getActivity() != null) {
            gridProductAdapter = new GridProductAdapter(getActivity(), gridProductDtosList);
            gridView.setAdapter(gridProductAdapter);
        }

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GridProductDtos gridProductDtos = (GridProductDtos) parent.getItemAtPosition(position);

                String img = gridProductDtos.getImage();
                String name = gridProductDtos.getName();
                String idValue = gridProductDtos.getId();
                String detail = gridProductDtos.getDetail();
                // Toast.makeText(getActivity(),videoId,Toast.LENGTH_LONG).show();
                Fragment productDetailFragment = new ProductDetailFragment();

                final Bundle bundle = new Bundle();
                bundle.putString("image", img);
                bundle.putString("name", name);
                productDetailFragment.setArguments(bundle);
                Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
                intent.putExtra("image", img);
                intent.putExtra("name", name);
                intent.putExtra("id", idValue);
                intent.putExtra("detail", detail);
                intent.putExtra(CommonDef.COMMENT_COUNT, gridProductDtos.getCommentCounts());
                intent.putExtra(CommonDef.LIKE_COUNT, gridProductDtos.getLike_count());
                intent.putExtra(CommonDef.SHARE_COUNT, gridProductDtos.getShare_count());
                intent.putExtra(CommonDef.IS_LIKED, gridProductDtos.getIsLiked());
                startActivity(intent);

            }
        });
//        gridView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                GridProductDtos gridProductDtos = (GridProductDtos) parent.getItemAtPosition(position);
//
//                String img = gridProductDtos.getImage();
//                String name = gridProductDtos.getName();
//                String idValue = gridProductDtos.getId();
//
//                // Toast.makeText(getActivity(),videoId,Toast.LENGTH_LONG).show();
//                Fragment productDetailFragment = new ProductDetailFragment();
//
//                final Bundle bundle = new Bundle();
//                bundle.putString("image", img);
//                bundle.putString("name", name);
//                productDetailFragment.setArguments(bundle);
//
//                Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
//                intent.putExtra("image", img);
//                intent.putExtra("name", name);
//                intent.putExtra("id", idValue);
//
//                startActivity(intent);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
    }
}
