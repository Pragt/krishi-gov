package com.ictfa.krishiguru.ui.fragment;



import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class TomorrowWeatherFragment extends Fragment {
    TextView min, max, daydesc, dayRainChance, dayRainDuration, dayRainAmount, dayThunderStorm, nightDesc,
            nightRainChance,nightRainDuration, nightRainAmount, nightThunderStorm;
    ImageView dayIcon, nightIcon;

    public TomorrowWeatherFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       // ProductDataListenerManager.getInstance().addDataGotTomorrowWeather(this);
        View view = inflater.inflate(R.layout.fragment_tomorrow_weather, container, false);
        min = (TextView)view.findViewById(R.id.tomorrowminTemp);
        max = (TextView)view.findViewById(R.id.tomorrowmaxTemp);
        daydesc = (TextView)view.findViewById(R.id.tomorrowdesc);
        dayRainChance = (TextView)view.findViewById(R.id.tomorrowRainChance);
        dayRainDuration = (TextView)view.findViewById(R.id.tomorrowRainDuration);
        dayRainAmount = (TextView)view.findViewById(R.id.tomorrowRainAmount);
        dayThunderStorm = (TextView)view.findViewById(R.id.tomorrowThunder);
        nightDesc = (TextView)view.findViewById(R.id.tomorrowNightdesc);
        nightRainChance = (TextView)view.findViewById(R.id.tomorrowNightRainChance);
        nightRainDuration = (TextView)view.findViewById(R.id.tomorrowNightRainDuration);
        nightRainAmount = (TextView)view.findViewById(R.id.tomorrowNightRainAmount);
        nightThunderStorm = (TextView)view.findViewById(R.id.tomorrowNightThunder);

        dayIcon = (ImageView)view.findViewById(R.id.tomorrowImg);
        nightIcon = (ImageView)view.findViewById(R.id.tomorrowNightImg);


        return view;
    }

}
