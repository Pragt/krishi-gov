package com.ictfa.krishiguru.ui.adapter;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sureshlama on 2/24/17.
 */

public class ForumData extends RealmObject implements Serializable, Cloneable {
    private String userImage;

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    @PrimaryKey
    private String questionID;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private String userId;

    public String getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(String isLiked) {
        this.isLiked = isLiked;
    }

    private String isLiked;

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    private String likeCount;

    public int getNumsOfImage() {
        return numsOfImage;
    }

    public void setNumsOfImage(int numsOfImage) {
        this.numsOfImage = numsOfImage;
    }

    public String getNameAndTitle() {
        return nameAndTitle;
    }

    public void setNameAndTitle(String nameAndTitle) {
        this.nameAndTitle = nameAndTitle;
    }

    private int numsOfImage;

    public String getCommentNums() {
        return commentNums;
    }

    public void setCommentNums(String commentNums) {
        this.commentNums = commentNums;
    }

    private String commentNums;

    public String getFormTime() {
        return formTime;
    }

    public void setFormTime(String formTime) {
        this.formTime = formTime;
    }

    private String formTime;
    private String nameAddress;


    private String nameAndTitle;
    private String profileImage = "";

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;
    private String desc;

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserImage() {
        return userImage;
    }

    public String getNameAddress() {
        return nameAddress;
    }

    public void setNameAddress(String nameAddress) {
        this.nameAddress = nameAddress;
    }

    @Override
    public ForumData clone() throws CloneNotSupportedException {
        return (ForumData) super.clone();
    }
}
