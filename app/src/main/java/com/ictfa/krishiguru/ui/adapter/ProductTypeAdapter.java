package com.ictfa.krishiguru.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.dtos.ProductTypeListDtos;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sureshlama on 5/22/17.
 */

public class ProductTypeAdapter extends BaseAdapter {
    Context activity;
    private LayoutInflater inflater;
    private List<ProductTypeListDtos> productTypeList;

    public ProductTypeAdapter(Context activity, List<ProductTypeListDtos> productTypeList) {
        this.activity = activity;
        this.productTypeList = productTypeList;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private static class ViewHolder {
        ImageView imageView;
        TextView title, discountPrice, actualPrice, viewdCounter;
    }

    @Override
    public int getCount() {
        return this.productTypeList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.productTypeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View convertedView = convertView;
        ViewHolder viewHolder;
        if (convertedView == null) {
            convertedView = inflater.inflate(R.layout.custome_product_detail, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertedView.findViewById(R.id.img);
            viewHolder.title = (TextView) convertedView.findViewById(R.id.titlenew);
            viewHolder.discountPrice = (TextView) convertedView.findViewById(R.id.discountedPrice);
            viewHolder.actualPrice = (TextView) convertedView.findViewById(R.id.actualPrice);
            viewHolder.viewdCounter = (TextView) convertedView.findViewById(R.id.viewCounter);
            convertedView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertedView.getTag();
        viewHolder.title.setText(productTypeList.get(position).getTitle());
        viewHolder.discountPrice.setText("रु." + productTypeList.get(position).getDiscountPrice());
        viewHolder.actualPrice.setText("रु." + productTypeList.get(position).getActualPrice());
        viewHolder.viewdCounter.setText(productTypeList.get(position).getViewdCounter());
        Picasso.with(activity).load(productTypeList.get(position).getImg()).placeholder(R.drawable.images).resize(500, 500).into(viewHolder.imageView);
        return convertedView;
    }
}
