package com.ictfa.krishiguru.ui.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.activity.PlayVideoActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sureshlama on 3/1/17.
 */

public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context activity;
    private List<VideoData> videoDatas;
    boolean isFromDashboard;

    public VideoAdapter(Context activity, List<VideoData> videoDatas, boolean isFromDashboard) {
        this.activity = activity;
        this.isFromDashboard = isFromDashboard;
        this.videoDatas = videoDatas;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (!isFromDashboard) {
            View view = LayoutInflater.from(activity).inflate(R.layout.custome_video_list, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_video_dashboard, parent, false);
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.width = (int) (parent.getWidth() * 0.45);
            layoutParams.height = (int) (parent.getWidth() * 0.6);
            view.setLayoutParams(layoutParams);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder;
        if (holder instanceof ViewHolder) {
            viewHolder = (ViewHolder) holder;
            viewHolder.textView.setText(videoDatas.get(position).getTitle());

            Picasso.with(activity).load(videoDatas.get(position).getVideoThumbnail()).resize(500, 500).into(viewHolder.imageView);

            viewHolder.imageView.getLayoutParams().width = (int) (holder.itemView.getWidth());
            viewHolder.imageView.getLayoutParams().height = (int) (holder.itemView.getWidth());
//            ((ViewHolder) holder).imageView.setLayoutParams(layoutParams);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, PlayVideoActivity.class);
                    intent.putExtra("videoId", videoDatas.get(position).videoId);
                    activity.startActivity(intent);
                }
            });
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (isFromDashboard) {
            if (videoDatas.size() > 10)
                return 10;
            else
                return videoDatas.size();
        } else
            return videoDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageThumbnil)
        ImageView imageView;
        @BindView(R.id.videoTitle)
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
