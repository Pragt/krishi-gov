package com.ictfa.krishiguru.ui.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.dashboard.DashBoardActivity;
import com.ictfa.krishiguru.ui.activity.RegistrationActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends DialogFragment {
    private EditText usernameView, passwordView;
    private Button loginbtn;
    private TextView newAccount;

    private String usernameValue, passwordValue;
    private TextView passwordChange;

    public LoginFragment() {
        // Required empty public constructor
    }



    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d!=null){
            DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;

            d.getWindow().setLayout(width, height);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_login, container, false);
        usernameView = (EditText)view.findViewById(R.id.loginUsername);
        passwordView = (EditText)view.findViewById(R.id.loginPassword);
        loginbtn = (Button)view.findViewById(R.id.loginBtn);
        newAccount = (TextView)view.findViewById(R.id.signup);
        passwordChange = (TextView)view.findViewById(R.id.forgetPassword);


        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validForm()){
                    getLogin();
                }
            }
        });

        newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),RegistrationActivity.class);
                startActivity(intent);
                dismiss();
            }
        });
        return view;
    }

    private boolean  validForm(){
        usernameValue = usernameView.getText().toString().trim();
        passwordValue = passwordView.getText().toString().trim();

        if(usernameValue.isEmpty()){
            Toast.makeText(getActivity(), "युजरनाम खाली छ", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(passwordValue.isEmpty()){
            Toast.makeText(getActivity(), "यपासवोर्द खाली छ", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    private void getLogin(){
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        String LOGIN_URL = "http://admin.ict4agri.com/api/v3/user/login?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA&username="+usernameValue+"&password="+passwordValue;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for(int i =0; i<jsonArray.length(); i++){
                                JSONObject jsonValue = jsonArray.getJSONObject(i);
                                if(jsonValue.has("status")){
                                    Toast.makeText(getActivity(), "युजरनाम वा पासवोर्द मिलेन",Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    JSONObject value = jsonArray.getJSONObject(0);

                                    String id = value.getString("id");
                                    String lat = value.getString("lat");
                                    String lon =  value.getString("lng");
                                    String username =  value.getString("username");
                                    String agency = value.getString("agency");
                                    String code = value.getString("code");
                                    String priorities = value.getString("priorities");
                                    String userType = value.getString("user_type");
                                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences(BaseApplication.PREFS_NAME, MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("loginStatus","1");
                                    editor.putString("userID",id);
                                    editor.putString("lat",lat);
                                    editor.putString("lng",lon);
                                    editor.putString("userName",username);
                                    editor.putString("agency",agency);
                                    editor.putString("code",code);
                                    editor.putString("priorities",priorities);
                                    editor.putString("userType",userType);
                                    editor.commit();
                                    Intent intent = new Intent(getActivity(),DashBoardActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                    dismiss();


                                }
                            }

                            int lengthOfArray = jsonArray.length();



                        }

                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


}
