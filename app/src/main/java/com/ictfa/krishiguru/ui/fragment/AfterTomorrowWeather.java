package com.ictfa.krishiguru.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ictfa.krishiguru.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfterTomorrowWeather extends Fragment {


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
      //  ProductDataListenerManager.getInstance().addDataGotAfterTomorrow(this);

    }

    public AfterTomorrowWeather() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_after_tomorrow_weather, container, false);

//        min = (TextView)view.findViewById(R.id.afterTomorrowminTemp);
//        max = (TextView)view.findViewById(R.id.afterTomorrowmaxTemp);
//        daydesc = (TextView)view.findViewById(R.id.afterTomorrowdesc);
//        dayRainChance = (TextView)view.findViewById(R.id.afterTomorrowRainChance);
//        dayRainDuration = (TextView)view.findViewById(R.id.afterTomorrowRainDuration);
//        dayRainAmount = (TextView)view.findViewById(R.id.afterTomorrowRainAmount);
//        dayThunderStorm = (TextView)view.findViewById(R.id.afterTomorrowThunder);
//        nightDesc = (TextView)view.findViewById(R.id.afterTomorrowNightdesc);
//        nightRainChance = (TextView)view.findViewById(R.id.afterTomorrowNightRainChance);
//        nightRainDuration = (TextView)view.findViewById(R.id.afterTomorrowNightRainDuration);
//        nightRainAmount = (TextView)view.findViewById(R.id.afterTomorrowNightRainAmount);
//        nightThunderStorm = (TextView)view.findViewById(R.id.afterTomorrowNightThunder);
//
//        dayIcon = (ImageView)view.findViewById(R.id.afterTomorrowImg);
//        nightIcon = (ImageView)view.findViewById(R.id.afterTomorrowNightImg);
        return view;
    }
    /**
    @Override
    public void onDataGot(JSONObject response) {
        Log.d(" listner Response", response.toString());

        try {
            JSONObject temperature = response.getJSONObject("temperature");
            String minTemp = temperature.getString("min");
            String maxTemp = temperature.getString("max");

            JSONObject day = response.getJSONObject("day");
            String icon = day.getString("icon");
            String  desc = day.getString("text");
            String probabilityOfRain = day.getString("probability_of_rain");

            ///  rainChance.setText("Chance of rain "+probabilityOfRain+ "%");


            min.setText("Min : "+minTemp + "°C");
            max.setText("Max : "+maxTemp+ "°C");

            Picasso.with(getActivity()).load(icon).into(dayIcon);
            daydesc.setText(desc);
            dayRainChance.setText("Chance of rain : "+probabilityOfRain+ "% ");

            //  conditionTextView.setText(desc);
            double hours_of_rain = day.getDouble("hours_of_rain");
            String duration;

            if(hours_of_rain<2){
                duration = "Duration of rain: "+hours_of_rain+  " Hour ";
            }

            else {
                duration = "Duration of rain: "+hours_of_rain+  " Hours ";
            }




            String thunderstorm = day.getString("thunderstorm");
            dayThunderStorm.setText("Chance of thunderStrom : "+ thunderstorm +"%");

            String rain = day.getString("rain");
            dayRainDuration.setText(duration);
            dayRainAmount.setText("("+rain+"  mm)");


            JSONObject night =  response.getJSONObject("night");
            String nightIconvalue = night.getString("icon");
            Picasso.with(getActivity()).load(nightIconvalue).into(nightIcon);
            String nightDescValue = night.getString("text");
            nightDesc.setText(nightDescValue);
            String nightProbailityOfRain = night.getString("probability_of_rain");
            nightRainChance.setText("Chance of rain : "+nightProbailityOfRain+ "% ");

            double nightHour_of_rain = night.getDouble("hours_of_rain");
            String nightRainDurationValue;

            if(nightHour_of_rain<2){
                nightRainDurationValue = "Duration of rain: "+hours_of_rain+  " Hour ";
            }

            else {
                nightRainDurationValue = "Duration of rain: "+hours_of_rain+  " Hours ";
            }


            nightRainDuration.setText(nightRainDurationValue);

            String nightThunderstrom = night.getString("thunderstorm");

            nightThunderStorm.setText("Chance of thunderStrom : "+ nightThunderstrom +"%");
            String nightRain = night.getString("rain");
            nightRainAmount.setText("("+nightRain+"  mm)");


        }

        catch (JSONException e) {
            e.printStackTrace();
        }

    }

    **/
}
