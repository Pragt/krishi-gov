package com.ictfa.krishiguru.ui.fragment;


import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ictfa.krishiguru.R;
import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageShowFragment extends DialogFragment {
    private ZoomageView showImg;

    public ImageShowFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_image_show, container, false);
        showImg = view.findViewById(R.id.showImage);
        Bundle bundle = this.getArguments();

        String imageUrl = bundle.getString("image");

        Picasso.with(getActivity()).load(imageUrl).resize(2024, 1080).centerInside().into(showImg);

        return view;
    }

    public static ImageShowFragment newInstance() {
        ImageShowFragment frag = new ImageShowFragment();
        return frag;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

}
