package com.ictfa.krishiguru.ui.fragment;


import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ictfa.krishiguru.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommentPopupDiaglog extends DialogFragment {

    public CommentPopupDiaglog() {
        //
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view =  inflater.inflate(R.layout.fragment_comment_popup_diaglog, container, false);
        Bundle bundle = this.getArguments();
        String url = bundle.getString("commentUrl");
        return view;
    }

    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d!=null){
            DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;

            d.getWindow().setLayout(width, height/3);
        }
    }

}
