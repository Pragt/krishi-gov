package com.ictfa.krishiguru.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.dashboard.DashBoardActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 2000;
    private int ref_id = 0;
    private int type = 0;
    RelativeLayout rlLogo;
    ImageView ivLogo;
    TextView tvAppName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        rlLogo = findViewById(R.id.rlLogo);
        ivLogo = findViewById(R.id.ivLogo);
        tvAppName = findViewById(R.id.tvAppName);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        printHashKey(this);

//        int[] abc = {0,-3,0,-4,0};
//        System.out.println("result" +  decodeArray(abc)+"");

        ivLogo.animate().scaleX(0.65f).setDuration(1000);
        ivLogo.animate().scaleY(0.65f).setDuration(1000);
        ivLogo.animate().translationY(-60f).setDuration(1000);
        tvAppName.animate().translationY(-120f).setDuration(1000);
        new Handler().postDelayed(() -> tvAppName.animate().alpha(1f).setDuration(1000), 1300);

        /*
         * Showing splash screen with a timer. This will be useful when you
         * want to show case your app logo / company
         */
        new Handler().postDelayed(() -> {
            if (getIntent().getExtras() != null) {
                for (String key : getIntent().getExtras().keySet()) {
                    try {
                        String value = getIntent().getExtras().getString(key);
                        if (key.equalsIgnoreCase("type")) {
                            type = Integer.parseInt(value);
                        } else if (key.equalsIgnoreCase("ref_id")) {
                            ref_id = Integer.parseInt(value);
                        }
                    } catch (Exception exx) {

                    }
                }
            }
//
            if (ref_id != 0 && type != 0) {
                Intent intent = new Intent(SplashActivity.this, DashBoardActivity.class);
                intent.putExtra("ref_id", ref_id);
                intent.putExtra("type", type);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                Intent i = new Intent(SplashActivity.this, DashBoardActivity.class);
                startActivity(i);
//                }
            }


            finish();
        }, SPLASH_TIME_OUT);

//        addFragment(new Step.Builder().setTitle("This is header")
//                .setContent("This is content")
//                .setBackgroundColor(Color.parseColor("#FF0957")) // int background color
//                .setDrawable(R.drawable.ic_chat_org) // int top drawable
//                .setSummary("This is summary")
//                .build());
//
//        addFragment(new Step.Builder().setTitle("This is header")
//                .setContent("This is content")
//                .setBackgroundColor(Color.parseColor("#9932CC")) // int background color
//                .setDrawable(R.drawable.ic_home_love) // int top drawable
//                .setSummary("This is summary")
//                .build());
//
//        addFragment(new Step.Builder().setTitle("This is header")
//                .setContent("This is content")
//                .setBackgroundColor(Color.parseColor("#228B22")) // int background color
//                .setDrawable(R.drawable.ic_action_lock) // int top drawable
//                .setSummary("This is summary")
//                .build());
    }

    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("prajit", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("prajit", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("prajit", "printHashKey()", e);
        }
    }

    public int decodeArray(int[] a) {
        int encodedNum = 0;

        for (int i = 1; i < a.length; i++) {
            int diff = a[i] - a[i - 1];
            int multiplier = 1;
            for (int j = a.length; j > i + 1; j--) {
                multiplier *= 10;
            }
            diff = diff * multiplier;
            if (diff < 0)
                diff = diff * -1;

            encodedNum += diff;
        }
        return encodedNum;
    }

}


