package com.ictfa.krishiguru.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.Weather.WeatherResponse;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.ui.adapter.WeatherTabAdapter;

import org.json.JSONObject;


public class WeatherActivity extends AppCompatActivity {
    private JSONObject weatherObject;
    private ViewPager viewPager;
    public static WeatherResponse.Forecasts forecasts;
    private TextView sumarryWeather;
    private ProgressDialog progressDialog;
    public String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        setContentView(R.layout.activity_weather);
          viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (bundle != null)
        {
            forecasts = (WeatherResponse.Forecasts) bundle.getSerializable(CommonDef.LIST_WEATHER);
            location = bundle.getString(CommonDef.LOCATION);
        }
        viewPager.setAdapter(new WeatherTabAdapter(getSupportFragmentManager(),
                WeatherActivity.this));
            progressDialog = new ProgressDialog(this);
        getSupportActionBar().setTitle("मौसम जानकारी");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        viewPager.setCurrentItem(0);

        weatherObject = new JSONObject();

        sumarryWeather = (TextView)findViewById(R.id.summaryWeather);


    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
