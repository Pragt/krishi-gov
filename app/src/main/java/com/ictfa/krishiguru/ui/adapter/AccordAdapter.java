package com.ictfa.krishiguru.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.dtos.AccorDetailDtos;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.List;

/**
 * Created by sureshlama on 5/21/17.
 */

public class AccordAdapter  extends BaseAdapter {
    private LayoutInflater inflater;
    private List<AccorDetailDtos> accordetails;

    public  AccordAdapter(Context activity, List<AccorDetailDtos> accordetails) {
        this.accordetails = accordetails;
        inflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    private static class ViewHolder{

        TextView title;
        HtmlTextView expandTitle;
        ExpandableLayout expandableLayout;
    }

    @Override
    public int getCount() {
        return this.accordetails.size();
    }

    @Override
    public Object getItem(int position) {
        return this.accordetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View convertedView= convertView;
        final ViewHolder viewHolder;
        if(convertedView==null){
            convertedView= inflater.inflate(R.layout.custome_according, parent, false);
            viewHolder= new ViewHolder();

            viewHolder.title= (TextView) convertedView.findViewById(R.id.intro);
            viewHolder.expandableLayout = (ExpandableLayout)convertedView.findViewById(R.id.expandable_layout_0);
            viewHolder.expandTitle= (HtmlTextView) convertedView.findViewById(R.id.expandIntro);

            convertedView.setTag(viewHolder);
        }else
            viewHolder= (ViewHolder) convertedView.getTag();
        viewHolder.title.setText(accordetails.get(position).getSubtitle());
        viewHolder.expandTitle.setHtml(accordetails.get(position).getDescription());

        viewHolder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   onTouchedListener.gettouched();
                if(viewHolder.expandableLayout.isExpanded()){
                    viewHolder.expandableLayout.collapse();
                }
                else {
                    viewHolder.expandableLayout.expand();
                }
            }
        });
        return convertedView;


    }

}