package com.ictfa.krishiguru.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.ui.adapter.VideoData;
import com.ictfa.krishiguru.ui.adapter.VideoListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.ictfa.krishiguru.helpers.UrlHelper.YOUTUBE_URL;

public class YoutubeListActivity extends AppCompatActivity {
    private ListView youtubeList;
    private List<VideoData> videoDatas = new ArrayList<>();
    private VideoListAdapter videoListAdapter;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_list);
        youtubeList = (ListView) findViewById(R.id.youtubeList);
        videoListAdapter = new VideoListAdapter(YoutubeListActivity.this, videoDatas);
        youtubeList.setAdapter(videoListAdapter);

        //get realm instance
        this.realm = RealmController.with(this).getRealm();
        getDataFromYoutube();
        getSupportActionBar().setTitle("कृषि भिडियो");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        youtubeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                VideoData videoDto = (VideoData) parent.getItemAtPosition(position);
                String videoId = videoDto.getVideoId();
                Intent intent = new Intent(YoutubeListActivity.this, PlayVideoActivity.class);
                intent.putExtra("videoId", videoId);
                startActivity(intent);
            }
        });
    }

    private void getDataFromYoutube() {

        RealmResults<VideoData> listVideo = realm.where(VideoData.class).findAll();
//        videoDatas = new ArrayList<>();
        for (VideoData data : listVideo)
            videoDatas.add(data);

        videoListAdapter.notifyDataSetChanged();
        Log.d("CHECK", "CHECK");
        RequestQueue queue = Volley.newRequestQueue(YoutubeListActivity.this);
        //  String url ="http://www.google.com";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, YOUTUBE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("YOUTUBEURL", YOUTUBE_URL);
                        Log.d("RESPONSE", response);
                        //  Toast.makeText(getActivity(),response,Toast.LENGTH_LONG).show();
                        try {
                            JSONObject youtubeData = new JSONObject(response);
                            JSONArray youtubeDataArray = youtubeData.getJSONArray("items");
                            videoDatas.clear();
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.delete(VideoData.class);
                                }
                            });
                            for (int i = 0; i < youtubeDataArray.length(); i++) {
                                JSONObject object = youtubeDataArray.getJSONObject(i);
                                JSONObject snippetObject = object.getJSONObject("snippet");
                                String title = snippetObject.getString("title");
                                String imageUrl = null, videoId = null;
                                if (snippetObject.has("thumbnails")) {
                                    JSONObject thumbnailObject = snippetObject.getJSONObject("thumbnails");
                                    JSONObject defaultObject = thumbnailObject.getJSONObject("default");
                                    imageUrl = defaultObject.getString("url");
                                    JSONObject resourceObject = snippetObject.getJSONObject("resourceId");
                                    videoId = resourceObject.getString("videoId");
                                }


                                VideoData videoData = new VideoData();
                                videoData.setTitle(title);
                                videoData.setVideoThumbnail(imageUrl);
                                videoData.setVideoId(videoId);

                                videoDatas.add(videoData);
                            }
                            youtubeList.setAdapter(new VideoListAdapter(YoutubeListActivity.this, videoDatas));
                            //videoListAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

