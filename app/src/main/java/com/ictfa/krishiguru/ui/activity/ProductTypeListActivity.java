package com.ictfa.krishiguru.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.productDetail.DetailProdcutActivity;
import com.ictfa.krishiguru.ui.adapter.ProductTypeAdapter;
import com.ictfa.krishiguru.ui.dtos.ProductTypeListDtos;
import com.ictfa.krishiguru.ui.widget.ExpandableHeightGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ProductTypeListActivity extends AppCompatActivity {
    private ExpandableHeightGridView gridView;
    private List<ProductTypeListDtos> productTypeListDtoses = new ArrayList<>();
    private ProductTypeAdapter productTypeAdapter;
    private String productId;
    String name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_type_list);
        Bundle extras = getIntent().getExtras();
        productId = extras.getString("id");
        name = extras.getString("name");
        getSupportActionBar().setTitle(name + " सम्बन्धि सामग्री");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        gridView = (ExpandableHeightGridView) findViewById(R.id.gridview);
        productTypeAdapter = new ProductTypeAdapter(ProductTypeListActivity.this, productTypeListDtoses);
        gridView.setAdapter(productTypeAdapter);
        getProductData();
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long ids) {
                ProductTypeListDtos object = (ProductTypeListDtos) parent.getItemAtPosition(position);
                String image = object.getImg();
                String description = object.getDescription();
                String id = object.getId();

                if (description.isEmpty()) {
                    description = "this is test only no data got.....";
                }
                String title = object.getTitle();
                String actualPrice = object.getActualPrice();
                String discountPrice = object.getDiscountPrice();
                // String name = object.ge
                Intent intent = new Intent(ProductTypeListActivity.this, DetailProdcutActivity.class);
                intent.putExtra("image", image);
                intent.putExtra("desc", description);
                intent.putExtra("title", title);
                intent.putExtra("actualPrice", actualPrice);
                intent.putExtra("discountPrice", discountPrice);
                intent.putExtra("video_url", object.getVideoUrl());
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });
    }

    private void getProductData() {
        productTypeListDtoses.clear();
        RequestQueue queue = Volley.newRequestQueue(ProductTypeListActivity.this);
        String url = BaseApplication.hostUrlProduct + productId + BaseApplication.getProductfeatureList;
        Log.d("FORMURL", url);
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            Log.d("utfstr", utfStr);
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject newsValue = jsonArray.getJSONObject(i);
                                ProductTypeListDtos productionTechnologyDtos = new ProductTypeListDtos();
                                String id = newsValue.getString("id");
                                String name = newsValue.getString("name");
                                String unit = newsValue.getString("unit");
                                String discountedPrice = newsValue.getString("price");
                                String actualPrice = newsValue.getString("discounted_price");
                                String description = newsValue.getString("description");
                                String view_count = newsValue.getString("view_count");
                                String video_url = newsValue.getString("video_url");

                                JSONArray images = newsValue.getJSONArray("images");
                                if (images != null && images.length() > 0) {
                                    JSONObject imageObject = images.getJSONObject(0);
                                    String imagevalue = imageObject.getString("name");
                                    String img = BaseApplication.Product_HOSTIMG + imagevalue;


                                    String dummyImg = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJoAmgMBEQACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAQMEBQYCB//EADoQAAEDAwIEAwYCCAcAAAAAAAEAAgMEBREhMQYSE0EiUXEUMkJhgZEHUhUjQ5KhwdHhFiUzcoKisf/EABsBAQADAQEBAQAAAAAAAAAAAAACAwQBBQYH/8QALREAAgIBBAAGAQIHAQAAAAAAAAECAxEEEiExBRMyQVFhIjOhFCRDUnGR0RX/2gAMAwEAAhEDEQA/APcUAIAQAgBACAEAIAQAgEygFQAgBACAEAIAQAgBACAEAIAQAgBACA5e4MaXOcGtAySdguN4WWBmmraaqc5tPPFKW+8GOyQowshP0vIJCmDJca3qqpay12e1uLaytl53yD9lCzVx+v8AIquxvGEdRqYTzRscdy0FSRwcUgCAEAIAQAgBACAEAIAQAgBAJkIBUBDu9I24Wuro355Z4XMyNCMjcfNRlFSTTB4T+F9/qaLiemFdITHI91JM57icknDf+2NfmsUIxqtSRCL5PfameOngfNK7lYwZcVubSWWTPM7FcorpxLdOK7nK2O3UcRp2F3u/7ADud9ty7RZoT3vc+kdccG24dudFVWmCphknZDM3mjZVOy8N7fTyVsJxayT8qb6RatqoHnDZmH6qakn7nHXNdodyFIgKgBACAEAIAQAgBACAg3m7UdloJa64zCGnjAy46kk6AAbkk7ALjeDqWXgp4LleLhC2qLaey0z9Ym1bOrNI3zLQ4BvpqVDLf0dwjiqn4lpoXT0stNXADwxuo+iXf8jL/JJOS5Qik3gzl0/E2ttcDobnZJLfVHAbNKeeIa7+Hf6FVSumuME3Xh89EkfiLRuhh9irqavlOA9pidCc/LJP/qjLUpe/7EGvc8Nh68lzqw2Xpc1Q8twMn3tFVdJLEkjM/o3/ABvx/LV2mC3ipb7Y9jRJBGNWuwMl3p2Hmc9lNSsnHL6NT2v0oqeHav8AytlrmOYGTuftkkHsPLyysdtnzwjfpdJPUtZ9KPQqC4PdGB0TG0AblTrs46PYnpkvcl+2Sn3TgKzeyPkx9xRfp6Ety84XP4hwOf8Anwu6RoLRxJS12GPe1rytNWqhP3PK1XhtlPKXBeg522Wo80VACAEAIAQAgEOyAyUVI+/XN9xuxa+2Ucp/RzA0t53DQynXXGoB76nYhV492ySTfCJdXcY4CRTBked3huXn6n+6qlbjo3U6TPMipnq2SkmaR7/m95Kq35Nqpa6S/wBFHeq6hETofFI9+nSaOYO9cqmzUQhx7mmnSWT5fX2Y53AwrpOv1hQ6ktbEMkfJRrsm+1wRt0FWeGzqPg+59J0Dbl02u0MvIOfHlkKMW92dv7lT8LpXKl+xXT/h7UURMwrjP5kNwVO3UTUfSXUeGUyeJSZp+FrdFBTOEkeX5yHFuqzUtWcyRusoVCUYdGiL2saBotGUitRbYw+vY3RQdiRcqJMr7hVMljwCs91kXFmqipxkUsc8tPIHxPLSDnRYYTcemehKuM1ho9G4H4qFaG0NY7EwHgcTuvd0Ws8z8J9nyPi/hfkvza+jbr0j54EAIAQAgBANzOYInGQ4bjVcfCOpNvCKCod+obDABHBG0MjYNmtGgCzzk2ehTXGHfZnbiTG7VyyzPVpaZQ11W4Mdh2T2VE5YRuqryxq1xxtBlkIdI7uVXVFL8n2XXNvhFzE9pG4WhNGVxY7zAdxhMnMHEj2EYOy42iSixrrRxjTAXMpInskyurK0ZIaf4rPZcka6qPkqpZ3OOQVhla2bo1oaLnEqonhHJBI1TJ1PA0yWSknbPAS2RhyCFfCWHlHZ1xsjtke08KXqO82qObP61ujx819HpbvNrT9z888R0b0tzj7exdrSYAQAgBACApLtWB0wgafC0647lUWT5wbtPViO5lLdLrFSxnJGfJZ52KJtp08pMxN1vgcXa51WKy/4PYp0+DMVl1LnYGTn5qjbKfZ6dVKHKW5u7PKg1OPRN1ItKe6PGMuyiukuyt0pk6O6gjVymtQiv+HOnXDI3R6hElRgjyVb3e6qpXN9FsakhkxyPOSqnCcuSxSig9mf5LnlSO+ZEOiR2Kr2yG9CmIhqi012c3EaVm+ilFl0WXv4f3Z1tvTYHuPRnPKfLK9LRXbLEvZnk+N6Tz6HNdo9iC+gPgxUAIAQDVVL0YJJPytJXJPCyShHdJIwNfcOmXyOIzuvPlPB79dWVgwF6vDpZnEu0z5rz5zlN8Hs01KK4MzUVb5CdVZGtI2QikMBxJ1KnguUh2OQt2Ki1ktXJNgqDtlZ5QDiTY5jjdUuJzBPpf1hAOVWo5lgjJ4RdU1G0gaLbCpGOy1osGUTcbLQq0ZXcx5tC3HuqXlor89jclvA+FVyqTJLUESppQxp00Wa6pJF9duWVEjNcLzT0IsjRZhq45W5Ba4HK0Qlgsmt9bR7pap/abdTy5yXMGV9VVLdBM/NdTDy7pR+yYrCgEAICuv7+W2yfMgKu14iaNKs2I8rv1QQ14BXl2vg+loiee3GUmQjKpqXB6sOiFlXk8igrhJMcaVFl8WPRuwVBouROhkHdUSRxotrfIMhUdSyVTXBqqAhzcr0K+jy7uC0YBhXoxsda4BSK2jpzmlDiTRXXDHIVlu6NdHZnZxh5wvGl6merDoY6fNI0AblShy8Fu7CPYeEyf0LCCc8pwvq9N+mj4DxJfzMi5WgwAgBAVfEYza3/IhVXek06T9VHk98BJK8yxZPpaXwYa5wkPLgPVVVvHB6MJcFcriw6C4SQ41RZdEdaos0RHWOwVBosLagccDVZbUVTRqbbN4N1fTPKPNvgW8UwPda0zFKA6JPmpZIbRHTY7rjZ1QK6tqA5pAKyXz4NdNeCof4nFeU+WblwiRRw5kaSO610Vc5ZVbP8WercMs6dpiGMZJK+k06xWj4fxCWdQy2V5iBACAhXiPq26oaN+TP2ULFmLLaZbbEzye8xnmPqvNkj6al8GWrKfmJOFllH3RsjIqJ6LXLThdjY12aIz+SM6ne3torFNMuTTEaw+SNouiPMYSoNlyeBzplRyWKRY0IIaMrNayE2XdHP09yq657XgzzhksY6wALUrUZ3TkV1wI2R3nFp0MSVMkh3ICqlbJlsaoobBJ3KqeWTO2MyUjDki2WVDFmVoHmttceUY7p4TPVLfF0aKBnkwL3K1iKR8TfLdZJ/ZJUyoEAIDlzQ4EHYjBCDODzHiWhdT1UkZGjTp6dl51scPB9FpLFOCZkqqPdZpI9GLK2WJUTiXRYw6EFVZaLUzjoDK7vLoyHGQBQcyxSHBA1c3smpD8UYAVcpEtxKjCqYH26BTicF5gpnDoFBg7aclDjJVOzOFfCJROWDTcOUJqK6NuNAcn0C36evMkeN4hfsqZ6GNl6x8qCAEAIAQGd4ttvtNN7SweKMeLHceaouhlZNuiv3S2s8zroS17gQsEkfQwllFVNHhUyRoiRnhZpIuRxhVFqO2hRZajoLhNDrFBkx5pwogXnXUdELl3IwONciZ1kiHVyugVz6Ligh5sLZXE8+6eD0Phy3+yUnUeMSSa+gXr0V7Vk+T12o82eF0i4V5hBACAEAIDh2CCD3QHnnGFk9jcaiFpNO4/unyWG+rbz7HuaHVb/AMX2YicakLHI9eDITxqVnmaIjfdZ2XIXK4WJgHarhNDzDooMmd5XDoZTB0AdUwdHWFEMlhSMLngAarVXEz2SSRvuF7IS1lTUtwwatafi/svY09HG6R8t4jruXCBsAtx4QqAEAIAQCHZAcOKAhVoZJC+OVrXMcMFpGQQuNZWGdTcXlHmPE1k9llfLSEvh/L8Tf6hYLtO1zHo93Sa+M/xnwzJSuIJBXnTR7UJZGC9UYL0w5kwWJic+qYJpj0b1BonkdDlHBLIFyYO5OmZPZcxkZJtJSyzPa1jHEk4AAV1dEmyiy+MFls3/AAzwyIOWouAy7dsW/wB/6L2dPpNvMj5jX+LeZ+FPXz/w2segAAAHkFvPBHAgOkAIAQAgEKA4cEBDqY8tIQGdutA6TmQGGu/D73lzmNLT5hZ7dNCz6Zu0+vtp47X2Zeqt9wpnYdTOkb+Zmv8ADdYJ6Kcfs9ujxSifbw/shGpDDyyAsI7OBCzSomu0elC6EuYvJ02piJ/1WfdR8uXwW7yVFIw7Pb91B1tkt7JMYDj4Tn0Ulp2yLux2WNNbKicjkgkcPTCujo5P2MtniNNfqmi9t/DMz3DqkRjyGpWuvQ/3Hm3+Nx/prP8Ak19ptMFEB0YvF+Y6lba6YV+lHiX6u69/m+PgvoWYAVpmJTAgHAgOkAIAQAgBAckIDhzMoCPJTh24QEOa3Mf8KAgzWOF+7QgIz+HIT8IQDf8AhmnO8Y+yAVvDNKP2Mf7q5hHdz+SVFY4o/dYB6Bd4OPnslR2uNvwplgkx0bWjRqAkshA7IB5rEA4AgOgEAqAEAIAQAgBAJhAIQgOS0FAIWBAJ00AnTQB00AvTCABGPJAdciAUNQHWEAIBUAIAQAgBACAEAIAQAgBAJhAGEAYQBhAGEAqAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQH//2Q==";
                                    //Toast.makeText(ProductTypeListActivity.this,image.toString(),Toast.LENGTH_SHORT).show();
                                    productionTechnologyDtos.setId(id);
                                    productionTechnologyDtos.setTitle(name);
                                    productionTechnologyDtos.setImg(img);

                                    Log.d("imageLink", img);
                                }
                                productionTechnologyDtos.setDiscountPrice(discountedPrice + " per " + unit);
                                productionTechnologyDtos.setActualPrice(actualPrice + " per " + unit);
                                productionTechnologyDtos.setViewdCounter(view_count);
                                productionTechnologyDtos.setDescription(description);
                                productionTechnologyDtos.setVideoUrl(video_url);
                                productTypeListDtoses.add(productionTechnologyDtos);
                            }
                            productTypeAdapter.notifyDataSetChanged();
                            Log.d("JSONRESPONSE=>>>>>>>>", jsonArray.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
