package com.ictfa.krishiguru.ui.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.news.NewsAdapter;
import com.ictfa.krishiguru.news.NewsData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {

    NewsAdapter newsAdapter;
    List<NewsData> newsDatas = new ArrayList<>();
    private ListView newsList;

    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
          View view = inflater.inflate(R.layout.fragment_news, container, false);

            newsList = (ListView)view.findViewById(R.id.newsList);
            newsAdapter = new NewsAdapter(getActivity(),newsDatas, false);
//            newsList.setAdapter(newsAdapter);
            getNewsData();
            return view;
    }

    private void getNewsData(){


        Log.d("CHECK", "CHECK");
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //  String url ="http://www.google.com";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BaseApplication.NEWS_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        newsDatas.clear();

                        try {
                            JSONObject NewsListData = new JSONObject(response);
                            JSONArray NewsDataArray = NewsListData.getJSONArray("data");

                            for (int i = 0; i<NewsListData.length(); i++){
                                JSONObject newsobject =  NewsDataArray.getJSONObject(i);
                                String title = newsobject.getString("title");
                                String image = newsobject.getString("image");
                                String description = newsobject.getString("description");

                                if(image.isEmpty()){
                                    image = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Traditional_Farming_Methods_and_Equipments.jpg/220px-Traditional_Farming_Methods_and_Equipments.jpg";
                                }
                                NewsData newsData = new NewsData();
                                newsData.setTitle(title);
                                newsData.setNewsImage(image);
                                newsData.setDesc(description);
                                newsDatas.add(newsData);

                            }
                            newsAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);


    }


}
