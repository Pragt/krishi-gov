package com.ictfa.krishiguru.ui.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ictfa.krishiguru.ui.fragment.AfterTomorrowWeather;
import com.ictfa.krishiguru.ui.fragment.TodayWeatherFragment;
import com.ictfa.krishiguru.ui.fragment.TomorrowWeatherFragment;

/**
 * Created by sureshlama on 3/28/17.
 */

public class FragmentAdapter extends FragmentStatePagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[]{"आज", "भोली" , "पर्सी"};

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
               return new  TodayWeatherFragment();
            case 1:
                return  new TomorrowWeatherFragment();
            case 2:
                return new AfterTomorrowWeather();

            default:
                return null;
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }


}
