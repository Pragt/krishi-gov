package com.ictfa.krishiguru.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.dtos.GridProductDtos;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sureshlama on 5/20/17.
 */

public class GridProductAdapter extends BaseAdapter {
    Context activity;
    private LayoutInflater inflater;
    private List<GridProductDtos> gridproductDtos;

    public GridProductAdapter(Context activity, List<GridProductDtos> gridproductDtos) {
        this.activity = activity;
        this.gridproductDtos = gridproductDtos;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private static class ViewHolder {
        ImageView imageView;
        TextView textView;
    }

    @Override
    public int getCount() {
        return this.gridproductDtos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.gridproductDtos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View convertedView = convertView;
        ViewHolder viewHolder;
        if (convertedView == null) {
            convertedView = inflater.inflate(R.layout.custome_category_list, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertedView.findViewById(R.id.imgimg);
            viewHolder.textView = (TextView) convertedView.findViewById(R.id.titlenew);
            convertedView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertedView.getTag();
        viewHolder.textView.setText(gridproductDtos.get(position).getName());

        Picasso.with(activity).load(gridproductDtos.get(position).getImage()).placeholder(R.drawable.images).resize(500, 500).into(viewHolder.imageView);
        /*if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.custome_video_list, null);*/

        ViewGroup.LayoutParams layoutParams = convertedView.getLayoutParams();
        layoutParams.width = (int) (parent.getWidth() * 0.33);
        convertedView.setLayoutParams(layoutParams);
        return convertedView;
    }
}

