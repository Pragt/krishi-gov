package com.ictfa.krishiguru.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.dashboard.SlideShowImage;
import com.ictfa.krishiguru.graph.BarChartActivity;
import com.ictfa.krishiguru.graph.Commodity;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.krishakByapariMulya.KrishyakByapariMulyaActivity;
import com.ictfa.krishiguru.myProfile.MyProfileActivity;
import com.ictfa.krishiguru.myProfile.UserProfileObject;
import com.ictfa.krishiguru.myProfile.UserType;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.ui.adapter.MarketGridAdapter;
import com.ictfa.krishiguru.ui.widget.ExpandableHeightGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class MarketActivity extends AppCompatActivity {
    SliderLayout sliderLayout;
    private ExpandableHeightGridView gridView;
    int[] titleArray = {R.string.khudraWholesell, R.string.kharid_bikri, R.string.kharidBikirDarta, R.string.bajarAnaylsis};
//    int[] imageArray = {R.drawable.ic_icon1, R.drawable.ic_icon2, R.drawable.ic_icon3, R.drawable.ic_icon4};
    private MarketGridAdapter adapter;
    private int loginStatus;
    SharedPreference mypref;
    private SharedPreference sharedPreference;
    CustomProgressDialog progressDialog;
    Alerts alerts;
    int userID;
    private ArrayList<Commodity> commodityList;
    private Realm realm;
    private ArrayList<SlideShowImage> listImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market2);
        getSupportActionBar().setTitle("बजार");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new CustomProgressDialog(this);
        sharedPreference = new SharedPreference(this);
        alerts = new Alerts(this);
        userID = sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
//        bannerLayout = (RelativeLayout)findViewById(R.id.sliderImage);
//        DisplayMetrics metrics = new DisplayMetrics();
//        ViewGroup.LayoutParams params = bannerLayout.getLayoutParams();
//       getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mypref = new SharedPreference(this);
        loginStatus = mypref.getIntValues(CommonDef.SharedPreference.IS_LOGIN);
        sharedPreference = new SharedPreference(this);

//        params.height = metrics.heightPixels / 3;

//        bannerLayout.setLayoutParams(params);

        sliderLayout = (SliderLayout) findViewById(R.id.marketBannerImg);
//        setImageSlider();
        gridView = (ExpandableHeightGridView) findViewById(R.id.marketGridView);
//        adapter = new MarketGridAdapter(MarketActivity.this, titleArray, imageArray);
        gridView.setAdapter(adapter);
        gridView.setExpanded(true);

        //get realm instance
        this.realm = RealmController.with(this).getRealm();
        listImage = new ArrayList<>();
        RealmResults<SlideShowImage> realmResults = this.realm.where(SlideShowImage.class).findAll();
        for (SlideShowImage image : realmResults)
            listImage.add(image);
        loadImage();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (position) {

                    case 0:
                        intent = new Intent(MarketActivity.this, WholesellRetailerPriceActivity.class);
                        startActivity(intent);
                        break;

                    case 1:
                        intent = new Intent(MarketActivity.this, KrishyakByapariMulyaActivity.class);
                        intent.putExtra("is_my_price", false);
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(MarketActivity.this, KharidBikriActivity.class);
                        startActivity(intent);
                        //finish();
                        break;
                    case 3:
                        getCommodityList();
                }
            }
        });


    }

    private void getCommodityList() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(MarketActivity.this);
        String url = UrlHelper.BASE_URL + "api/v3/market/commodity?apikey=" + UrlHelper.API_KEY;
        commodityList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONObject jsonResponse = object.getJSONObject("code");
                            int status = jsonResponse.getInt("status");
                            String message = jsonResponse.getString("message");
                            if (status != 1) {
                                alerts.showSuccessAlert(message);
                            } else {
                                Commodity commodity;
                                JSONArray librarylistJson = object.getJSONArray("data");
                                for (int i = 0; i < librarylistJson.length(); i++) {
                                    commodity = new Commodity();
                                    JSONObject type = (JSONObject) librarylistJson.get(i);
                                    commodity.id = type.getString("id");
                                    commodity.name = type.getString("commodity_name");
                                    commodityList.add(commodity);
                                }
                            }
//                            mAdapter.notifyDataSetChanged();
                            Toast.makeText(MarketActivity.this, "success", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(MarketActivity.this, BarChartActivity.class);
                            intent.putExtra(CommonDef.COMMODITY_LIST, commodityList);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        if (loginStatus== 1) {
//            menu.findItem(R.id.login).setTitle("लग आउट गर्नुहोस्");
//        }
//
//        menu.findItem(R.id.notifications).setVisible(false);
//        return true;
//    }

    void getUserProfile() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = UrlHelper.BASE_URL + "api/v3/user/" + userID + "/profile?apikey=" + UrlHelper.API_KEY;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONArray responseJson = object.getJSONArray("data");
//                            String status = jsonObject.getString("status");
//                            String messege = jsonObject.getString("message");
                            if (responseJson != null && responseJson.length() > 0) {
                                JSONObject jsonObject = (JSONObject) responseJson.get(0);
                                UserProfileObject userProfile = new UserProfileObject();
                                userProfile.name = jsonObject.getString("name");
                                userProfile.address = jsonObject.getString("location");
                                userProfile.phone = jsonObject.getString("phone");
                                userProfile.image = jsonObject.getString("image");
                                userProfile.UserType = UserType.TYPE_FARMER;
                                if (jsonObject.getString("user_type").equalsIgnoreCase("farmer")) {
                                    userProfile.UserType = UserType.TYPE_FARMER;
                                } else if (jsonObject.getString("user_type").equalsIgnoreCase("trader")) {
                                    userProfile.UserType = UserType.TYPE_TRADER;
                                    JSONArray marketJsonArray = jsonObject.getJSONArray("market");
                                    if (marketJsonArray.length() > 0) {
                                        JSONObject market = (JSONObject) marketJsonArray.get(0);
                                        userProfile.marketName = market.getString("market_name");
                                        userProfile.marketType = market.getString("market_type");
                                    }

                                } else if (jsonObject.getString("user_type").equalsIgnoreCase("organisation")) //change this to orgajisation
                                {
                                    userProfile.UserType = UserType.TYPE_ORGANISATION;
                                    JSONArray organisationJson = jsonObject.getJSONArray("organisation");
                                    if (organisationJson.length() > 0) {
                                        JSONObject organisation = (JSONObject) organisationJson.get(0);
                                        userProfile.org_name = organisation.getString("organization_name");
                                        userProfile.org_phone = organisation.getString("organization_phone");
                                        userProfile.org_type = organisation.getString("organization_type");
                                        userProfile.org_address = organisation.getString("organization_address");
                                        userProfile.toll_free_no = organisation.getString("organization_toll_free");
                                    }

                                }

                                Intent intent = new Intent(MarketActivity.this, MyProfileActivity.class);
                                intent.putExtra(CommonDef.USER_PROFILE, userProfile);
                                startActivity(intent);
                            } else
                                Toast.makeText(MarketActivity.this, "Error Occured", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadImage() {
//        sliderLayout.removeAllViews();

        sliderLayout.setDuration(3000);
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);
        sliderLayout.setMinimumHeight(CommonMethods.dpToPx(this, 250));
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

        sliderLayout.removeAllSliders();
        TextSliderView[] textSliderViews = new TextSliderView[listImage.size()];
        for (int i = 0; i < listImage.size(); i++) {
            textSliderViews[i] = new TextSliderView(MarketActivity.this);
            textSliderViews[i].image(listImage.get(i).getImage())
                    .description(listImage.get(i).getTitle())
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            sliderLayout.addSlider(textSliderViews[i]);
        }
    }

}
