package com.ictfa.krishiguru.ui.dtos;

/**
 * Created by sureshlama on 5/26/17.
 */

public class wholeSellRetailPriceDtos {

    public void setId(String id) {
        this.id = id;
    }

    String id;

    public String getsNum() {
        return sNum;
    }

    public void setsNum(String sNum) {
        this.sNum = sNum;
    }

    public String sNum;
    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    String product;
    String quantity;
    String retailPrice;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getWholeSellPrice() {
        return wholeSellPrice;
    }

    public void setWholeSellPrice(String wholeSellPrice) {
        this.wholeSellPrice = wholeSellPrice;
    }

    String wholeSellPrice;


}
