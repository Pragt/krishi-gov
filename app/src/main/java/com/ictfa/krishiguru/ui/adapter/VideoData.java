package com.ictfa.krishiguru.ui.adapter;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sureshlama on 2/27/17.
 */

public class VideoData extends RealmObject {
    public String getVideoThumbnail() {
        return videoThumbnail;
    }

    public void setVideoThumbnail(String videoThumbnail) {
        this.videoThumbnail = videoThumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    String videoThumbnail;
    String title;

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    @PrimaryKey
    String videoId;

}
