package com.ictfa.krishiguru.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.BeATrader.BeATraderActivity;
import com.ictfa.krishiguru.MultipleTraders.object.TraderType;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.login.LoginActivity;
import com.ictfa.krishiguru.myProfile.UserType;
import com.ictfa.krishiguru.ui.adapter.MarketTabAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class KharidBikriActivity extends AppCompatActivity {
    CustomProgressDialog progressDialog;
    Alerts alerts;
    SharedPreference preference;
    private TabLayout tabLayout;
    private ArrayList<TraderType> traderTypeList;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market);

        progressDialog = new CustomProgressDialog(this);
        alerts = new Alerts(this);
        preference = new SharedPreference(this);

        getSupportActionBar().setTitle(getResources().getString(R.string.kharidBikirDarta));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new MarketTabAdapter(getSupportFragmentManager(),
                KharidBikriActivity.this));
        viewPager.setOffscreenPageLimit(2);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        viewPager.setCurrentItem(0);

        viewPager.setAdapter(new MarketTabAdapter(getSupportFragmentManager(),
                KharidBikriActivity.this));
        viewPager.setOffscreenPageLimit(2);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        viewPager.setCurrentItem(0);

        if (preference.getStringValues(CommonDef.SharedPreference.USERTYPE).equalsIgnoreCase(UserType.TYPE_TRADER)) {
            tabLayout.setVisibility(View.VISIBLE);
        } else
            tabLayout.setVisibility(View.GONE);


        int i = tabLayout.getTabAt(viewPager.getCurrentItem()).getPosition();
        // Toast.makeText(KharidBikriActivity.this,tabLayout.ta,Toast.LENGTH_SHORT).show();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    //  tabLayout.getTabAt(0).getCustomView().setBackgroundColor(Color.parseColor("#198C19"));
                    // tabLayout.getTabAt(1).getIcon().setAlpha(100);
                    //  tabLayout.getTabAt(2).getIcon().setAlpha(100);
                } else if (tab.getPosition() == 1) {
                    //    tabLayout.getTabAt(0).getIcon().setAlpha(100);
                    //  tabLayout.getTabAt(1).getIcon().setAlpha(255);
                    //tabLayout.getTabAt(2).getIcon().setAlpha(100);

                } else if (tab.getPosition() == 2) {
                    //tabLayout.getTabAt(0).getIcon().setAlpha(100);
                    //tabLayout.getTabAt(1).getIcon().setAlpha(100);
                    //tabLayout.getTabAt(2).getIcon().setAlpha(255);

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            case R.id.be_a_trader:
                if (preference.getIntValues(CommonDef.SharedPreference.IS_LOGIN) == 1)
                    getTraderTypeList();
                else {
                    startActivity(new Intent(KharidBikriActivity.this, LoginActivity.class));
                }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getTraderTypeList() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = UrlHelper.BASE_URL + "api/v3/trader/type?apikey=" + UrlHelper.API_KEY;
        traderTypeList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        pd.hidepd();
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONArray traderTypeListJson = object.getJSONArray("data");
                            for (int i = 0; i < traderTypeListJson.length(); i++) {
                                JSONObject object1 = (JSONObject) traderTypeListJson.get(i);
                                TraderType traderType = new TraderType();
                                traderType.id = object1.getString("id");
                                traderType.type = object1.getString("type");
                                traderType.nepali_name = object1.getString("nepali_name");
                                traderTypeList.add(traderType);
                            }

                            Intent intent = new Intent(KharidBikriActivity.this, BeATraderActivity.class);
                            intent.putExtra(CommonDef.TRADER_TYPE, traderTypeList);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                pd.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.buy_sell_trader, menu);

        if (preference.getStringValues(CommonDef.SharedPreference.USERTYPE).equalsIgnoreCase(UserType.TYPE_TRADER)) {
            MenuItem item = menu.findItem(R.id.be_a_trader);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
