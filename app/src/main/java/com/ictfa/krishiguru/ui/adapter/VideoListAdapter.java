package com.ictfa.krishiguru.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sureshlama on 2/27/17.
 */

public class VideoListAdapter extends BaseAdapter {
        Context activity;
    private LayoutInflater inflater;
    private List<VideoData> videoDatas;

    public  VideoListAdapter(Context activity, List<VideoData> videoDatas) {
        this.activity = activity;
        this.videoDatas = videoDatas;
        inflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    private static class ViewHolder{
        ImageView imageView;
        TextView textView;
    }

    @Override
    public int getCount() {
        return this.videoDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return this.videoDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View convertedView= convertView;
        ViewHolder viewHolder;
        if(convertedView==null){
            convertedView= inflater.inflate(R.layout.custome_video_list, parent, false);
            viewHolder= new ViewHolder();
            viewHolder.imageView= (ImageView) convertedView.findViewById(R.id.imageThumbnil);
            viewHolder.textView= (TextView) convertedView.findViewById(R.id.videoTitle);
        convertedView.setTag(viewHolder);
        }else
            viewHolder= (ViewHolder) convertedView.getTag();
        viewHolder.textView.setText(videoDatas.get(position).getTitle());

        Picasso.with(activity).load(videoDatas.get(position).getVideoThumbnail()).resize(500, 500).into(viewHolder.imageView);
        /*if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.custome_video_list, null);*/


        return convertedView;
    }

}
