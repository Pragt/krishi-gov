package com.ictfa.krishiguru.ui.dtos;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by sureshlama on 5/15/17.
 */

public class Farmer_OtherPrice_List extends RealmObject implements Serializable {
    public String id;
    private String userImage;
    public String productName;
    public String productImage;
    public String createdAt;

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }


    String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProductNQuantity() {
        return productNQuantity;
    }

    public void setProductNQuantity(String productNQuantity) {
        this.productNQuantity = productNQuantity;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    String name;
    String address;
    String productNQuantity;
    String quality;
    String rate;
    String date;
    String type;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    String phone;
    int commentCount;

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

//    public ArrayList<Comment> getListComment() {
//        return listComment;
//    }
//
//    public void setListComment(ArrayList<Comment> listComment) {
//        this.listComment = listComment;
//    }
//
//    ArrayList<Comment> listComment;

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}