package com.ictfa.krishiguru.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;

import org.json.JSONException;
import org.json.JSONObject;

public class GridProductTypeActivity extends AppCompatActivity {
        private String id;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_product_type);
        Bundle extras = getIntent().getExtras();
        id = extras.getString("id");
        progressDialog = new ProgressDialog(this);
        productList();

    }


    private void productList(){
        RequestQueue queue = Volley.newRequestQueue(GridProductTypeActivity.this);
        //  String url ="http://www.google.com";

// Request a string response from the provided URL.
        progressDialog.setMessage(" लोड हुँदै... ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        String firsturl = BaseApplication.hostUrlProduct;
        String finalUrl = firsturl+id+BaseApplication.getProductfeatureList;
        Log.d("finalurl",finalUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // newsDatas.clear();

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            progressDialog.dismiss();



                           // newsAdapter.notifyDataSetChanged();
                            Log.d("JSONRESPONSE=>>>>>>>>", jsonObject.toString());
                        }

                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
                progressDialog.dismiss();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
