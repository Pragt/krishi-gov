package com.ictfa.krishiguru.ui.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ictfa.krishiguru.WeatherFragment.WeatherFragmentDay1;
import com.ictfa.krishiguru.WeatherFragment.WeatherFragmentDay2;
import com.ictfa.krishiguru.WeatherFragment.WeatherFragmentDay3;

/**
 * Created by sureshlama on 4/18/17.
 */

public class WeatherTabAdapter extends FragmentStatePagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[]{"आज", "भोली" , "पर्सी"};

    public WeatherTabAdapter (FragmentManager fm, Context context) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle b;
        switch (position) {
            case 0:
                WeatherFragmentDay1 fragment1 = new WeatherFragmentDay1();
                return fragment1;

            case 1:
                WeatherFragmentDay2 fragment2 = new WeatherFragmentDay2();
                return fragment2;
            case 2:
                WeatherFragmentDay3 fragment3 = new WeatherFragmentDay3();
                return fragment3;
            default:
                return new WeatherFragmentDay1();
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }


}
