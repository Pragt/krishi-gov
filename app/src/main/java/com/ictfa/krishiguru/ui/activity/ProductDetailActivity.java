package com.ictfa.krishiguru.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.Comment.CommentProductActivity;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;
import com.ictfa.krishiguru.ui.adapter.AccordAdapter;
import com.ictfa.krishiguru.ui.dtos.AccorDetailDtos;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.view.ViewHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductDetailActivity extends AppCompatActivity implements ObservableScrollViewCallbacks {
    @BindView(R.id.tv_comment_count)
    TextView tvCommentCount;
    @BindView(R.id.ll_comments)
    LinearLayout llComments;
    @BindView(R.id.ll_share)
    LinearLayout llShare;
    @BindView(R.id.ll_like)
    LinearLayout llLike;
    @BindView(R.id.likecounter)
    TextView tvLike;
    int commentCount;
    int likeCount;
    String isLiked;
    SharedPreference sharedPreference;
    int userId;
    private String image;
    private String name;
    private String id;
    private ImageView imageView;
    private CustomProgressDialog pd1;
    private String detailvalue;
    private List<AccorDetailDtos> accorDetailDtoses = new ArrayList<>();
    private AccordAdapter accordAdapter;
    private View mToolbarView;
    private View mListBackgroundView;
    private ObservableListView accoList;
    private int mParallaxImageHeight;
    private Toolbar toolbar;
    private FloatingActionButton fbtn;
    private TextView goLin;
    Alerts alerts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        Bundle extras = getIntent().getExtras();
        ButterKnife.bind(this);
        pd1 = new CustomProgressDialog(this);
        alerts = new Alerts(this);
        imageView = (ImageView) findViewById(R.id.detailImg);
        image = extras.getString("image");
        name = extras.getString("name");
        id = extras.getString("id");
        detailvalue = extras.getString("detail");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        commentCount = extras.getInt(CommonDef.COMMENT_COUNT);
        likeCount = extras.getInt(CommonDef.LIKE_COUNT);
        int shareCount = extras.getInt(CommonDef.SHARE_COUNT);
        isLiked = extras.getString(CommonDef.IS_LIKED);
        sharedPreference = new SharedPreference(this);
        userId = sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
        tvLike.setText(likeCount + " Likes");
        tvCommentCount.setText(commentCount + " Comments");

        if (isLiked.equalsIgnoreCase("true")) {
            tvLike.setTextColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        setTitle(name);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        pd1 = new CustomProgressDialog(this);
        Picasso.with(ProductDetailActivity.this).load(image).resize(500, 500).into(imageView);
        // intro = (TextView)findViewById(R.id.intro);
        fbtn = (FloatingActionButton) findViewById(R.id.floating_action_button_fab_with_listview);
        fbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailActivity.this, ProductTypeListActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);

            }
        });
        goLin = (TextView) findViewById(R.id.goLink);
        goLin.setText(name + " मा  आवश्यक किनमेल गर्नुहोस");

        goLin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailActivity.this, ProductTypeListActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("name", name);
                startActivity(intent);
            }
        });


        accoList = (ObservableListView) findViewById(R.id.accoList);
        accoList.setScrollViewCallbacks(this);
        accordAdapter = new AccordAdapter(ProductDetailActivity.this, accorDetailDtoses);
        accoList.setAdapter(accordAdapter);
        //  accoList.setExpanded(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProductDetailActivity.this, "cliked", Toast.LENGTH_SHORT).show();
            }
        });
        mToolbarView = findViewById(R.id.toolbar);
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, getResources().getColor(R.color.colorPrimary)));
        toolbar.setTitle(name);
        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.parallax_image_height);
        View paddingView = new View(this);
        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,
                mParallaxImageHeight);
        paddingView.setLayoutParams(lp);

        // This is required to disable header's list selector effect
        paddingView.setClickable(true);

        try {
            accoList.addHeaderView(paddingView);
        }catch (Exception exx)
        {

        }

        mListBackgroundView = findViewById(R.id.list_background);

        getData();


    }

    @OnClick(R.id.ll_comments)
    void onCOmmentsClicked ()
    {
        getComments();
    }

    private void getComments() {
        pd1.showpd(this.getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(this);
        String zone = UrlHelper.BASE_URL + "api/v3/production/comments/" + id + "?apikey=" + UrlHelper.API_KEY;
        final ArrayList<Comment> listComment = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd1.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray data = jsonObject.getJSONArray("data");
                            Comment comment;
                            if (data !=null && data.length() > 0)
                            {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject commentObj = data.getJSONObject(i);
                                    comment = new Comment();
                                    comment.comment_id = Integer.parseInt(commentObj.getString("id"));
                                    comment.comment = commentObj.getString("comment");
                                    comment.commented_by = commentObj.getString("commented_by");
                                    comment.date = commentObj.getString("created_at");
                                    listComment.add(comment);
                                }

                            }

                            Intent intent = new Intent(ProductDetailActivity.this, CommentProductActivity.class);
                            intent.putExtra(CommonDef.COMMENTS, listComment);
                            intent.putExtra(CommonDef.PRODUCT_ID, id);
                            startActivityForResult(intent, CommonDef.REQUEST_COMMENT);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(ProductDetailActivity.this.getResources().getString(R.string.no_internet_connection));
                pd1.hidepd();
                // mTextView.setText("That didn't work!");
            }



        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void getData() {
        accorDetailDtoses.clear();
        try {
            JSONArray value = new JSONArray(detailvalue);
            for (int i = 0; i < value.length(); i++) {
                JSONObject getValue = value.getJSONObject(i);
                AccorDetailDtos accorDetailDtos = new AccorDetailDtos();
                String subtitle = getValue.getString("subtitle");
                String description = getValue.getString("description");
                accorDetailDtos.setSubtitle(subtitle);
                accorDetailDtos.setDescription(description);
                accorDetailDtoses.add(accorDetailDtos);

            }
            accordAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.ll_share)
    void onShareClicked() {
        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle("")
                .setContentDescription(
                        "")

                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.ictfa.krishiguru"))

                .build();
        ShareDialog.show(this, linkContent);
    }

    @OnClick(R.id.ll_like)
    void onLikeClicked() {
        if (isLiked.equals("true")) {
            Toast.makeText(this, "you already like this", Toast.LENGTH_SHORT).show();
        } else if (userId == 0) {
            Toast.makeText(this, "you should login first", Toast.LENGTH_SHORT).show();

        } else {
            int likevalue = likeCount + 1;
            tvLike.setText(likevalue + " Like");
            sendLike(id, userId);
            tvLike.setTextColor(this.getResources().getColor(R.color.colorPrimaryDark));
            isLiked = "true";
        }
    }

    private void sendLike(String product_id, int UserID) {
        RequestQueue queue = Volley.newRequestQueue(this);
        //  String url ="http://www.google.com";
        String url = UrlHelper.BASE_URL + "api/v3/production/" + product_id + "/like?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA&user_id=" + UserID;
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("LIKERESPONSE", response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ProductDetailActivity.this, "not work", Toast.LENGTH_SHORT).show();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onScrollChanged(accoList.getCurrentScrollY(), false, false);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / mParallaxImageHeight);
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
        ViewHelper.setTranslationY(imageView, -scrollY / 2);

        // Translate list background
        ViewHelper.setTranslationY(mListBackgroundView, Math.max(0, -scrollY + mParallaxImageHeight));
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonDef.REQUEST_COMMENT)
            if (resultCode == RESULT_OK)
            {
                int count = data.getIntExtra(CommonDef.COMMENT_COUNT, 0);
                tvCommentCount.setText(count + " Comments");
            }
    }
}
