package com.ictfa.krishiguru.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;

/**
 * Created by sureshlama on 5/12/17.
 */

public class MarketGridAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    int[] title;
    int[] image;

    public MarketGridAdapter(Activity activity, int[] title, int[] image) {
        this.activity = activity;
        this.title= title;
        this.image=image;
    }


    @Override
    public int getCount() {
        return this.title.length;
    }

    @Override
    public Object getItem(int position) {
        return this.title[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.custome_market_grid, null);
        ImageView icon = (ImageView) convertView.findViewById(R.id.imageicon);
        TextView textViewTitle = (TextView) convertView.findViewById(R.id.title);
        textViewTitle.setText(title[position]);
        icon.setImageResource(image[position]);
        return convertView;

    }
}
