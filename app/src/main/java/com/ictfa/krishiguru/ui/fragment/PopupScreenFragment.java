package com.ictfa.krishiguru.ui.fragment;


import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.registerOrganization.object.Zone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class PopupScreenFragment extends DialogFragment {
    private Spinner zoneSpinner, districtSpinner;
    private Alerts alerts;
    private CustomProgressDialog pd;
    private ArrayList<Zone> zoneList;
    private ArrayList<String> zoneStringList;
    private ArrayList<Zone> districtList;
    private ArrayList<String> districtStringList;


    private WhenuserSelectLocation onSelectLocation;

    public PopupScreenFragment() {
        // Required empty public constructor
    }

    public void setListner(WhenuserSelectLocation listner) {
        onSelectLocation = listner;
    }

//    @Override
//    public void onAttach(Context context) {
////        super.onAttach(context);
////        this.mContext = (Activity) context;
//    }
//
//    @Override
//    public void onAttach(Activity activity) {
////        super.onAttach(activity);
////        this.mContext = (MyProfileActivity) activity;
//
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.dialog_theme);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_popup_screen, container, false);

        zoneSpinner = (Spinner) view.findViewById(R.id.spinnerZone);
        districtSpinner = (Spinner) view.findViewById(R.id.spinnerDistrict);
        alerts = new Alerts(getActivity());
        pd = new CustomProgressDialog(getActivity());

        getZone();
        zoneStringList = new ArrayList<>();
        loadZoneSpinner();

        zoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getItemAtPosition(position).toString();
                if (position != 0) {
                    getDistrict(zoneList.get(position - 1).id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Nothing to do
            }
        });

        ArrayAdapter<String> districtSpinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.district)); //selected item will look like a spinner set from XML
        districtSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        districtSpinner.setAdapter(districtSpinnerArrayAdapter);


        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String districtValue = districtSpinner.getSelectedItem().toString();

                if (position != 0) {
//                      lat = districtList.get(position).
//                    onSelectLocation.done(lat, lon);
                    onSelectLocation.location(districtValue, String.valueOf(districtList.get(position - 1).id));
                    //Toast.makeText(getActivity(),lat +lon,Toast.LENGTH_SHORT).show();
                    dismiss();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Nothing to do.
            }
        });


        return view;
    }


    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;

            d.getWindow().setLayout(width, height / 3);
        }
    }

    private void getDistrict(int id) {
        pd.showpd(getResources().getString(R.string.loading_district));
        RequestQueue queue = Volley.newRequestQueue(getContext());
        districtList = new ArrayList<>();
        districtStringList = new ArrayList<>();
        String zone = UrlHelper.BASE_URL + "api/v3/district/list/" + id + "?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //forumDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(response);
                            JSONArray listObject = object.getJSONArray("data");
//                             String id = object.getString(object.getString("id"));
                            JSONObject zoneObject;
                            Zone zone;
                            for (int i = 0; i < listObject.length(); i++) {
                                zone = new Zone();
                                zoneObject = (JSONObject) listObject.get(i);
                                zone.id = zoneObject.getInt("id");
                                zone.name = zoneObject.getString("name");
                                districtList.add(zone);
                                districtStringList.add(zoneObject.getString("name"));
                                Log.i("Prajit", "onResponse: " + zone.name);
                            }
                            loadDistrictSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadDistrictSpinner() {
        districtStringList.add(0, "जिल्ला छानुहोस");
        ArrayAdapter<String> districtSpinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, districtStringList); //selected item will look like a spinner set from XML
        districtSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        districtSpinner.setAdapter(districtSpinnerAdapter);
    }

    private void getZone() {
        pd.showpd(getResources().getString(R.string.loading_zone));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(getContext());
        zoneList = new ArrayList<>();
        String zone = UrlHelper.BASE_URL + "api/v3/zone/list?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //forumDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(response);
                            JSONArray listObject = object.getJSONArray("data");
//                             String id = object.getString(object.getString("id"));
                            JSONObject zoneObject;
                            zoneStringList = new ArrayList<>();
                            Zone zone;
                            for (int i = 0; i < listObject.length(); i++) {
                                zone = new Zone();
                                zoneObject = (JSONObject) listObject.get(i);
                                zone.id = zoneObject.getInt("id");
                                zone.name = zoneObject.getString("name");
                                zoneList.add(zone);
                                zoneStringList.add(zoneObject.getString("name"));
                                Log.i("Prajit", "onResponse: " + zone.name);
                            }
                            loadZoneSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                pd.hidepd();
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadZoneSpinner() {
        zoneStringList.add(0, "अञ्चल छानुहोस");
        ArrayAdapter<String> zoneSpinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, zoneStringList); //selected item will look like a spinner set from XML
        zoneSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        zoneSpinner.setAdapter(zoneSpinnerArrayAdapter);
    }

    public interface WhenuserSelectLocation {

        void location(String location, String district_id);
    }

}





