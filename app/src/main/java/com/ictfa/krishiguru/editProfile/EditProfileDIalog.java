package com.ictfa.krishiguru.editProfile;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.FileUtils;
import com.ictfa.krishiguru.helpers.GPSTracker;
import com.ictfa.krishiguru.helpers.ImagePicker;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myProfile.MyProfileActivity;
import com.ictfa.krishiguru.myProfile.UserProfileObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import cz.msebera.android.httpclient.Header;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.PermissionChecker.checkSelfPermission;

/**
 * Created by Prajeet on 10/09/2017.
 */

public class EditProfileDIalog extends DialogFragment implements LocationListener {
    SharedPreference sharedPreference;
    CustomProgressDialog pd;
    private ImageView ivProfilePic;
    private EditText edtName;
    private Button btnSave;
    private MyProfileActivity mContext;
    private String image;
    private Uri imageUri;
    private GPSTracker gps;
    private LocationManager locationManager;
    public String lat = "27.7172";
    public String lon = "85.3240";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = (MyProfileActivity) context;
    }

    @Override
    public void onAttach(Activity activity) {




        super.onAttach(activity);
        this.mContext = (MyProfileActivity) activity;

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        sharedPreference = new SharedPreference(getActivity());
        pd = new CustomProgressDialog((Activity) getActivity());
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_edit_profile, container, false);
        CommonMethods.setupUI(view.findViewById(R.id.rl_edit_profile), getActivity());
        ivProfilePic = (ImageView) view.findViewById(R.id.iv_profile_pic);
        edtName = (EditText) view.findViewById(R.id.edt_name);
        btnSave = (Button) view.findViewById(R.id.btn_save);
        UserProfileObject userProfile = mContext.userProfile;
        if (!userProfile.image.equalsIgnoreCase(""))
        {
            Picasso.with(mContext).load(userProfile.image).
                    placeholder(getResources().getDrawable(R.drawable.com_facebook_profile_picture_blank_square)).
                    into(ivProfilePic);
        }

        edtName.setText(userProfile.name);

        this.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doSave();
            }
        });

        ivProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
//            Toast.makeText(this,"You need have granted permission",Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(getActivity());

            // Check if GPS enabled
            if (!gps.canGetLocation()) {
                gps.showSettingsAlert();
            }
        }


        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        }catch (Exception exx){}

        return view;
    }

    private void doSave() {
        updateProfile();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CommonDef.REQUEST_STORAGE_CAMERA);
                return;
            }
        }
        startActivityForResult(ImagePicker.getPickImageChooserIntent((Activity) mContext), CommonDef.PICK_IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonDef.PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                imageUri = data.getData();
                ivProfilePic.setImageURI(imageUri);
                Bitmap imageBmp;
                try {
                    imageBmp = FileUtils.getThumbnail(getActivity(), imageUri, 1080);
                    image = FileUtils.storeBitmapToFile(imageBmp, getActivity());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else // From camera
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    image = ImagePicker.getImage.getAbsolutePath();
                } else
                    image = ImagePicker.outputFileUri.getPath();

                ivProfilePic.setImageURI(Uri.fromFile(new File(image)));
            }

        }
    }


    void updateProfile() {
        pd.showpd(getResources().getString(R.string.please_wait));
        RequestParams params = new RequestParams();

        if (image != null && !image.equalsIgnoreCase("")) {
            File myFile = new File(image);

            try {
                params.put("image", myFile, "image/jpeg");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
//        }


        params.put("name", edtName.getText().toString());
        params.put("lat", lat);
        params.put("lng", lon);

        //params.put();

        AsyncHttpClient client = new AsyncHttpClient();
        String url = UrlHelper.BASE_URL + "api/v3/user/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/update?apikey=" + UrlHelper.API_KEY;
        client.post(url, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                pd.hidepd();
                try {
                    Log.d("responseRegestration", responseBody.toString());
                    String status = responseBody.getString("status");
                    //Toast.makeText(RegistrationActivity.this, message,Toast.LENGTH_SHORT).show();

                    if (status.equals("1")) {
                        sharedPreference.setKeyValues(CommonDef.SharedPreference.USERNAME, edtName.getText().toString());
                        mContext.userProfile.name = edtName.getText().toString();
                        mContext.tvName.setText(edtName.getText().toString());
                        mContext.ivProfilePic.setImageURI(imageUri);
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.success), Toast.LENGTH_LONG).show();
                        dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    pd.hidepd();
                }

            }


        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            startActivityForResult(ImagePicker.getPickImageChooserIntent(getActivity()), CommonDef.PICK_IMAGE_REQUEST);
        }
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//
//        switch (requestCode) {
//            case 1: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//
//                    // contacts-related task you need to do.
//
//                    gps = new GPSTracker(LoginActivity.this);
//
//                    // Check if GPS enabled
//                    if (gps.canGetLocation()) {
//
//                        lat = String.valueOf(gps.getLatitude());
//                        lon = String.valueOf(gps.getLongitude());
//
//                        // \n is for new line
////                        Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + lat + "\nLong: " + lon, Toast.LENGTH_LONG).show();
//                    } else {
//                        // Can't get location.
//                        // GPS or network is not enabled.
//                        // Ask user to enable GPS/network in settings.
//                        gps.showSettingsAlert();
//                    }
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                        ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//
//                    }
//
//                    Toast.makeText(this, "You need to grant permission", Toast.LENGTH_SHORT).show();
//                }
//                return;
//            }
//        }
//    }

    @Override
    public void onLocationChanged(Location location) {
//        Toast.makeText(this, "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude(), Toast.LENGTH_SHORT).show();
        lat = String.valueOf(location.getLatitude());
        lon = String.valueOf(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}

