package com.ictfa.krishiguru.packages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Packages extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("base_price")
    @Expose
    public String basePrice;
    @SerializedName("tsc")
    @Expose
    public String tsc;
    @SerializedName("vat")
    @Expose
    public String vat;
    @SerializedName("total_price")
    @Expose
    public String totalPrice;
    @SerializedName("validity")
    @Expose
    public Integer validity;
    @SerializedName("created_at")
    @Expose
    public String createdAt;


}