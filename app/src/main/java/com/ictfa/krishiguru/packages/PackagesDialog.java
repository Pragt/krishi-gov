package com.ictfa.krishiguru.packages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.chabBot.model.SubscriptionTypeResponse;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.otp.AddMobileNumberDialogFragment;
import com.ictfa.krishiguru.retrofit.ApiClient;
import com.ictfa.krishiguru.retrofit.ApiInterface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static java.sql.DriverManager.println;

/**
 * Created by Prajeet on 07/10/2017.
 */

public class PackagesDialog extends DialogFragment implements PackagesAdapter.onItemCLickListener {
    @BindView(R.id.rvNotifications)
    RecyclerView rvNotifications;


    @BindView(R.id.tvNoNotification)
    TextView tvNoNotifiations;

    @BindView(R.id.tvMore)
    TextView tvMore;

    boolean isBasicPackageChoosed = false;
    int packageProductId = 0;
    String packageProductName = "";

    Alerts alerts;
    CustomProgressDialog pd;
    SharedPreference sharedPreference;
    private ArrayList<Packages> listPackages;
    private PackagesAdapter mAdapter;
    private boolean isFromNotification = false;
    private int total_count;
    private boolean isLoading;
    private boolean hasSetPackage;
    private PackageChooseListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_package_list, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Do all the stuff to initialize your custom view
        packageProductId = getArguments().getInt("packageProductId", 0);
        packageProductName = getArguments().getString("packageProductName");
        tvMore.setVisibility(View.GONE);

        init();
        pd.showpd(getString(R.string.please_wait));
        getPackages();

        listPackages = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvNotifications.setLayoutManager(layoutManager);
        listPackages = new ArrayList<>();
        mAdapter = new PackagesAdapter(getActivity(), listPackages);
        mAdapter.setListener(this);
        mAdapter.addAll(listPackages, isBasicPackageChoosed);
        rvNotifications.setAdapter(mAdapter);
    }

    @OnClick(R.id.tvMore)
    void onMoreCLicked() {
        if (!isBasicPackageChoosed) {
            isBasicPackageChoosed = true;
            tvMore.setText("Back");
        } else {
            isBasicPackageChoosed = false;
            tvMore.setText("थप प्याकेजहरु");
        }

        mAdapter.addAll(listPackages, isBasicPackageChoosed);
    }

    private void getPackages() {

        ApiClient.getClient().create(ApiInterface.class).getPackageList("O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA", String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            pd.hidepd();
                            listPackages.clear();
                            listPackages.addAll(s.data.listPackages);
                            mAdapter.addAll(listPackages, isBasicPackageChoosed);
                            tvMore.setVisibility(View.VISIBLE);
                            if (s.data.userFound) {
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_LOGIN, 1);
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_DEVICE_REGISTERED, true);
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.USER_ID, Integer.parseInt(s.data.login.id));
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.USERNAME, s.data.login.name);
                                sharedPreference.setKeyValues("priorities", s.data.login.priorities);
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.USERTYPE, s.data.login.userType);
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.VERIFIED_NUMBER, s.data.login.verifiedMobileNumber);
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, s.data.login.hasPackageSubscribed);

                                if (s.data.login.isNumberVerified.equalsIgnoreCase("1"))
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_NUMBER_VERIFIED, true);
                                if (s.data.login.isOtpSend.equalsIgnoreCase("1"))
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_OTP_SEND, true);
                            }
                        }
                        ,
                        e -> {
                            pd.hidepd();
                            alerts.showErrorAlert(e.getLocalizedMessage());
                            e.printStackTrace();
                        }, () ->
                                println("supervisor list"));


//        RequestQueue queue = Volley.newRequestQueue(this);
//        //  String url ="http://www.google.com";
//
//// Request a string response from the provided URL.
//        String url = UrlHelpers.BASE_URL + "subscription/packages?apikey=" + UrlHelper.API_KEY;
//        Log.d("notification", url);
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        swipeRefreshLayout.setRefreshing(false);
//
//                        mAdapter.notifyDataSetChanged();
//                        pd.hidepd();
//
//                        try {
////                            if (!isFromNotification)
////                                pd.hidepd();
//                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
//                            JSONObject jsonObject = new JSONObject(utfStr);
//                            JSONObject code = jsonObject.getJSONObject("code");
//                            int status = code.getInt("status");
//                            Packages packages;
//                            realm.executeTransaction(new Realm.Transaction() {
//                                @Override
//                                public void execute(Realm realm) {
//                                    realm.delete(Packages.class);
//                                }
//                            });
//
//                            JSONArray jsonArray = jsonObject.getJSONArray("data");
//                            listPackages = new ArrayList<>();
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                tvNoNotifiations.setVisibility(View.GONE);
//                                packages = new Packages();
//                                JSONObject jsonNotification = jsonArray.getJSONObject(i);
//                                packages.setId(jsonNotification.getInt("id"));
//                                packages.setName(jsonNotification.getString("name"));
//                                packages.setDescription(jsonNotification.getString("description"));
//                                packages.setBase_price(jsonNotification.getString("base_price"));
//                                packages.setTsc(jsonNotification.getString("tsc"));
//                                packages.setVat(jsonNotification.getString("vat"));
//                                packages.setTotal_price(jsonNotification.getString("total_price"));
//                                packages.setValidity(jsonNotification.getInt("validity"));
//                                packages.setCreated_at(jsonNotification.getString("created_at"));
//                                listPackages.add(packages);
//
//
//                                realm.beginTransaction();
//                                realm.copyToRealmOrUpdate(packages);
//                                realm.commitTransaction();
//                            }
//
//                            mAdapter.addAll(listPackages);
//
//                        } catch (JSONException e) {
//                            if (!isFromNotification)
//                                pd.hidepd();
//                            e.printStackTrace();
//                        } catch (UnsupportedEncodingException e) {
//                            if (!isFromNotification)
//                                pd.hidepd();
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                // mTextView.setText("That didn't work!");
//                try {
//                    if (!isFromNotification)
//                        pd.hidepd();
//                    swipeRefreshLayout.setRefreshing(false);
//                } catch (Exception exx) {
//
//                }
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("user_id", String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)));
//                return params;
//            }
//        };
//// Add the request to the RequestQueue.
//        queue.add(stringRequest);
    }


    private void init() {
        sharedPreference = new SharedPreference(getActivity());
        alerts = new Alerts(getActivity());
        pd = new CustomProgressDialog(getActivity());

    }

    @Override
    public void onItemClicked(Packages packages) {
//        if (sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN) == 1) {
        sharedPreference.setKeyValues(CommonDef.SharedPreference.PACKAGE_ID, packages.id);
        if (sharedPreference.getBoolValues(CommonDef.SharedPreference.IS_NUMBER_VERIFIED)) {
            buyPackage(packages);
        } else {
            FragmentManager fm = this.getChildFragmentManager();
            AddMobileNumberDialogFragment editNameDialogFragment = AddMobileNumberDialogFragment.newInstance("Some Title");
            editNameDialogFragment.setListener(new AddMobileNumberDialogFragment.onNumberAddSuccess() {
                @Override
                public void onSuccess() {
                    buyPackage(packages);
                }
            });
            editNameDialogFragment.show(fm, "fragment_edit_name");
        }
//        } else {
//            startActivity(new Intent(getActivity(), LoginActivity.class));
//        }
    }

    @Override
    public void onNewPackageBuy(Packages packages) {
        buyPackage(packages);
    }


    public void buyPackage(Packages packages) {

        alerts.buyPackageConfirmation("के तपाई यो प्याकेज किन्न चाहनुहुन्छ?", new Alerts.OnConfirmationClickListener() {
            @Override
            public void onYesClicked() {
                pd.showpd(getString(R.string.please_wait));
                ApiClient.getHttspClient().create(ApiInterface.class).subscribePackage(packages.id, "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA",
                        sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID), packageProductId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(s -> {
                                    pd.hidepd();
                                    if (s.code.status == 1) {
//                                        Intent intent = new Intent();
//                                        intent.putExtra("packageProductId", packageProductId);
//                                        intent.putExtra("packageProductName", packageProductName);
//                                        intent.putExtra("validity", packages.validity);
//                                        setResult(RESULT_OK, intent);
//                                        finish();
                                        listener.onPackageChoosed(packageProductId, packageProductName, packages.validity, s.getData());
                                        dismiss();
                                    } else if (s.code.status == 0) {
                                        alerts.showErrorAlert(s.code.message);
                                    }
                                }
                                ,
                                e -> {
                                    pd.hidepd();
                                    alerts.showErrorAlert("Could not connect to server. Please try again later.");
                                    e.printStackTrace();
                                }, () ->
                                        println("supervisor list"));
            }

            @Override
            public void onNoClicked() {

            }
        }, 100);

    }

    public void setListener(PackageChooseListener listener) {
        this.listener = listener;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface PackageChooseListener {
        void onPackageChoosed(int productId, String packageProductName, int validity, SubscriptionTypeResponse.Data data);
    }

}
