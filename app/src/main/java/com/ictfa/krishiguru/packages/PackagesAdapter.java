package com.ictfa.krishiguru.packages;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ictfa.krishiguru.R;

import java.util.ArrayList;

/**
 * Created by sureshlama on 2/24/17.
 */

public class PackagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM = 2;
    private static final int TYPE_PROGRESS = 1;
    private Activity activity;
    private ArrayList<Packages> packagesList;
    private onItemCLickListener listener;
    private Boolean hasSetPackge = false;

    public void setListener(onItemCLickListener listener) {
        this.listener = listener;

    }

    // ForumData formData;
    public PackagesAdapter(Activity activity, ArrayList<Packages> packagesList) {
        this.activity = activity;
        this.packagesList = packagesList;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(activity).inflate(R.layout.item_packages, parent, false);
        return new myViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myViewHolder) {
            myViewHolder holder1 = (myViewHolder) holder;
            if ((int) Double.parseDouble(packagesList.get(position).basePrice) != 0)
                holder1.tvPackageName.setText(packagesList.get(position).name + ": रु. " +(int)  (Double.parseDouble(packagesList.get(position).basePrice)) +"+  रु. " +  String.format("%.2f", Double.parseDouble(packagesList.get(position).tsc) + Double.parseDouble(packagesList.get(position).vat)) + "  कर");
            else
                holder1.tvPackageName.setText(packagesList.get(position).name);
            holder1.itemView.setOnClickListener(v -> {
                listener.onItemClicked(packagesList.get(position));
            });
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return packagesList.size();
    }


    public class myViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvPackageName;
//        private final TextView tvPackageDesc;
//        private final TextView tvPrice;
//        private final TextView tvSubscribe;
//        private final TextView tvValidity;
//        private final ExpandableLayout expandableLayout;
//        private final ConstraintLayout cvPackages;
//        private final Button btnUnSubscribe;

        public myViewHolder(View convertView) {
            super(convertView);
            tvPackageName = (TextView) convertView.findViewById(R.id.package_name);
//            tvPackageDesc = (TextView) convertView.findViewById(R.id.tvPackageDesc);
//            tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
//            tvValidity = (TextView) convertView.findViewById(R.id.tvValidity);
//            tvSubscribe = (TextView) convertView.findViewById(R.id.tvSubscribe);
//            btnUnSubscribe = convertView.findViewById(R.id.btnUnSubscribe);
//            cvPackages = convertView.findViewById(R.id.cvPackages);
//            expandableLayout = convertView.findViewById(R.id.expandable_layout_0);
//
//            });
        }
    }

    private class myProgressViewHolder extends RecyclerView.ViewHolder {
        public myProgressViewHolder(View view) {
            super(view);
        }
    }

    public void addAll(ArrayList<Packages> list, boolean isBasicChoosed) {
        packagesList = new ArrayList<>();
        if (!isBasicChoosed) {
            for (int i = 0; i < list.size(); i++) {
                packagesList.add(list.get(i));
                if (i == 1)
                    break;
            }
        } else {
            for (int i = 2; i < list.size(); i++) {
                packagesList.add(list.get(i));
                if (i == 1)
                    break;
            }
        }

        notifyDataSetChanged();
    }

    public interface onItemCLickListener {
        void onItemClicked(Packages packages);


        void onNewPackageBuy(Packages packageId);
    }

}
