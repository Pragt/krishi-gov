package com.ictfa.krishiguru.retrofit;

/**
 * Created by Prajeet Naga on 1/22/19, 11:14 PM.
 **/
public class UrlHelpers {
    public static final String BASE_URL = "http://admin.ict4agri.com/api/v3/";
    public static final String HTTP_BASE_URL = "https://admin.ict4agri.com/api/v3/";
}
