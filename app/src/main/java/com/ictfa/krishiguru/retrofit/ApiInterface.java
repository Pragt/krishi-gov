package com.ictfa.krishiguru.retrofit;

import com.ictfa.krishiguru.Weather.WeatherResponse;
import com.ictfa.krishiguru.awaskyakBiruwaSankhya.model.AwashyakBiruwaResponse;
import com.ictfa.krishiguru.baliPatro.model.BaliPatroResponse;
import com.ictfa.krishiguru.chabBot.adapter.DataToServer;
import com.ictfa.krishiguru.chabBot.adapter.TitleResponse;
import com.ictfa.krishiguru.chabBot.model.AskQuestionResponse;
import com.ictfa.krishiguru.chabBot.model.ChatHistoryResponse;
import com.ictfa.krishiguru.chabBot.model.ProductionListResponse;
import com.ictfa.krishiguru.chabBot.model.ProductionTechnologyResponse;
import com.ictfa.krishiguru.chabBot.model.SubscriptionTypeResponse;
import com.ictfa.krishiguru.chabBot.model.SubscriptionTypeResponseBot;
import com.ictfa.krishiguru.commonInterface.NormalResponse;
import com.ictfa.krishiguru.dashboard.NcellDataLoginResponse;
import com.ictfa.krishiguru.listPackageProduct.PackageProductResponse;
import com.ictfa.krishiguru.packages.PackagesResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Prajeet Naga on 1/22/19, 11:12 PM.
 **/
public interface ApiInterface {
    @GET("p_type_ask")
    Observable<ProductionListResponse> getProductionTechnology(@Query("apikey") String apiKey);

    @GET("p_type")
    Observable<ProductionListResponse> getProductionForBuyTechnology(@Query("apikey") String apiKey);

    @FormUrlEncoded
    @POST("titles/{production_id}/list")
    Observable<TitleResponse> getTitles(@Path(value = "production_id", encoded = true) int production_id,
                                        @Query("apikey") String apiKey,
                                        @Field("user_id") int userId);


    @FormUrlEncoded
    @POST("subscription/{package_id}/title")
    Observable<SubscriptionTypeResponseBot> getSubscriptionType(@Path(value = "package_id", encoded = true) int packageId,
                                                                @Query("apikey") String apiKey,
                                                                @Field("user_id") int user_id,
                                                                @Field("production_id") int subscriptionId);

    @FormUrlEncoded
    @POST("package/{package_id}/subscribe")
    Observable<SubscriptionTypeResponse> subscribePackage(@Path(value = "package_id", encoded = true) int packageId,
                                                          @Query("apikey") String apiKey,
                                                          @Field("user_id") int user_id,
                                                          @Field("production_id") int subscriptionId);

    @FormUrlEncoded
    @POST("ask/{package_id}/question")
    Observable<AskQuestionResponse> askToExpert(@Path(value = "package_id", encoded = true) int packageId,
                                                @Query("apikey") String apiKey,
                                                @Field("user_id") int user_id,
                                                @Field("production_type_id") int typeId,
                                                @Field("question") String question
    );

    @Multipart
    @POST("ask/{package_id}/question")
    Observable<AskQuestionResponse> askToExpertWithImage(@Path(value = "package_id", encoded = true) int packageId,
                                                         @Query("apikey") String apiKey,
                                                         @Part("user_id") RequestBody user_id,
                                                         @Part("production_type_id") RequestBody typeId,
                                                         @Part("technology_id") RequestBody technologyId,
                                                         @Part("question") RequestBody question,
                                                         @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("log/{user_id}/history")
    Observable<ChatHistoryResponse> getChatHistory(@Path(value = "user_id", encoded = true) int userId,
                                                   @Query("apikey") String apiKey,
                                                   @Field("timestamp") Long timestamp);

    @POST("bot/{user_id}/log")
    Observable<SubscriptionTypeResponse> syncToServer(@Path(value = "user_id", encoded = true) int userId,
                                                      @Query("apikey") String apiKey,
                                                      @Body List<DataToServer> listData
    );

    @GET("technology/{production_id}/list")
    Observable<ProductionTechnologyResponse> getTechnology(@Path(value = "production_id", encoded = true) int production_id,
                                                           @Query("apikey") String apiKey);


    @FormUrlEncoded
    @POST("technology/18/list")
    Observable<PackageProductResponse> getPackageProduct(@Query("apikey") String apiKey, @Field("user_id") String userId);

    @GET("technology_ask/{production_id}/list")
    Observable<ProductionTechnologyResponse> getTechnologyAsk(@Path(value = "production_id", encoded = true) int production_id,
                                                              @Query("apikey") String apiKey);

    @FormUrlEncoded
    @POST("package/{package_id}/unsubscribe")
    Observable<NormalResponse> unsubscribe(@Path(value = "package_id", encoded = true) int packageId,
                                           @Field("user_id") int user_id,
                                           @Query("apikey") String apiKey
    );

    @POST("eligible/{user_id}/ask")
    Observable<NormalResponse> checkUserEligible(@Path(value = "user_id", encoded = true) int packageId,
                                                 @Query("apikey") String apiKey
    );

    @GET("ncell/data")
    Observable<NcellDataLoginResponse> checkIfNcellData(@Query("apikey") String apiKey
    );

    @FormUrlEncoded
    @POST("subscription/packages?")
    Observable<PackagesResponse> getPackageList(@Query("apikey") String apiKey,
                                                @Field("user_id") String userId
    );

    @GET("weather/{user_id}/forecast?")
    Observable<WeatherResponse> getWeather(@Path(value = "user_id", encoded = true) String user_id,
                                           @Query("apikey") String apiKey,
                                           @Query("lat") String lat,
                                           @Query("lng") String lng
    );

    @GET("crop/calendar")
    Observable<BaliPatroResponse> getBaliPatroResponse(@Query("apikey") String apiKey
    );

    @GET("crop/calculator")
    Observable<AwashyakBiruwaResponse> getCropCalculatorResponse(@Query("apikey") String apiKey
    );
}
