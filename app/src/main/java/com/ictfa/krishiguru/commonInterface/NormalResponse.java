package com.ictfa.krishiguru.commonInterface;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ictfa.krishiguru.chabBot.model.ProductionListResponse;

import java.util.List;

public class NormalResponse {
    @SerializedName("meta")
    @Expose
    public List<Object> meta = null;
    @SerializedName("code")
    @Expose
    public ProductionListResponse.Code code;
}