package com.ictfa.krishiguru.dashboard;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ictfa.krishiguru.MultipleTraders.object.TraderType;
import com.ictfa.krishiguru.Notifications.NotificationObject;
import com.ictfa.krishiguru.Notifications.NotificationsFragment;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.Weather.WeatherObject;
import com.ictfa.krishiguru.Weather.WeatherResponse;
import com.ictfa.krishiguru.baliPatro.BaliPatroActivity;
import com.ictfa.krishiguru.byawasaikUtpadanPrabidhi.KrishiTechnology;
import com.ictfa.krishiguru.chabBot.ChatBotActivity;
import com.ictfa.krishiguru.graph.BarChartActivity;
import com.ictfa.krishiguru.graph.Commodity;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.GPSTracker;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.krishilibrary.KrishiLibraryActivity;
import com.ictfa.krishiguru.krishilibrary.Object.LibraryType;
import com.ictfa.krishiguru.listPackageProduct.PackageProductFragment;
import com.ictfa.krishiguru.login.LoginActivity;
import com.ictfa.krishiguru.malkhadCalculator.MalkhadCalculatorActivity;
import com.ictfa.krishiguru.myOrganization.view.HamroOrganizationActivity;
import com.ictfa.krishiguru.myProfile.MyProfileActivity;
import com.ictfa.krishiguru.myProfile.UserProfileObject;
import com.ictfa.krishiguru.myProfile.UserType;
import com.ictfa.krishiguru.news.NewsListActivity;
import com.ictfa.krishiguru.prathamiktaBadalnuhosh.PrathamiktaActivity;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.retrofit.ApiClient;
import com.ictfa.krishiguru.retrofit.ApiInterface;
import com.ictfa.krishiguru.ui.activity.WeatherActivity;
import com.ictfa.krishiguru.ui.activity.YoutubeListActivity;
import com.ictfa.krishiguru.utilities.LandMeasurement;
import com.ictfa.krishiguru.utilities.WeightMeasurement;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static java.sql.DriverManager.println;

/**
 * this is a activity that displayed first to users.
 */

public class DashBoardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LocationListener {

    public DrawerLayout drawer;
    public String lat = "27.7172";
    public String lon = "85.3240";
    SharedPreference sharedPreference;
    Alerts alerts;
    CustomProgressDialog progressDialog;
    boolean doubleBackToExitPressedOnce = false;
    int notification_count = 0;
    BottomNavigationView mBottomNavigationView;
    FloatingActionButton floatingActionButton;
    WeatherResponse.Forecasts forecasts;
    private NavigationView navigationView;
    private Realm realm;
    private Menu menu;
    private ArrayList<LibraryType> libraryTypeList;
    private int loginStatus;
    private GPSTracker gps;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.home:
                    fragment = new DashBoardFragment();
                    loadFragment(fragment);
//                    floatingActionButton.setVisibility(View.VISIBLE);
                    return true;
                case R.id.more:
                    fragment = new FragmentMore();
                    loadFragment(fragment);
//                    floatingActionButton.setVisibility(View.VISIBLE);
                    return true;
                case R.id.notification:
                    fragment = new NotificationsFragment();
                    loadFragment(fragment);
//                    floatingActionButton.setVisibility(View.GONE);
                    return true;

                case R.id.packages:
                    fragment = new PackageProductFragment();
                    loadFragment(fragment);
//                    floatingActionButton.setVisibility(View.VISIBLE);
                    return true;
                default:
                    if (sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN) == 1) {
                        if (sharedPreference.getStringValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED).equalsIgnoreCase("1")) {
                            startActivity(new Intent(DashBoardActivity.this, ChatBotActivity.class));
                        } else {
                            mBottomNavigationView.getMenu().getItem(1).setChecked(true);
                            Fragment fragment1 = new PackageProductFragment();
                            loadFragment(fragment1);
                            alerts.showToastMsg("Please choose any one of the following package to continue");
                        }
                    } else {
                        startActivity(new Intent(DashBoardActivity.this, LoginActivity.class));
                    }
                    return true;

            }
        }
    };

    @SuppressLint("RestrictedApi")
    @NotNull
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        try {
            setSupportActionBar(toolbar); // failure in OS version 4
        } catch (Exception exx) {

        }
        ButterKnife.bind(this);
        alerts = new Alerts(this);
        progressDialog = new CustomProgressDialog(this);
        sharedPreference = new SharedPreference(this);
        floatingActionButton = findViewById(R.id.fab);
        lat = sharedPreference.getStringValues(CommonDef.SharedPreference.LATUTUDE);
        lon = sharedPreference.getStringValues(CommonDef.SharedPreference.LONGITUDE);

        //get realm instance
        this.realm = RealmController.with(this).getRealm();
//        sharedPreference.setKeyValues(CommonDef.SharedPreference.USER_ID, 4201);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        getSupportActionBar().setIcon(R.drawable.img_logo);
        toggle.syncState();
        JSONObject weatherObj = new JSONObject();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mBottomNavigationView = findViewById(R.id.bottomNavigationView);
        mBottomNavigationView.setItemIconTintList(null);
        initNavigationDrawer();
        //  banner = (RelativeLayout)findViewById(R.id.locationValue);


        //  Toast.makeText(DashBoardActivity.this,userID,Toast.LENGTH_SHORT).show();
        loginStatus = sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN);

//        getWeatherData("27.7172", "85.3240");

        loadFragment(new DashBoardFragment());
        mBottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigationView.setItemIconTintList(null);
//        removeShiftMode(mBottomNavigationView);

        floatingActionButton.setOnClickListener(v -> {
            if (sharedPreference.getStringValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED).equalsIgnoreCase("1")) {
                startActivity(new Intent(this, ChatBotActivity.class));
            } else {
                mBottomNavigationView.getMenu().getItem(1).setChecked(true);
                Fragment fragment = new PackageProductFragment();
                loadFragment(fragment);
                alerts.showToastMsg("Please choose any one of the following package to continue");
            }
        });

        if (getIntent().getExtras() != null) {
            int ref_id = getIntent().getExtras().getInt("ref_id");
            int type = getIntent().getExtras().getInt("type");
            if (ref_id != 0 && type != 0) {
                mBottomNavigationView.getMenu().getItem(3).setChecked(true);
                Fragment fragment = new NotificationsFragment();
                Bundle intent = new Bundle();
                intent.putInt("ref_id", ref_id);
                intent.putInt("type", type);
                fragment.setArguments(intent);
                loadFragment(fragment);
                floatingActionButton.setVisibility(View.GONE);
            }

//            if (ref_id != 0 && type != 0) {
//                onNotificationClickListner(ref_id, type, -1, -1);
//            } else {
////                pd.showpd(getResources().getString(R.string.please_wait));
//            }
//            isFromNotification = getIntent().getExtras().getBoolean("is_from_notification");
        } else {
//            pd.showpd(getResources().getString(R.string.please_wait));
        }

    }

    private void initNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {


            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                Intent share;
                int id = menuItem.getItemId();
                Intent intent;
                switch (id) {
                    case R.id.packages:
                        mBottomNavigationView.setSelectedItemId(R.id.packages);
                        drawer.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.animalMeasure:
                        if (!menuItem.isChecked()) {
//                            navigationView.setSelected(false);
//                            menuItem.setChecked(false);
                            drawer.closeDrawer(GravityCompat.START);
                            intent = new Intent(DashBoardActivity.this, WeightMeasurement.class);
                            startActivity(intent);
                        }
                        break;
                    case R.id.landMeasure:
                        intent = new Intent(DashBoardActivity.this, LandMeasurement.class);
                        startActivity(intent);
                        break;
                    case R.id.krishiNews:
                        intent = new Intent(DashBoardActivity.this, NewsListActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.krishiTech:
                        intent = new Intent(DashBoardActivity.this, KrishiTechnology.class);
                        startActivity(intent);
                        break;
                    case R.id.krishi_baazar:
                        getCommodityList();
                        break;
                    case R.id.hamro_sanstha:
                        intent = new Intent(DashBoardActivity.this, HamroOrganizationActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.krishi_library:
                        loadLibrary();
                        break;
                    case R.id.krishi_video:
                        intent = new Intent(DashBoardActivity.this, YoutubeListActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.weatherForecast:
                        getWeatherData(DashBoardFragment.lat, DashBoardFragment.lon);
//                        if (ContextCompat.checkSelfPermission(DashBoardActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DashBoardActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                            ActivityCompat.requestPermissions(DashBoardActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//
//                        } else {
////            Toast.makeText(this,"You need have granted permission",Toast.LENGTH_SHORT).show();
//                            gps = new GPSTracker(DashBoardActivity.this);
//
//                            // Check if GPS enabled
//                            if (!gps.canGetLocation()) {
//                                gps.showSettingsAlert();
//                            }
//                        }
//
//                        try {
//                            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, DashBoardActivity.this);
//                        } catch (Exception exx) {
//                        }

                        break;
                    case R.id.malkhad_calculator:
                        intent = new Intent(DashBoardActivity.this, MalkhadCalculatorActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.bali_patro:
                        intent = new Intent(DashBoardActivity.this, BaliPatroActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.invite_friends:

                    case R.id.share:
                        share = new Intent(android.content.Intent.ACTION_SEND);
                        share.setType("text/plain");
                        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                        // Add data to the intent, the receiving app will decide
                        // what to do with it.
                        share.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
                        share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.ictfa.krishiguru&hl=en");

                        startActivity(Intent.createChooser(share, "Share link!"));
                        break;

                    // Add data to the intent, the receiving app will decide
                    // what to do with it.

                    case R.id.rate_us:
                        Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        // To count with Play market backstack, After pressing back button,
                        // to taken back to our application, we need to add following flags to intent.
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                        }
                        break;


                }
                return true;
            }
        });
    }


    private void getWeatherData(String lat, String lon) {
        if (DashBoardFragment.lat.isEmpty() || DashBoardFragment.lat == "0.0") {
            lat = "27.7172";
            lon = "85.3240";
        } else {
            lat = DashBoardFragment.lat;
            lon = DashBoardFragment.lon;
        }


        progressDialog.showpd(getString(R.string.please_wait));
        ApiClient.getClient().create(ApiInterface.class).getWeather(String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)), "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA", lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            progressDialog.hidepd();
                            forecasts = s.data.forecasts;
                            Intent intent = new Intent(DashBoardActivity.this, WeatherActivity.class);
                            intent.putExtra(CommonDef.LIST_WEATHER, forecasts);
                            intent.putExtra(CommonDef.LOCATION, s.data.location);
                            startActivity(intent);
                        }
                        ,
                        e -> {
                            progressDialog.hidepd();
                            alerts.showErrorAlert(e.getLocalizedMessage());
                            e.printStackTrace();
                        }, () ->
                                println("supervisor list"));
    }


    private void getWeatherData(String district) {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(DashBoardActivity.this);
        ArrayList<WeatherObject> listWeather = new ArrayList<>();
        String Weather = UrlHelper.BASE_URL + "api/v3/weather/forecast/" + district + "?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Weather,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //forumDatas.clear();
                        progressDialog.hidepd();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

                            JSONObject jsonObject = new JSONObject(utfStr);
                            WeatherObject weatherObject;
                            JSONArray jsonArray = jsonObject.getJSONArray("forecast");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                weatherObject = new WeatherObject();
                                JSONObject newsValue = jsonArray.getJSONObject(i);
                                JSONObject temperature = newsValue.getJSONObject("temperature");
                                String minTemp = temperature.getString("min");
                                String maxTemp = temperature.getString("max");
                                weatherObject.minTemp = minTemp;
                                weatherObject.maxTemp = maxTemp;

                                JSONObject Night;
                                JSONObject Day;

                                Night = newsValue.getJSONObject("night");

                                Day = newsValue.getJSONObject("day");


                                String desc = Night.getString("text");
                                String probabilityOfRain = Night.getString("probability_of_rain");
                                double hours_of_rain = Night.getDouble("hours_of_rain");
                                String thunderstorm = Night.getString("thunderstorm");
                                String rain = Night.getString("rain");
                                String iconNight = Night.getString("icon");
                                weatherObject.nightDescription = desc;
                                weatherObject.chanceOfRainNight = probabilityOfRain;
                                weatherObject.durationOfRainNight = hours_of_rain + "";
                                weatherObject.chanceOfThunderNight = thunderstorm;
                                weatherObject.iconNight = iconNight;

                                String descDay = Day.getString("text");
                                String probabilityOfRainDay = Day.getString("probability_of_rain");
                                double hours_of_rain_day = Day.getDouble("hours_of_rain");
                                String thunderstormDay = Day.getString("thunderstorm");
                                String rainDay = Day.getString("rain");
                                String iconDay = Day.getString("icon");

                                weatherObject.dayDescription = descDay;
                                weatherObject.chanceOfRainDay = probabilityOfRainDay;
                                weatherObject.durationOfRainDay = hours_of_rain_day + "";
                                weatherObject.chanceOfThunderDay = thunderstormDay;
                                weatherObject.iconDay = iconDay;

                                listWeather.add(weatherObject);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
                progressDialog.hidepd();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
/**
 if (id == R.id.nav_camera) {
 // Handle the camera action
 } else if (id == R.id.nav_gallery) {

 } else if (id == R.id.nav_slideshow) {

 } else if (id == R.id.nav_manage) {

 } else if (id == R.id.nav_share) {

 } else if (id == R.id.nav_send) {

 }
 **/
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkIfNcellData();
        getUserProfile(false);
        navigationView.setCheckedItem(0);
        View view = navigationView.getHeaderView(0);
        TextView tvName = view.findViewById(R.id.tv_name);
        TextView tvAddress = view.findViewById(R.id.tv_address);
        Button btnLogin = view.findViewById(R.id.btnLogin);
        ImageView ivProfilePic = view.findViewById(R.id.iv_profile_pic);
//        forumDatas = new ArrayList<>();
        loginStatus = sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN);
        if (loginStatus == 1) {
            tvName.setText(sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME));
            tvAddress.setText(sharedPreference.getStringValues(CommonDef.SharedPreference.ADDRESS));

            if (!sharedPreference.getStringValues(CommonDef.SharedPreference.USER_IMAGE).equalsIgnoreCase("")) {
                Picasso.with(this).load(sharedPreference.getStringValues(CommonDef.SharedPreference.USER_IMAGE)).
                        placeholder(getResources().getDrawable(R.drawable.com_facebook_profile_picture_blank_square)).
                        into(ivProfilePic);
            }

            btnLogin.setText(getString(R.string.log_out));

        } else {
            tvName.setText(getString(R.string.app_name));
            btnLogin.setText(getString(R.string.login));
            tvAddress.setText("ललितपुर, नेपाल");
            ivProfilePic.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.splash_icon));

            ivProfilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getUserProfile(true);
                }
            });

            tvName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getUserProfile(true);
                }
            });

        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginStatus == 1) {
                    alerts.showConfirmationDialog("के तपाई लग आउट गर्न चाहनु हुन्छ ?", new Alerts.OnConfirmationClickListener() {
                        @Override
                        public void onYesClicked() {
                            sharedPreference.clearData();
                            loginStatus = 0;
                            tvName.setText(getString(R.string.app_name));
                            btnLogin.setText(getString(R.string.login));
                            ivProfilePic.setImageDrawable(ContextCompat.getDrawable(DashBoardActivity.this, R.drawable.splash_icon));
                            realm.beginTransaction();
                            realm.deleteAll();
                            realm.commitTransaction();
                            onResume();
                        }

                        @Override
                        public void onNoClicked() {

                        }
                    }, 100);
                } else {
                    Intent intent = new Intent(DashBoardActivity.this, LoginActivity.class);
                    intent.putExtra("is_from_login", true);
                    startActivityForResult(intent, CommonDef.IS_FROM_LOGIN);
                }
            }
        });

//
//        if (sharedPreference.getBoolValues(CommonDef.SharedPreference.IS_DEVICE_REGISTERED) || loginStatus == 1) {
//            getNotifications();
//            showNotificationIcon();
//        } else
//            registerDevice();
//        try {
//        } catch (Exception exx) {
//
//        }
    }

    private void checkIfNcellData() {
        if (loginStatus == 0) {
            ApiClient.getClient().create(ApiInterface.class).checkIfNcellData("O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                                if (s.data.userFound) {
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_LOGIN, 1);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_DEVICE_REGISTERED, true);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.USER_ID, Integer.parseInt(s.data.login.id));
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.USERNAME, s.data.login.name);
                                    sharedPreference.setKeyValues("priorities", s.data.login.priorities);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.USERTYPE, s.data.login.userType);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.VERIFIED_NUMBER, s.data.login.verifiedMobileNumber);
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, s.data.login.hasPackageSubscribed);

                                    if (s.data.login.isNumberVerified.equalsIgnoreCase("1"))
                                        sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_NUMBER_VERIFIED, true);
                                    if (s.data.login.isOtpSend.equalsIgnoreCase("1"))
                                        sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_OTP_SEND, true);

                                    setLoggedIn();
                                }
                            }
                            ,
                            e -> {
//                            alerts.showErrorAlert("Could not connect to server. Please try again later.");
                                e.printStackTrace();
                            }, () ->
                                    println("supervisor list"));
        }
    }

    private void setLoggedIn() {
        getUserProfile(false);
        navigationView.setCheckedItem(0);
        View view = navigationView.getHeaderView(0);
        TextView tvName = view.findViewById(R.id.tv_name);
        TextView tvAddress = view.findViewById(R.id.tv_address);
        Button btnLogin = view.findViewById(R.id.btnLogin);
        ImageView ivProfilePic = view.findViewById(R.id.iv_profile_pic);
//        forumDatas = new ArrayList<>();
        loginStatus = sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN);
        if (loginStatus == 1) {
            tvName.setText(sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME));
            tvAddress.setText(sharedPreference.getStringValues(CommonDef.SharedPreference.ADDRESS));

            if (!sharedPreference.getStringValues(CommonDef.SharedPreference.USER_IMAGE).equalsIgnoreCase("")) {
                Picasso.with(this).load(sharedPreference.getStringValues(CommonDef.SharedPreference.USER_IMAGE)).
                        placeholder(getResources().getDrawable(R.drawable.com_facebook_profile_picture_blank_square)).
                        into(ivProfilePic);
            }

            btnLogin.setText(getString(R.string.log_out));

        }
    }

    void getUserProfile(boolean goToProfile) {
        if (goToProfile)
            progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = UrlHelper.BASE_URL + "api/v3/user/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/profile?apikey=" + UrlHelper.API_KEY;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //forumDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                            JSONObject object = new JSONObject(utfStr);
                            JSONArray responseJson = object.getJSONArray("data");
//                            String status = jsonObject.getString("status");
//                            String messege = jsonObject.getString("message");
                            if (responseJson != null && responseJson.length() > 0) {
                                JSONObject jsonObject = (JSONObject) responseJson.get(0);
                                UserProfileObject userProfile = new UserProfileObject();
                                userProfile.name = jsonObject.getString("name");
                                userProfile.address = jsonObject.getString("location");
                                userProfile.phone = jsonObject.getString("phone");
                                userProfile.image = jsonObject.getString("image");
                                userProfile.UserType = UserType.TYPE_FARMER;
                                loadUserProfile(userProfile);
                                if (jsonObject.getString("user_type").equalsIgnoreCase("farmer")) {
                                    userProfile.UserType = UserType.TYPE_FARMER;
                                } else if (jsonObject.getString("user_type").equalsIgnoreCase("trader")) {
                                    userProfile.UserType = UserType.TYPE_TRADER;
                                    JSONArray marketJsonArray = jsonObject.getJSONArray("market");
                                    if (marketJsonArray.length() > 0) {
                                        JSONObject market = (JSONObject) marketJsonArray.get(0);
                                        userProfile.marketName = market.getString("market_name");
                                        userProfile.marketType = market.getString("market_type");
                                    }

                                } else if (jsonObject.getString("user_type").equalsIgnoreCase("organisation")) //change this to orgajisation
                                {
                                    userProfile.UserType = UserType.TYPE_ORGANISATION;
                                    JSONArray organisationJson = jsonObject.getJSONArray("organisation");
                                    if (organisationJson.length() > 0) {
                                        JSONObject organisation = (JSONObject) organisationJson.get(0);
                                        userProfile.id = organisation.getString("id");
                                        userProfile.org_name = organisation.getString("organization_name");
                                        userProfile.org_phone = organisation.getString("organization_phone");
                                        userProfile.org_type = organisation.getString("organization_type");
                                        userProfile.org_address = organisation.getString("organization_address");
                                        userProfile.toll_free_no = organisation.getString("organization_toll_free");
                                    }

                                }
                                if (goToProfile) {
                                    Intent intent = new Intent(DashBoardActivity.this, MyProfileActivity.class);
                                    intent.putExtra(CommonDef.USER_PROFILE, userProfile);
                                    startActivity(intent);
                                }
                            }
//                            else
//                                Toast.makeText(DashBoardActivity.this, "Error Occured", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
//                            alerts.showToastMsg(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadUserProfile(UserProfileObject userProfile) {
        View view = navigationView.getHeaderView(0);
        TextView tvName = view.findViewById(R.id.tv_name);
        TextView tvAddress = view.findViewById(R.id.tv_address);
        ImageView ivProfilePic = view.findViewById(R.id.iv_profile_pic);
        tvName.setText(userProfile.name);
        tvAddress.setText(userProfile.address);
        sharedPreference.setKeyValues(CommonDef.SharedPreference.ADDRESS, userProfile.address);
        sharedPreference.setKeyValues(CommonDef.SharedPreference.USER_IMAGE, userProfile.image);

        if (!userProfile.image.equalsIgnoreCase("")) {
            Picasso.with(this).load(userProfile.image).
                    placeholder(getResources().getDrawable(R.drawable.com_facebook_profile_picture_blank_square)).
                    into(ivProfilePic);
        }

        ivProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserProfile(true);
            }
        });

        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserProfile(true);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "कृपया बाहिर निस्कन पुनः BACK थिचनु होला |", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }


    private void showNotificationIcon() {
        try {
            menu.findItem(R.id.notifications).setVisible(true);
            menu.findItem(R.id.notifications).setTitle(notification_count + "");
            View rlNotifications = menu.findItem(R.id.notifications).getActionView();
            TextView tvNotificationCount = rlNotifications.findViewById(R.id.tv_notifications_count);
            tvNotificationCount.setText(notification_count + "");
        } catch (Exception exx) {

        }
    }

    private void hideNotificationIcon() {
        if (menu != null)
            menu.findItem(R.id.notifications).setVisible(false);
    }


    private void registerDevice() {
//        progressDialog.showpd(getResources().getString(R.string.please_wait));
        if (loginStatus == 0 && !sharedPreference.getBoolValues(CommonDef.SharedPreference.IS_DEVICE_REGISTERED)) {
            RequestQueue queue = Volley.newRequestQueue(this);
            String url = UrlHelper.BASE_URL + "api/v3/user/anonymous?apikey=" + UrlHelper.API_KEY;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                        pd.hidepd();
//                        progressDialog.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                                JSONObject object = new JSONObject(utfStr);
                                JSONObject code = object.getJSONObject("code");
                                JSONObject user = code.getJSONObject("user");

                                String userId = user.getString("id");
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_DEVICE_REGISTERED, true);
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.ANYNOMOUS_USER_ID, Integer.parseInt(userId));
                                getNotifications();

                            } catch (JSONException e) {
                                e.printStackTrace();
//                                alerts.showToastMsg(e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }

            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    String token = FirebaseInstanceId.getInstance().getToken();
                    String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lat", lat);
                    params.put("lng", lon);
                    params.put("fcm_token", token);
                    params.put("device_code", deviceId);
                    return params;
                }
            };
            queue.add(stringRequest);
        }
    }

    private void getNotifications() {
        RequestQueue queue = Volley.newRequestQueue(this);

        //  String url ="http://www.google.com";

// Request a string response from the provided URL.
        String url = UrlHelper.BASE_URL + "api/v3/user/" + (loginStatus == 1 ? sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) : sharedPreference.getIntValues(CommonDef.SharedPreference.ANYNOMOUS_USER_ID)) + "/notifications?apikey=" + UrlHelper.API_KEY;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

                            JSONObject jsonObject = new JSONObject(utfStr);

                            JSONObject meta = jsonObject.getJSONObject("code");
                            int status = meta.getInt("status");
                            NotificationObject notificationObject;
                            if (status == 1) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                notification_count = 0;
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    if (object.getString("is_read").equalsIgnoreCase("0"))
                                        notification_count++;
                                    notificationObject = new NotificationObject();
                                    JSONObject jsonNotification = jsonArray.getJSONObject(i);
                                    notificationObject.setRef_id(jsonNotification.getInt("ref_id"));
                                    notificationObject.setId(jsonNotification.getInt("id"));
                                    notificationObject.setType(jsonNotification.getInt("type"));
                                    notificationObject.setMsg(jsonNotification.getString("title"));
                                    notificationObject.setImage(jsonNotification.getString("image"));
                                    notificationObject.setTime(jsonNotification.getString("created_at"));
                                    notificationObject.setIsRead(jsonNotification.getString("is_read"));

                                    realm.beginTransaction();
                                    realm.copyToRealmOrUpdate(notificationObject);
                                    realm.commitTransaction();

                                }

                                showNotificationIcon();

                            }
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void getTraderTypeList() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = UrlHelper.BASE_URL + "api/v3/trader/type?apikey=" + UrlHelper.API_KEY;
        ArrayList<TraderType> traderTypeList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        pd.hidepd();
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                            JSONObject object = new JSONObject(utfStr);
                            JSONArray traderTypeListJson = object.getJSONArray("data");
                            for (int i = 0; i < traderTypeListJson.length(); i++) {
                                JSONObject object1 = (JSONObject) traderTypeListJson.get(i);
                                TraderType traderType = new TraderType();
                                traderType.id = object1.getString("id");
                                traderType.type = object1.getString("type");
                                traderType.nepali_name = object1.getString("nepali_name");
                                traderTypeList.add(traderType);
                                if (i == 1)
                                    break;
                            }

                            Intent intent = new Intent(DashBoardActivity.this, PrathamiktaActivity.class);
                            intent.putExtra(CommonDef.TRADER_TYPE, traderTypeList);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                pd.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadLibrary() {
        Intent intent;
        if (!sharedPreference.getBoolValues(CommonDef.IS_LIBRARY_LOADED)) {
            progressDialog.showpd(getResources().getString(R.string.please_wait));
            getLibrary(false);
        } else {
//            realm.refresh();
            RealmResults<LibraryType> realmList = realm.where(LibraryType.class).findAll();
            LibraryType type;
            libraryTypeList = new ArrayList<LibraryType>();
            for (LibraryType result : realmList) {
                type = new LibraryType();
                type.setId(result.getId());
                type.setType(result.getType());
                libraryTypeList.add(type);
            }

            intent = new Intent(this, KrishiLibraryActivity.class);
            intent.putExtra(CommonDef.LIBRARY_TYPE, libraryTypeList);
            startActivity(intent);

            getLibrary(true);
        }
    }

    private void getLibrary(final boolean isAlreadyLoaded) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = UrlHelper.BASE_URL + "api/v3/library/type?apikey=" + UrlHelper.API_KEY;
        libraryTypeList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //forumDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                            JSONObject object = new JSONObject(utfStr);
                            JSONObject jsonResponse = object.getJSONObject("code");
                            int status = jsonResponse.getInt("status");
                            String message = jsonResponse.getString("message");
                            if (!message.equalsIgnoreCase("listed")) {
                                alerts.showSuccessAlert(message);
                            } else {
                                LibraryType libraryType;
                                JSONArray librarylistJson = object.getJSONArray("data");
                                for (int i = 0; i < librarylistJson.length(); i++) {
                                    libraryType = new LibraryType();
                                    JSONObject type = (JSONObject) librarylistJson.get(i);
                                    libraryType.setId(type.getString("id"));
                                    libraryType.setType(CommonMethods.decodeUnicode(type.getString("name")));
                                    libraryTypeList.add(libraryType);
                                }
                            }

                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.delete(LibraryType.class);
                                }
                            });

                            for (LibraryType b : libraryTypeList) {
                                // Persist your data easily
                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(b);
                                realm.commitTransaction();
                            }
                            sharedPreference.setKeyValues(CommonDef.IS_LIBRARY_LOADED, true);
//                            mAdapter.notifyDataSetChanged();
                            if (!isAlreadyLoaded) {
                                Intent intent = new Intent(DashBoardActivity.this, KrishiLibraryActivity.class);
                                intent.putExtra(CommonDef.LIBRARY_TYPE, libraryTypeList);
                                startActivity(intent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    @SuppressLint("RestrictedApi")
    void removeShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShifting(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());

            }
        } catch (NoSuchFieldException e) {
            Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void getCommodityList() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(DashBoardActivity.this);
        String url = UrlHelper.BASE_URL + "api/v3/market/commodity?apikey=" + UrlHelper.API_KEY;
        ArrayList<Commodity> commodityList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                            JSONObject object = new JSONObject(utfStr);
                            JSONObject jsonResponse = object.getJSONObject("code");
                            int status = jsonResponse.getInt("status");
                            String message = jsonResponse.getString("message");
                            if (status != 1) {
                                alerts.showSuccessAlert(message);
                            } else {
                                Commodity commodity;
                                JSONArray librarylistJson = object.getJSONArray("data");
                                for (int i = 0; i < librarylistJson.length(); i++) {
                                    commodity = new Commodity();
                                    JSONObject type = (JSONObject) librarylistJson.get(i);
                                    commodity.id = type.getString("id");
                                    commodity.name = type.getString("commodity_name");
                                    commodityList.add(commodity);
                                }
                            }
//                            mAdapter.notifyDataSetChanged();
                            Toast.makeText(DashBoardActivity.this, "success", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(DashBoardActivity.this, BarChartActivity.class);
                            intent.putExtra(CommonDef.COMMODITY_LIST, commodityList);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        try {
            switch (requestCode) {
                case 1: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        // permission was granted, yay! Do the

                        // contacts-related task you need to do.

                        gps = new GPSTracker();

                        // Check if GPS enabled
                        if (gps.canGetLocation()) {

                            lat = String.valueOf(gps.getLatitude());
                            lon = String.valueOf(gps.getLongitude());

                            getWeatherData(lat, lon);

                            // \n is for new line
//                        Toast.makeText(getActivity().getActivity().getApplicationContext(), "Your Location is - \nLat: " + lat + "\nLong: " + lon, Toast.LENGTH_LONG).show();
                        } else {
                            // Can't get location.
                            // GPS or network is not enabled.
                            // Ask user to enable GPS/network in settings.
                            gps.showSettingsAlert();
                        }

                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
//                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//
//                        }

                        Toast.makeText(DashBoardActivity.this, "You need to grant permission", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
            }
        } catch (Exception exx) {

        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}




