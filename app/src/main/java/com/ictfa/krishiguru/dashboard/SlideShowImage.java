package com.ictfa.krishiguru.dashboard;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Prajeet on 03/10/2017.
 */

public class SlideShowImage extends RealmObject{

    @PrimaryKey
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image;
}
