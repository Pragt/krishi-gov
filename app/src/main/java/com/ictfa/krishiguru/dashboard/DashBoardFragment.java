package com.ictfa.krishiguru.dashboard;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ictfa.krishiguru.Comment.FullQuestionActivity;
import com.ictfa.krishiguru.MultipleTraders.object.TraderType;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.Weather.WeatherObject;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.byawasaikUtpadanPrabidhi.KrishiTechnology;
import com.ictfa.krishiguru.chabBot.adapter.ChatData;
import com.ictfa.krishiguru.chabBot.adapter.DataToServer;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.GPSTracker;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.kinmel.KinmelActivity;
import com.ictfa.krishiguru.krishakByapariMulya.CommentOnKirshakByapaariActivity;
import com.ictfa.krishiguru.krishakByapariMulya.KrishyakByapariMulyaActivity;
import com.ictfa.krishiguru.krishiCalulator.KrishiCalculatorActivity;
import com.ictfa.krishiguru.krishilibrary.KrishiLibraryActivity;
import com.ictfa.krishiguru.krishilibrary.Object.LibraryType;
import com.ictfa.krishiguru.login.LoginActivity;
import com.ictfa.krishiguru.myOrganization.view.HamroOrganizationActivity;
import com.ictfa.krishiguru.myOrganization.view.NoticeAdapter;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;
import com.ictfa.krishiguru.myOrganization.view.Objects.NoticeObject;
import com.ictfa.krishiguru.news.NewsAdapter;
import com.ictfa.krishiguru.news.NewsData;
import com.ictfa.krishiguru.news.NewsListActivity;
import com.ictfa.krishiguru.prathamiktaBadalnuhosh.PrathamiktaActivity;
import com.ictfa.krishiguru.questionPost.AskQuestionActivity;
import com.ictfa.krishiguru.questionPost.FormListActivity;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.retrofit.ApiClient;
import com.ictfa.krishiguru.retrofit.ApiInterface;
import com.ictfa.krishiguru.ui.activity.YoutubeListActivity;
import com.ictfa.krishiguru.ui.adapter.ForumData;
import com.ictfa.krishiguru.ui.adapter.GridAdapter;
import com.ictfa.krishiguru.ui.adapter.VideoAdapter;
import com.ictfa.krishiguru.ui.adapter.VideoData;
import com.ictfa.krishiguru.ui.dtos.Farmer_OtherPrice_List;
import com.ictfa.krishiguru.ui.fragment.ImageShowFragment;
import com.ictfa.krishiguru.ui.widget.ExpandableHeightGridView;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmResults;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.ictfa.krishiguru.helpers.UrlHelper.YOUTUBE_URL;
import static java.sql.DriverManager.println;

/**
 * Created by Prajeet Naga on 12/23/18, 9:11 PM.
 **/
public class DashBoardFragment extends Fragment implements LocationListener, NoticeAdapter.onItemClickListner {
    public static String lat = "27.7172";
    public static String lon = "85.3240";
    TextView tvBestPrice;
    int[] titleArray = {R.string.krishiprabidihi, R.string.krishCalculator, R.string.kinmel, R.string.krishilibrary};
    int[] imageArray = {R.drawable.production_tech, R.drawable.market_analysis, R.drawable.kinmel, R.drawable.library};
    //for form
    LinearLayout formList;
    List<ForumData> forumDatas = new ArrayList<>();
    //for news
    RecyclerView rvNews;
    List<NewsData> newsDatas = new ArrayList<>();
    SharedPreference sharedPreference;
    Alerts alerts;
    CustomProgressDialog progressDialog;
    RecyclerView rvBuySell;
    ImageView ivAddQns;
    boolean doubleBackToExitPressedOnce = false;
    int notification_count = 0;
    TextView puraNews, puraForm;
    TextView tvMoreNotices, tvMoreVideos, tvMoreData;
    RecyclerView rvNotices, rvVideos;
    private SliderLayout sliderLayout;
    // List<VideoData> videoDatas = new ArrayList<>();private int loginStatus;
    private ImageView readForm, readVideos;
    //for grid
    private ExpandableHeightGridView gridView;
    private GridAdapter adapter;
    //for videos
    private VideoAdapter gridVideoAdapter;
    private List<VideoData> videoDatas = new ArrayList<>();
    private ArrayList<LibraryType> libraryTypeList;
    private NavigationView navigationView;
    private ArrayList<WeatherObject> listWeather;
    private String selectedLocation = "Kathmandu";
    private ArrayList<SlideShowImage> listImage;
    private Realm realm;
    private Menu menu;
    //    SwipeRefreshLayout swipeRefreshLayout;
    private NewsAdapter newsAdapter;
    private int loginStatus;
    private ArrayList<TraderType> traderTypeList;
    private GPSTracker gps;
    private LocationManager locationManager;
    private ArrayList<Farmer_OtherPrice_List> otherPrice_lists;
    private KharidBirkiAdapter otherPriceAdapter;
    private ArrayList<NoticeObject> listNotice;
    private NoticeAdapter noticeAdapter;
    private NestedScrollView nestedScrollView;

    private ImageView imgMenu;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        alerts = new Alerts(getActivity());
        listImage = new ArrayList<>();
        progressDialog = new CustomProgressDialog(getActivity());
        sharedPreference = new SharedPreference(getActivity());
        ivAddQns = view.findViewById(R.id.iv_add_qns);
        imgMenu = view.findViewById(R.id.img_menu);
        nestedScrollView = view.findViewById(R.id.nestedScrollView);
        nestedScrollView.setNestedScrollingEnabled(true);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), ((DashBoardActivity) getActivity()).drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        ((DashBoardActivity) getActivity()).drawer.setDrawerListener(toggle);
        imgMenu.setOnClickListener(v -> {
            if (((DashBoardActivity) getActivity()).drawer.isDrawerOpen(GravityCompat.START)) {
                ((DashBoardActivity) getActivity()).drawer.closeDrawer(Gravity.LEFT);
            } else {
                ((DashBoardActivity) getActivity()).drawer.openDrawer(Gravity.LEFT);
            }
        });

        lat = sharedPreference.getStringValues(CommonDef.SharedPreference.LATUTUDE);
        lon = sharedPreference.getStringValues(CommonDef.SharedPreference.LONGITUDE);
        rvBuySell = view.findViewById(R.id.rvBuySell);

        rvNotices = view.findViewById(R.id.rvNotices);
        rvVideos = view.findViewById(R.id.rvVideos);
        tvMoreNotices = view.findViewById(R.id.moreNotices);
        tvMoreVideos = view.findViewById(R.id.moreVideos);
        tvMoreData = view.findViewById(R.id.moreData);
        listNotice = new ArrayList<>();
        rvNotices.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvBuySell.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
//            Toast.makeText(this,"You need have granted permission",Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(getActivity());

            // Check if GPS enabled
            if (!gps.canGetLocation()) {
                gps.showSettingsAlert();
            }
        }


        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        } catch (Exception exx) {
        }
        //get realm instance
        this.realm = RealmController.with(this).getRealm();
//        sharedPreference.setKeyValues(CommonDef.SharedPreference.USER_ID, 4201);

        JSONObject weatherObj = new JSONObject();
        //  banner = (RelativeLayout)findViewById(R.id.locationValue);
        readForm = view.findViewById(R.id.menuForm);
        rvNews = view.findViewById(R.id.rvNews);

        puraForm = view.findViewById(R.id.puraFormHernus);
        puraNews = view.findViewById(R.id.moreNews);
        gridView = view.findViewById(R.id.gridview);
        sliderLayout = view.findViewById(R.id.bannerimg);


        tvBestPrice = view.findViewById(R.id.weatherData);

        //  Toast.makeText(getActivity(),userID,Toast.LENGTH_SHORT).show();
        loginStatus = sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN);
        tvBestPrice.setSelected(true);
        //  minTempTextView.setSelected(true);


//        getWeatherData("27.7172", "85.3240");

        setImageSlider();
        setGrid();
        setNews();
        setVideoData();
        puraClick();
        getPrioroties();
        getKharidBikri();

        tvBestPrice.setOnClickListener(v -> getTraderTypeList());

        formList = view.findViewById(R.id.ll_forom);

        // popuate news and forom from database

        loadNotices();

        RealmResults<ForumData> realmResults1 = realm.where(ForumData.class).findAll();
        for (ForumData a : realmResults1)
            forumDatas.add(realm.copyToRealm(a));
        populateForom();

        RealmResults<SlideShowImage> realmResults = this.realm.where(SlideShowImage.class).findAll();
        for (SlideShowImage image : realmResults)
            listImage.add(realm.copyToRealm(image));
        loadImage();

        ivAddQns.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AskQuestionActivity.class);
            startActivity(intent);
        });
    }

    private void getKharidBikri() {
        otherPrice_lists = new ArrayList<>();
        RealmResults<Farmer_OtherPrice_List> realmResults = realm.where(Farmer_OtherPrice_List.class).findAll();
        for (Farmer_OtherPrice_List a : realmResults)
            otherPrice_lists.add(realm.copyToRealm(a));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    otherPriceAdapter = new KharidBirkiAdapter(getActivity(), otherPrice_lists);
                    otherPriceAdapter.setListener(new KharidBirkiAdapter.OnCommentClickListner() {
                        @Override
                        public void onCommentClicked(@NotNull int pos) {
                            getBuyerSellerComments(pos);
                        }
                    });
                    rvBuySell.setAdapter(otherPriceAdapter);
                }
            }
        }, 10);


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = "http://admin.ict4agri.com/api/v3/market/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/buyer_seller?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
        Log.d("FORMURL", url);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray data = jsonObject.getJSONArray("data");
//                            otherPrice_lists = new ArrayList<>();
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.delete(Farmer_OtherPrice_List.class);
                                }
                            });
                            otherPrice_lists = new ArrayList<>();
                            for (int i = 0; i < data.length(); i++) {
                                Farmer_OtherPrice_List farmer_otherPrice_list = new Farmer_OtherPrice_List();

                                JSONObject getvalue = data.getJSONObject(i);

                                String mystatus = getvalue.getString("is_am_i");
                                String name = getvalue.getString("name");
                                String id = getvalue.getString("id");
                                String product = getvalue.getString("product_name");
                                String userImage = getvalue.getString("image");
                                String phone = getvalue.getString("phone");
                                String address = getvalue.getString("address");
                                String productImg = getvalue.getString("product_image");
                                String product_quantity = getvalue.getString("product_quantity");
                                String unit = getvalue.getString("product_unit");
                                String valid_to = getvalue.getString("valid_to");
                                String product_quality = getvalue.getString("product_quality");
                                String type = getvalue.getString("type");
                                String price;
                                if (type.equalsIgnoreCase("seller")) {
                                    price = getvalue.getString("product_price");
                                } else
                                    price = getvalue.getString("product_price_min") + " - " + getvalue.getString("product_price_max");
                                int comment_count = Integer.parseInt(getvalue.getString("comment_count"));
//
                                farmer_otherPrice_list.setName(name);
                                farmer_otherPrice_list.id = id;
                                farmer_otherPrice_list.productImage = productImg;
                                farmer_otherPrice_list.setProductImage("null");
                                farmer_otherPrice_list.setAddress(address);
                                farmer_otherPrice_list.setProductImage(productImg);
                                farmer_otherPrice_list.setUserImage(userImage);
                                farmer_otherPrice_list.setType(type);
                                farmer_otherPrice_list.createdAt = getvalue.getString("created_at");


                                farmer_otherPrice_list.setProductNQuantity("परिमाण -" + product_quantity + " " + unit);

                                farmer_otherPrice_list.setQuality(product_quality);
                                farmer_otherPrice_list.productName = product;

                                farmer_otherPrice_list.setRate("रु. " + price + "/" + unit);
                                farmer_otherPrice_list.setDate(valid_to + " सम्म ");
                                farmer_otherPrice_list.setPhone(phone);
                                farmer_otherPrice_list.setCommentCount(comment_count);
//                                    farmer_otherPrice_list.setListComment(commentList);
                                otherPrice_lists.add(farmer_otherPrice_list);
                                realm.beginTransaction();
                                realm.insertOrUpdate(farmer_otherPrice_list);
                                realm.commitTransaction();
                            }
                            if (getActivity() != null) {
                                otherPriceAdapter = new KharidBirkiAdapter(getActivity(), otherPrice_lists);
                                otherPriceAdapter.setListener(new KharidBirkiAdapter.OnCommentClickListner() {
                                    @Override
                                    public void onCommentClicked(int pos) {
                                        getBuyerSellerComments(pos);
                                    }
                                });
                                rvBuySell.setAdapter(otherPriceAdapter);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void getBuyerSellerComments(int pos) {

        progressDialog.showpd(getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String zone;

        Farmer_OtherPrice_List item = otherPrice_lists.get(pos);

        if (item.getType().equalsIgnoreCase("buyer"))
            zone = UrlHelper.BASE_URL + "api/v3/buyer/" + item.id + "/comments?apikey=" + UrlHelper.API_KEY;
        else
            zone = UrlHelper.BASE_URL + "api/v3/seller/" + item.id + "/comments?apikey=" + UrlHelper.API_KEY;

        Log.d("Comments", zone);
        ArrayList<Comment> listComment = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                response -> {
                    progressDialog.hidepd();
                    //forumDatas.clear();
                    //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                    try {
                        String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                        JSONObject jsonObject1 = new JSONObject(utfStr);
                        JSONArray commentList = jsonObject1.getJSONArray("data");
                        ArrayList<Comment> comments = new ArrayList<>();
                        Comment comment;

                        if (commentList != null) {
                            for (int j = 0; j < commentList.length(); j++) {
                                JSONObject commentJson = commentList.getJSONObject(j);
                                comment = new Comment();
                                comment.comment_id = commentJson.getInt("id");
                                comment.comment = commentJson.getString("comment");
                                comment.commented_by = commentJson.getString("commented_by");
//                                    JSONObject created_at = commentJson.getJSONObject("created_at");
                                comment.date = commentJson.getString("created_at");
//                                    comment.timezone_type = created_at.getInt("timezone_type");
//                                    comment.timezone = created_at.getString("timezone");
                                comments.add(comment);
                            }


                            try {
                                Intent intent = new Intent(getActivity(), CommentOnKirshakByapaariActivity.class);
                                intent.putExtra(CommonDef.COMMENTS, comments);
                                intent.putExtra(CommonDef.PRODUCT_ID, item.id);
                                intent.putExtra(CommonDef.KHARID_BIKRI_OBJECT, item);

                                intent.putExtra("type", 1);
                                intent.putExtra(CommonDef.IS_FARMER, !item.getType().equalsIgnoreCase("buyer"));
                                intent.putExtra(CommonDef.IS_MY_PRICE, false);
                                startActivityForResult(intent, 100);
                            } catch (Exception exx) {

                            }

                        }

                    } catch (JSONException e) {
                        alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                        e.printStackTrace();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                progressDialog.hidepd();
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    private void loadNotices() {
        listNotice = new ArrayList<>();
        if (noticeAdapter != null)
            noticeAdapter.notifyDataSetChanged();
        RealmResults<NoticeObject> realmResults = realm.where(NoticeObject.class).findAll();
        for (NoticeObject a : realmResults)
            listNotice.add(realm.copyToRealm(a));


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                noticeAdapter = new NoticeAdapter(getActivity(), listNotice, new NoticeAdapter.onItemClickListner() {
                    @Override
                    public void onItemClicked(int position) {

                    }

                    @Override
                    public void onShareToFb(View itemView, String imageUrl) {

                    }
                }, true);
                rvNotices.setAdapter(noticeAdapter);
            }
        }, 10);


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = UrlHelper.BASE_URL + "api/v3/organisation/notices?apikey=" + UrlHelper.API_KEY + "&length=10";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        swipeRefreshLayout.setRefreshing(false);
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                            JSONObject object = new JSONObject(response);
//                            JSONObject jsonResponse = object.getJSONObject("code");
//                            int status = jsonResponse.getInt("status");
//                            String message = jsonResponse.getString("message");
//                            if (status != 1) {
//                                alerts.showSuccessAlert(message);
//                            } else {
                            NoticeObject noticeObject;
                            JSONArray noticeList = object.getJSONArray("data");
                            listNotice = new ArrayList<>();
                            noticeAdapter.notifyDataSetChanged();
                            for (int i = 0; i < noticeList.length(); i++) {
                                JSONObject notice = noticeList.getJSONObject(i);
                                noticeObject = new NoticeObject();
                                noticeObject.id = Integer.parseInt(notice.getString("id"));
                                noticeObject.org_name = notice.getString("organisation_name");
                                noticeObject.notice = notice.getString("notice");
                                noticeObject.valid_to = notice.getString("valid_to");
                                noticeObject.phone = notice.getString("phone");
                                noticeObject.toll_free = notice.getString("toll_free");
                                noticeObject.contact_person = notice.getString("contact_person");
                                noticeObject.comment_count = notice.getInt("comment_count");
                                noticeObject.share_count = notice.getInt("share_count");
                                noticeObject.days_remaining = notice.getInt("remaining");
                                noticeObject.postedDate = notice.getString("created_at");
                                noticeObject.imageUrl = notice.getString("image_url");
                                noticeObject.title = notice.getString("title");
                                noticeObject.type = notice.getString("type");
                                noticeObject.noticeFile = notice.getString("notice_file");
                                listNotice.add(noticeObject);
                            }

                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.delete(NoticeObject.class);
                                }
                            });

                            for (NoticeObject b : listNotice) {
                                // Persist your data easily
                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(b);
                                realm.commitTransaction();
                            }

                            noticeAdapter = new NoticeAdapter(getActivity(), listNotice, new NoticeAdapter.onItemClickListner() {
                                @Override
                                public void onItemClicked(int position) {

                                }

                                @Override
                                public void onShareToFb(View itemView, String imageUrl) {

                                }
                            }, true);
                            rvNotices.setAdapter(noticeAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (getActivity() != null)
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
            }
        });
// Add the request to the RequestQueue.
        // Add the request to the RequestQueue.
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private void getPrioroties() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = UrlHelper.BASE_URL + "api/v3/trader/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/best-price?apikey=" + UrlHelper.API_KEY;
        traderTypeList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        pd.hidepd();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        String bestPrice = "";
                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                            JSONObject object = new JSONObject(utfStr);
                            JSONObject data = object.getJSONObject("data");
                            JSONObject pref = data.getJSONObject("preference");
                            bestPrice = pref.getString("market") + " मा आज व्यापारीले गर्ने अधिकतम खरीद मुल्य";
                            JSONArray jsonArray = data.getJSONArray("best-prices");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object1 = (JSONObject) jsonArray.get(i);
                                if (i == 0)
                                    bestPrice = bestPrice + ": " + object1.getString("crop_name") + ": " + object1.getString("price");
                                else
                                    bestPrice = bestPrice + ", " + object1.getString("crop_name") + ": " + object1.getString("price");
                            }

                            if (jsonArray.length() > 0)
                                tvBestPrice.setText(bestPrice);


                        } catch (JSONException e) {
                            e.printStackTrace();
//                            alerts.showToastMsg(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                pd.hidepd();
//                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

//    private void getWeatherData(String district) {
//        progressDialog.showpd(getResources().getString(R.string.please_wait));
//        JSONObject jsonObject;
//        RequestQueue queue = Volley.newRequestQueue(getActivity());
//        listWeather = new ArrayList<>();
//        String Weather = UrlHelper.BASE_URL + "api/v3/weather/forecast/" + district + "?apikey=" + UrlHelper.API_KEY;
//
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, Weather,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        //forumDatas.clear();
//                        progressDialog.hidepd();
//                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
//                        try {
//                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
//
//                            JSONObject jsonObject = new JSONObject(utfStr);
//                            WeatherObject weatherObject;
//                            JSONArray jsonArray = jsonObject.getJSONArray("forecast");
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                weatherObject = new WeatherObject();
//                                JSONObject newsValue = jsonArray.getJSONObject(i);
//                                JSONObject temperature = newsValue.getJSONObject("temperature");
//                                String minTemp = temperature.getString("min");
//                                String maxTemp = temperature.getString("max");
//                                weatherObject.minTemp = minTemp;
//                                weatherObject.maxTemp = maxTemp;
//
//                                JSONObject Night;
//                                JSONObject Day;
//
//                                Night = newsValue.getJSONObject("night");
//
//                                Day = newsValue.getJSONObject("day");
//
//
//                                String desc = Night.getString("text");
//                                String probabilityOfRain = Night.getString("probability_of_rain");
//                                double hours_of_rain = Night.getDouble("hours_of_rain");
//                                String thunderstorm = Night.getString("thunderstorm");
//                                String rain = Night.getString("rain");
//                                String iconNight = Night.getString("icon");
//                                weatherObject.nightDescription = desc;
//                                weatherObject.chanceOfRainNight = probabilityOfRain;
//                                weatherObject.durationOfRainNight = hours_of_rain + "";
//                                weatherObject.chanceOfThunderNight = thunderstorm;
//                                weatherObject.iconNight = iconNight;
//
//                                String descDay = Day.getString("text");
//                                String probabilityOfRainDay = Day.getString("probability_of_rain");
//                                double hours_of_rain_day = Day.getDouble("hours_of_rain");
//                                String thunderstormDay = Day.getString("thunderstorm");
//                                String rainDay = Day.getString("rain");
//                                String iconDay = Day.getString("icon");
//
//                                weatherObject.dayDescription = descDay;
//                                weatherObject.chanceOfRainDay = probabilityOfRainDay;
//                                weatherObject.durationOfRainDay = hours_of_rain_day + "";
//                                weatherObject.chanceOfThunderDay = thunderstormDay;
//                                weatherObject.iconDay = iconDay;
//
//                                listWeather.add(weatherObject);
//
//                            }
//
//                            Intent intent = new Intent(getActivity(), WeatherActivity.class);
//                            intent.putExtra(CommonDef.LIST_WEATHER, listWeather);
//                            startActivity(intent);
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        } catch (UnsupportedEncodingException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                // mTextView.setText("That didn't work!");
//                progressDialog.hidepd();
//            }
//        });
//// Add the request to the RequestQueue.
//        queue.add(stringRequest);
//    }

    public void puraClick() {


        puraForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FormListActivity.class);
                startActivity(intent);
            }
        });


        puraNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewsListActivity.class);
                startActivity(intent);
            }
        });


        tvMoreVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), YoutubeListActivity.class);
                startActivity(intent);
            }
        });

        tvMoreData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), KrishyakByapariMulyaActivity.class);
                intent.putExtra("is_my_price", false);
                startActivity(intent);
            }
        });


        tvMoreNotices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HamroOrganizationActivity.class);
                startActivity(intent);
            }
        });

    }

    public boolean isNetworkAvailable(final Context context) {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

//    private void imageMenuClick() {
//        readForm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PopupMenu popupMenu = new PopupMenu(getActivity(), readForm);
//                popupMenu.getMenuInflater()
//                        .inflate(R.menu.image_menu, popupMenu.getMenu());
//
//                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    public boolean onMenuItemClick(MenuItem item) {
//                        Intent intent = new Intent(getActivity(), FormListActivity.class);
//                        startActivity(intent);
//                        return true;
//                    }
//                });
//                popupMenu.show();
//            }
//        });


//        readVideos.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PopupMenu popupMenu = new PopupMenu(getActivity(), readVideos);
//                popupMenu.getMenuInflater()
//                        .inflate(R.menu.image_menu, popupMenu.getMenu());
//                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    public boolean onMenuItemClick(MenuItem item) {
//                        Intent intent = new Intent(getActivity(), YoutubeListActivity.class);
//                        startActivity(intent);
//                        return true;
//                    }
//                });
//                popupMenu.show();
//            }
//        });

//    }

    private void setImageSlider() {
        HttpConnector httpConnector = new HttpConnector(1);

    }

    private void setGrid() {

        adapter = new GridAdapter(getActivity(), titleArray, imageArray);
        gridView.setAdapter(adapter);
        gridView.setExpanded(true);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (position) {

                    case 0:
                        intent = new Intent(getActivity(), KrishiTechnology.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(getActivity(), KrishiCalculatorActivity.class);
                        startActivity(intent);

                        break;
                    case 2:
                        intent = new Intent(getActivity(), KinmelActivity.class);
                        startActivity(intent);
                        break;
                    case 3:
                        loadLibrary();
//                        intent = new Intent(getActivity(), KrishiLibraryActivity.class);
//                        startActivity(intent);
                        break;

                    default:
                        break;

                }
            }
        });

//        setVideoData();
    }

    private void loadLibrary() {
        Intent intent;
        if (!sharedPreference.getBoolValues(CommonDef.IS_LIBRARY_LOADED)) {
            progressDialog.showpd(getResources().getString(R.string.please_wait));
            getLibrary(false);
        } else {
//            realm.refresh();
            RealmResults<LibraryType> realmList = realm.where(LibraryType.class).findAll();
            LibraryType type;
            libraryTypeList = new ArrayList<LibraryType>();
            for (LibraryType result : realmList) {
                type = new LibraryType();
                type.setId(result.getId());
                type.setType(result.getType());
                libraryTypeList.add(type);
            }

            intent = new Intent(getActivity(), KrishiLibraryActivity.class);
            intent.putExtra(CommonDef.LIBRARY_TYPE, libraryTypeList);
            startActivity(intent);

            getLibrary(true);
        }
    }

    private void getLibrary(final boolean isAlreadyLoaded) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = UrlHelper.BASE_URL + "api/v3/library/type?apikey=" + UrlHelper.API_KEY;
        libraryTypeList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //forumDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                            JSONObject object = new JSONObject(utfStr);
                            JSONObject jsonResponse = object.getJSONObject("code");
                            int status = jsonResponse.getInt("status");
                            String message = jsonResponse.getString("message");
                            if (!message.equalsIgnoreCase("listed")) {
                                alerts.showSuccessAlert(message);
                            } else {
                                LibraryType libraryType;
                                JSONArray librarylistJson = object.getJSONArray("data");
                                for (int i = 0; i < librarylistJson.length(); i++) {
                                    libraryType = new LibraryType();
                                    JSONObject type = (JSONObject) librarylistJson.get(i);
                                    libraryType.setId(type.getString("id"));
                                    libraryType.setType(CommonMethods.decodeUnicode(type.getString("name")));
                                    libraryTypeList.add(libraryType);
                                }
                            }

                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.delete(LibraryType.class);
                                }
                            });

                            for (LibraryType b : libraryTypeList) {
                                // Persist your data easily
                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(b);
                                realm.commitTransaction();
                            }
                            sharedPreference.setKeyValues(CommonDef.IS_LIBRARY_LOADED, true);
//                            noticeAdapter.notifyDataSetChanged();
                            if (!isAlreadyLoaded) {
                                Intent intent = new Intent(getActivity(), KrishiLibraryActivity.class);
                                intent.putExtra(CommonDef.LIBRARY_TYPE, libraryTypeList);
                                startActivity(intent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


/*
        this method set news feed in list
 */



    /*
    this method sets videodata
     */

    /*
    this method set newslistview.
     */
    private void setNews() {
        populateNews();
        HttpConnector httpConnector = new HttpConnector(2);

    }

    private void setVideoData() {
        getDataFromYoutube();
    }

    /**
     * gettting data from youtube.
     */
    private void getDataFromYoutube() {
        RealmResults<VideoData> listVideo = realm.where(VideoData.class).findAll();
        for (VideoData data : listVideo)
            videoDatas.add(realm.copyToRealm(data));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                rvVideos.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                gridVideoAdapter = new VideoAdapter(getActivity(), videoDatas, true);
                rvVideos.setAdapter(gridVideoAdapter);
            }
        }, 10);


        Log.d("CHECK", "CHECK");
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //  String url ="http://www.google.com";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, YOUTUBE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d("RESPONSE", response);
                        //  Toast.makeText(getActivity(),response,Toast.LENGTH_LONG).show();
                        try {
                            JSONObject youtubeData = new JSONObject(response);
                            JSONArray youtubeDataArray = youtubeData.getJSONArray("items");

                            videoDatas.clear();
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.delete(VideoData.class);
                                }
                            });
//
                            for (int i = 0; i < youtubeDataArray.length(); i++) {
                                JSONObject object = youtubeDataArray.getJSONObject(i);
                                JSONObject snippetObject = object.getJSONObject("snippet");
                                String title = snippetObject.getString("title");

                                String imageUrl = null, videoId = null;
                                if (snippetObject.has("thumbnails")) {
                                    JSONObject thumbnailObject = snippetObject.getJSONObject("thumbnails");
                                    JSONObject defaultObject = thumbnailObject.getJSONObject("default");
                                    imageUrl = defaultObject.getString("url");
                                    JSONObject resourceObject = snippetObject.getJSONObject("resourceId");
                                    videoId = resourceObject.getString("videoId");
                                }


                                VideoData videoData = new VideoData();
                                videoData.setTitle(title);
                                videoData.setVideoThumbnail(imageUrl);
                                videoData.setVideoId(videoId);

                                videoDatas.add(videoData);

                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(videoData);
                                realm.commitTransaction();

                            }
                            gridVideoAdapter.notifyDataSetChanged();
                            //videoListAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

//    @Override
//    protected void onStop() {
//        sliderLayout.stopAutoCycle();
//        super.onStop();
//
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        this.menu = menu;
//        if (loginStatus == 1) {
//            menu.findItem(R.id.login).setTitle("लग आउट गर्नुहोस्");
//            menu.findItem(R.id.notifications).setVisible(true);
//
//        }
////        else {
////            notification_count = 0;
//////            hideNotificationIcon();
////        }
//        registerDevice();
//        MenuItem view = menu.findItem(R.id.notifications);
//        RelativeLayout rlNotifications = (RelativeLayout) view.getActionView();
//        rlNotifications.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), EmptyFragment.class);
//                startActivity(intent);
//
//            }
//        });
//
//
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(final MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//
//
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        switch (id) {
//            case R.id.login:
//                if (loginStatus == 1) {
//                    alerts.showConfirmationDialog("के तपाई लग आउट गर्न चाहनु हुन्छ ?", new Alerts.OnConfirmationClickListener() {
//                        @Override
//                        public void onYesClicked() {
//                            sharedPreference.clearData();
//                            tvBestPrice.setText("प्राथमिकता छान्नुहोस्");
//                            setForm();
//                            loginStatus = 0;
//                            menu.findItem(R.id.login).setTitle("लगइन गर्नुहोस");
//                            notification_count = 0;
//                            showNotificationIcon();
//                            registerDevice();
//                        }
//
//                        @Override
//                        public void onNoClicked() {
//
//                        }
//                    }, 110);
//
//                } else {
//                    Intent intent = new Intent(getActivity(), LoginActivity.class);
//                    intent.putExtra("is_from_login", true);
//                    startActivityForResult(intent, CommonDef.IS_FROM_LOGIN);
//                }
//                break;
//            case R.id.my_profile:
//                if (sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN) == 1)
//                    getUserProfile();
//                else {
//                    Intent intent = new Intent(this, LoginActivity.class);
//                    startActivity(intent);
//                }
//                break;
//            case R.id.change_priority:
//                getTraderTypeList();
//                break;
//
//            case R.id.feedback:
//                Intent intent = new Intent(this, SendFeedback.class);
//                startActivity(intent);
//                break;
//
//            case R.id.about_is:
//                Intent intent1 = new Intent(this, FAQActivity.class);
//                startActivity(intent1);
//
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
///**
// if (id == R.id.nav_camera) {
// // Handle the camera action
// } else if (id == R.id.nav_gallery) {
//
// } else if (id == R.id.nav_slideshow) {
//
// } else if (id == R.id.nav_manage) {
//
// } else if (id == R.id.nav_share) {
//
// } else if (id == R.id.nav_send) {
//
// }
// **/
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }
//
//    @Override
//    public void location(String location, String district_id) {
//        userLocation.setText(location);
//        selectedLocation = location;
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        navigationView.setCheckedItem(0);
////        forumDatas = new ArrayList<>();
//        loginStatus = sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN);
//        if (loginStatus == 1) {
//            if (menu != null) {
//                menu.findItem(R.id.login).setTitle("लग आउट गर्नुहोस्");
//                menu.findItem(R.id.notifications).setVisible(true);
//            }
//        }
//
//        if (sharedPreference.getBoolValues(CommonDef.SharedPreference.IS_DEVICE_REGISTERED) || loginStatus == 1) {
//            getNotifications();
//            showNotificationIcon();
//        } else
//            registerDevice();
//        try {
//            setForm();
//        } catch (Exception exx) {
//
//        }
//    }

//    void getUserProfile() {
//        progressDialog.showpd(getResources().getString(R.string.please_wait));
//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url = UrlHelper.BASE_URL + "api/v3/user/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/profile?apikey=" + UrlHelper.API_KEY;
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        progressDialog.hidepd();
//                        //forumDatas.clear();
//                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
//                        try {
//                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
//                            JSONObject object = new JSONObject(utfStr);
//                            JSONArray responseJson = object.getJSONArray("data");
////                            String status = jsonObject.getString("status");
////                            String messege = jsonObject.getString("message");
//                            if (responseJson != null && responseJson.length() > 0) {
//                                JSONObject jsonObject = (JSONObject) responseJson.get(0);
//                                UserProfileObject userProfile = new UserProfileObject();
//                                userProfile.name = jsonObject.getString("name");
//                                userProfile.address = jsonObject.getString("location");
//                                userProfile.phone = jsonObject.getString("phone");
//                                userProfile.image = jsonObject.getString("image");
//                                userProfile.UserType = UserType.TYPE_FARMER;
//                                if (jsonObject.getString("user_type").equalsIgnoreCase("farmer")) {
//                                    userProfile.UserType = UserType.TYPE_FARMER;
//                                } else if (jsonObject.getString("user_type").equalsIgnoreCase("trader")) {
//                                    userProfile.UserType = UserType.TYPE_TRADER;
//                                    JSONArray marketJsonArray = jsonObject.getJSONArray("market");
//                                    if (marketJsonArray.length() > 0) {
//                                        JSONObject market = (JSONObject) marketJsonArray.get(0);
//                                        userProfile.marketName = market.getString("market_name");
//                                        userProfile.marketType = market.getString("market_type");
//                                    }
//
//                                } else if (jsonObject.getString("user_type").equalsIgnoreCase("organisation")) //change this to orgajisation
//                                {
//                                    userProfile.UserType = UserType.TYPE_ORGANISATION;
//                                    JSONArray organisationJson = jsonObject.getJSONArray("organisation");
//                                    if (organisationJson.length() > 0) {
//                                        JSONObject organisation = (JSONObject) organisationJson.get(0);
//                                        userProfile.id = organisation.getString("id");
//                                        userProfile.org_name = organisation.getString("organization_name");
//                                        userProfile.org_phone = organisation.getString("organization_phone");
//                                        userProfile.org_type = organisation.getString("organization_type");
//                                        userProfile.org_address = organisation.getString("organization_address");
//                                        userProfile.toll_free_no = organisation.getString("organization_toll_free");
//                                    }
//
//                                }
//
//                                Intent intent = new Intent(getActivity(), MyProfileActivity.class);
//                                intent.putExtra(CommonDef.USER_PROFILE, userProfile);
//                                startActivity(intent);
//                            } else
//                                Toast.makeText(getActivity(), "Error Occured", Toast.LENGTH_SHORT).show();
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            alerts.showToastMsg(e.getMessage());
//                        } catch (UnsupportedEncodingException e) {
//                            alerts.showToastMsg(e.getMessage());
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.hidepd();
//                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
//                // mTextView.setText("That didn't work!");
//            }
//        });
//// Add the request to the RequestQueue.
//        queue.add(stringRequest);
//    }
//
//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            if (doubleBackToExitPressedOnce) {
//                super.onBackPressed();
//                return;
//            }
//
//            this.doubleBackToExitPressedOnce = true;
//            Toast.makeText(this, "कृपया बाहिर निस्कन पुनः BACK थिचनु होला |", Toast.LENGTH_SHORT).show();
//
//            new Handler().postDelayed(new Runnable() {
//
//                @Override
//                public void run() {
//                    doubleBackToExitPressedOnce = false;
//                }
//            }, 2000);
//        }
//    }

    private void loadForomData(View convertView, final int position) {
        CircleImageView icon = convertView.findViewById(R.id.formImage);
        TextView nameAndAddrress = convertView.findViewById(R.id.formNameAndAddress);
        TextView time = convertView.findViewById(R.id.formTime);
        TextView title = convertView.findViewById(R.id.formTitle);
        TextView desc = convertView.findViewById(R.id.formDesc);
        TextView comment = convertView.findViewById(R.id.numsOfComments);
        // HorizontalListView imageForm = (HorizontalListView)convertView.findViewById(R.id.imageList);
        ImageView descImg = convertView.findViewById(R.id.descImage);

//        ImageView commentImg = (ImageView) convertView.findViewById(R.id.commentIcon);
        ImageView shareImg = convertView.findViewById(R.id.iconShare);
        final TextView shareNow = convertView.findViewById(R.id.shareNow);

        final ImageView likeIcon = convertView.findViewById(R.id.likeicon);
        final TextView likeCounter = convertView.findViewById(R.id.likecounter);
        final TextView likeCount = convertView.findViewById(R.id.likecounter);
        final LinearLayout llComments = convertView.findViewById(R.id.ll_comments);
        String urlString = "";
        URL myURL = null;
        try {
            myURL = new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        final ForumData forumData = forumDatas.get(position);


        Picasso.with(getContext()).load(R.drawable.ic_icon_man).into(icon);
//        if (forumData.getProfileImage().isEmpty()) {
//        } else {
//            Picasso.with(activity).load(forumData.getProfileImage()).into(icon);
//        }

        if (forumData.getIsLiked().equals("true"))
            likeCounter.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        else
            likeCounter.setTextColor(getResources().getColor(R.color.black));


        shareImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareNow.callOnClick();
            }
        });


//        shareNow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                SharePostToFB.share(getActivity(), convertView.getDrawingCache(), userImage);
//                shareNow.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Bitmap b = getBitmapRootView(findViewById(R.id.cvQuestion));
//                        SharePostToFB.share(getActivity(), b, forumData.getProfileImage());
//                    }
//
//                    private Bitmap getBitmapRootView(View view) {
//                        View rootView = view;
//                        rootView.setDrawingCacheEnabled(true);
//                        return rootView.getDrawingCache();
//                    }
//                });
//            }
//        });

        likeCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                likeIcon.callOnClick();
            }
        });

        likeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginStatus == 1) {
                    if (forumData.getIsLiked().equals("true")) {
                        Toast.makeText(getActivity(), "you already like this", Toast.LENGTH_SHORT).show();
                    } else {
                        int likevalue = Integer.parseInt(forumData.getLikeCount()) + 1;
                        likeCount.setText(likevalue + " Like");
                        sendLike(forumData.getQuestionID(), forumData.getUserId());
                        likeCounter.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                        forumData.setIsLiked("true");
                        forumData.setLikeCount(String.valueOf(likevalue));
                    }
                } else {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                }
            }

        });
        nameAndAddrress.setText(forumData.getNameAddress());
        time.setText(forumData.getFormTime());
        title.setText(forumData.getTitle());
        desc.setText(forumData.getDesc());
        comment.setText(forumData.getCommentNums() + " Comments");
        likeCount.setText(forumData.getLikeCount() + " Like");

        if (forumData.getProfileImage().equalsIgnoreCase(""))
            descImg.setVisibility(View.GONE);
        else
            Picasso.with(getActivity()).load(forumData.getProfileImage()).resize(500, 500).into(descImg);

        if (!forumData.getUserImage().equalsIgnoreCase(""))
            Picasso.with(getActivity()).load(forumData.getUserImage()).resize(500, 500).into(icon);

        descImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImageShowFragment imageShowFragment = ImageShowFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("image", forumData.getProfileImage());
                imageShowFragment.setArguments(bundle);
                imageShowFragment.show(getActivity().getFragmentManager(), "sad");
            }
        });


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llComments.callOnClick();
            }
        });

        llComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getComments(forumData.getQuestionID(), position);
            }
        });


        int countofImages = 3;

        String[] imagelist = new String[countofImages];
        imagelist[0] = "http://agrifarming.in/wp-content/uploads/2015/03/Harvesting-Potatoes.jpg";
        // imagelist[1]= "http://www.asiafarming.com/wp-content/uploads/2016/02/Growing-Potatoes-in-Greenhouse.jpg";
        imagelist[1] = "http://www.asiafarming.com/wp-content/uploads/2016/02/Potato-Cultivation-506x330.jpg";
    }

    public void getComments(String questionID, final int position) {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String zone = UrlHelper.BASE_URL + "api/v3/forum/comments/" + questionID + "?apikey=" + UrlHelper.API_KEY;
        final ArrayList<Comment> listComment = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog.hidepd();
                            //forumDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                                JSONObject jsonObject = new JSONObject(utfStr);
                                JSONArray data = jsonObject.getJSONArray("data");
                                Comment comment;
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject commentObj = data.getJSONObject(i);
                                        comment = new Comment();
                                        comment.comment_id = Integer.parseInt(commentObj.getString("id"));
                                        comment.comment = commentObj.getString("comment");
                                        comment.commented_by = commentObj.getString("commented_by");
                                        comment.date = commentObj.getString("created_at");
                                        listComment.add(comment);
                                    }

                                }

                                Intent intent = new Intent(getActivity(), FullQuestionActivity.class);
                                intent.putExtra(CommonDef.QUESTION_ID, forumDatas.get(position).getQuestionID());
                                intent.putExtra(CommonDef.COMMENTS, listComment);
                                startActivity(intent);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception exx) {
                        }
                    }
                }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getActivity().getResources().getString(R.string.no_internet_connection));
                progressDialog.hidepd();
                // mTextView.setText("That didn't work!");
            }


        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void sendLike(String QuestionID, String UserID) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //  String url ="http://www.google.com";
        String url = "http://admin.ict4agri.com/api/v3/question/" + QuestionID + "/like?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA&user_id=" + UserID;
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("LIKERESPONSE", response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "not work", Toast.LENGTH_SHORT).show();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void populateNews() {
        RealmResults<NewsData> realmResults = realm.where(NewsData.class).findAll();
        newsDatas = new ArrayList<>();
        for (NewsData a : realmResults)
            newsDatas.add(realm.copyToRealm(a));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                rvNews.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                rvNews.setAdapter(new NewsAdapter(getActivity(), newsDatas, true));
            }
        }, 10);


    }

    private void populateForom() {
        formList.removeAllViews();
        for (int i = 0; i < forumDatas.size(); i++) {
            View convertView = LayoutInflater.from(getActivity()).inflate(R.layout.custome_form_list, null);
            loadForomData(convertView, i);
            formList.addView(convertView);

            if (i == 2)
                break;
        }
    }

    private void loadImage() {
//        sliderLayout.removeAllViews();
        sliderLayout.setDuration(3000);
        sliderLayout.setMinimumHeight(CommonMethods.dpToPx(getActivity(), 250));
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

        sliderLayout.removeAllSliders();
        TextSliderView[] textSliderViews = new TextSliderView[listImage.size()];
        for (int i = 0; i < listImage.size(); i++) {
            textSliderViews[i] = new TextSliderView(getActivity());
            textSliderViews[i].image(listImage.get(i).getImage())
                    .description(listImage.get(i).getTitle())
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderLayout.addSlider(textSliderViews[i]);
        }
    }

    @Override
    public void onItemClicked(int position) {

    }

    @Override
    public void onShareToFb(View itemView, String imageUrl) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == CommonDef.IS_FROM_LOGIN)
//            if (resultCode == RESULT_OK) {
//                loginStatus = 1;
//                if (menu != null) {
//                    menu.findItem(R.id.login).setTitle("लग आउट गर्नुहोस्");
//                    menu.findItem(R.id.notifications).setVisible(true);
//                }
//                getNotifications();
//            }
    }

    private void getTraderTypeList() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = UrlHelper.BASE_URL + "api/v3/trader/type?apikey=" + UrlHelper.API_KEY;
        traderTypeList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        pd.hidepd();
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                            JSONObject object = new JSONObject(utfStr);
                            JSONArray traderTypeListJson = object.getJSONArray("data");
                            for (int i = 0; i < traderTypeListJson.length(); i++) {
                                JSONObject object1 = (JSONObject) traderTypeListJson.get(i);
                                TraderType traderType = new TraderType();
                                traderType.id = object1.getString("id");
                                traderType.type = object1.getString("type");
                                traderType.nepali_name = object1.getString("nepali_name");
                                traderTypeList.add(traderType);
                            }

                            Intent intent = new Intent(getActivity(), PrathamiktaActivity.class);
                            intent.putExtra(CommonDef.TRADER_TYPE, traderTypeList);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                            progressDialog.hidepd();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                pd.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        try {
            switch (requestCode) {
                case 1: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        // permission was granted, yay! Do the

                        // contacts-related task you need to do.

                        gps = new GPSTracker(getActivity());

                        // Check if GPS enabled
                        if (gps.canGetLocation()) {

                            lat = String.valueOf(gps.getLatitude());
                            lon = String.valueOf(gps.getLongitude());

                            registerDevice();

                            // \n is for new line
//                        Toast.makeText(getActivity().getActivity().getApplicationContext(), "Your Location is - \nLat: " + lat + "\nLong: " + lon, Toast.LENGTH_LONG).show();
                        } else {
                            // Can't get location.
                            // GPS or network is not enabled.
                            // Ask user to enable GPS/network in settings.
                            gps.showSettingsAlert();
                        }

                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
//                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//
//                        }

                        Toast.makeText(getContext(), "You need to grant permission", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
            }
        } catch (Exception exx) {

        }
    }

    private void registerDevice() {
//        progressDialog.showpd(getResources().getString(R.string.please_wait));
        if (loginStatus == 0 && !sharedPreference.getBoolValues(CommonDef.SharedPreference.IS_DEVICE_REGISTERED)) {
            RequestQueue queue = Volley.newRequestQueue(getContext());
            String url = UrlHelper.BASE_URL + "api/v3/user/anonymous?apikey=" + UrlHelper.API_KEY;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                        pd.hidepd();
//                        progressDialog.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                                JSONObject object = new JSONObject(utfStr);
                                JSONObject code = object.getJSONObject("code");
                                JSONObject user = code.getJSONObject("user");

                                String userId = user.getString("id");
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.IS_DEVICE_REGISTERED, true);
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.ANYNOMOUS_USER_ID, Integer.parseInt(userId));

                            } catch (JSONException e) {
                                e.printStackTrace();
//                                alerts.showToastMsg(e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }

            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    String token = FirebaseInstanceId.getInstance().getToken();
                    String deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lat", lat);
                    params.put("lng", lon);
                    params.put("fcm_token", token);
                    params.put("device_code", deviceId);
                    return params;
                }
            };
            queue.add(stringRequest);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
//        Toast.makeText(this, "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude(), Toast.LENGTH_SHORT).show();
        lat = String.valueOf(location.getLatitude());
        lon = String.valueOf(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onResume() {
        super.onResume();
        setNews();
        setVideoData();
        setImageSlider();
        getPrioroties();
        loadNotices();
        getKharidBikri();
        checkUserPackageSubscribed();
        syncToServerChatHistory();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

    }

    void syncToServerChatHistory() {
        ArrayList<DataToServer> list = new ArrayList<>();
        RealmResults realmList = realm.where(ChatData.class).findAll();
        if (realmList != null) {
            for (Object object : realmList) {
                ChatData chatData = (ChatData) object;
                if (!chatData.is_synced) {
                    DataToServer data = new DataToServer(chatData.id, chatData.type, chatData.data, chatData.is_synced, chatData.timestamp);
                    list.add(data);
                }
            }
        }

        if (!list.isEmpty()) {
            ApiClient.getClient().create(ApiInterface.class).syncToServer(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID), "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA", list)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                                if (s.code.status == 1) {
                                    RealmResults resultList = realm.where(ChatData.class).findAll();
                                    if (resultList != null) {
                                        realm.beginTransaction();
                                        for (Object chatData : resultList) {
                                            ChatData chatData1 = (ChatData) chatData;
                                            if (!chatData1.is_synced) {
                                                chatData1.is_synced = true;
                                                realm.insertOrUpdate(chatData1);
                                            }
                                        }
                                        realm.commitTransaction();
                                    }
                                } else {
//                            alerts.showErrorAlert(s.code.message)
                                }
                            }
                            ,
                            e -> {
//                                alerts.showErrorAlert("Could not connect to server. Please try again later.");
                                e.printStackTrace();
                            }, () ->
                                    println("supervisor list"));
        }
    }

    private void checkUserPackageSubscribed() {
        ApiClient.getClient().create(ApiInterface.class).checkUserEligible(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID), "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            if (s.code.status == 1) {
                                // eligible
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, "1");
                            } else if (s.code.status == 0) {
                                sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, "0");
                            }
                        }
                        ,
                        e -> {
//                            alerts.showErrorAlert("Could not connect to server. Please try again later.");
                            e.printStackTrace();
                        }, () ->
                                println("supervisor list"));
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public class HttpConnector {
        String imageResponse;
        String newsListResponse;

        public HttpConnector(final int requestType) {
            RequestQueue queue = Volley.newRequestQueue(getActivity());
            String url = "";

            switch (requestType) {
                case 1:
                    url = BaseApplication.IMAGE_SLIDER_API;
                    break;

                case 2:
                    url = BaseApplication.NEWS_API;
                    break;
                case 3:
                    url = BaseApplication.FORM_QUESTION_LIST_API + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
                    break;

            }

            // String url = BaseApplication.IMAGE_SLIDER_API;

            //  Toast.makeText(activity,url,Toast.LENGTH_SHORT).show();
// Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (requestType == 1) {
                                setImageResponse(response);
                                String[] arrayTitle = new String[4];
                                String[] arrayImage = new String[4];
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    TextSliderView[] textSliderViews = new TextSliderView[jsonArray.length()];

                                    SlideShowImage showImage;
                                    listImage = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject imageValue = jsonArray.getJSONObject(i);
                                        String title = imageValue.getString("title");
                                        String imageresource = imageValue.getString("image");

                                        showImage = new SlideShowImage();
                                        showImage.setTitle(title);
                                        showImage.setImage(imageresource);
                                        listImage.add(showImage);
                                    }

                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.delete(SlideShowImage.class);
                                        }
                                    });

                                    for (SlideShowImage b : listImage) {
                                        // Persist your data easily
//                                        if (b != null) {
//                                            realm.beginTransaction();
//                                            realm.copyToRealmOrUpdate(b);
//                                            realm.commitTransaction();
//                                        }
                                    }

                                    loadImage();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            if (requestType == 2) {
//                                swipeRefreshLayout.setRefreshing(false);
                                try {
                                    String utfStr = new String(response.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

                                    JSONObject jsonObject = new JSONObject(utfStr);
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    newsDatas = new ArrayList<>();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject newsValue = jsonArray.getJSONObject(i);
                                        String title = newsValue.getString("title");
                                        String imageresource = newsValue.getString("image");
                                        String description = newsValue.getString("description");
                                        String id = newsValue.getString("id");
                                        NewsData newsData = new NewsData();

                                        newsData.setDesc(description);
                                        newsData.setTitle(title);
                                        newsData.setId(id);
                                        newsData.setNewsImage(imageresource);
                                        newsDatas.add(newsData);

                                    }

                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.delete(NewsData.class);
                                        }
                                    });

                                    for (NewsData b : newsDatas) {
                                        // Persist your data easily
                                        if (b != null) {
                                            realm.beginTransaction();
                                            realm.copyToRealmOrUpdate(b);
                                            realm.commitTransaction();
                                        }
                                    }

                                    populateNews();
                                    Log.d("JSONRESPONSE=>>>>>>>>", jsonArray.toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
//                                    swipeRefreshLayout.setRefreshing(false);
                                }
                            }

//                            if (requestType == 3) {
////
//                                try {
//                                    String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
//                                    JSONObject jsonObject = new JSONObject(utfStr);
//                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
//                                    int counter = 3;
//                                    forumDatas = new ArrayList<>();
//                                    for (int i = 0; i < jsonArray.length(); i++) {
//                                        JSONObject newsValue = jsonArray.getJSONObject(i);
//
//                                        String askBy = newsValue.getString("ask_by");
//                                        String address = newsValue.getString("district");
//                                        String title = newsValue.getString("question");
//                                        String imageresource = newsValue.getString("image_url");
//                                        String user_image = newsValue.getString("user_image");
//                                        String description = newsValue.getString("description");
//                                        String Time = newsValue.getString("created_at");
//                                        String commentCount = newsValue.getString("comment_count");
//                                        String noHTMLString = description.replaceAll("\\<.*?>", "");
//
//                                        String likeCount = newsValue.getString("like_count");
//                                        String LikeStatus = newsValue.getString("is_liked");
//                                        String questionID = newsValue.getString("id");
////                                        if (commentCount.equalsIgnoreCase("0")) {
////                                            commentCount = commentCount + " comment";
////
////
////                                        } else {
////                                            commentCount = commentCount + " comments";
////                                        }
//
//                                        TimeAgo timeAgo = new TimeAgo();
//                                        String stringTimeAgo = "0";
//                                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
////                                        try {
//////                                            Date date = format.parse(Time);
//////                                            stringTimeAgo = timeAgo.toDuration(date);
////
////
////                                        } catch (ParseException e) {
////                                            e.printStackTrace();
////                                        }
//                                        //long duration = date.getTime();
//                                        // Toast.makeText(getActivity(),duration+"",Toast.LENGTH_SHORT).show();
//                                        ForumData forumData = new ForumData();
//
//                                        forumData.setNameAddress(askBy + " " + "-" + " " + address);
//                                        forumData.setFormTime(Time);
//                                        forumData.setProfileImage(imageresource);
//                                        forumData.setDesc(description);
//                                        forumData.setTitle(title);
//                                        forumData.setCommentNums(commentCount);
//                                        forumData.setLikeCount(likeCount);
//                                        forumData.setIsLiked(LikeStatus);
//                                        forumData.setUserId(String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)));
//                                        forumData.setQuestionID(questionID);
//                                        forumData.setUserImage(user_image);
//                                        forumDatas.add(forumData);
//                                    }
//
//                                    realm.executeTransaction(new Realm.Transaction() {
//                                        @Override
//                                        public void execute(Realm realm) {
//                                            realm.delete(ForumData.class);
//                                        }
//                                    });
//
//                                    for (ForumData b : forumDatas) {
//                                        // Persist your data easily
//                                        if (b != null) {
//                                            realm.beginTransaction();
//                                            realm.copyToRealmOrUpdate(b);
//                                            realm.commitTransaction();
//                                        }
//                                    }
//
//                                    populateForom();
//
//                                    Log.d("JSONRESPONSE=>>>>>>>>", jsonArray.toString());
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                } catch (UnsupportedEncodingException e) {
//                                    e.printStackTrace();
//                                }
//                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
// Add the request to the RequestQueue.
            queue.add(stringRequest);

        }

        public void setImageResponse(String imageResponse) {
            this.imageResponse = imageResponse;
        }

    }
}