package com.ictfa.krishiguru.dashboard

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.ButterKnife
import com.ictfa.krishiguru.R
import com.ictfa.krishiguru.aboutUs.AboutUs
import com.ictfa.krishiguru.faq.FAQActivity
import com.ictfa.krishiguru.helpers.Alerts
import com.ictfa.krishiguru.helpers.CommonDef
import com.ictfa.krishiguru.helpers.SharedPreference
import com.ictfa.krishiguru.login.LoginActivity
import com.ictfa.krishiguru.realm.RealmController
import com.ictfa.krishiguru.retrofit.ApiClient
import com.ictfa.krishiguru.retrofit.ApiInterface
import com.ictfa.krishiguru.sendFeedback.SendFeedback
import com.ictfa.krishiguru.termsAndConditions.TermsAndConditions
import io.realm.Realm
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
Created by Prajeet Naga on 12/23/18, 10:25 PM.
 **/

class FragmentMore : Fragment() {

    var llLogin: LinearLayout? = null
    var llDisclaimer: LinearLayout? = null
    var llFaq: LinearLayout? = null
    var llFeedback: LinearLayout? = null
    var llChangePriority: LinearLayout? = null
    var llMyProfile: LinearLayout? = null
    var llAboutUs: LinearLayout? = null

    val alerts: Alerts by lazy {
        Alerts(activity)
    }

    lateinit var tvLogin: TextView

    val sharedPreference: SharedPreference by lazy {
        SharedPreference(activity)
    }

    var loginStatus = 0

    private var realm: Realm? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_more, container, false)
        ButterKnife.bind(this, view)
        this.realm = RealmController.with(this).getRealm();

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        llLogin = view.findViewById(R.id.ll_login)
        llFeedback = view.findViewById(R.id.ll_feedback)
        llChangePriority = view.findViewById(R.id.ll_change_priority)
        llMyProfile = view.findViewById(R.id.ll_my_profile)
        llAboutUs = view.findViewById(R.id.llAboutUs)
        llDisclaimer = view.findViewById(R.id.llDisclaimer)
        llFaq = view.findViewById(R.id.llFaq)
        tvLogin = view.findViewById(R.id.tvLogin)

        llLogin!!.setOnClickListener {
            if (loginStatus == 1) {
                alerts.showConfirmationDialog("के तपाई लग आउट गर्न चाहनु हुन्छ ?", object : Alerts.OnConfirmationClickListener {
                    override fun onYesClicked() {
                        sharedPreference.clearData()
                        loginStatus = 0
                        tvLogin.text = resources.getString(R.string.login)
                        realm?.beginTransaction()
                        realm?.deleteAll()
                        realm?.commitTransaction()

                    }

                    override fun onNoClicked() {

                    }
                }, 110)

            } else {
                val intent = Intent(activity, LoginActivity::class.java)
                intent.putExtra("is_from_login", true)
                startActivityForResult(intent, CommonDef.IS_FROM_LOGIN)
            }
        }

        llFeedback!!.setOnClickListener {
            val intent = Intent(activity, SendFeedback::class.java)
            intent.putExtra("is_from_login", true)
            startActivityForResult(intent, CommonDef.IS_FROM_LOGIN)
        }

        llChangePriority!!.setOnClickListener {
            dashBoardActivity.getTraderTypeList()
        }

        llMyProfile!!.setOnClickListener {
            if (sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN) == 1)
                dashBoardActivity.getUserProfile(true)
            else {
                val intent = Intent(activity, LoginActivity::class.java)
                startActivity(intent)
            }

        }

        llAboutUs!!.setOnClickListener {
            val intent = Intent(activity, AboutUs::class.java)
            intent.putExtra("is_from_login", true)
            startActivityForResult(intent, CommonDef.IS_FROM_LOGIN)
        }

        llFaq!!.setOnClickListener {
            val intent = Intent(activity, FAQActivity::class.java)
            intent.putExtra("is_from_login", true)
            startActivityForResult(intent, CommonDef.IS_FROM_LOGIN)
        }

        llDisclaimer!!.setOnClickListener {
            val intent = Intent(activity, TermsAndConditions::class.java)
            intent.putExtra("is_from_login", true)
            startActivityForResult(intent, CommonDef.IS_FROM_LOGIN)
        }

    }

    lateinit var dashBoardActivity: DashBoardActivity

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        dashBoardActivity = context as DashBoardActivity
    }


    override fun onResume() {
        super.onResume()
        checkUserPackageSubscribed()
        loginStatus = sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN)
        if (loginStatus == 1) {
            tvLogin.text = resources.getString(R.string.log_out)
        }
    }

    private fun checkUserPackageSubscribed() {
        ApiClient.getClient().create(ApiInterface::class.java).checkUserEligible(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID), "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    if (s.code.status == 1) {
                        // eligible
                        sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, "1")
                    } else if (s.code.status == 0) {
                        sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, "0")
                    }
                },
                        { e ->
                            //                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
                            e.printStackTrace()
                        }, { println("supervisor list") })
    }
}