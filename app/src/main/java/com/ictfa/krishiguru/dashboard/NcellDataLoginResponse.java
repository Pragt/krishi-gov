package com.ictfa.krishiguru.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NcellDataLoginResponse {

    @SerializedName("meta")
    @Expose
    public List<Object> meta = null;
    @SerializedName("data")
    @Expose
    public Data data = null;
    @SerializedName("code")
    @Expose
    public Code code;


    public class Code {

        @SerializedName("status")
        @Expose
        public Integer status;
        @SerializedName("message")
        @Expose
        public String message;

    }

    public class Data {
        @SerializedName("login")
        @Expose
        public Login login;
        @SerializedName("user_found")
        @Expose
        public Boolean userFound;

    }

    public class Login {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("location")
        @Expose
        public String location;
        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("is_ncell_number")
        @Expose
        public String isNcellNumber;
        @SerializedName("is_dnd")
        @Expose
        public String isDnd;
        @SerializedName("is_number_verified")
        @Expose
        public String isNumberVerified;
        @SerializedName("is_otp_send")
        @Expose
        public String isOtpSend;
        @SerializedName("has_package_subscribed")
        @Expose
        public String hasPackageSubscribed;
        @SerializedName("verified_mobile_number")
        @Expose
        public String verifiedMobileNumber;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("user_type")
        @Expose
        public String userType;
        @SerializedName("priorities")
        @Expose
        public String priorities;

    }


}