package com.ictfa.krishiguru.dashboard

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.ictfa.krishiguru.R
import com.ictfa.krishiguru.ui.dtos.Farmer_OtherPrice_List
import com.squareup.picasso.Picasso
import java.util.*

/**
 * Created by Prajeet Naga on 2/4/19, 10:56 PM.
 */
class KharidBirkiAdapter// ForumData formData;
(private val activity: Activity, private val farmerOtherDatas: ArrayList<Farmer_OtherPrice_List>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        view = LayoutInflater.from(parent.context).inflate(R.layout.item_kharid_bikri, parent, false)
        val layoutParams = view.layoutParams
        layoutParams.width = (parent.width * 0.45).toInt()
        view.layoutParams = layoutParams
        return ViewHolder(view)
    }

    private lateinit var listener: OnCommentClickListner

    fun setListener(commentClickListner: OnCommentClickListner) {
        this.listener = commentClickListner
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val viewHolder = holder
            try {
                viewHolder.productName.text = farmerOtherDatas.get(position).productName

                viewHolder.tvPrice.setText(farmerOtherDatas.get(position).getRate())

                if (farmerOtherDatas.get(position).type.equals("seller")) {
                    holder.tvTag.text = "बिक्रि"
                } else
                    holder.tvTag.text = "खरिद"

                viewHolder.itemView.setOnClickListener {
                    listener?.onCommentClicked(position)
                }

                if (farmerOtherDatas[position].productImage != "") {
                    Picasso.with(activity).load(farmerOtherDatas[position].productImage).resize(500, 500).into(viewHolder.productImage)
                }
            } catch (exx: Exception) {
            }

        }
    }

    override fun getItemCount(): Int {
        return farmerOtherDatas.size
    }

    inner class ViewHolder(convertedView: View) : RecyclerView.ViewHolder(convertedView) {

        var productImage: ImageView
        var productName: TextView
        var tvPrice: TextView
        var tvTag: TextView


        init {
            productImage = convertedView.findViewById<View>(R.id.ivPhoto) as ImageView
            productName = convertedView.findViewById<View>(R.id.tvName) as TextView
            tvPrice = convertedView.findViewById<View>(R.id.tvPrice) as TextView
            tvTag = convertedView.findViewById<View>(R.id.tvTag) as TextView
        }
    }

    public interface OnCommentClickListner {
        fun onCommentClicked(pos : Int)
    }
}
