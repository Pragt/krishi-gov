package com.ictfa.krishiguru.dashboard

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ictfa.krishiguru.R

/**
Created by Prajeet Naga on 12/23/18, 9:57 PM.
 **/

public class EmptyFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_packages, container, false);
    }
}