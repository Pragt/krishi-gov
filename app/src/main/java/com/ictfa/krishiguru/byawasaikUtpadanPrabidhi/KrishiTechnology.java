package com.ictfa.krishiguru.byawasaikUtpadanPrabidhi;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.ui.adapter.ProductCategoryTabAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import me.relex.circleindicator.CircleIndicator;

public class KrishiTechnology extends AppCompatActivity {
    SharedPreference sharedPreference;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CustomProgressDialog progressDialog;
    private ArrayList<CommonObject> productCategory;
    private CircleIndicator indicator;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_krishi_technology);
        getSupportActionBar().setTitle("व्यवसायिक उत्पादन प्रविधि");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        // viewPager.set
        progressDialog = new CustomProgressDialog(this);
        sharedPreference = new SharedPreference(this);

        //get realm instance
        this.realm = RealmController.with(this).getRealm();

        RealmResults<CommonObject> realmList = realm.where(CommonObject.class).findAll();
        CommonObject type;
        productCategory = new ArrayList<CommonObject>();
        for (CommonObject result : realmList) {
            type = new CommonObject();
            type.setId(result.getId());
            type.setName(result.getName());
            productCategory.add(type);
        }
        setViewPagerAdapter();

        if (!sharedPreference.getBoolValues("isTechnologyloaded")) {
            progressDialog.showpd(getResources().getString(R.string.please_wait));
        }
        getProductCategory();
        //   Toast.makeText(KrishiTechnology.this,productCategory.length+"",Toast.LENGTH_SHORT).show();
    }

    private void getProductCategory() {
        RequestQueue queue = Volley.newRequestQueue(KrishiTechnology.this);
        //  String url ="http://www.google.com";
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BaseApplication.productionCategoryType,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESPONSECATEGORY", response);
                        progressDialog.hidepd();

                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            Log.d("utfstr", utfStr);
                            JSONObject jsonObject = new JSONObject(utfStr);
                            getJson(jsonObject);
                            // JSONObject jsonObject = new JSONObject(response);
//                            progressDialog.dismiss();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                // mTextView.setText("That didn't work!");
                //   progressDialog.dismiss();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void getJson(JSONObject jsonObject) {
        if (jsonObject.has("data")) {
            // Toast.makeText(KrishiTechnology.this,"data cha",Toast.LENGTH_SHORT).show();
            JSONArray jsonArray = null;
            try {
                jsonArray = jsonObject.getJSONArray("data");

                productCategory = new ArrayList<>();
                CommonObject object;
                //  Toast.makeText(KrishiTechnology.this,productCategory.length+"",Toast.LENGTH_SHORT).show();
                for (int i = 0; i < jsonArray.length(); i++) {
                    object = new CommonObject();
                    JSONObject value = jsonArray.getJSONObject(i);
                    String name = value.getString("name");
                    object.setName(name);
                    object.setId(value.getString("id"));
                    productCategory.add(object);
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(CommonObject.class);
                    }
                });

                for (CommonObject b : productCategory) {
                    // Persist your data easily
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(b);
                    realm.commitTransaction();
                }

                sharedPreference.setKeyValues("isTechnologyloaded", true);

                setViewPagerAdapter();
                //      ProductDataListenerManager.getInstance().callOnlineDataListeners(categoryArray, productCategory[viewPager.getCurrentItem()]);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(KrishiTechnology.this, "no data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setViewPagerAdapter() {
        viewPager.setAdapter(new ProductCategoryTabAdapter(getSupportFragmentManager(),
                KrishiTechnology.this, productCategory));
        viewPager.setOffscreenPageLimit(productCategory.size());
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        viewPager.setCurrentItem(0);
        indicator.setViewPager(viewPager);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}


