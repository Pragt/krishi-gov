package com.ictfa.krishiguru.byawasaikUtpadanPrabidhi;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.ui.adapter.ProductionTechnologyAdapter;
import com.ictfa.krishiguru.ui.dtos.ProductionTechnologyDtos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ProductionTechnologyActivity extends AppCompatActivity {
    private List<ProductionTechnologyDtos> productionTechnologyDtoses = new ArrayList<>();
    private ProductionTechnologyAdapter productionTechnologyAdapter;
    private ListView productionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production_technology);
        productionList = (ListView)findViewById(R.id.productionList);
        getSupportActionBar().setTitle("व्यवसायिक उत्पादन प्रविधि");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        productionTechnologyAdapter = new ProductionTechnologyAdapter(ProductionTechnologyActivity.this,productionTechnologyDtoses);
        productionList.setAdapter(productionTechnologyAdapter);
        getProductionTechnology();
    }

    private void getProductionTechnology(){
        RequestQueue queue = Volley.newRequestQueue(ProductionTechnologyActivity.this);
        String url = BaseApplication.productTechnology;
        Log.d("FORMURL",url);
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();

                        productionTechnologyDtoses.clear();
                        try {
                            String utfStr =new String(response.getBytes ("ISO-8859-1"), "UTF-8");
                            Log.d("utfstr", response);
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                             for (int i = 0 ; i<jsonArray.length(); i++){
                                JSONObject datavalue = jsonArray.getJSONObject(i);
                                  ProductionTechnologyDtos productionTechnologyDtos = new ProductionTechnologyDtos();
                                 String productionType = datavalue.getString("production_type");
                                 String productionName = datavalue.getString("production_name");
                                 String descrption = datavalue.getString("description");
                                 String createdDate = datavalue.getString("created_at");
                                 String   subtitles = datavalue.getString("subtitles");
                                 String image = datavalue.getString("images");

                                 JSONArray images = datavalue.getJSONArray("images");
                                 JSONArray subtitlearray = datavalue.getJSONArray("subtitles");

                                 Log.d("subtitleArray", subtitlearray.toString());

                                 productionTechnologyDtos.setProductionType(productionType);
                                 productionTechnologyDtos.setProductionName(productionName);
                                 productionTechnologyDtos.setProductionDescription(descrption);
                                 productionTechnologyDtos.setCreatedDate(createdDate);
                                productionTechnologyDtoses.add(productionTechnologyDtos);

                             }
            productionTechnologyAdapter.notifyDataSetChanged();

                        }

                        catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
