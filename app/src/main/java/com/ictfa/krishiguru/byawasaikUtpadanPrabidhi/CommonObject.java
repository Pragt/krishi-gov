package com.ictfa.krishiguru.byawasaikUtpadanPrabidhi;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Prajeet on 03/09/2017.
 */

public class CommonObject extends RealmObject {
    private String name;

    @PrimaryKey
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
