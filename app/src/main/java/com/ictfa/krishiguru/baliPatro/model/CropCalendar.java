package com.ictfa.krishiguru.baliPatro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class CropCalendar extends RealmObject {

    @SerializedName("monthName")
    @Expose
    public String monthName;
    @SerializedName("terai")
    @Expose
    public CropInfo terai;
    @SerializedName("madhya_pahad")
    @Expose
    public CropInfo madhyaPahad;
    @SerializedName("uccha_pahad")
    @Expose
    public CropInfo ucchaPahad;

}
