package com.ictfa.krishiguru.baliPatro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class BaliPatroResponse {

    @SerializedName("meta")
    @Expose
    public List<Object> meta = null;
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("code")
    @Expose
    public Code code;


    public class Code {

        @SerializedName("status")
        @Expose
        public Integer status;
        @SerializedName("message")
        @Expose
        public String message;

    }


    public class Data {

        @SerializedName("crop_calendar")
        @Expose
        public List<CropCalendar> cropCalendar = null;

    }
}