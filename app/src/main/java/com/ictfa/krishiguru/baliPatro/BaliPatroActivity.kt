package com.ictfa.krishiguru.baliPatro

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.ictfa.krishiguru.R
import com.ictfa.krishiguru.awaskyakBiruwaSankhya.model.CropCalculatorTable
import com.ictfa.krishiguru.baliPatro.model.CropCalendar
import com.ictfa.krishiguru.baliPatro.model.CropInfo
import com.ictfa.krishiguru.helpers.Alerts
import com.ictfa.krishiguru.realm.RealmController
import com.ictfa.krishiguru.retrofit.ApiClient
import com.ictfa.krishiguru.retrofit.ApiInterface
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_bali_patro.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class BaliPatroActivity : AppCompatActivity() {

    private val regionName = listOf("तराई (६० - ३०० मीटर)",
            "मध्य पहाड (३०० - ९०० मीटर)",
            "उच्च पहाड (९०० - १८०० मीटर)")

    private val monthsName = listOf("बैशाख",
            "जेठ",
            "असार",
            "श्रावण",
            "भदौ",
            "असोज",
            "कार्तिक",
            "मंसिर",
            "पुष",
            "माघ",
            "फागुन",
            "चैत"
    )

    private val cropCalendarList = ArrayList<CropCalendar>()

    private val realm: Realm by lazy {
        RealmController.with(this).realm
    }

    private val alerts: Alerts by lazy {
        Alerts(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bali_patro)
        title = "बाली पात्रो"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initViews()
        populateData()
        getDataFromServer()

        btn_calculate.setOnClickListener {
            var result = when (sp_region.selectedItemPosition) {
                0 -> cropCalendarList[sp_month.selectedItemPosition].terai as CropInfo
                1 -> cropCalendarList[sp_month.selectedItemPosition].madhyaPahad as CropInfo
                2 -> cropCalendarList[sp_month.selectedItemPosition].ucchaPahad as CropInfo
                else -> null
            }

            if (result != null) {
                grResults.visibility = View.VISIBLE
                tv_result.text = "${sp_region.selectedItem} उचाइमा ${sp_month.selectedItem} महिनामा निम्न कृषि कार्यहरु गरिन्छ |"
                tv_bui_rakhne_result.text = if (result.nurseryBiuRakhne.isNullOrEmpty()) "छैन" else result.nurseryBiuRakhne
                tv_berna_sorne_result.text = if (result.bernaSarne.isNullOrEmpty()) "छैन" else result.bernaSarne
                tv_ropne_result.text = if (result.ropne.isNullOrEmpty()) "छैन" else result.ropne
                tv_tipne_result.text = if (result.tipne.isNullOrEmpty()) "छैन" else result.tipne
                scrollView.postDelayed(Runnable {
                    scrollView.fullScroll(View.FOCUS_DOWN)
                }, 400)
            }
        }

        sp_region.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                grResults.visibility = View.GONE
            } // to close the onItemSelected
        }

        sp_month.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                grResults.visibility = View.GONE
            } // to close the onItemSelected
        }
    }

    private fun populateData() {
        val realmList = realm.where(CropCalendar::class.java).findAll()
        if (realmList.isNotEmpty()) {
            cropCalendarList.clear()
            for (cropCalendar in realmList) {
                cropCalendarList.add(cropCalendar)
            }
        }
    }

    private fun initViews() {
        val regionAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, regionName) //selected item will look like a spinner set from XML

        regionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_region.adapter = regionAdapter

        val monthsAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, monthsName) //selected item will look like a spinner set from XML

        monthsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_month.adapter = monthsAdapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                // app icon in action bar clicked; go home
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun getDataFromServer() {
        ApiClient.getClient().create(ApiInterface::class.java).getBaliPatroResponse("O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    if (s.code.status == 0) {
                        realm.executeTransaction {
                            it.delete(CropCalculatorTable::class.java)
                        }
                        for (packagesProduct in s.data.cropCalendar) {
                            realm.beginTransaction()
                            realm.insertOrUpdate(packagesProduct)
                            realm.commitTransaction()
                        }
                        populateData()
                    } else {
                        alerts.showToastMsg(s.code.message)
                    }
                },
                        { e ->
                            alerts.showToastMsg("Could not connect to server. Please try again later.")
                            e.printStackTrace()
                        },
                        { kotlin.io.println("supervisor list") })
    }
}