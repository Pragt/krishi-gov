package com.ictfa.krishiguru.baliPatro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class CropInfo extends RealmObject {

        @SerializedName("nursery_biu_rakhne")
        @Expose
        public String nurseryBiuRakhne;
        @SerializedName("berna_sarne")
        @Expose
        public String bernaSarne;
        @SerializedName("ropne")
        @Expose
        public String ropne;
        @SerializedName("tipne")
        @Expose
        public String tipne;

    }
