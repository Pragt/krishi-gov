package com.ictfa.krishiguru.task.weatherListener;

import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by suresh on 15/11/16.
 */

public class WeatherDataListenerManager {

    //the static instace of the class
    private static WeatherDataListenerManager manager;

    // the list of listeners
    private List<WeatherDataGotListener> weatherDataGotListeners = new ArrayList<>();


    /**
     * Since the class is a singleton class. User
     */
    private WeatherDataListenerManager() {
       //empty constructor
    }

    /**
     * Returns the instance of ProductDataListenerManager class.
     *
     * @return DataListManager
     */
    public static WeatherDataListenerManager getInstance(){
        if(manager == null){
            manager = new WeatherDataListenerManager();
        }
        return manager;
    }


    /*
     * Call the registered DataGotListeners
     */
    public void callOnlineDataListeners(int position, JSONObject jsonObject){
        Log.d("==>", "callOnlineDataListeners: size of lisneters "+this.weatherDataGotListeners.size());
        for (WeatherDataGotListener weatherDataGotListener : this.weatherDataGotListeners) {
            if(weatherDataGotListener != null){
                weatherDataGotListener.onDataGot( position, jsonObject);
            }
        }
    }


}
