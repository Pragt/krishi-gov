package com.ictfa.krishiguru.task.weatherListener;

import org.json.JSONObject;

/**
 * Created by suresh on 15/11/16.
 */

public interface WeatherDataGotListener {
    void onDataGot(int position, JSONObject jsonObject);
}
