package com.ictfa.krishiguru.task.productCategoryListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by suresh on 15/11/16.
 */

public class ProductDataListenerManager {

    //the static instace of the class
    private static ProductDataListenerManager manager;

    // the list of listeners
    private List<ProductTypeListener> productTypeListeners = new ArrayList<>();
   // private List<DataGotSellDateListener> dataGotSellDateListeners = new ArrayList<>();

    /**
     * Since the class is a singleton class. User
     */
    private ProductDataListenerManager() {
       //empty constructor
    }


}
