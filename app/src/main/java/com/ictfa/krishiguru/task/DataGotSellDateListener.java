package com.ictfa.krishiguru.task;

/**
 * Created by sureshlama on 5/9/17.
 */

public interface DataGotSellDateListener {

    void onDataGotSell(int position, String date);
}
