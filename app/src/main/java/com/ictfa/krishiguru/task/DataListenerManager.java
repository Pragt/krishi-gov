package com.ictfa.krishiguru.task;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by suresh on 15/11/16.
 */

public class DataListenerManager {

    //the static instace of the class
    private static DataListenerManager manager;

    // the list of listeners
    private List<DataGotListener> dataGotListeners = new ArrayList<>();
    private List<DataGotSellDateListener> dataGotSellDateListeners = new ArrayList<>();

    /**
     * Since the class is a singleton class. User
     */
    private DataListenerManager() {
       //empty constructor
    }

    /**
     * Returns the instance of ProductDataListenerManager class.
     *
     * @return DataListManager
     */
    public static DataListenerManager getInstance(){
        if(manager == null){
            manager = new DataListenerManager();
        }
        return manager;
    }

    /**
     * Add/ Register a new {@link DataGotListener}
     *
     * @param dataGotListener
     */


    public void addDataGotListeners(DataGotListener dataGotListener) {
        this.dataGotListeners.add(dataGotListener);
    }

        public void addDataGotSellListener(DataGotSellDateListener dataGotSellDateListener){
            this.dataGotSellDateListeners.add(dataGotSellDateListener);
        }
    /*
     * Call the registered DataGotListeners
     */
    public void callOnlineDataListeners(int position, String date){
        Log.d("==>", "callOnlineDataListeners: size of lisneters "+this.dataGotListeners.size());
        for (DataGotListener dataGotListener : this.dataGotListeners) {
            if(dataGotListener != null){
                dataGotListener.onDataGot( position, date);
            }
        }
    }


    public void callOnDataSellDateListner(int postion, String date){
        for(DataGotSellDateListener dataGotSellDateListener : this.dataGotSellDateListeners){
            if(dataGotSellDateListener!=null){
                dataGotSellDateListener.onDataGotSell(postion,date);
            }
        }
    }


}
