package com.ictfa.krishiguru.helpers;

import android.app.Activity;
import android.app.ProgressDialog;

public class CustomProgressDialog {

    ProgressDialog mProgress;

    public CustomProgressDialog(Activity activity) {
        mProgress = new ProgressDialog(activity);
        mProgress.setIndeterminate(false);
    }

    public void showpd(String message) {
        mProgress.setMessage(message);
        mProgress.setCancelable(false);
        if (!mProgress.isShowing())
            mProgress.show();
    }

    public void hidepd() {
        if (mProgress != null && mProgress.isShowing())
            mProgress.dismiss();
    }
}
