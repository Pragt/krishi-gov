package com.ictfa.krishiguru.helpers;

/**
 * Created by Prajeet on 01/07/2017.
 */

public class CommonDef {

    /*
    code    user type
....    ..........
 0      super admin
 1      farmer
 2      trader
 3      agency
 4      project head
 5      site head
 7      organisation
     */

    public static final String ORGANIZATION_LIST = "Organization_list";
    public static final String PROFILE = "profile";
    public static final String NOTICE = "Notice";
    public static final int REQUEST_COMMENT = 100;
    public static final String LIBRARY_TYPE = "Library_type";
    public static final int REQUEST_STORAGE_CAMERA = 1002;
    public static final int REQUEST_ADD_CROP = 18;
    public static final String USER_PROFILE = "User_profile";
    public static final String TRADER_TYPE = "Trader type list";
    public static final int PICK_IMAGE_REQUEST = 1009;
    public static final String QUESTION = "Question";
    public static final String COMMENTS = "Comments";
    public static final String COMMENT_COUNT = "Comment_count";
    public static final String LIKE_COUNT = "Like Couont";
    public static final String SHARE_COUNT = "share_count";
    public static final String IS_LIKED = "isLiked";
    public static final String PRODUCT_ID = "Id";
    public static final String COMMODITY_LIST = "Commdity_list";
    public static final String IS_FARMER = "Is_Farmer";
    public static final String LIST_WEATHER = "WEATHER_LIST";
    public static final String IS_EDIT_MODE = "Is edit mode";
    public static final String IS_LIBRARY_LOADED = "Is library loaded";
    public static final String ARTICLE_ID = "Article_id";
    public static final String NEWS_ID = "News_ID";
    public static final String NAME = "name";
    public static final int IS_FROM_LOGIN = 01;
    public static final String QUESTION_ID = "QuestionId";
    public static final String LOCATION = "Location";
    public static final String IS_MY_PRICE = "Is_my_price";
    public static final String KHARID_BIKRI_OBJECT = "KHARID_BIKRI_OBJECT";


    public class SharedPreference {
        public static final String USER_ID = "User_Id";
        public static final String ORG_ID = "ORG_ID";
        public static final String IS_LOGIN = "IS_LOGIN";
        public static final String LATUTUDE = "Latitude";
        public static final String LONGITUDE = "Longitude";
        public static final String USERNAME = "Username";
        public static final String AGENCY = "Agency";
        public static final String USERTYPE = "UserType";
        public static final String IS_DEVICE_REGISTERED = "Is_device_registered";
        public static final String ANYNOMOUS_USER_ID = "Free_user";
        public static final String IS_OTP_SEND = "Is_otp_send";
        public static final String IS_NUMBER_VERIFIED = "Is_verified";
        public static final String VERIFIED_NUMBER = "Verified number";
        public static final String CHAT = "chat";
        public static final String HAS_PACKAGE_SUBSCRIBED = "Has package subscribed";
        public static final String PACKAGE_ID = "PackageId";
        public static final String ADDRESS = "Address";
        public static final String USER_IMAGE = "image";
    }
}
