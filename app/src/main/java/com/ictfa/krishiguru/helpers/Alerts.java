package com.ictfa.krishiguru.helpers;

import android.app.Activity;
import android.view.LayoutInflater;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by pragt on 5/14/17.
 */

public class Alerts {
    private final LayoutInflater li;
    Activity activity;

    public Alerts(Activity activity) {
        this.activity = activity;
        li = LayoutInflater.from(activity);
    }

    public void showToastMsg(String msg) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }

    public void showSuccessAlert(String msg) {
        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Success")
                .setContentText(msg)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        activity.finish();
                    }
                })
                .show();
    }

    public void showSuccess(String msg) {
        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Success")
                .setContentText(msg)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showErrorAlert(String msg) {
        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Oops...")
                .setContentText(msg)
                .show();
    }

    public void showWarningAlert(String msg) {
        new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("")
                .setContentText(msg)
                .show();
    }

    public void showConfirmationDialog(String message, final OnConfirmationClickListener listener, int requestCode) {
        new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(message)
                .setCancelText("नगर्नुहोस")
                .setConfirmText("गर्नुहोस")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        listener.onNoClicked();
                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                        listener.onYesClicked();
                    }
                })
                .show();
    }


    public void buyPackageConfirmation(String message, final OnConfirmationClickListener listener, int requestCode) {
        new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(message)
//                .setCancelText("चाहदिन")
                .setConfirmText("चाहन्छु")
                .showCancelButton(true)
//                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        sDialog.cancel();
//                        listener.onNoClicked();
//                    }
//                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                        listener.onYesClicked();
                    }
                })
                .show();
    }


    public interface OnConfirmationClickListener {
        void onYesClicked();

        void onNoClicked();
    }
}
