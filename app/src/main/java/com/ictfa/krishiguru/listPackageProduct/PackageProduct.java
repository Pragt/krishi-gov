package com.ictfa.krishiguru.listPackageProduct;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PackageProduct extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("is_my_package")
    @Expose
    public Boolean isMyPackage;
    @SerializedName("package")
    @Expose
    public PackageDesc _package;

}