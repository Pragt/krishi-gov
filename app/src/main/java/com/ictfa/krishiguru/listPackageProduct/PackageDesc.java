package com.ictfa.krishiguru.listPackageProduct;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class PackageDesc extends RealmObject {

        @SerializedName("recurring")
        @Expose
        public Boolean recurring;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("subscription_date")
        @Expose
        public String subscriptionDate;
        @SerializedName("expire_date")
        @Expose
        public String expireDate;

    }