package com.ictfa.krishiguru.listPackageProduct;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PackageProductResponse {

    @SerializedName("meta")
    @Expose
    public List<Object> meta = null;
    @SerializedName("data")
    @Expose
    public List<PackageProduct> data = null;
    @SerializedName("code")
    @Expose
    public Code code;
    @SerializedName("recent")
    @Expose
    public Recent recent;


    public class Code {

        @SerializedName("status")
        @Expose
        public Integer status;
        @SerializedName("message")
        @Expose
        public String message;

    }

    public class Recent {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("valid_days")
        @Expose
        public int validDays;
        @SerializedName("subscription_date")
        @Expose
        public String subscriptionDate;
        @SerializedName("expire_date")
        @Expose
        public String expireDate;
        @SerializedName("is_paid")
        @Expose
        public Integer isPaid;

    }
}

