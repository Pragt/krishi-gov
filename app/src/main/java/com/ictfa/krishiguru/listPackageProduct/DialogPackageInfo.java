package com.ictfa.krishiguru.listPackageProduct;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ictfa.krishiguru.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Prajeet on 07/10/2017.
 */

public class DialogPackageInfo extends DialogFragment {

    @BindView(R.id.tvPackageProductName)
    TextView tvPackageProductName;

    @BindView(R.id.tvPackageName)
    TextView tvPackageName;

    @BindView(R.id.tvExpiryDate)
    TextView tvExpiryDate;

    @BindView(R.id.tvSubscriptionDate)
    TextView tvSubscriptionDate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_recurring_package_info, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Do all the stuff to initialize your custom view
        tvPackageProductName.setText(getArguments().getString("packageProductName") + " को प्याकेज बिवरण");
        tvPackageName.setText("लिएकाे सेवाः " + getArguments().getString("packageName") + " + विज्ञ परामर्श ");
        if (getArguments().getBoolean("isRenewed"))
            tvExpiryDate.setText("    स्वतः नविकरण मिति: " + getArguments().getString("expiryDate") + "    ");
        else
            tvExpiryDate.setText("    सेवा समाप्त मिति: " + getArguments().getString("expiryDate") + "    ");
        tvSubscriptionDate.setText("    सेवा शुरू मिति: " + getArguments().getString("subscribedDate") + "    ");

    }
}
