package com.ictfa.krishiguru.listPackageProduct

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import butterknife.ButterKnife
import com.ictfa.krishiguru.R
import com.ictfa.krishiguru.chabBot.ChatBotActivity
import com.ictfa.krishiguru.helpers.Alerts
import com.ictfa.krishiguru.helpers.CommonDef
import com.ictfa.krishiguru.helpers.CustomProgressDialog
import com.ictfa.krishiguru.helpers.SharedPreference
import com.ictfa.krishiguru.realm.RealmController
import com.ictfa.krishiguru.retrofit.ApiClient
import com.ictfa.krishiguru.retrofit.ApiInterface
import io.realm.Realm
import kotlinx.android.synthetic.main.notifications_list_activity.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*

/**
 * Created by Prajeet on 07/10/2017.
 */

class PackageProductFragment : Fragment(), PackageProductAdapter.onItemCLickListener {

    lateinit var alerts: Alerts
    lateinit var pd: CustomProgressDialog
    lateinit var sharedPreference: SharedPreference
    var listPackageProducts: ArrayList<PackageProduct> = arrayListOf()
    private var mAdapter: PackageProductAdapter? = null
    private var realm: Realm? = null
    private val isFromNotification = false
    private val total_count: Int = 0
    private val isLoading: Boolean = false
    private val hasSetPackage: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.notifications_list_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this, view)

        init()

        listPackageProducts = ArrayList()
        val layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        rvNotifications!!.layoutManager = layoutManager
        listPackageProducts = ArrayList()
        mAdapter = PackageProductAdapter(activity!!, listPackageProducts)
        mAdapter!!.setListener(this)
        rvNotifications!!.adapter = mAdapter
        val realmListPackages = realm!!.where(PackageProduct::class.java).findAll()
        for (packageProduct in realmListPackages) {
            listPackageProducts.add(realm!!.copyFromRealm(packageProduct))
            tvNoNotification!!.visibility = View.GONE
        }
        mAdapter!!.addAll(listPackageProducts)

        swipe_refresh?.setOnRefreshListener { getPackages() }
    }


    private fun getPackages() {
        ApiClient.getClient().create(ApiInterface::class.java).getPackageProduct("O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA", sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID).toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    swipe_refresh?.isRefreshing = false
                    if (s.code.status == 1) {
                        realm?.executeTransaction {
                            it.delete(PackageProduct::class.java)
                        }
                        listPackageProducts.clear()
                        listPackageProducts.addAll(s.data)
                        mAdapter?.addAll(listPackageProducts)

                        for (packagesProduct in s.data) {
                            realm?.beginTransaction()
                            realm?.insertOrUpdate(packagesProduct)
                            realm?.commitTransaction()
                            tvNoNotification!!.visibility = View.GONE
                        }

                        s.recent?.let {
                            if (it.isPaid == 1) {
                                val intent = Intent(activity, ChatBotActivity::class.java)
                                intent.putExtra("is_another", true)
                                intent.putExtra("package_id", it.id)
                                intent.putExtra("packageProductName", it.title)
                                intent.putExtra("validity", it.validDays)
                                startActivity(intent)
                            }
                        }

                    } else {
                        alerts.showToastMsg(s.code.message)
                    }
                },
                        { e ->
                            alerts.showToastMsg("Could not connect to server. Please try again later.")
                            swipe_refresh?.isRefreshing = false
                            e.printStackTrace()
                        },
                        { kotlin.io.println("supervisor list") })

    }


    private fun init() {
        sharedPreference = SharedPreference(activity)
        this.realm = RealmController.with(activity).realm
        alerts = Alerts(activity)
        pd = CustomProgressDialog(activity)

    }

    override fun onItemClicked(packageProduct: PackageProduct) {
        val dialog = DialogPackageInfo()
        val bundle = Bundle()
        bundle.putString("packageName", packageProduct._package.name)
        bundle.putString("packageProductName", packageProduct.title)
        bundle.putString("subscribedDate", packageProduct._package.subscriptionDate)
        bundle.putString("expiryDate", packageProduct._package.expireDate)
        bundle.putBoolean("isRenewed", packageProduct._package.recurring)
        dialog.arguments = bundle
        dialog.show(fragmentManager!!, "dialog")
    }

    override fun onUnsubscribeClicked(packageId: Int, pos: Int) {
        alerts.showConfirmationDialog("के तपाई package unsubscribe गर्न चाहनु हुन्छ?\n", object : Alerts.OnConfirmationClickListener {
            override fun onYesClicked() {
                unsubscribePackage(packageId, pos)
            }

            override fun onNoClicked() {

            }
        }, 100)
    }

    override fun onNewPackageBuy(packageProduct: PackageProduct) {
//        val intent = Intent(activity, DialogPackageInfo::class.java)
//        intent.putExtra("packageProductId", packageProduct.id)
//        intent.putExtra("packageProductName", packageProduct.title)
//        startActivityForResult(intent, 100)
//        val dialogFragment = PackagesDialog()
//        dialogFragment.setListener { productId, packageProductName, validity, data ->
//            for ((index, packages) in listPackageProducts.withIndex()) {
//                if (packages.id == productId) {
//                    listPackageProducts.get(index).isMyPackage = true
//                    break
//                }
//            }
//
//            if (data.consent_ok) {
//                mAdapter?.notifyDataSetChanged()
//                val intent = Intent(activity, ChatBotActivity::class.java)
//                intent.putExtra("is_another", true)
//                intent.putExtra("package_id", productId)
//                intent.putExtra("packageProductName", packageProductName)
//                intent.putExtra("validity", validity)
//                startActivity(intent)
//            } else {
//                NcellWebViewActivity.start(activity!!, data.redirect_url)
//            }
//        }
//        val bundle = Bundle()
//        bundle.putInt("packageProductId", packageProduct.id)
//        bundle.putString("packageProductName", packageProduct.title)
//        dialogFragment.arguments = bundle
//        dialogFragment.show(fragmentManager!!, "dialog")
        alerts.showErrorAlert("we are on maintaince for this service.")
    }

    private fun unsubscribePackage(packageId: Int, pos: Int) {
        ApiClient.getClient().create(ApiInterface::class.java).unsubscribe(packageId, sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID), "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    listPackageProducts[pos].isMyPackage = false
                    mAdapter!!.addAll(listPackageProducts)
                    sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, "0")
                    for (i in listPackageProducts.indices) {
                        if (listPackageProducts[i].isMyPackage!!) {
                            sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, "1")
                            break
                        }
                    }

                    val packages = listPackageProducts[pos]
                    packages.isMyPackage = false
                    realm!!.beginTransaction()
                    realm!!.copyToRealmOrUpdate(packages)
                    realm!!.commitTransaction()
                    alerts.showSuccess(listPackageProducts[pos].title + " Package unsubscribed successfully.")
                },
                        { e ->
                            alerts.showErrorAlert("Could not connect to server. Please try again later.")
                            e.printStackTrace()
                        }, { println("supervisor list") })
    }


    override fun onResume() {
        super.onResume()
        getPackages()
    }

}
