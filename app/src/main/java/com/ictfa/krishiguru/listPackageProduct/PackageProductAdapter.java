package com.ictfa.krishiguru.listPackageProduct;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by pragt on 2/24/17.
 */

public class PackageProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM = 2;
    private static final int TYPE_PROGRESS = 1;
    private Activity activity;
    private ArrayList<PackageProduct> packagesList;
    private Random randomGenerator;
    private onItemCLickListener listener;
    private Boolean hasSetPackge = false;
    private SharedPreference sharedPreference;

    public void setListener(onItemCLickListener listener) {
        this.listener = listener;

    }

    // ForumData formData;
    public PackageProductAdapter(Activity activity, ArrayList<PackageProduct> packagesList) {
        this.activity = activity;
        sharedPreference = new SharedPreference(activity);


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(activity).inflate(R.layout.item_package_product, parent, false);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = (int) (parent.getWidth() * 0.339);
        layoutParams.height = layoutParams.width;
        view.setLayoutParams(layoutParams);
        return new myViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myViewHolder) {
            myViewHolder holder1 = (myViewHolder) holder;
            holder1.tvProductName.setText(packagesList.get(position).title);
//            if (hasSetPackge) {
//                holder1.tvSubscribe.setVisibility(View.INVISIBLE);
//                if (!packagesList.get(position).isMyPackage) {
//                    holder1.itemView.setAlpha(0.8f);
//                    holder1.btnUnSubscribe.setVisibility(View.GONE);
//                } else {
//                    holder1.itemView.setAlpha(1f);
//                    holder1.btnUnSubscribe.setVisibility(View.VISIBLE);
//                }
//            } else {
//                holder1.itemView.setAlpha(1f);
//                holder1.btnUnSubscribe.setVisibility(View.GONE);
//            }

            if (packagesList.get(position).isMyPackage) {
                if (packagesList.get(position)._package.recurring) {
                    holder1.tvSubscribe.setVisibility(View.GONE);
                    holder1.btnUnSubscribe.setVisibility(View.VISIBLE);
                } else {
                    holder1.tvSubscribe.setVisibility(View.VISIBLE);
//                    holder1.tvSubscribe.setText("Expires on " + packagesList.get(position)._package.expireDate);
                    holder1.tvSubscribe.setText("Subscribed");
                    holder1.btnUnSubscribe.setVisibility(View.GONE);
                }
                holder1.itemView.setAlpha(0.7f);
            } else {
                holder1.tvSubscribe.setVisibility(View.VISIBLE);
                holder1.btnUnSubscribe.setVisibility(View.GONE);
                holder1.tvSubscribe.setText("Subscribe");
                holder1.itemView.setAlpha(1f);

            }

            holder1.btnUnSubscribe.setOnClickListener(v -> {
                listener.onUnsubscribeClicked(packagesList.get(position).id, position);
            });

            String image = "http://admin.ict4agri.com/" + packagesList.get(position).image;
            Picasso.with(activity).load(image).resize(500, 500).into(((myViewHolder) holder).ivPackageProduct);

            holder1.itemView.setOnClickListener(v -> {
                if (!packagesList.get(position).isMyPackage) {
                    listener.onNewPackageBuy(packagesList.get(position));
                } else {
                    listener.onItemClicked(packagesList.get(position));
                }
            });
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return packagesList.size();
    }


    public class myViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvProductName;
        private final TextView tvSubscribe;
        private final ConstraintLayout cvPackages;
        private final TextView btnUnSubscribe;
        private final ImageView ivPackageProduct;

        public myViewHolder(View convertView) {
            super(convertView);
            tvProductName = (TextView) convertView.findViewById(R.id.tvProductName);
            tvSubscribe = (TextView) convertView.findViewById(R.id.tvSubscribe);
            btnUnSubscribe = convertView.findViewById(R.id.btnUnSubscribe);
            cvPackages = convertView.findViewById(R.id.cvPackages);
            ivPackageProduct = convertView.findViewById(R.id.ivPackageProduct);

            cvPackages.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent e) {
                    if (e.getAction() == MotionEvent.ACTION_DOWN) {
                        cvPackages.setAlpha(1f);
                    } else if (e.getAction() == MotionEvent.ACTION_UP) {
                        cvPackages.setAlpha(0.8f);
                    }
                    return false;
                }
            });
        }
    }

    private class myProgressViewHolder extends RecyclerView.ViewHolder {
        public myProgressViewHolder(View view) {
            super(view);
        }
    }

    public void addAll(ArrayList<PackageProduct> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isMyPackage) {
                sharedPreference.setKeyValues(CommonDef.SharedPreference.HAS_PACKAGE_SUBSCRIBED, "1");
                hasSetPackge = true;
                break;
            }
            hasSetPackge = false;
        }

        this.packagesList = list;
        notifyDataSetChanged();
    }

    public interface onItemCLickListener {
        void onItemClicked(PackageProduct packageProduct);

        void onUnsubscribeClicked(int packageId, int pos);

        void onNewPackageBuy(PackageProduct packageProduct);
    }
}
