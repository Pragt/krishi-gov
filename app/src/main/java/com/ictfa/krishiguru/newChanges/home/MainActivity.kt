package com.ictfa.krishiguru.newChanges.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ictfa.krishiguru.R

/**
Created by Prajeet Naga on 12/23/18, 8:31 PM.
 **/
class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_new)
    }
}