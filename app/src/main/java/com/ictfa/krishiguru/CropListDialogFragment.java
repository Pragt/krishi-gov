package com.ictfa.krishiguru;


import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.ui.adapter.MarketNameAdapter;
import com.ictfa.krishiguru.ui.dtos.MarketNameList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CropListDialogFragment extends DialogFragment implements MarketNameAdapter.MarketClickListner {

    private ListView marketName;
    private List<MarketNameList> marketNameLists = new ArrayList<>();
    private MarketNameAdapter marketNameAdapter;
    private CustomProgressDialog progressDialog;
    CropFilterListner listner;
    private TextView title;

    public CropListDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.dialog_theme);

        // Inflate the layout for this fragment

        progressDialog = new CustomProgressDialog(getActivity());
        View view = inflater.inflate(R.layout.fragment_market_name_dialog, container, false);
        marketName = (ListView)view.findViewById(R.id.marketList);
        marketNameAdapter = new MarketNameAdapter(getActivity(),marketNameLists);
        title = (TextView)view.findViewById(R.id.market_name);
        title.setText("बालीहरु ");
        marketNameAdapter.setListner(this);
        marketName.setAdapter(marketNameAdapter);
        return view;
    }

    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d!=null){
            DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;

            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData(){
       marketNameLists.clear();
       RequestQueue queue = Volley.newRequestQueue(getActivity());
       progressDialog.showpd(" लोड हुँदै... ");

       String url = UrlHelper.BASE_URL + "api/v3/category/list?apikey=" + UrlHelper.API_KEY;
       StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
               new Response.Listener<String>() {
                   @Override
                   public void onResponse(String response) {
                       progressDialog.hidepd();
                       try {
                           String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");

                           JSONObject jsonObject = new JSONObject(utfStr);

                           JSONArray jsonArray = jsonObject.getJSONArray("data");

                           for(int i = 0; i<jsonArray.length(); i++){

                               JSONObject value = jsonArray.getJSONObject(i);

                                    MarketNameList marketNameList = new MarketNameList();
                                   String id = value.getString("id");
                                   String MarketName = value.getString("name");
                                   marketNameList.setId(id);
                                    marketNameList.setMarketName(MarketName);
                                    marketNameLists.add(marketNameList);

                           }

                           marketNameAdapter.notifyDataSetChanged();
                           Log.d("JSONRESPONSE=>>>>>>>>", jsonArray.toString());
                       }

                       catch (JSONException e) {
                           e.printStackTrace();
                       } catch (UnsupportedEncodingException e) {
                           e.printStackTrace();
                       }

                   }
               }, new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {
               progressDialog.hidepd();
               // mTextView.setText("That didn't work!");

           }
       });
// Add the request to the RequestQueue.
       queue.add(stringRequest);
    }

    @Override
    public void onMarketClicked(String market_id, String market_name) {
        this.listner.doFilter(market_id, market_name);
        dismiss();
    }

    public void setListner(CropFilterListner listner) {
        this.listner = listner;
    }

    public interface CropFilterListner {
        void doFilter(String cropId, String crop_type);
    }
}
