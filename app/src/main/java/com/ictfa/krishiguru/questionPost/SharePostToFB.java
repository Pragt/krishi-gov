package com.ictfa.krishiguru.questionPost;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;

import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

public class SharePostToFB {

    public static void share(Context mContext, Bitmap bm, String userImage) {
        SharePhoto post = new SharePhoto.Builder()
                .setBitmap(bm)
                .setCaption("Krishi Guru")

                .build();

        ShareHashtag shareHashtag = new ShareHashtag.Builder()
                .setHashtag("Krishi Guru").setHashtag("https://play.google.com/store/apps/details?id=com.ictfa.krishiguru").build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(post)
                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.ictfa.krishiguru"))
                .setShareHashtag(shareHashtag)
                .build();

        if (!userImage.isEmpty()) {
            SharePhoto photo = new SharePhoto.Builder()
                    .setImageUrl(Uri.parse(userImage))
                    .setCaption("Krishi Guru")
                    .build();
            SharePhotoContent content1 = new SharePhotoContent.Builder()
                    .addPhoto(post)
                    .addPhoto(photo)
                    .setShareHashtag(shareHashtag)
                    .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.ictfa.krishiguru"))
                    .setRef("https://play.google.com/store/apps/details?id=com.ictfa.krishiguru")
                    .build();
            ShareDialog shareDialog = new ShareDialog((Activity) mContext);
            shareDialog.show((Activity) mContext, content1);
        } else {
            ShareDialog.show((Activity) mContext, content);
        }

    }
}