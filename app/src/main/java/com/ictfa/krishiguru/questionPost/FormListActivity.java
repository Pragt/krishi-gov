package com.ictfa.krishiguru.questionPost;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.customViews.EndlessRecyclerOnScrollListener;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.ui.adapter.ForumData;
import com.ictfa.krishiguru.utils.TimeAgo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

public class FormListActivity extends AppCompatActivity implements FormAdapter.FormAdapterListner{
    CustomProgressDialog pd;
    LinearLayoutManager layoutManager;
    private RecyclerView rvForamList;
    private FormAdapter formAdapter;
    private List<ForumData> forumDatas = new ArrayList<>();
    private SharedPreference mypref;
    private int userID;
    private int total_count;
    private int total_displayed = -1;
    private boolean isLoading = false;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Realm realm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_list);
        rvForamList = (RecyclerView) findViewById(R.id.formListActivity);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);

        getSupportActionBar().setTitle("कृषि फोरम");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mypref = new SharedPreference(this);
        userID = mypref.getIntValues(CommonDef.SharedPreference.USER_ID);

        pd = new CustomProgressDialog(this);
        layoutManager = new LinearLayoutManager(this);
        rvForamList.setLayoutManager(layoutManager);


        this.realm = RealmController.with(this).getRealm();
        RealmResults<ForumData> realmResults = realm.where(ForumData.class).findAll();
        for (ForumData a : realmResults)
            forumDatas.add(a);

        formAdapter = new FormAdapter(FormListActivity.this, realm.copyFromRealm(forumDatas));
        formAdapter.setListner(this);
        rvForamList.setAdapter(formAdapter);

        formAdapter.notifyDataSetChanged();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFormListData(true);
            }
        });
        if (forumDatas.size() == 0)
            pd.showpd(getResources().getString(R.string.please_wait));
        else
            swipeRefreshLayout.setRefreshing(true);
        getFormListData(true);

        rvForamList.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (total_count > total_displayed && !isLoading)
                    getFormListData(false);
            }
        });
    }

    private void getFormListData(final boolean clearOld) {
        if (!isLoading)
            forumDatas.add(null);

        formAdapter.addAll(forumDatas);

        RequestQueue queue = Volley.newRequestQueue(FormListActivity.this);
        String url = BaseApplication.FORM_QUESTION_LIST_API + userID;
        isLoading = true;
        Log.d("FORMURL", url);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url + "&length=" + 10 + "&start=" + (int) (!clearOld ? forumDatas.size() : 0),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        swipeRefreshLayout.setRefreshing(false);
                        isLoading = false;
                        forumDatas.remove(forumDatas.size() - 1);
                        //forumDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            Log.d("utfstr", utfStr);
                            JSONObject jsonObject = new JSONObject(utfStr);

                            JSONObject jsonResponse = jsonObject.getJSONObject("meta");
                            total_count = jsonResponse.getInt("total_count");
                            total_displayed = jsonResponse.getInt("total_displayed");

                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            if (clearOld) {
                                forumDatas = new ArrayList<>();
                                formAdapter = new FormAdapter(FormListActivity.this, realm.copyFromRealm(forumDatas));
                                formAdapter.setListner(FormListActivity.this);
                                rvForamList.setAdapter(formAdapter);
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.delete(ForumData.class);
                                    }
                                });
                            }

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject newsValue = jsonArray.getJSONObject(i);

                                String askBy = newsValue.getString("ask_by");
                                String address = newsValue.getString("district");
                                String title = newsValue.getString("question");
                                String imageresource = newsValue.getString("image_url");
                                String user_image = newsValue.getString("user_image");
                                String description = newsValue.getString("description");
                                String Time = newsValue.getString("created_at");
                                String commentCount = newsValue.getString("comment_count");
                                String commentCountNum = newsValue.getString("comment_count");
                                String noHTMLString = description.replaceAll("\\<.*?>", "");

                                String likeCount = newsValue.getString("like_count");
                                String LikeStatus = newsValue.getString("is_liked");
                                String questionID = newsValue.getString("id");
//                                if(commentCount.equalsIgnoreCase("0")){
//                                    commentCount = commentCount+ " comment";
//                                }
//                                else {
//                                    commentCount = commentCount+ " comments";
//                                }

                                TimeAgo timeAgo = new TimeAgo();
                                String stringTimeAgo = "0";
                                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                                try {

                                    Date date = format.parse(Time);
                                    stringTimeAgo = timeAgo.toDuration(date);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                //long duration = date.getTime();
                                // Toast.makeText(DashBoardActivity.this,duration+"",Toast.LENGTH_SHORT).show();
                                ForumData forumData = new ForumData();

                                forumData.setNameAddress(askBy + " " + "-" + " " + address);
                                forumData.setFormTime(Time);
                                forumData.setProfileImage(imageresource);
                                forumData.setUserImage(user_image);
                                forumData.setDesc(description);
                                forumData.setTitle(title);
                                forumData.setCommentNums(commentCount);
                                forumData.setLikeCount(likeCount);
                                forumData.setIsLiked(LikeStatus);
                                forumData.setUserId(String.valueOf(userID));
                                forumData.setQuestionID(questionID);
                                forumDatas.add(forumData);
                            }

                            for (ForumData b : forumDatas) {
                                // Persist your data easily
                                if (b != null) {
                                    realm.beginTransaction();
                                    realm.copyToRealmOrUpdate(b);
                                    realm.commitTransaction();
                                }
                            }

                            formAdapter.addAll(forumDatas);
                            Log.d("JSONRESPONSE=>>>>>>>>", jsonArray.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                pd.hidepd();
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;

            case R.id.add_news:
                Intent intent = new Intent(FormListActivity.this, AskQuestionActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.addnews_menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        forumDatas = new ArrayList<>();
//        getFormListData(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                try {
                    int count = data.getIntExtra("commentCount", 0);
                    int pos = data.getIntExtra("pos", 0);
                    String likeCount = data.getStringExtra("likeCount");
                    String isLiked = data.getStringExtra("isLiked");
                    forumDatas.get(pos).setCommentNums(count + "");
                    forumDatas.get(pos).setLikeCount(likeCount);
                    forumDatas.get(pos).setIsLiked(isLiked);
                    formAdapter.notifyDataSetChanged();
                }catch (Exception exx){}

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v("test","Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission

        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("Test","Permission is granted");
                return true;
            } else {

                Log.v("Test","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("Test","Permission is granted");
            return true;
        }
    }


    @Override
    public void onShareClicked(int position, View itemView, String userImage) {
      if (isStoragePermissionGranted()){
          Bitmap b = getBitmapRootView(itemView);
          SharePostToFB.share(this, b, userImage);
        }
    }

    private Bitmap getBitmapRootView(View view)
    {
        View rootView = view;
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

}
