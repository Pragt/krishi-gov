package com.ictfa.krishiguru.questionPost;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.Comment.FullQuestionActivity;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;
import com.ictfa.krishiguru.login.LoginActivity;
import com.ictfa.krishiguru.ui.adapter.FormImageList;
import com.ictfa.krishiguru.ui.adapter.FormImageListAdapter;
import com.ictfa.krishiguru.ui.adapter.ForumData;
import com.ictfa.krishiguru.ui.fragment.ImageShowFragment;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sureshlama on 2/24/17.
 */

public class FormAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_PROGRESS = 1;
    private static final int TYPE_ITEM = 2;
    private final SharedPreference sharedPreference;
    List<FormImageList> formImageLists = new ArrayList<>();
    FormImageListAdapter formImageListAdapter;
    Alerts alerts;
    CustomProgressDialog pd1;
    private Activity activity;
    private List<ForumData> forumDatas;
    protected FormAdapterListner mListner;

    // ForumData formData;
    public FormAdapter(Activity activity, List<ForumData> forumDatas) {
        this.activity = activity;
        this.forumDatas = forumDatas;
        sharedPreference = new SharedPreference(activity);
        pd1 = new CustomProgressDialog(activity);
        alerts = new Alerts(activity);
    }

    @Override
    public int getItemViewType(int position) {
        if (forumDatas.get(position) == null)
            return TYPE_PROGRESS;
        else
            return TYPE_ITEM;
    }

    void setListner(FormAdapterListner listner){
        this.mListner = listner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_ITEM) {
            view = LayoutInflater.from(activity).inflate(R.layout.custome_form_list, parent, false);
            return new myViewHolder(view);
        } else
            view = LayoutInflater.from(activity).inflate(R.layout.item_progress_dialog, parent, false);
        return new myProgressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myViewHolder) {
            final myViewHolder holder1 = (myViewHolder) holder;
            final ForumData forumData;
            try {
                forumData = forumDatas.get(position).clone();
                if (forumData.getIsLiked().equals("true"))
                    holder1.likeCounter.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                else
                    holder1.likeCounter.setTextColor(activity.getResources().getColor(R.color.black));

                Picasso.with(activity).load(R.drawable.ic_icon_man).into(holder1.icon);
//        if (forumData.getProfileImage().isEmpty()) {
//        } else {
//            Picasso.with(activity).load(forumData.getProfileImage()).into(icon);
//        }


                holder1.shareImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder1.shareNow.callOnClick();
                    }
                });


                holder1.shareNow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListner.onShareClicked(position, holder.itemView, forumData.getProfileImage());
                    }
                });


                holder1.likeCounter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder1.likeIcon.callOnClick();
                    }
                });

                holder1.likeIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN) == 1) {
                            if (forumData.getIsLiked() != null) {
                                if (forumData.getIsLiked().equals("true")) {
                                    Toast.makeText(activity, "you already like this", Toast.LENGTH_SHORT).show();
                                } else {
                                    int likevalue = Integer.parseInt(forumData.getLikeCount()) + 1;
                                    holder1.likeCount.setText(likevalue + " Like");
                                    sendLike(forumData.getQuestionID(), forumData.getUserId());
                                    holder1.likeCounter.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                                    forumData.setIsLiked("true");
                                    forumData.setLikeCount(String.valueOf(likevalue));
                                }
                            }
                        }else
                        {
                            Intent intent = new Intent(activity, LoginActivity.class);
                            activity.startActivity(intent);
                        }
                    }
                });
                holder1.nameAndAddrress.setText(forumData.getNameAddress());
                holder1.time.setText(forumData.getFormTime());
                holder1.title.setText(forumData.getTitle());
                holder1.desc.setText(forumData.getDesc());
                holder1.comment.setText(forumData.getCommentNums() + " Comments");
                holder1.likeCount.setText(forumData.getLikeCount() + " Like");

                if (forumData.getProfileImage().equalsIgnoreCase(""))
                    holder1.descImg.setVisibility(View.GONE);
                else {
                    Picasso.with(activity).load(forumData.getProfileImage()).networkPolicy(NetworkPolicy.OFFLINE).resize(500, 500).into(holder1.descImg);
                    holder1.descImg.setVisibility(View.VISIBLE);
                }

                if (!forumData.getUserImage().equalsIgnoreCase(""))
                    Picasso.with(activity).load(forumData.getUserImage()).into(holder1.icon);

                holder1.descImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ImageShowFragment imageShowFragment = ImageShowFragment.newInstance();
                        Bundle bundle = new Bundle();
                        bundle.putString("image", forumData.getProfileImage());
                        imageShowFragment.setArguments(bundle);
                        imageShowFragment.show(activity.getFragmentManager(), "sad");
                    }
                });

                holder1.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder1.llComments.callOnClick();
                    }
                });

                holder1.llComments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getComments(forumData.getQuestionID(), position);
                    }
                });

                formImageListAdapter = new FormImageListAdapter(activity, formImageLists);
                //imageForm.setAdapter(formImageListAdapter);

                int countofImages = 3;

                String[] imagelist = new String[countofImages];
                imagelist[0] = "http://agrifarming.in/wp-content/uploads/2015/03/Harvesting-Potatoes.jpg";
                // imagelist[1]= "http://www.asiafarming.com/wp-content/uploads/2016/02/Growing-Potatoes-in-Greenhouse.jpg";
                imagelist[1] = "http://www.asiafarming.com/wp-content/uploads/2016/02/Potato-Cultivation-506x330.jpg";

                formImageLists.clear();
                for (int i = 0; i < 2; i++) {
                    FormImageList formImageList = new FormImageList();
                    formImageList.setFormImages(imagelist[i]);

                    formImageLists.add(formImageList);
                }
                formImageListAdapter.notifyDataSetChanged();
            } catch (CloneNotSupportedException e) {

            }

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return this.forumDatas.size();
    }


    public void getComments(String questionID, final int position) {
        pd1.showpd(activity.getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(activity);
        String zone = UrlHelper.BASE_URL + "api/v3/forum/comments/" + questionID + "?apikey=" + UrlHelper.API_KEY;
        final ArrayList<Comment> listComment = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd1.hidepd();
                        //forumDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray data = jsonObject.getJSONArray("data");
                            Comment comment;
                            if (data != null && data.length() > 0) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject commentObj = data.getJSONObject(i);
                                    comment = new Comment();
                                    comment.comment_id = Integer.parseInt(commentObj.getString("id"));
                                    comment.comment = commentObj.getString("comment");
                                    comment.commented_by = commentObj.getString("commented_by");
                                    comment.date = commentObj.getString("created_at");
                                    listComment.add(comment);
                                }

                            }

                            Intent intent = new Intent(activity, FullQuestionActivity.class);
                            intent.putExtra(CommonDef.QUESTION_ID, forumDatas.get(position).getQuestionID());
                            intent.putExtra(CommonDef.COMMENTS, listComment);
                            intent.putExtra("pos", position);
                            activity.startActivityForResult(intent, 100);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(activity.getResources().getString(R.string.no_internet_connection));
                pd1.hidepd();
                // mTextView.setText("That didn't work!");
            }


        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    private void sendLike(String QuestionID, String UserID) {
        RequestQueue queue = Volley.newRequestQueue(activity);
        //  String url ="http://www.google.com";
        String url = "http://admin.ict4agri.com/api/v3/question/" + QuestionID + "/like?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA&user_id=" + UserID;
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("LIKERESPONSE", response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(activity, "not work", Toast.LENGTH_SHORT).show();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void addAll(List<ForumData> forumDatas) {
        this.forumDatas = forumDatas;
        notifyDataSetChanged();
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        private final CircleImageView icon;
        private final TextView nameAndAddrress, time, title, desc, comment, likeCounter, likeCount, shareNow;
        private final ImageView descImg, shareImg, likeIcon;
        private final LinearLayout llComments;

        public myViewHolder(View convertView) {
            super(convertView);

            icon = (CircleImageView) convertView.findViewById(R.id.formImage);
            nameAndAddrress = (TextView) convertView.findViewById(R.id.formNameAndAddress);
            time = (TextView) convertView.findViewById(R.id.formTime);
            title = (TextView) convertView.findViewById(R.id.formTitle);
            desc = (TextView) convertView.findViewById(R.id.formDesc);
            comment = (TextView) convertView.findViewById(R.id.numsOfComments);
            // HorizontalListView imageForm = (HorizontalListView)convertView.findViewById(R.id.imageList);
            descImg = (ImageView) convertView.findViewById(R.id.descImage);

//        ImageView commentImg = (ImageView) convertView.findViewById(R.id.commentIcon);
            shareImg = (ImageView) convertView.findViewById(R.id.iconShare);
            shareNow = (TextView) convertView.findViewById(R.id.shareNow);

            likeIcon = (ImageView) convertView.findViewById(R.id.likeicon);
            likeCounter = (TextView) convertView.findViewById(R.id.likecounter);
            likeCount = (TextView) convertView.findViewById(R.id.likecounter);
            llComments = (LinearLayout) convertView.findViewById(R.id.ll_comments);
        }
    }

    private class myProgressViewHolder extends RecyclerView.ViewHolder {
        public myProgressViewHolder(View view) {
            super(view);
        }
    }

    public interface FormAdapterListner{
        void onShareClicked(int position, View itemView, String userImage);
    }
}
