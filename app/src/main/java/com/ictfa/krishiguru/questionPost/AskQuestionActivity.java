package com.ictfa.krishiguru.questionPost;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.FileUtils;
import com.ictfa.krishiguru.helpers.ImagePicker;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.login.LoginActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class AskQuestionActivity extends AppCompatActivity {

    @BindView(R.id.et_qns_title)
    EditText edtQuestionTitle;

    @BindView(R.id.et_question_desc)
    EditText edtQuestionDesc;

    @BindView(R.id.btn_publish)
    Button btnPublish;

    @BindView(R.id.ivPicture)
    ImageView selectImage;

    @BindView(R.id.rlChooseImage)
    RelativeLayout rlChooseImage;

    Alerts alerts;
    SharedPreference sharedPreference;
    CustomProgressDialog pd;
    private String image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_question);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(getResources().getString(R.string.write_question));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        alerts = new Alerts(this);
        pd = new CustomProgressDialog(this);
        sharedPreference = new SharedPreference(this);
    }

    @OnClick(R.id.btn_publish)
    void onPublishClicked() {
        if (sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) != 0) {
            JSONObject jsonObject;
            RequestQueue queue = Volley.newRequestQueue(AskQuestionActivity.this);
            String url = UrlHelper.BASE_URL + "api/v3/forum/question/ask?apikey=" + UrlHelper.API_KEY;


            if (isValid()) {
                pd.showpd(getResources().getString(R.string.please_wait));

                RequestParams params = new RequestParams();
                params.put("user_id", String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)));
                params.put("question", edtQuestionTitle.getText().toString());
                params.put("description", edtQuestionDesc.getText().toString());

                if (image != null && !image.equalsIgnoreCase("")) {
                    final File myFile = new File(image);
                    try {
                        params.put("image", myFile, "image/jpeg");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }


                AsyncHttpClient client = new AsyncHttpClient();
                client.setConnectTimeout(10000);
                client.setTimeout(10000);
                client.setResponseTimeout(30000);
                client.post(url, params, new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                        pd.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
//                        String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                        JSONObject jsonObject =responseBody;
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        JSONObject responseObj = jsonArray.getJSONObject(0);
                        int status = responseObj.getInt("status");
                        String message = responseObj.getString("message");
                        if (status == 1) {
                            alerts.showToastMsg(message);
                            finish();
                        }
                        else
                            alerts.showErrorAlert(message);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        pd.hidepd();
                    }

                });
            }
        } else {
            Intent intent = new Intent(AskQuestionActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }

    @OnClick(R.id.rlChooseImage)
    void onChooseImageClicked() {
        showFileChooser();
    }

    private void showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CommonDef.REQUEST_STORAGE_CAMERA);
                return;
            }
        }
        startActivityForResult(ImagePicker.getPickImageChooserIntent(this), CommonDef.PICK_IMAGE_REQUEST);
    }

    private boolean isValid() {
        if (edtQuestionTitle.getText().toString().isEmpty()) {
            alerts.showToastMsg(getResources().getString(R.string.write_question));
            return false;
        } else if (edtQuestionDesc.getText().toString().isEmpty()) {
            alerts.showToastMsg(getResources().getString(R.string.question_desc));
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CommonDef.PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            Uri imageUri;
            if (data != null && data.getData() != null) {
                imageUri = data.getData();
                selectImage.setImageURI(imageUri);
                Bitmap imageBmp;
                try {
                    imageBmp = FileUtils.getThumbnail(this, imageUri, 1080);
                    image = FileUtils.storeBitmapToFile(imageBmp, this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else // From camera
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    image = ImagePicker.getImage.getAbsolutePath();
                } else
                    image = ImagePicker.outputFileUri.getPath();

                selectImage.setImageURI(Uri.fromFile(new File(image)));
            }


                //Getting the Bitmap from Gallery
                //Setting the Bitmap to ImageView
//                image = getPath(imageUri);


        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            startActivityForResult(ImagePicker.getPickImageChooserIntent(this), CommonDef.PICK_IMAGE_REQUEST);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
