package com.ictfa.krishiguru.Comment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.customViews.GridSpacingItemDecoration;
import com.ictfa.krishiguru.fullNoticeView.CommentAdapter;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;
import com.ictfa.krishiguru.login.LoginActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prajeet on 03/08/2017.
 */

public class CommentProductActivity extends AppCompatActivity {

    ArrayList<Comment> listComment;

    @BindView(R.id.rv_comments)
    RecyclerView rvComments;
    @BindView(R.id.rl_add_comments)
    RelativeLayout rlAddComments;
    @BindView(R.id.edt_comment)
    EditText edtComment;
    @BindView(R.id.iv_send)
    ImageView ivSend;
    SharedPreference sharedPreference;
    CustomProgressDialog pd1;
    Alerts alerts;
    private CommentAdapter mAdapter;
    String productId;
    private int userId;
    private String name;

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_view);
        CommonMethods.setupUI(findViewById(R.id.rl_full_question), this);
        setTitle(getResources().getString(R.string.comments));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            listComment = (ArrayList<Comment>) getIntent().getSerializableExtra(CommonDef.COMMENTS);
            productId = getIntent().getStringExtra(CommonDef.PRODUCT_ID);
        }

        init();
        sharedPreference = new SharedPreference(this);
        pd1 = new CustomProgressDialog(this);
        alerts = new Alerts(this);
        userId = sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
        name = sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME);
    }

    private void init() {

//        ImageView commentImg = (ImageView) findViewById(R.id.commentIcon);

        LinearLayout llComments = (LinearLayout) findViewById(R.id.ll_comments);

        String urlString = "http://agrifarming.in/wp-content/uploads/2015/03/potato-farming.jpg";
        URL myURL = null;
        try {
            myURL = new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(this, 0), true));
        rvComments.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CommentAdapter(this, listComment);
        rvComments.setAdapter(mAdapter);
    }


    @OnClick(R.id.iv_send)
    void onSendClick() {
        if (!edtComment.getText().toString().isEmpty()) {
            doComment(edtComment.getText().toString());
        }
    }

    private void doComment(final String comment) {
        if (sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) == 0) {
            Intent intent = new Intent(CommentProductActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            pd1.showpd(getResources().getString(R.string.registering));
            JSONObject jsonObject;
            RequestQueue queue = Volley.newRequestQueue(CommentProductActivity.this);
            String commentUrl = UrlHelper.BASE_URL + "api/v3/production/" + productId + "/comment?apikey=" + UrlHelper.API_KEY;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, commentUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pd1.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObj = new JSONObject(response);
                                JSONArray codeJson = jsonObj.getJSONArray("data");
                                JSONObject object = codeJson.getJSONObject(0);
                                String status = object.getString("status");
                                String message = object.getString("message");
                                Comment newComment;
                                if (status.equalsIgnoreCase("1")) {
                                    newComment = new Comment();
                                    newComment.comment = comment;
                                    newComment.commented_by = sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME);
                                    newComment.date = "now";
                                    mAdapter.addComment(newComment);
                                    listComment.add(newComment);
                                    rvComments.smoothScrollToPosition(listComment.size());
                                    edtComment.setText("");
                                } else
                                    alerts.showErrorAlert(message);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                    pd1.hidepd();
                    // mTextView.setText("That didn't work!");
                }


            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("tag", "login");
                    params.put("user_id", String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)));
                    params.put("comment", comment);
                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(CommonDef.COMMENT_COUNT, listComment.size());
        setResult(RESULT_OK, intent );
        finish();
    }
}