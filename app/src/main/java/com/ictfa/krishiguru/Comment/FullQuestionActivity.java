package com.ictfa.krishiguru.Comment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.customViews.GridSpacingItemDecoration;
import com.ictfa.krishiguru.fullNoticeView.CommentAdapter;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;
import com.ictfa.krishiguru.questionPost.SharePostToFB;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.login.LoginActivity;
import com.ictfa.krishiguru.ui.adapter.ForumData;
import com.ictfa.krishiguru.ui.fragment.ImageShowFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * Created by Prajeet on 03/08/2017.
 */

public class FullQuestionActivity extends AppCompatActivity {

    ArrayList<Comment> listComment;
    ForumData forumData;
    ForumData mainObject;
    String QuestionId = "";

    @BindView(R.id.rv_comments)
    RecyclerView rvComments;
    @BindView(R.id.rl_add_comments)
    RelativeLayout rlAddComments;
    private CommentAdapter mAdapter;
    @BindView(R.id.edt_comment)
    EditText edtComment;
    @BindView(R.id.iv_send)
    ImageView ivSend;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;

    SharedPreference sharedPreference;
    CustomProgressDialog pd1;
    Alerts alerts;
    private TextView commentCount;
    private int userId;
    private String name;
    int position;
    private Realm realm;

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.activity_full_question);
        CommonMethods.setupUI(findViewById(R.id.rl_full_question), this);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.comments));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().getExtras() != null) {
            listComment = (ArrayList<Comment>) getIntent().getSerializableExtra(CommonDef.COMMENTS);

            this.realm = RealmController.with(this).getRealm();
            if (getIntent().getBooleanExtra("isFromNotification", false)) {
                forumData = (ForumData) getIntent().getSerializableExtra(CommonDef.QUESTION);
            } else {
                QuestionId = getIntent().getStringExtra(CommonDef.QUESTION_ID);
                mainObject =
                        realm.where(ForumData.class).equalTo("questionID", QuestionId).findFirst();
                forumData = realm.copyFromRealm(mainObject);

            }

            position = getIntent().getIntExtra("pos", 0);
            QuestionId = forumData.getQuestionID();
        }

        init();
        sharedPreference = new SharedPreference(this);
        pd1 = new CustomProgressDialog(this);
        alerts = new Alerts(this);
        userId = sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
        name = sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME);
    }

    private void init() {
        CircleImageView icon = (CircleImageView) findViewById(R.id.formImage);
        TextView nameAndAddrress = (TextView) findViewById(R.id.formNameAndAddress);
        TextView time = (TextView) findViewById(R.id.formTime);
        TextView title = (TextView) findViewById(R.id.formTitle);
        TextView desc = (TextView) findViewById(R.id.formDesc);
        commentCount = (TextView) findViewById(R.id.numsOfComments);
        // HorizontalListView imageForm = (HorizontalListView)convertView.findViewById(R.id.imageList);
        ImageView descImg = (ImageView) findViewById(R.id.descImage);

//        ImageView commentImg = (ImageView) findViewById(R.id.commentIcon);
        ImageView shareImg = (ImageView) findViewById(R.id.iconShare);
        final TextView shareNow = (TextView) findViewById(R.id.shareNow);

        final ImageView likeIcon = (ImageView) findViewById(R.id.likeicon);
        final TextView likeCounter = (TextView) findViewById(R.id.likecounter);
        final TextView likeCount = (TextView) findViewById(R.id.likecounter);
        LinearLayout llComments = (LinearLayout) findViewById(R.id.ll_comments);

        if (forumData == null)
            finish();

        if (forumData.getProfileImage() != null && !forumData.getProfileImage().equalsIgnoreCase(""))
            Picasso.with(this).load(forumData.getProfileImage()).resize(500, 500).into(descImg);
        else
            descImg.setVisibility(View.GONE);
        descImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImageShowFragment imageShowFragment = ImageShowFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("image", forumData.getProfileImage());
                imageShowFragment.setArguments(bundle);
                imageShowFragment.show(getFragmentManager(), "sad");
            }
        });

        if (forumData.getIsLiked().equals("true"))
            likeCounter.setTextColor(this.getResources().getColor(R.color.colorPrimaryDark));
        else
            likeCounter.setTextColor(this.getResources().getColor(R.color.black));


        shareImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareNow.callOnClick();
            }
        });


        shareNow.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Bitmap b = getBitmapRootView(findViewById(R.id.cvQuestion));
                                            SharePostToFB.share(FullQuestionActivity.this, b, forumData.getProfileImage());
                                        }

                                        private Bitmap getBitmapRootView(View view) {
                                            View rootView = view;
                                            rootView.setDrawingCacheEnabled(true);
                                            return rootView.getDrawingCache();
                                        }
                                    });



        likeCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                likeIcon.callOnClick();
            }
        });

        likeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreference.getIntValues(CommonDef.SharedPreference.IS_LOGIN) == 1) {
                    if (forumData.getIsLiked().equals("true")) {
                        Toast.makeText(FullQuestionActivity.this, "you already like this", Toast.LENGTH_SHORT).show();
                    } else {
                        int likevalue = Integer.parseInt(forumData.getLikeCount()) + 1;
                        likeCount.setText(likevalue + " Like");
                        sendLike(forumData.getQuestionID(), forumData.getUserId());
                        likeCounter.setTextColor(FullQuestionActivity.this.getResources().getColor(R.color.colorPrimaryDark));
                        forumData.setIsLiked("true");
                        forumData.setLikeCount(String.valueOf(likevalue));
                    }
                } else {
                    Intent intent = new Intent(FullQuestionActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });
        nameAndAddrress.setText(forumData.getNameAddress());
        time.setText(forumData.getFormTime());
        title.setText(forumData.getTitle());
        desc.setText(forumData.getDesc());
        commentCount.setText(forumData.getCommentNums() + " Comments");
        likeCount.setText(forumData.getLikeCount() + " Like");

        if (forumData.getProfileImage().equalsIgnoreCase(""))
            Picasso.with(this).load("http://agrifarming.in/wp-content/uploads/2015/03/Harvesting-Potatoes.jpg").resize(500, 500).into(descImg);
        else
            Picasso.with(this).load(forumData.getProfileImage()).resize(500, 500).into(descImg);

        if (!forumData.getUserImage().equalsIgnoreCase(""))
            Picasso.with(this).load(forumData.getUserImage()).into(icon);

        descImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImageShowFragment imageShowFragment = ImageShowFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("image", forumData.getProfileImage());
                imageShowFragment.setArguments(bundle);
                imageShowFragment.show(FullQuestionActivity.this.getFragmentManager(), "sad");
            }
        });


//        formImageListAdapter = new FormImageListAdapter(activity, formImageLists);
//        //imageForm.setAdapter(formImageListAdapter);
//
//        int countofImages = 3;
//
//        String[] imagelist = new String[countofImages];
//        imagelist[0] = "http://agrifarming.in/wp-content/uploads/2015/03/Harvesting-Potatoes.jpg";
//        // imagelist[1]= "http://www.asiafarming.com/wp-content/uploads/2016/02/Growing-Potatoes-in-Greenhouse.jpg";
//        imagelist[1] = "http://www.asiafarming.com/wp-content/uploads/2016/02/Potato-Cultivation-506x330.jpg";
//
//        formImageLists.clear();
//        for (int i = 0; i < 2; i++) {
//            FormImageList formImageList = new FormImageList();
//            formImageList.setFormImages(imagelist[i]);
//
//            formImageLists.add(formImageList);
//        }
//        formImageListAdapter.notifyDataSetChanged();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvComments.setNestedScrollingEnabled(false);
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(this, 0), true));
        rvComments.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CommentAdapter(this, listComment);
        rvComments.setAdapter(mAdapter);
        if (listComment.size() > 1)
            rvComments.smoothScrollToPosition(listComment.size() - 1);
        nestedScrollView.post(new Runnable() {
            @Override
            public void run() {
                nestedScrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void sendLike(String QuestionID, String UserID) {
        RequestQueue queue = Volley.newRequestQueue(this);
        //  String url ="http://www.google.com";
        String url = "http://admin.ict4agri.com/api/v3/question/" + QuestionID + "/like?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA&user_id=" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("LIKERESPONSE", response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(FullQuestionActivity.this, "not work", Toast.LENGTH_SHORT).show();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @OnClick(R.id.iv_send)
    void onSendClick() {
        if (!edtComment.getText().toString().isEmpty()) {
            doComment(edtComment.getText().toString());
        }
    }

    private void doComment(final String comment) {
        if (sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) == 0) {
            Intent intent = new Intent(FullQuestionActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            pd1.showpd(getResources().getString(R.string.please_wait));
            JSONObject jsonObject;
            RequestQueue queue = Volley.newRequestQueue(FullQuestionActivity.this);
            String commentUrl = UrlHelper.BASE_URL + "api/v3/question/" + QuestionId + "/comment?apikey=" + UrlHelper.API_KEY;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, commentUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pd1.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObj = new JSONObject(response);
                                JSONObject codeJson = jsonObj.getJSONObject("code");
                                String status = codeJson.getString("status");
                                String message = codeJson.getString("message");
                                Comment newComment;
                                if (status.equalsIgnoreCase("1")) {
                                    newComment = new Comment();
                                    newComment.comment = comment;
                                    newComment.commented_by = sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME);
                                    newComment.date = "now";
//                                    mAdapter.addComment(newComment);
                                    listComment.add(newComment);
                                    mAdapter.notifyItemInserted(listComment.size());
                                    commentCount.setText(listComment.size() + " Comments");
                                    rvComments.smoothScrollToPosition(listComment.size() - 1);
                                    edtComment.setText("");
                                    nestedScrollView.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            nestedScrollView.fullScroll(View.FOCUS_DOWN);
                                        }
                                    });
                                } else
                                    alerts.showErrorAlert(message);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                    pd1.hidepd();
                    // mTextView.setText("That didn't work!");
                }


            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("tag", "login");
                    params.put("user_id", String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)));
                    params.put("comment", comment);
                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent();
            intent.putExtra("commentCount", listComment.size());
            intent.putExtra("pos", position);
            intent.putExtra("likeCount", forumData.getLikeCount());
            intent.putExtra("isLiked", forumData.getIsLiked());
            setResult(RESULT_OK, intent);
        } catch (Exception exx) {
            exx.printStackTrace();
        }

        finish();
    }
}
