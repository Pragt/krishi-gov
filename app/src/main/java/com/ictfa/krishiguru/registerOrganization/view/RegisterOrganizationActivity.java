package com.ictfa.krishiguru.registerOrganization.view;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myOrganization.view.HamroOrganizationActivity;
import com.ictfa.krishiguru.myProfile.UserProfileObject;
import com.ictfa.krishiguru.myProfile.UserType;
import com.ictfa.krishiguru.registerOrganization.object.Zone;
import com.ictfa.krishiguru.login.LoginActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prajeet on 01/07/2017.
 */

public class RegisterOrganizationActivity extends AppCompatActivity {
    @BindView(R.id.et_org_name)
    EditText edtOrgName;
    @BindView(R.id.et_phone)
    EditText edtPhone;
    @BindView(R.id.et_toll_free)
    EditText edtTollFree;
    @BindView(R.id.et_contact_person)
    EditText edtContactPerson;
    @BindView(R.id.sp_zone)
    Spinner spZone;
    @BindView(R.id.sp_disctict)
    Spinner spDistrict;
    @BindView(R.id.sp_type)
    Spinner spOrgType;
    @BindView(R.id.btn_register)
    Button btnRegister;
    CustomProgressDialog pd1;
    CustomProgressDialog pd2;
    CustomProgressDialog pd3;
    Alerts alerts;
    SharedPreference sharedPreference;
    UserProfileObject user;
    boolean isEditMode = false;
    private ArrayList<Zone> zoneList;
    private ArrayList<String> zoneStringList;
    private ArrayList<Zone> districtList;
    private ArrayList<String> districtStringList;
    private ArrayList<Zone> orgTypeList;
    private ArrayList<String> orgTypeStringList;
    private String message;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_organization);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(getResources().getString(R.string.register_organization));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

        if (getIntent().getExtras() != null) {
            user = (UserProfileObject) getIntent().getExtras().getSerializable(CommonDef.USER_PROFILE);
            if (user != null) {
                edtOrgName.setText(user.org_name);
                edtPhone.setText(user.org_phone);
                edtTollFree.setText(user.toll_free_no);
                getSupportActionBar().setTitle(getResources().getString(R.string.update_organization));
                btnRegister.setText(getResources().getString(R.string.update));
                isEditMode = true;
            }
        }

        getZone();
        getOrganizationType();
    }

    private void init() {
        sharedPreference = new SharedPreference(this);
        zoneList = new ArrayList<>();
        zoneStringList = new ArrayList<>();
        alerts = new Alerts(this);
        districtList = new ArrayList<>();
        districtStringList = new ArrayList<>();
        orgTypeList = new ArrayList<>();
        orgTypeStringList = new ArrayList<>();
        pd1 = new CustomProgressDialog(this);
        pd2 = new CustomProgressDialog(this);
        pd3 = new CustomProgressDialog(this);

        spZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getDistrict(zoneList.get(i).id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getDistrict(int id) {
        pd2.showpd(getResources().getString(R.string.loading_district));
        RequestQueue queue = Volley.newRequestQueue(RegisterOrganizationActivity.this);
        districtList = new ArrayList<>();
        districtStringList = new ArrayList<>();
        String zone = UrlHelper.BASE_URL + "api/v3/district/list/" + id + "?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd2.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(response);
                            JSONArray listObject = object.getJSONArray("data");
//                             String id = object.getString(object.getString("id"));
                            JSONObject zoneObject;
                            Zone zone;
                            for (int i = 0; i < listObject.length(); i++) {
                                zone = new Zone();
                                zoneObject = (JSONObject) listObject.get(i);
                                zone.id = zoneObject.getInt("id");
                                zone.name = zoneObject.getString("name");
                                districtList.add(zone);
                                districtStringList.add(zoneObject.getString("name"));
                                Log.i("Prajit", "onResponse: " + zone.name);
                            }
                            loadDistrictSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd2.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void getZone() {
        pd1.showpd(getResources().getString(R.string.loading_zone));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(RegisterOrganizationActivity.this);
        zoneList = new ArrayList<>();
        orgTypeStringList = new ArrayList<>();
        String zone = UrlHelper.BASE_URL + "api/v3/zone/list?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd1.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(response);
                            JSONArray listObject = object.getJSONArray("data");
//                             String id = object.getString(object.getString("id"));
                            JSONObject zoneObject;
                            Zone zone;
                            for (int i = 0; i < listObject.length(); i++) {
                                zone = new Zone();
                                zoneObject = (JSONObject) listObject.get(i);
                                zone.id = zoneObject.getInt("id");
                                zone.name = zoneObject.getString("name");
                                zoneList.add(zone);
                                zoneStringList.add(zoneObject.getString("name"));
                                Log.i("Prajit", "onResponse: " + zone.name);
                            }
                            loadZoneSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                pd1.hidepd();
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void getOrganizationType() {
        pd3.showpd(getResources().getString(R.string.loading_org_type));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(RegisterOrganizationActivity.this);
        orgTypeStringList = new ArrayList<>();
        orgTypeList = new ArrayList<>();
        String zone = UrlHelper.BASE_URL + "api/v3/organisation/type?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd3.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONArray listObject = new JSONArray(response);
//                             String id = object.getString(object.getString("id"));
                            JSONObject zoneObject;
                            Zone zone;
                            for (int i = 0; i < listObject.length(); i++) {
                                zone = new Zone();
                                zoneObject = (JSONObject) listObject.get(i);
                                zone.id = zoneObject.getInt("id");
                                zone.name = zoneObject.getString("name");
                                orgTypeList.add(zone);
                                orgTypeStringList.add(zoneObject.getString("name"));
                                Log.i("Prajit", "onResponse: " + zone.name);
                            }
                            loadOrgTypeSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                pd3.hidepd();
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadOrgTypeSpinner() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, orgTypeStringList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spOrgType.setAdapter(spinnerArrayAdapter);
    }

    private void loadZoneSpinner() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, zoneStringList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spZone.setAdapter(spinnerArrayAdapter);
    }

    private void loadDistrictSpinner() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, districtStringList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDistrict.setAdapter(spinnerArrayAdapter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_register)
    void onRegisterBtnClicked() {
        if (isValid()) {
            doRegister();
        }
    }

    private void doRegister() {
        if (sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) == 0) {
            Intent intent = new Intent(RegisterOrganizationActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            pd1.showpd(getResources().getString(R.string.registering));
            JSONObject jsonObject;
            RequestQueue queue = Volley.newRequestQueue(RegisterOrganizationActivity.this);

            String url;
            if (!isEditMode)
                url = UrlHelper.BASE_URL + "api/v3/organisation/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/register?apikey=" + UrlHelper.API_KEY;
            else
                url = UrlHelper.BASE_URL + "api/v3/organisation/" + user.id + "/edit?apikey=" + UrlHelper.API_KEY;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pd1.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject code = jsonObject.getJSONObject("code");
                                String status = code.getString("status");
                                message = code.getString("message");
                                if (status.equalsIgnoreCase("1")) {

                                    JSONObject data = jsonObject.getJSONObject("data");
                                    String org_id = data.getString("org_id");
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.ORG_ID, org_id);

                                    Toast.makeText(RegisterOrganizationActivity.this, getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.USERTYPE, UserType.TYPE_ORGANISATION);
                                    finish();
                                    if (!isEditMode) {
                                        Intent intent = new Intent(RegisterOrganizationActivity.this, HamroOrganizationActivity.class);
                                        startActivity(intent);
                                    }
//                                    alerts.showSuccessAlert(message);
                                } else
                                    alerts.showErrorAlert(message);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                    pd1.hidepd();
                    // mTextView.setText("That didn't work!");
                }


            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("tag", "login");
                    params.put("name", edtOrgName.getText().toString());
                    params.put("zone_id", String.valueOf(zoneList.get(spZone.getSelectedItemPosition()).id));
                    params.put("district_id", String.valueOf(districtList.get(spDistrict.getSelectedItemPosition()).id));
                    params.put("type", String.valueOf(orgTypeList.get(spOrgType.getSelectedItemPosition()).id));
                    params.put("phone", edtPhone.getText().toString());
                    params.put("toll_free", edtTollFree.getText().toString());
                    params.put("contact_person", edtContactPerson.getText().toString());
                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    private boolean isValid() {
        if (edtOrgName.getText().toString().equalsIgnoreCase("")) {
            alerts.showWarningAlert(getResources().getString(R.string.emtpy_org_name));
            return false;
        } else if (edtPhone.getText().toString().equalsIgnoreCase("")) {
            alerts.showWarningAlert(getResources().getString(R.string.empty_phone));
            return false;
        } else if (spZone.getSelectedItemPosition() == -1) {
            alerts.showWarningAlert(getResources().getString(R.string.empty_zone));
            return false;
        } else if (spDistrict.getSelectedItemPosition() == -1) {
            alerts.showWarningAlert(getResources().getString(R.string.empty_district));
            return false;
        } else if (spOrgType.getSelectedItemPosition() == -1) {
            alerts.showWarningAlert(getResources().getString(R.string.empty_org_type));
            return false;
        }

        return true;
    }
}
