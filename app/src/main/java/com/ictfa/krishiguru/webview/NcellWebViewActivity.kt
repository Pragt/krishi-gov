package com.ictfa.krishiguru.webview

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.ictfa.krishiguru.R
import kotlinx.android.synthetic.main.activity_webview.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*


/**
Created by Prajeet Naga on 2019-11-17, 12:24.
 **/
class NcellWebViewActivity : AppCompatActivity() {

    companion object {
        const val REDIRECTION_URL = "URL"
        fun start(callingActivity: Activity, redirectionURL: String) {
            val intent = Intent(callingActivity, NcellWebViewActivity::class.java)
            intent.putExtra(REDIRECTION_URL, redirectionURL)
            callingActivity.startActivity(intent)
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        toolbar.title = getString(R.string.app_name)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        wv_data.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                title = "Loading..."
                include.progressbar.visibility = View.VISIBLE
                include.progressbar.progress = (progress) //Make the bar disappear after URL is loaded

                // Return the app name after finish loading
                if (progress == 100)
                    include.progressbar.visibility = View.GONE
            }
        }
        wv_data.webViewClient = WebViewClient()
        wv_data.settings.javaScriptEnabled = true
        wv_data.settings.domStorageEnabled = true
        wv_data.loadUrl(intent.getStringExtra(REDIRECTION_URL))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}