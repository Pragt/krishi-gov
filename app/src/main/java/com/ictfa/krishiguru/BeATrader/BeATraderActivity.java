package com.ictfa.krishiguru.BeATrader;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.MultipleTraders.object.TraderType;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myProfile.UserType;
import com.ictfa.krishiguru.registerOrganization.object.Zone;
import com.ictfa.krishiguru.ui.activity.KharidBikriActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BeATraderActivity extends AppCompatActivity {
    ArrayList<TraderType> listTraderType;
    @BindView(R.id.sp_trader_type)
    Spinner spTraderType;
    @BindView(R.id.sp_market_name)
    Spinner spMarketName;
    @BindView(R.id.sp_disctict)
    Spinner spDistrict;
    @BindView(R.id.btn_save)
    Button btnSave;
    CustomProgressDialog progressDialog;
    Alerts alerts;
    SharedPreference sharedPreference;
    boolean isEditMode = false;
    private ArrayList<Zone> zoneList;
    private ArrayList<String> zoneStringList;
    private ArrayList<Zone> districtList;
    private ArrayList<String> districtStringList;
    private ArrayList<TraderType> marketTypeList;
    private ArrayList<String> countryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_be_atrader);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.bt_trader));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle b = getIntent().getExtras();
        progressDialog = new CustomProgressDialog(this);
        sharedPreference = new SharedPreference(this);
        zoneList = new ArrayList<>();
        zoneStringList = new ArrayList<>();
        alerts = new Alerts(this);
        districtList = new ArrayList<>();
        districtStringList = new ArrayList<>();
        alerts = new Alerts(this);
        if (b != null) {
            listTraderType = (ArrayList<TraderType>) getIntent().getSerializableExtra(CommonDef.TRADER_TYPE);
            isEditMode = getIntent().getBooleanExtra(CommonDef.IS_EDIT_MODE, false);
            loadTraderType();
        }

        if (isEditMode) {
            setTitle(getResources().getString(R.string.change_trader));
            btnSave.setText(getResources().getString(R.string.update));
        }

        spTraderType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        marketTypeList = new ArrayList<TraderType>();
                        loadMarketSpinner();
                        spDistrict.setVisibility(View.GONE);
                        break;
                    case 1:
                        getZone();
                        spDistrict.setVisibility(View.VISIBLE);
                        loadZoneSpinner();
                        loadDistrictSpinner();
                        break;
                    case 2:
                        spDistrict.setVisibility(View.GONE);
                        loadMarketList();
                        break;
                    case 3:
                        spDistrict.setVisibility(View.GONE);
                        loadSpinnnerCountry();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void loadSpinnnerCountry() {
        countryList = new ArrayList<>();
        countryList.add(0, getResources().getString(R.string.choose_country));
        countryList.add("चीन");
        countryList.add("भारत ");
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countryList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMarketName.setAdapter(spinnerArrayAdapter);
    }

    private void loadMarketList() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = UrlHelper.BASE_URL + "api/v3/market/list?apikey=" + UrlHelper.API_KEY;
        marketTypeList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        pd.hidepd();
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONArray traderTypeListJson = object.getJSONArray("data");
                            for (int i = 0; i < traderTypeListJson.length(); i++) {
                                JSONObject object1 = (JSONObject) traderTypeListJson.get(i);
                                TraderType traderType = new TraderType();
                                traderType.id = object1.getString("id");
                                traderType.type = object1.getString("name");
                                traderType.nepali_name = object1.getString("nepali_name");
                                marketTypeList.add(traderType);
                            }

                            loadMarketSpinner();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                pd.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadMarketSpinner() {
        ArrayList<String> listMarketString = new ArrayList<>();
        listMarketString.add("बजार को नाम छान्नुहोस्");
        for (int i = 0; i < marketTypeList.size(); i++) {
            listMarketString.add(marketTypeList.get(i).nepali_name);
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listMarketString); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMarketName.setAdapter(spinnerArrayAdapter);
    }

    private void loadTraderType() {
        ArrayList<String> listZoneString = new ArrayList<>();
        listZoneString.add("ब्यापारीको किसिम छान्नुहोस्");
        for (int i = 0; i < listTraderType.size(); i++) {
            listZoneString.add(listTraderType.get(i).nepali_name);
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listZoneString); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTraderType.setAdapter(spinnerArrayAdapter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_save)
    void onSaveClicked() {
        if (isAllValid()) {
            progressDialog.showpd(getResources().getString(R.string.please_wait));
            JSONObject jsonObject;
            RequestQueue queue = Volley.newRequestQueue(BeATraderActivity.this);

            String url;
            if (!isEditMode)
                url = UrlHelper.BASE_URL + "api/v3/user/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/be_trader?apikey=" + UrlHelper.API_KEY;
            else
                url = UrlHelper.BASE_URL + "api/v3/be_trader/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/edit?apikey=" + UrlHelper.API_KEY;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                JSONObject jsonObject = new JSONObject(utfStr);
                                JSONObject code = jsonObject.getJSONObject("code");
                                String status = code.getString("status");
                                String message = code.getString("message");
                                if (status.equalsIgnoreCase("1")) {
                                    Toast.makeText(BeATraderActivity.this, getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.USERTYPE, UserType.TYPE_TRADER);
                                    if (!isEditMode) {
                                        Intent intent = new Intent(BeATraderActivity.this, KharidBikriActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                    finish();
//                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.ORG_ID, data.getInt("id"));
//                                    alerts.showSuccessAlert(message);
                                } else
                                    alerts.showErrorAlert(message);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                    progressDialog.hidepd();
                    // mTextView.setText("That didn't work!");
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    if (spTraderType.getSelectedItemPosition() == 1) {
                        params.put("type_id", listTraderType.get(spTraderType.getSelectedItemPosition() - 1).id);
                        params.put("district_id", String.valueOf(districtList.get(spDistrict.getSelectedItemPosition() - 1).id));
                    } else if (spTraderType.getSelectedItemPosition() == 2) {
                        params.put("tag", "login");
                        params.put("type_id", listTraderType.get(spTraderType.getSelectedItemPosition() - 1).id);
                        params.put("market_id", marketTypeList.get(spMarketName.getSelectedItemPosition() - 1).id);

                    } else if (spTraderType.getSelectedItemPosition() == 3) {
                        params.put("type_id", listTraderType.get(spTraderType.getSelectedItemPosition() - 1).id);
                        params.put("country_id", (spMarketName.getSelectedItemPosition() == 1) ? "105" : "48");
                    }

                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    private boolean isAllValid() {
        if (spTraderType.getSelectedItemPosition() <= 0) {
            alerts.showErrorAlert("कृपया ब्यापारीको किसिम छान्नुहोस्");
            return false;
        } else if (spMarketName.getSelectedItemPosition() <= 0) {
            if (spTraderType.getSelectedItemPosition() == 1)
                alerts.showErrorAlert("कृपया अञ्चल को नाम छान्नुहोस्");
            else if (spTraderType.getSelectedItemPosition() == 2)
                alerts.showErrorAlert("कृपया बजार को नाम छान्नुहोस्");
            else if (spTraderType.getSelectedItemPosition() == 3)
                alerts.showErrorAlert("कृपया देश  को नाम छान्नुहोस्");
            return false;
        } else if (spTraderType.getSelectedItemPosition() == 1) {
            if (spDistrict.getSelectedItemPosition() == 0) {
                alerts.showErrorAlert("कृपया जिल्ला  को नाम छान्नुहोस्");
                return false;
            }
        }
        return true;
    }

    private void getDistrict(int id) {
        progressDialog.showpd(getResources().getString(R.string.loading_district));
        RequestQueue queue = Volley.newRequestQueue(BeATraderActivity.this);
        districtList = new ArrayList<>();
        districtStringList = new ArrayList<>();
        String zone = UrlHelper.BASE_URL + "api/v3/district/list/" + id + "?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(response);
                            JSONArray listObject = object.getJSONArray("data");
//                             String id = object.getString(object.getString("id"));
                            JSONObject zoneObject;
                            Zone zone;
                            for (int i = 0; i < listObject.length(); i++) {
                                zone = new Zone();
                                zoneObject = (JSONObject) listObject.get(i);
                                zone.id = zoneObject.getInt("id");
                                zone.name = zoneObject.getString("name");
                                districtList.add(zone);
                                districtStringList.add(zoneObject.getString("name"));
                                Log.i("Prajit", "onResponse: " + zone.name);
                            }
                            loadDistrictSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    private void getZone() {
        progressDialog.showpd(getResources().getString(R.string.loading_zone));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(BeATraderActivity.this);
        zoneList = new ArrayList<>();
        String zone = UrlHelper.BASE_URL + "api/v3/zone/list?apikey=" + UrlHelper.API_KEY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(response);
                            JSONArray listObject = object.getJSONArray("data");
//                             String id = object.getString(object.getString("id"));
                            JSONObject zoneObject;
                            Zone zone;
                            zoneStringList = new ArrayList<>();
                            for (int i = 0; i < listObject.length(); i++) {
                                zone = new Zone();
                                zoneObject = (JSONObject) listObject.get(i);
                                zone.id = zoneObject.getInt("id");
                                zone.name = zoneObject.getString("name");
                                zoneList.add(zone);
                                zoneStringList.add(zoneObject.getString("name"));
                                Log.i("Prajit", "onResponse: " + zone.name);
                            }
                            loadZoneSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                progressDialog.hidepd();
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void loadZoneSpinner() {
        zoneStringList.add(0, getResources().getString(R.string.choose_zone));
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, zoneStringList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMarketName.setAdapter(spinnerArrayAdapter);

        spMarketName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spTraderType.getSelectedItemPosition() == 1) {
                    if (i == 0) {
                        districtStringList = new ArrayList<String>();
                        loadDistrictSpinner();
                    } else
                        getDistrict(zoneList.get(i - 1).id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void loadDistrictSpinner() {
        districtStringList.add(0, getResources().getString(R.string.choose_district));
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, districtStringList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDistrict.setAdapter(spinnerArrayAdapter);
    }
}
