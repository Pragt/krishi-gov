package com.ictfa.krishiguru.krishakByapariMulya;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.customViews.GridSpacingItemDecoration;
import com.ictfa.krishiguru.fullNoticeView.CommentAdapter;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.login.LoginActivity;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;
import com.ictfa.krishiguru.ui.dtos.Farmer_OtherPrice_List;
import com.ictfa.krishiguru.ui.fragment.ImageShowFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prajeet on 03/08/2017.
 */

public class CommentOnKirshakByapaariActivity extends AppCompatActivity {

    ArrayList<Comment> listComment;

    @BindView(R.id.rv_comments)
    RecyclerView rvComments;
    @BindView(R.id.rl_add_comments)
    RelativeLayout rlAddComments;
    @BindView(R.id.edt_comment)
    EditText edtComment;
    @BindView(R.id.iv_send)
    ImageView ivSend;
    @BindView(R.id.date)
    TextView tvDate;
    @BindView(R.id.quantity)
    TextView tvQuantity;
    @BindView(R.id.tv_comment_count)
    TextView tvCommentCount;
    @BindView(R.id.product_name)
    TextView tvProductName;
    @BindView(R.id.tv_created_at)
    TextView tvCreatedAt;
    @BindView(R.id.productType)
    TextView tvProductType;
    @BindView(R.id.rate)
    TextView tvRate;
    @BindView(R.id.name)
    TextView tvName;
    @BindView(R.id.adddress)
    TextView tvAddress;
    @BindView(R.id.productImg)
    ImageView productImg;
    @BindView(R.id.proPic)
    ImageView profilePic;
    @BindView(R.id.ll_call)
    LinearLayout llCall;
    SharedPreference sharedPreference;
    CustomProgressDialog pd1;
    Alerts alerts;
    int userId;
    String name;
    String productId;
    boolean isFarmer;
    boolean isMyPrice;
    private CommentAdapter mAdapter;
    int pos = 0;
    Farmer_OtherPrice_List item;

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.activity_kharid_bikri_detail);
        CommonMethods.setupUI(findViewById(R.id.rl_full_question), this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            listComment = (ArrayList<Comment>) getIntent().getSerializableExtra(CommonDef.COMMENTS);
            productId = getIntent().getStringExtra(CommonDef.PRODUCT_ID);
            isFarmer = getIntent().getBooleanExtra(CommonDef.IS_FARMER, false);
            isMyPrice = getIntent().getBooleanExtra(CommonDef.IS_MY_PRICE, false);
            pos = getIntent().getIntExtra("pos", 0);

            item = (Farmer_OtherPrice_List) getIntent().getSerializableExtra(CommonDef.KHARID_BIKRI_OBJECT);
            populateData();
        }

        init();
        sharedPreference = new SharedPreference(this);
        pd1 = new CustomProgressDialog(this);
        alerts = new Alerts(this);
        userId = sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
        name = sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME);
    }

    @OnClick(R.id.ll_call)
    void onCallClicked() {

        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(item.getPhone().trim())));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);

    }

    private void populateData() {
        tvQuantity.setText(item.getProductNQuantity());
        tvProductType.setText("गुणस्तर - " + item.getQuality());
        tvDate.setText(getString(R.string.expiry_date) + " : " + item.getDate());
        tvCreatedAt.setText(item.createdAt);
        tvCommentCount.setText(item.getCommentCount() + " Comments");
        tvProductName.setText("नाम - " + item.productName);
        tvRate.setText(item.getRate());
        tvName.setText(item.getName());
        tvAddress.setText(item.getAddress());
        if (!item.getUserImage().equals("")) {
            Picasso.with(this).load(item.getUserImage()).resize(500, 500).into(profilePic);
        }

        if (item.getType().equalsIgnoreCase("buyer"))
            setTitle(getResources().getString(R.string.buyer));
        else
            setTitle(getResources().getString(R.string.seller));

        /**
         *  if (newsData.getNewsImage().isEmpty()) {

         Picasso.with(activity).load("https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Traditional_Farming_Methods_and_Equipments.jpg/220px-Traditional_Farming_Methods_and_Equipments.jpg").into(icon);
         } else{
         Picasso.with(activity).load(newsData.getNewsImage()).into(icon);
         }
         */

//        if(!farmenrPriceDatas.get(position).getUserImage().equals("")){
//            Picasso.with(activity).load(farmenrPriceDatas.get(position).getUserImage()).resize(500, 500).into(viewHolder.profileImage);
//        }


        if (!item.productImage.equals("")) {
            Picasso.with(this).load(item.productImage).resize(500, 500).into(productImg);
            productImg.setVisibility(View.VISIBLE);
        } else {
            productImg.setVisibility(View.GONE);
        }


        productImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImageShowFragment imageShowFragment = ImageShowFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("image", item.getProductImage());
                imageShowFragment.setArguments(bundle);
                imageShowFragment.show(getFragmentManager(), "go");
            }
        });
    }

    private void init() {

//        ImageView commentImg = (ImageView) findViewById(R.id.commentIcon);

        LinearLayout llComments = (LinearLayout) findViewById(R.id.ll_comments);

        String urlString = "http://agrifarming.in/wp-content/uploads/2015/03/potato-farming.jpg";
        URL myURL = null;
        try {
            myURL = new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(this, 0), true));
        rvComments.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CommentAdapter(this, listComment);
        rvComments.setAdapter(mAdapter);
    }


    @OnClick(R.id.iv_send)
    void onSendClick() {
        if (!edtComment.getText().toString().isEmpty()) {
            doComment(edtComment.getText().toString());
        }
    }

    private void doComment(final String comment) {
        if (sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) == 0) {
            Intent intent = new Intent(CommentOnKirshakByapaariActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            pd1.showpd(getResources().getString(R.string.registering));
            JSONObject jsonObject;
            RequestQueue queue = Volley.newRequestQueue(CommentOnKirshakByapaariActivity.this);

            String commentUrl;
            if (!isFarmer)
                commentUrl = UrlHelper.BASE_URL + "api/v3/buy/" + productId + "/comment?apikey=" + UrlHelper.API_KEY;
            else
                commentUrl = UrlHelper.BASE_URL + "api/v3/sell/" + productId + "/comment?apikey=" + UrlHelper.API_KEY;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, commentUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pd1.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObj = new JSONObject(response);
//                                JSONArray codeJson = jsonObj.getJSONArray("data");
                                JSONObject object = jsonObj.getJSONObject("code");
                                String status = object.getString("status");
                                String message = object.getString("message");
                                Comment newComment;
                                if (status.equalsIgnoreCase("1")) {
                                    newComment = new Comment();
                                    newComment.comment = comment;
                                    newComment.commented_by = name;
                                    newComment.date = "now";
                                    mAdapter.addComment(newComment);
//                                    listComment.add(0, newComment);
                                    rvComments.smoothScrollToPosition(0);
                                    edtComment.setText("");
                                } else
                                    alerts.showErrorAlert(message);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                    pd1.hidepd();
                    // mTextView.setText("That didn't work!");
                }


            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("tag", "login");
                    params.put("user_id", String.valueOf(userId));
                    params.put("comment", comment);
                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(CommonDef.COMMENT_COUNT, listComment.size());
        intent.putExtra(CommonDef.COMMENTS, listComment);
        intent.putExtra(CommonDef.IS_FARMER, isFarmer);
        intent.putExtra(CommonDef.IS_MY_PRICE, isMyPrice);
        intent.putExtra("pos", pos);
        setResult(RESULT_OK, intent);
        finish();
    }
}