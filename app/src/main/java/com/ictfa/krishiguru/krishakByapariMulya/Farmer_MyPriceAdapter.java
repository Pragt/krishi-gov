package com.ictfa.krishiguru.krishakByapariMulya;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.dtos.Farmer_OtherPrice_List;
import com.ictfa.krishiguru.ui.fragment.ImageShowFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sureshlama on 5/15/17.
 */

public class Farmer_MyPriceAdapter  extends BaseAdapter {
    Activity activity;
    private LayoutInflater inflater;
    private List<Farmer_OtherPrice_List> farmenrPriceDatas;
    boolean isFarmer;
    boolean isMyPrice;
    FarmerOtherPriceAdapter.CommentListner commentListner;

    public Farmer_MyPriceAdapter(Activity activity, List<Farmer_OtherPrice_List> farmenrPriceDatas, boolean isFarmer, boolean isMyPrice, FarmerOtherPriceAdapter.CommentListner commentListner) {
        this.activity = activity;
        this.farmenrPriceDatas = farmenrPriceDatas;
        this.commentListner = commentListner;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isFarmer = isFarmer;
        this.isMyPrice = isMyPrice;
    }

    private static class ViewHolder {
        ImageView  productImage;
        TextView productName, productNQuantity, productType, rate, date, tvCommentCount, tvCreatedAt;
    }

    @Override
    public int getCount() {
        return this.farmenrPriceDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return this.farmenrPriceDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View convertedView = convertView;
        ViewHolder viewHolder;
        if (convertedView == null) {
            convertedView = inflater.inflate(R.layout.my_price_list, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.productImage = (ImageView)convertedView.findViewById(R.id.productImg);
            viewHolder.productNQuantity = (TextView)convertedView.findViewById(R.id.quantity);
            viewHolder.tvCommentCount = (TextView) convertedView.findViewById(R.id.tv_comment_count);
            viewHolder.productName = (TextView) convertedView.findViewById(R.id.product_name);
            viewHolder.tvCreatedAt = (TextView) convertedView.findViewById(R.id.tv_created_at);

            viewHolder.productType = (TextView) convertedView.findViewById(R.id.productType);

            viewHolder.rate = (TextView) convertedView.findViewById(R.id.rate);
            viewHolder.date = (TextView) convertedView.findViewById(R.id.date);
            viewHolder.tvCreatedAt.setText(farmenrPriceDatas.get(position).createdAt);



            convertedView.setTag(viewHolder);
        }
        else
            viewHolder = (ViewHolder) convertedView.getTag();

        viewHolder.productType.setText("गुणस्तर - " + farmenrPriceDatas.get(position).getType());
        viewHolder.date.setText(activity.getResources().getString(R.string.expiry_date)+ " : " + farmenrPriceDatas.get(position).getDate());
        viewHolder.productNQuantity.setText(farmenrPriceDatas.get(position).getProductNQuantity());
        viewHolder.tvCommentCount.setText(farmenrPriceDatas.get(position).getCommentCount()+" Comments");
        viewHolder.productName.setText("नाम - " + farmenrPriceDatas.get(position).productName);

        viewHolder.rate.setText(farmenrPriceDatas.get(position).getRate());
        /**
         *  if (newsData.getNewsImage().isEmpty()) {

         Picasso.with(activity).load("https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Traditional_Farming_Methods_and_Equipments.jpg/220px-Traditional_Farming_Methods_and_Equipments.jpg").into(icon);
         } else{
         Picasso.with(activity).load(newsData.getNewsImage()).into(icon);
         }
         */

//        if(!farmenrPriceDatas.get(position).getUserImage().equals("")){
//            Picasso.with(activity).load(farmenrPriceDatas.get(position).getUserImage()).resize(500, 500).into(viewHolder.profileImage);
//        }


        if(!farmenrPriceDatas.get(position).productImage.equals("")){
            Picasso.with(activity).load(farmenrPriceDatas.get(position).productImage).resize(500, 500).into(viewHolder.productImage);
            viewHolder.productImage.setVisibility(View.VISIBLE);
        }else
        {
            viewHolder.productImage.setVisibility(View.GONE);
        }


        viewHolder.productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImageShowFragment imageShowFragment = ImageShowFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("image", farmenrPriceDatas.get(position).getProductImage());
                imageShowFragment.setArguments(bundle);
                imageShowFragment.show(activity.getFragmentManager(),"go");
            }
        });

        convertedView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commentListner.doComment(position, isFarmer, isMyPrice);
            }
        });

        /*if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.custome_video_list, null);*/


        return convertedView;
    }
}
