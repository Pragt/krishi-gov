package com.ictfa.krishiguru.krishakByapariMulya;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.ictfa.krishiguru.CropListDialogFragment;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.activity.KharidBikriActivity;
import com.ictfa.krishiguru.ui.fragment.PopupScreenFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class KrishyakByapariMulyaActivity extends AppCompatActivity implements PopupScreenFragment.WhenuserSelectLocation, CropListDialogFragment.CropFilterListner {
    @BindView(R.id.location)
    TextView tvLocation;
    @BindView(R.id.crop_type)
    TextView tvCropType;
    String district_id = "";
    String crop_id = "";
    private TabLayout tabLayout;
    private BuySellTabAdapter mAdapter;
    private ViewPager viewPager;
    boolean isMyPrice = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_sell);
        ButterKnife.bind(this);
        isMyPrice = getIntent().getBooleanExtra("is_my_price", false);
        if (!isMyPrice)
            getSupportActionBar().setTitle(getResources().getString(R.string.others_price));
        else
            getSupportActionBar().setTitle(getResources().getString(R.string.my_price));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        mAdapter = new BuySellTabAdapter(getSupportFragmentManager(), isMyPrice,
                KrishyakByapariMulyaActivity.this);

        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        viewPager.setCurrentItem(0);

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            case R.id.filter_by_crop:
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                CropListDialogFragment dilagCrops = new CropListDialogFragment();
                dilagCrops.setListner(KrishyakByapariMulyaActivity.this);
                dilagCrops.show(fragmentManager1, "alert");
                break;
            case R.id.filter_by_location:
                FragmentManager fragmentManager = getSupportFragmentManager();
                PopupScreenFragment dialogChooseDistrict = new PopupScreenFragment();
                dialogChooseDistrict.setListner(this);
                dialogChooseDistrict.show(fragmentManager, "alert");
                break;
            case R.id.profile:
                Intent intent = new Intent(KrishyakByapariMulyaActivity.this, KrishyakByapariMulyaActivity.class);
                intent.putExtra("is_my_price", true);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.buy_sell_menu, menu);
        if (isMyPrice)
            menu.findItem(R.id.profile).setVisible(false);


        return true;
    }

    @OnClick(R.id.fab)
    void onFabClicked() {
        Intent intent = new Intent(KrishyakByapariMulyaActivity.this, KharidBikriActivity.class);
        startActivity(intent);
    }

    @Override
    public void location(String location, String district_id) {
        tvLocation.setText(location);
        this.district_id = district_id;
        mAdapter = ((BuySellTabAdapter) viewPager.getAdapter());
        FarmerPriceFragment fragment1 =
                (FarmerPriceFragment) mAdapter.instantiateItem(viewPager, 1);
        fragment1.getMyList(crop_id, district_id);

        TraderPriceFragment fragment2 =
                (TraderPriceFragment) mAdapter.instantiateItem(viewPager, 0);
        fragment2.getMyList(crop_id, district_id);
    }

    @Override
    public void doFilter(String cropId, String crop_type) {
        tvCropType.setText(crop_type);
        this.crop_id = cropId;
        mAdapter = ((BuySellTabAdapter) viewPager.getAdapter());
        FarmerPriceFragment fragment1 =
                (FarmerPriceFragment) mAdapter.instantiateItem(viewPager, 1);
        fragment1.getMyList(crop_id, district_id);

        TraderPriceFragment fragment2 =
                (TraderPriceFragment) mAdapter.instantiateItem(viewPager, 0);
        fragment2.getMyList(crop_id, district_id);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
