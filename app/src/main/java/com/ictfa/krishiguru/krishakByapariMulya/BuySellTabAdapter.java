package com.ictfa.krishiguru.krishakByapariMulya;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * Created by sureshlama on 5/12/17.
 */

public class BuySellTabAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    boolean isMyPrice = false;
    private String tabTitles[] = new String[]{"खरीद", "बिक्री"};

    public BuySellTabAdapter(FragmentManager fm, boolean isMyPrice, Context context) {
        super(fm);
        this.isMyPrice = isMyPrice;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                //  buyFragment1.setB
                return new TraderPriceFragment(isMyPrice);
            case 1:
                return new FarmerPriceFragment(isMyPrice);

            default:
                return null;
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}

