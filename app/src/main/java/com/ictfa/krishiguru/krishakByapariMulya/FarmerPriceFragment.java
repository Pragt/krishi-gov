package com.ictfa.krishiguru.krishakByapariMulya;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;
import com.ictfa.krishiguru.ui.dtos.Farmer_OtherPrice_List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class FarmerPriceFragment extends Fragment implements FarmerOtherPriceAdapter.CommentListner {
    CustomProgressDialog pd;
    String crop_id = "";
    String district_id = "";
    private ListView myList, otherList;
    private Farmer_MyPriceAdapter myPriceAdapter;
    private FarmerOtherPriceAdapter otherPriceAdapter;
    private List<Farmer_OtherPrice_List> otherProductList = new ArrayList<>();
    private List<Farmer_OtherPrice_List> myProductList = new ArrayList<>();
    private SharedPreference mypref;
    private int userID;
    boolean isMyPrice = false;
    Alerts alerts;

    public FarmerPriceFragment(boolean isMyPrice) {
        // Required empty public constructor
        this.isMyPrice = isMyPrice;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_farmer_price_fragment, container, false);

        myList = (ListView) view.findViewById(R.id.myList);
        myPriceAdapter = new Farmer_MyPriceAdapter(getActivity(), myProductList, true, isMyPrice, this);
        myList.setAdapter(myPriceAdapter);
        mypref = new SharedPreference(getActivity());
        pd = new CustomProgressDialog(getActivity());
        userID = mypref.getIntValues(CommonDef.SharedPreference.USER_ID);

        otherList = (ListView) view.findViewById(R.id.otherList);
        otherPriceAdapter = new FarmerOtherPriceAdapter(getActivity(), otherProductList, true, isMyPrice, this);
        otherList.setAdapter(otherPriceAdapter);
        //  getOtherList();
        getMyList(crop_id, district_id);
        alerts = new Alerts(getActivity());
        return view;
    }

    public void getMyList(String crop_id, String districtId) {
        pd.showpd(getActivity().getResources().getString(R.string.please_wait));
        otherProductList.clear();
        myProductList.clear();
        myPriceAdapter.notifyDataSetChanged();
        otherPriceAdapter.notifyDataSetChanged();
//        myList.setExpanded(true);
        this.crop_id = crop_id;
        this.district_id = districtId;
        if (isMyPrice)
            otherList.setVisibility(View.GONE);
        else
            myList.setVisibility(View.GONE);

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = "http://admin.ict4agri.com/api/v3/market/" + userID + "/sellers?apikey=O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA" + "&district_id=" + districtId + "&category_id=" + crop_id;


        Log.d("FORMURL", url);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray data = jsonObject.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                Farmer_OtherPrice_List farmer_otherPrice_list = new Farmer_OtherPrice_List();

                                JSONObject getvalue = data.getJSONObject(i);

                                String mystatus = getvalue.getString("is_am_i");

                                String name = getvalue.getString("name");
                                String id = getvalue.getString("id");
                                String product = getvalue.getString("product_name");
                                String userImage = getvalue.getString("image");
                                String phone = getvalue.getString("phone");
                                String address = getvalue.getString("address");
                                String price = getvalue.getString("product_price");
                                String productImg = getvalue.getString("product_image");
                                String product_quantity = getvalue.getString("product_quantity");
                                String unit = getvalue.getString("product_unit");
                                String valid_to = getvalue.getString("valid_to");
                                String product_quality = getvalue.getString("product_quality");
                                int comment_count = Integer.parseInt(getvalue.getString("comment_count"));
//
                                farmer_otherPrice_list.setName(name);
                                farmer_otherPrice_list.id = id;
                                farmer_otherPrice_list.productImage = productImg;
                                farmer_otherPrice_list.setProductImage("null");
                                farmer_otherPrice_list.setAddress(address);
                                farmer_otherPrice_list.setProductImage(productImg);
                                farmer_otherPrice_list.setUserImage(userImage);
                                farmer_otherPrice_list.setType("seller");
                                farmer_otherPrice_list.createdAt = getvalue.getString("created_at");


                                farmer_otherPrice_list.setProductNQuantity("परिमाण -" + product_quantity + " " + unit);

                                farmer_otherPrice_list.setQuality(product_quality);
                                farmer_otherPrice_list.productName = product;

                                farmer_otherPrice_list.setRate("भाउ- रु. " + price + "/" + unit);
                                farmer_otherPrice_list.setDate(valid_to + " सम्म ");
                                farmer_otherPrice_list.setPhone(phone);
                                farmer_otherPrice_list.setCommentCount(comment_count);
//                                    farmer_otherPrice_list.setListComment(commentList);
                                if (mystatus.equalsIgnoreCase("true"))
                                    myProductList.add(farmer_otherPrice_list);
                                else
                                    otherProductList.add(farmer_otherPrice_list);
                            }

                            if (otherProductList.size() == 0) {
                                myList.setVisibility(View.GONE);
                                otherList.setVisibility(View.GONE);
                            } else {
                                myList.setVisibility(View.VISIBLE);
                                otherList.setVisibility(View.VISIBLE);
                            }

                            myPriceAdapter.notifyDataSetChanged();
                            otherPriceAdapter.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.hidepd();
                alerts.showToastMsg(getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
//        getMyList(crop_id, district_id);
    }

    @Override
    public void doComment(int pos, boolean isFarmer, boolean isMyPrice) {

        pd.showpd(getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String zone;

        if (isMyPrice)
            zone = UrlHelper.BASE_URL + "api/v3/seller/" + myProductList.get(pos).id + "/comments?apikey=" + UrlHelper.API_KEY;
        else
            zone = UrlHelper.BASE_URL + "api/v3/seller/" + otherProductList.get(pos).id + "/comments?apikey=" + UrlHelper.API_KEY;

        Log.d("Comments", zone);
        ArrayList<Comment> listComment = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //forumDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray commentList = jsonObject.getJSONArray("data");
                            ArrayList<Comment> comments = new ArrayList<>();
                            Comment comment;
                            if (commentList != null) {
                                for (int j = 0; j < commentList.length(); j++) {
                                    JSONObject commentJson = commentList.getJSONObject(j);
                                    comment = new Comment();
                                    comment.comment_id = commentJson.getInt("id");
                                    comment.comment = commentJson.getString("comment");
                                    comment.commented_by = commentJson.getString("commented_by");
//                                    JSONObject created_at = commentJson.getJSONObject("created_at");
                                    comment.date = commentJson.getString("created_at");
//                                    comment.timezone_type = created_at.getInt("timezone_type");
//                                    comment.timezone = created_at.getString("timezone");
                                    comments.add(comment);
                                }

                                Intent intent = new Intent(getActivity(), CommentOnKirshakByapaariActivity.class);
                                if (isMyPrice) {
                                    intent.putExtra(CommonDef.COMMENTS, comments);
                                    intent.putExtra(CommonDef.KHARID_BIKRI_OBJECT, myProductList.get(pos));
                                    intent.putExtra(CommonDef.PRODUCT_ID, myProductList.get(pos).id);
                                } else {
                                    intent.putExtra(CommonDef.COMMENTS, comments);
                                    intent.putExtra(CommonDef.PRODUCT_ID, otherProductList.get(pos).id);
                                    intent.putExtra(CommonDef.KHARID_BIKRI_OBJECT, otherProductList.get(pos));
                                }
                                intent.putExtra("pos", pos);
                                intent.putExtra("type", 1);
                                intent.putExtra(CommonDef.IS_FARMER, isFarmer);
                                intent.putExtra(CommonDef.IS_MY_PRICE, isMyPrice);
                                startActivityForResult(intent, 101);


                            }

                        } catch (JSONException e) {
                            alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                pd.hidepd();
                // mTextView.setText("That didn't work!");
            }


        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 101) {
                boolean isFarmer = data.getBooleanExtra(CommonDef.IS_FARMER, false);
                boolean isMyPrice = data.getBooleanExtra(CommonDef.IS_MY_PRICE, false);
                int pos = data.getIntExtra("pos", 0);
                int commentCount = data.getIntExtra(CommonDef.COMMENT_COUNT, 0);
                ArrayList<Comment> comment = (ArrayList<Comment>) data.getSerializableExtra(CommonDef.COMMENTS);
                if (isMyPrice) {
                    myProductList.get(pos).setCommentCount(commentCount);
//                    farmer_myPrice_lists.get(pos).setListComment(comment);
                    myPriceAdapter.notifyDataSetChanged();
                } else {
                    otherProductList.get(pos).setCommentCount(commentCount);
//                    otherProductList.get(pos).setListComment(comment);
                    otherPriceAdapter.notifyDataSetChanged();
                }
            }
        }
    }
}