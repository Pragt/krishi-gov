package com.ictfa.krishiguru.krishakByapariMulya;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.ui.dtos.Farmer_OtherPrice_List;
import com.ictfa.krishiguru.ui.fragment.ImageShowFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sureshlama on 5/15/17.
 */

public class FarmerOtherPriceAdapter extends BaseAdapter {
    Activity activity;
    boolean isFarmer;
    boolean isMyPrice;
    private LayoutInflater inflater;
    private List<Farmer_OtherPrice_List> farmerOtherDatas;
    private CommentListner commentListner;

    public FarmerOtherPriceAdapter(Activity activity, List<Farmer_OtherPrice_List> farmerOtherDatas, boolean isFarmer, boolean isMyPrice, CommentListner commentListner) {
        this.activity = activity;
        this.farmerOtherDatas = farmerOtherDatas;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isFarmer = isFarmer;
        this.isMyPrice = isMyPrice;
        this.commentListner = commentListner;
    }

    @Override
    public int getCount() {
        return this.farmerOtherDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return this.farmerOtherDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View convertedView = convertView;
        ViewHolder viewHolder;
//        if (convertedView == null) {
            convertedView = inflater.inflate(R.layout.other_price_list, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.profileImage = (CircleImageView) convertedView.findViewById(R.id.proPic);
            viewHolder.productImage = (ImageView) convertedView.findViewById(R.id.productImg);
            viewHolder.name = (TextView) convertedView.findViewById(R.id.name);
            viewHolder.address = (TextView) convertedView.findViewById(R.id.adddress);
            viewHolder.productNQuantity = (TextView) convertedView.findViewById(R.id.quantity);
            viewHolder.productName = (TextView) convertedView.findViewById(R.id.product_name);
            viewHolder.tvCommentCount = (TextView) convertedView.findViewById(R.id.tv_comment_count);
           viewHolder.tvCreatedAt = (TextView) convertedView.findViewById(R.id.tv_created_at);

            viewHolder.productType = (TextView) convertedView.findViewById(R.id.productType);

            viewHolder.rate = (TextView) convertedView.findViewById(R.id.rate);
            viewHolder.date = (TextView) convertedView.findViewById(R.id.date);
            viewHolder.llCall = (LinearLayout) convertedView.findViewById(R.id.ll_call);
            viewHolder.address.setText(farmerOtherDatas.get(position).getAddress());

            convertedView.setTag(viewHolder);

            viewHolder = (ViewHolder) convertedView.getTag();

            viewHolder.productType.setText("गुणस्तर - " + farmerOtherDatas.get(position).getQuality());
            viewHolder.name.setText(farmerOtherDatas.get(position).getName());
            viewHolder.address.setText(farmerOtherDatas.get(position).getAddress());
            viewHolder.address.setVisibility(View.VISIBLE);
            viewHolder.productNQuantity.setText(farmerOtherDatas.get(position).getProductNQuantity());
            viewHolder.date.setText(activity.getResources().getString(R.string.expiry_date)+ " : " + farmerOtherDatas.get(position).getDate());
            viewHolder.productName.setText("नाम -" + farmerOtherDatas.get(position).productName);
            viewHolder.tvCommentCount.setText(farmerOtherDatas.get(position).getCommentCount() + " Comments");
            viewHolder.tvCreatedAt.setText(farmerOtherDatas.get(position).createdAt);

            viewHolder.rate.setText(farmerOtherDatas.get(position).getRate());
            /**
             *  if (newsData.getNewsImage().isEmpty()) {

             Picasso.with(activity).load("https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Traditional_Farming_Methods_and_Equipments.jpg/220px-Traditional_Farming_Methods_and_Equipments.jpg").into(icon);
             } else{
             Picasso.with(activity).load(newsData.getNewsImage()).into(icon);
             }
             */

            if (!farmerOtherDatas.get(position).getUserImage().equals("")) {
                Picasso.with(activity).load(farmerOtherDatas.get(position).getUserImage()).resize(500, 500).into(viewHolder.profileImage);
            }


            final int pos = position;
            viewHolder.productImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ImageShowFragment imageShowFragment = ImageShowFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putString("image", farmerOtherDatas.get(pos).getProductImage());
                    imageShowFragment.setArguments(bundle);
                    imageShowFragment.show(activity.getFragmentManager(), "go");
                }
            });

            convertedView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    commentListner.doComment(pos, isFarmer, isMyPrice);
                }
            });


            viewHolder.llCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(farmerOtherDatas.get(pos).getPhone().trim())));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(callIntent);
                }
            });


            if (!farmerOtherDatas.get(pos).productImage.equals("")) {
                Picasso.with(activity).load(farmerOtherDatas.get(pos).productImage).resize(500, 500).into(viewHolder.productImage);
                viewHolder.productName.setVisibility(View.VISIBLE);
            } else
                viewHolder.productImage.setVisibility(View.VISIBLE);
        /*if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.custome_video_list, null);*/
//        }

        return convertedView;
    }

    private static class ViewHolder {
        CircleImageView profileImage;
        ImageView productImage;
        TextView name, address, productName, productNQuantity, productType, rate, date, tvCommentCount, tvCreatedAt;
        LinearLayout llCall;
    }

    public interface CommentListner{
        void doComment(int pos, boolean isFarmer, boolean isMyPrice);
    }

}


