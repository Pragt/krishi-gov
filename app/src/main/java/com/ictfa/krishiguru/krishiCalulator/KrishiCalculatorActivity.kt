package com.ictfa.krishiguru.krishiCalulator

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.ictfa.krishiguru.R
import com.ictfa.krishiguru.awaskyakBiruwaSankhya.AwashyakBiruwaSankhyaActivity
import com.ictfa.krishiguru.baliPatro.BaliPatroActivity
import com.ictfa.krishiguru.graph.BarChartActivity
import com.ictfa.krishiguru.graph.Commodity
import com.ictfa.krishiguru.helpers.Alerts
import com.ictfa.krishiguru.helpers.CommonDef
import com.ictfa.krishiguru.helpers.CustomProgressDialog
import com.ictfa.krishiguru.helpers.UrlHelper
import com.ictfa.krishiguru.malkhadCalculator.MalkhadCalculatorActivity
import com.ictfa.krishiguru.utilities.LandMeasurement
import com.ictfa.krishiguru.utilities.WeightMeasurement
import kotlinx.android.synthetic.main.activity_krishi_calculator.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.util.*

class KrishiCalculatorActivity : AppCompatActivity() {

    private val alerts: Alerts by lazy {
        Alerts(this)
    }

    private val progressDialog: CustomProgressDialog by lazy {
        CustomProgressDialog(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_krishi_calculator)
        title = getString(R.string.krishCalculator)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        btn_bajar_bislesan.setOnClickListener {
            getCommodityList()
        }

        btn_bali_patro.setOnClickListener {
            startActivity(Intent(this, BaliPatroActivity::class.java))
        }

        btn_malkhad.setOnClickListener {
            startActivity(Intent(this, MalkhadCalculatorActivity::class.java))
        }

        btn_awaskyak_biruwa_sankhya.setOnClickListener {
            startActivity(Intent(this, AwashyakBiruwaSankhyaActivity::class.java))
        }

        btn_pasu_taul.setOnClickListener {
            startActivity(Intent(this, WeightMeasurement::class.java))
        }

        btn_jagga_mapak.setOnClickListener {
            startActivity(Intent(this, LandMeasurement::class.java))
        }
    }

    private fun getCommodityList() {
        progressDialog.showpd(resources.getString(R.string.please_wait))
        val queue = Volley.newRequestQueue(this)
        val url = UrlHelper.BASE_URL + "api/v3/market/commodity?apikey=" + UrlHelper.API_KEY
        val commodityList = ArrayList<Commodity>()
        val stringRequest = StringRequest(Request.Method.GET, url,
                Response.Listener { response ->
                    progressDialog.hidepd()
                    //formDatas.clear();
                    //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                    try {
                        val utfStr = String(response.toByteArray(charset("ISO-8859-1")), Charset.forName("UTF-8"))
                        val `object` = JSONObject(utfStr)
                        val jsonResponse = `object`.getJSONObject("code")
                        val status = jsonResponse.getInt("status")
                        val message = jsonResponse.getString("message")
                        if (status != 1) {
                            alerts.showSuccessAlert(message)
                        } else {
                            var commodity: Commodity
                            val librarylistJson = `object`.getJSONArray("data")
                            for (i in 0 until librarylistJson.length()) {
                                commodity = Commodity()
                                val type = librarylistJson[i] as JSONObject
                                commodity.id = type.getString("id")
                                commodity.name = type.getString("commodity_name")
                                commodityList.add(commodity)
                            }
                        }
                        Toast.makeText(this, "success", Toast.LENGTH_SHORT).show()
                        val intent = Intent(this, BarChartActivity::class.java)
                        intent.putExtra(CommonDef.COMMODITY_LIST, commodityList)
                        startActivity(intent)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        alerts.showToastMsg(e.message)
                    } catch (e: UnsupportedEncodingException) {
                        alerts.showToastMsg(e.message)
                        e.printStackTrace()
                    }
                }, Response.ErrorListener {
            progressDialog.hidepd()
            alerts.showToastMsg(resources.getString(R.string.no_internet_connection))
            // mTextView.setText("That didn't work!");
        })
        // Add the request to the RequestQueue.
        queue.add(stringRequest)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                // app icon in action bar clicked; go home
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}