package com.ictfa.krishiguru.sendFeedback;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.login.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendFeedback extends AppCompatActivity {

    @BindView(R.id.et_feedback)
    EditText edtFeedback;

    @BindView(R.id.btn_send)
    Button btnSend;
    Alerts alerts;

    SharedPreference prefs;
    CustomProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_feedback);
        CommonMethods.setupUI(findViewById(R.id.rl_send_feedback), this);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("प्रतिक्रिया पठाउनु होस");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        alerts = new Alerts(this);
        prefs = new SharedPreference(this);
        pd = new CustomProgressDialog(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_send)
    void onSendClick() {
        if (edtFeedback.getText().toString().isEmpty())
            alerts.showToastMsg("कृपया आफ्नो प्रतिक्रिया लेख्नुहोस");
        else if (prefs.getIntValues(CommonDef.SharedPreference.IS_LOGIN) == 1)
            doSendFeedback();
        else
        {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    private void doSendFeedback() {
        pd.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(SendFeedback.this);
        //  String url ="http://www.google.com";

// Request a string response from the provided URL.
        String url = UrlHelper.BASE_URL + "api/v3/agency/" + 0 + "/feedback?apikey=" + UrlHelper.API_KEY;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");

                            JSONObject jsonObject = new JSONObject(utfStr);

                            JSONObject meta = jsonObject.getJSONObject("code");
                            int status = meta.getInt("status");
                            if (status == 1) {
                                alerts.showSuccessAlert("तपाईको प्रतिक्रिया दर्ता भएको छ, धन्यवाद!");
                            }else
                            {
                                alerts.showErrorAlert(meta.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mTextView.setText("That didn't work!");
                pd.hidepd();
                alerts.showErrorAlert(getResources().getString(R.string.no_internet_connection));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("suggester_id", String.valueOf(prefs.getIntValues(CommonDef.SharedPreference.USER_ID)));
                params.put("suggestion", String.valueOf(edtFeedback.getText().toString()));

                return params;
            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
