package com.ictfa.krishiguru.awaskyakBiruwaSankhya.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CropCalculator {

    @SerializedName("tarkari")
    @Expose
    public ArrayList<Product> tarkari = null;
    @SerializedName("falfull")
    @Expose
    public ArrayList<Product> falfull = null;

}
