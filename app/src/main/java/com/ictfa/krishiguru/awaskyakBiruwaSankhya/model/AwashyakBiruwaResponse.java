package com.ictfa.krishiguru.awaskyakBiruwaSankhya.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Prajeet Naga on 9/13/20, 11:33 AM.
 **/

public class AwashyakBiruwaResponse {
    @SerializedName("meta")
    @Expose
    public List<Object> meta = null;
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("code")
    @Expose
    public Code code;

    public class Code {

        @SerializedName("status")
        @Expose
        public Integer status;
        @SerializedName("message")
        @Expose
        public String message;

    }

    public class Data {

        @SerializedName("crop_calculator")
        @Expose
        public List<CropCalculator> cropCalculator = null;

    }
}
