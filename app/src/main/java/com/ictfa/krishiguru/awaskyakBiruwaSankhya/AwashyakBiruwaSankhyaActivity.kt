package com.ictfa.krishiguru.awaskyakBiruwaSankhya

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.ictfa.krishiguru.R
import com.ictfa.krishiguru.awaskyakBiruwaSankhya.model.CropCalculatorTable
import com.ictfa.krishiguru.awaskyakBiruwaSankhya.model.JatTable
import com.ictfa.krishiguru.awaskyakBiruwaSankhya.model.ProductTable
import com.ictfa.krishiguru.helpers.Alerts
import com.ictfa.krishiguru.helpers.CommonMethods
import com.ictfa.krishiguru.realm.RealmController
import com.ictfa.krishiguru.retrofit.ApiClient
import com.ictfa.krishiguru.retrofit.ApiInterface
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_awashyak_biruwa_sankhya.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class AwashyakBiruwaSankhyaActivity : AppCompatActivity() {

    private var isRopani = true

    private val realm: Realm by lazy {
        RealmController.with(this).realm
    }

    private val alerts: Alerts by lazy {
        Alerts(this)
    }

    private var cropCalculatorTable: CropCalculatorTable? = null

    private var line_line: Int = 0
    private var bot_bot: Int = 0

    private val biruwaKisim = listOf("तरकारी",
            "फलफुल ")

    private val jaggaKisim = listOf("रोपनी/आना",
            "बिग्गा/कठ्ठा")

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_awashyak_biruwa_sankhya)
        title = "आवश्यक बिरुवा संख्या"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        CommonMethods.setupUI(findViewById(R.id.clAwaskhyakBiruwa), this)
        populateData()

        val biruwaAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, biruwaKisim) //selected item will look like a spinner set from XML

        biruwaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_biruwa_type.adapter = biruwaAdapter

        val jaggaKisimAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, jaggaKisim) //selected item will look like a spinner set from XML

        jaggaKisimAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_jagga_kisim.adapter = jaggaKisimAdapter

        btnCalculate.setOnClickListener {
            if (et_aana.text.isNotBlank() || et_ropani.text.isNotBlank()) {
                grResult.visibility = View.VISIBLE
                tvRequiredBiruwa.text = "तपाईको जग्गामा ${totalNoOfBiruwa()} बिरुवा चाहिन्छ |"
                tvRequiredBotBot.text = "बोट - बोटको दुरी $line_line सेमी हुनुपर्छ |"
                tvRequiredLineLine.text = "लाइन - लाइनको दुरी $bot_bot सेमी हुनुपर्छ |"

                scrollView.postDelayed({
                    scrollView.fullScroll(View.FOCUS_DOWN)
                }, 400)
            } else {
                grResult.visibility = View.GONE
                alerts.showToastMsg("कृपया जग्गा नाप प्रदान गर्नुहोस्")
            }
        }

        sp_biruwa_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (cropCalculatorTable != null)
                    setBiruwaNameAdapter()
            } // to close the onItemSelected
        }

        sp_biruwa_name.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                setBiruwakoJat()
            } // to close the onItemSelected
        }

        sp_biruwa_jaat.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (sp_biruwa_type.selectedItemPosition == 0) {
                    line_line = cropCalculatorTable!!.tarkari[sp_biruwa_name.selectedItemPosition]!!.jat[sp_biruwa_jaat.selectedItemPosition]!!.lineLine
                    bot_bot = cropCalculatorTable!!.tarkari[sp_biruwa_name.selectedItemPosition]!!.jat[sp_biruwa_jaat.selectedItemPosition]!!.botBot
                } else {
                    line_line = cropCalculatorTable!!.falfull[sp_biruwa_name.selectedItemPosition]!!.jat[sp_biruwa_jaat.selectedItemPosition]!!.lineLine
                    bot_bot = cropCalculatorTable!!.falfull[sp_biruwa_name.selectedItemPosition]!!.jat[sp_biruwa_jaat.selectedItemPosition]!!.botBot
                }
            } // to close the onItemSelected
        }

        sp_jagga_kisim.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                isRopani = position == 0
                updateMeasurement()
            } // to close the onItemSelected
        }
    }

    private fun updateMeasurement() {
        if (isRopani) {
            tvFirstValue.text = "रोपनी"
            tvSecondValue.text = "आना"
        } else {
            tvFirstValue.text = "बिग्गा"
            tvSecondValue.text = "कठ्ठा"
        }
    }

    private fun totalNoOfBiruwa(): Int {
        var totalCmSq: Double = 0.0
        if (sp_jagga_kisim.selectedItemPosition == 0) {
            var ropani = if (et_ropani.text.toString().isNotEmpty()) et_ropani.text.toString().toInt() else 0
            var aana = if (et_aana.text.toString().isNotEmpty()) et_aana.text.toString().toInt() else 0
            totalCmSq = (ropani + (aana / 16.0)) * 5087370
        } else {
            var bigga = if (et_ropani.text.toString().isNotEmpty()) et_ropani.text.toString().toInt() else 0
            var katha = if (et_aana.text.toString().isNotEmpty()) et_aana.text.toString().toInt() else 0
            totalCmSq = (bigga + (katha * 0.0751 / 1.502)) * 67726381
        }

        return (totalCmSq / (bot_bot * line_line)).toInt()

    }

    private fun setBiruwaNameAdapter() {
        var biruwaNames = ArrayList<String>()
        if (sp_biruwa_type.selectedItemPosition == 0) {
            for (tarkarus in cropCalculatorTable!!.tarkari) {
                biruwaNames.add(tarkarus.name)
            }
        } else {
            for (falful in cropCalculatorTable!!.falfull) {
                biruwaNames.add(falful.name)
            }
        }

        val biruwaNameAdapter: ArrayAdapter<String> = ArrayAdapter(this, android.R.layout.simple_spinner_item, biruwaNames) //selected item will look like a spinner set from XML

        biruwaNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_biruwa_name.adapter = biruwaNameAdapter
    }


    private fun setBiruwakoJat() {
        var biruwaJatNames = ArrayList<String>()
        if (sp_biruwa_type.selectedItemPosition == 0) {
            for (jat in cropCalculatorTable!!.tarkari[sp_biruwa_name.selectedItemPosition]!!.jat) {
                biruwaJatNames.add(jat.name)
            }
        } else {
            for (falful in cropCalculatorTable!!.falfull[sp_biruwa_name.selectedItemPosition]!!.jat) {
                biruwaJatNames.add(falful.name)
            }
        }

        val biruwaJatAdapter: ArrayAdapter<String> = ArrayAdapter(this, android.R.layout.simple_spinner_item, biruwaJatNames) //selected item will look like a spinner set from XML

        biruwaJatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_biruwa_jaat.adapter = biruwaJatAdapter
    }

    private fun populateData() {
        cropCalculatorTable = realm.where(CropCalculatorTable::class.java).findFirst()

        if (cropCalculatorTable != null) {
            val biruwaAdapter: ArrayAdapter<String> = ArrayAdapter(this, android.R.layout.simple_spinner_item, biruwaKisim) //selected item will look like a spinner set from XML

            biruwaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_biruwa_type.adapter = biruwaAdapter
        } else {
            getDataFromServer()
        }
    }

    private fun getDataFromServer() {
        ApiClient.getClient().create(ApiInterface::class.java).getCropCalculatorResponse("O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    if (s.code.status == 0) {

                        var tarkariTable = RealmList<ProductTable>()
                        var falfulTable = RealmList<ProductTable>()
                        var cropCalculatorTable = CropCalculatorTable()
                        for (cropCalculator in s.data.cropCalculator) {
                            cropCalculator.tarkari?.let {
                                for (product in cropCalculator.tarkari) {
                                    var jatList = RealmList<JatTable>()
                                    for (jat in product.jat) {
                                        var jatTable = JatTable()
                                        jatTable.name = jat.name
                                        jatTable.lineLine = jat.lineLine
                                        jatTable.botBot = jat.botBot
                                        jatList.add(jatTable)
                                    }
                                    var productTable = ProductTable()
                                    productTable.name = product.name
                                    productTable.jat = jatList
                                    tarkariTable.add(productTable)
                                }
                            }
                            cropCalculator.falfull?.let {
                                for (product in cropCalculator.falfull) {
                                    var jatList = RealmList<JatTable>()
                                    for (jat in product.jat) {
                                        var jatTable = JatTable()
                                        jatTable.name = jat.name
                                        jatTable.lineLine = jat.lineLine
                                        jatTable.botBot = jat.botBot
                                        jatList.add(jatTable)
                                    }
                                    var productTable = ProductTable()
                                    productTable.name = product.name
                                    productTable.jat = jatList
                                    falfulTable.add(productTable)
                                }
                            }
                        }
                        cropCalculatorTable.falfull = falfulTable
                        cropCalculatorTable.tarkari = tarkariTable
                        realm.executeTransaction {
                            it.delete(CropCalculatorTable::class.java)
                        }
                        realm.beginTransaction()
                        realm.insertOrUpdate(cropCalculatorTable)
                        realm.commitTransaction()
                        populateData()
                    } else {
                        alerts.showToastMsg(s.code.message)
                    }
                },
                        { e ->
                            alerts.showToastMsg("Could not connect to server. Please try again later.")
                            e.printStackTrace()
                        },
                        { kotlin.io.println("supervisor list") })
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                // app icon in action bar clicked; go home
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}