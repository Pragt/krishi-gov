package com.ictfa.krishiguru.awaskyakBiruwaSankhya.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ProductTable extends RealmObject {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("jat")
    @Expose
    public RealmList<JatTable> jat = null;

}