package com.ictfa.krishiguru.awaskyakBiruwaSankhya.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class CropCalculatorTable extends RealmObject {

    @SerializedName("tarkari")
    @Expose
    public RealmList<ProductTable> tarkari = null;
    @SerializedName("falfull")
    @Expose
    public RealmList<ProductTable> falfull = null;

}