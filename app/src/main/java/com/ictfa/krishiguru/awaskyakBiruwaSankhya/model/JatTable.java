package com.ictfa.krishiguru.awaskyakBiruwaSankhya.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class JatTable extends RealmObject {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("line_line")
    @Expose
    public Integer lineLine;
    @SerializedName("bot_bot")
    @Expose
    public Integer botBot;
}