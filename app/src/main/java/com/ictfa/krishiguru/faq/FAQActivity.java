package com.ictfa.krishiguru.faq;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.ictfa.krishiguru.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FAQActivity extends AppCompatActivity {

    private BufferedReader reader;
    private StringBuilder text = new StringBuilder();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        getSupportActionBar().setTitle(getString(R.string.faq));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("faq.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                text.append(mLine);
                text.append('\n');
            }
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Error reading file!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }

            TextView output = (TextView) findViewById(R.id.tv_about_us);
            WebView webview = (WebView) findViewById(R.id.wv_data);
            webview.setVisibility(View.VISIBLE);
            WebView.setWebContentsDebuggingEnabled(false);
            webview.loadDataWithBaseURL(null, text.toString(), "text/html", "utf-8", null);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
