package com.ictfa.krishiguru.fullNoticeView;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pragt on 3/5/17.
 */

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    ArrayList<Comment> listComment;

    public CommentAdapter(Context mContext, ArrayList<Comment> listNotice) {
        this.mContext = mContext;
        this.listComment = listNotice;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_comment, parent, false);
        return new myNoticeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myNoticeViewHolder) {
            final myNoticeViewHolder holder1 = (myNoticeViewHolder) holder;
            holder1.tvName.setText(listComment.get(position).commented_by);
            holder1.tvComment.setText(listComment.get(position).comment);
            holder1.tvDateTime.setText(listComment.get(position).date);
        }
    }

    @Override
    public int getItemCount() {
        return listComment.size();
    }

    public void addComment(Comment newComment) {
        listComment.add(newComment);
        notifyItemInserted(listComment.size());
    }


    public class myNoticeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_comment)
        TextView tvComment;
        @BindView(R.id.tv_date_time)
        TextView tvDateTime;


        public myNoticeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
