package com.ictfa.krishiguru.fullNoticeView;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.customViews.GridSpacingItemDecoration;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.login.LoginActivity;
import com.ictfa.krishiguru.myOrganization.view.Objects.Comment;
import com.ictfa.krishiguru.myOrganization.view.Objects.NoticeObject;
import com.ictfa.krishiguru.realm.RealmController;
import com.ictfa.krishiguru.ui.fragment.ImageShowFragment;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Prajeet on 08/07/2017.
 */

public class FullNoticeViewActivity extends AppCompatActivity {

    private static final String TAG = "prajit";
    NoticeObject noticeObject;
    @BindView(R.id.tv_org_name)
    TextView tvOrgName;
    @BindView(R.id.tv_day_left)
    TextView tvDaysLeft;
    @BindView(R.id.tv_notice)
    TextView tvNotice;
    @BindView(R.id.tv_comment_count)
    TextView tvCommentCount;
    @BindView(R.id.rv_comments)
    RecyclerView rvComments;
    @BindView(R.id.rl_add_comments)
    RelativeLayout rlAddComments;
    private CommentAdapter mAdapter;
    ArrayList<Comment> listComment;
    @BindView(R.id.edt_comment)
    EditText edtComment;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.iv_send)
    ImageView ivSend;
    @BindView(R.id.btnDownload)
    Button btnDownload;
    @BindView(R.id.ivPdf)
    ImageView ivPdf;
    String name;
    int user_id;
    SharedPreference sharedPreference;
    CustomProgressDialog pd1;
    Alerts alerts;
    int notice_id;
    private Comment newComment;
    int commentCount = 0;
    @BindView(R.id.ll_call)
    LinearLayout llCall;
    @BindView(R.id.ll_share)
    LinearLayout llShare;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    @BindView(R.id.ivPhoto)
    ImageView ivPhoto;
    private Realm realm;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_notice);
        CommonMethods.setupUI(findViewById(R.id.rl_full_notice), this);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.notice));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        this.realm = RealmController.with(this).getRealm();

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.e("RESSULT", "Success");
                //   getAndAddSlots();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.e("RESSULT", "error=" + error.toString());
//                Toast.makeText(Complete_Run_Activity.this, "Unable to Share Image to Facebook.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void init() {
        listComment = new ArrayList<>();
        pd1 = new CustomProgressDialog(this);
        alerts = new Alerts(this);
        sharedPreference = new SharedPreference(this);
        user_id = sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
        name = sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            noticeObject = (NoticeObject) bundle.getSerializable(CommonDef.NOTICE);
            tvOrgName.setText(noticeObject.org_name);
//            tvNotice.setText(noticeObject.notice);
            tvNotice.setText( Html.fromHtml( noticeObject.notice) );
            tvNotice.setMovementMethod(LinkMovementMethod.getInstance());
            tvTitle.setText(noticeObject.title);
            tvType.setText(noticeObject.type);
            tvCommentCount.setText(noticeObject.comment_count + " Comments");
            notice_id = noticeObject.id;
            commentCount = noticeObject.comment_count;
            getComments(notice_id);

            if (noticeObject.days_remaining == 0)
                tvDaysLeft.setText(":      म्याद सकियो");
            else if (noticeObject.days_remaining == 1) {
                tvDaysLeft.setText("आज, १ दिन बांकी");
            } else
                tvDaysLeft.setText(noticeObject.days_remaining + " दिन बाकी ");

            if (noticeObject.imageUrl != null && noticeObject.imageUrl.equalsIgnoreCase(""))
                ivPhoto.setVisibility(View.GONE);
            else {
                Picasso.with(this).load(noticeObject.imageUrl).networkPolicy(NetworkPolicy.OFFLINE).resize(500, 500).into(ivPhoto);
                ivPhoto.setVisibility(View.VISIBLE);
            }

            if (noticeObject.imageUrl != null && !noticeObject.imageUrl.equalsIgnoreCase(""))
                Picasso.with(this).load(noticeObject.imageUrl).into(ivPhoto);

            ivPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ImageShowFragment imageShowFragment = ImageShowFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putString("image", noticeObject.imageUrl);
                    imageShowFragment.setArguments(bundle);
                    imageShowFragment.show(getFragmentManager(), "sad");
                }
            });

            if (!TextUtils.isEmpty(noticeObject.noticeFile)) {
                ivPdf.setVisibility(View.VISIBLE);
                btnDownload.setVisibility(View.VISIBLE);
            }

        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvComments.setNestedScrollingEnabled(false);
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(this, 0), true));
        rvComments.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CommentAdapter(this, listComment);
        rvComments.setAdapter(mAdapter);
    }

    @OnClick(R.id.btnDownload)
    void onDownloadClicked() {
        isStoragePermissionGranted();
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                downloadPdf(noticeObject.noticeFile);
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
            downloadPdf(noticeObject.noticeFile);
        }
    }

    public void getComments(int noticeId) {
        pd1.showpd(getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(this);
        String zone = UrlHelper.BASE_URL + "api/v3/notice/" + noticeId + "/comments?apikey=" + UrlHelper.API_KEY;
        Log.d("Comments", zone);
        listComment = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, zone,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd1.hidepd();
                        //forumDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONArray commentList = jsonObject.getJSONArray("data");
                            ArrayList<Comment> comments = new ArrayList<>();
                            Comment comment;
                            if (commentList != null) {
                                for (int j = 0; j < commentList.length(); j++) {
                                    JSONObject commentJson = commentList.getJSONObject(j);
                                    comment = new Comment();
                                    comment.comment_id = commentJson.getInt("id");
                                    comment.comment = commentJson.getString("comment");
                                    comment.commented_by = commentJson.getString("commented_by");
//                                    JSONObject created_at = commentJson.getJSONObject("created_at");
                                    comment.date = commentJson.getString("created_at");
//                                    comment.timezone_type = created_at.getInt("timezone_type");
//                                    comment.timezone = created_at.getString("timezone");
                                    comments.add(comment);
                                }
                                listComment = comments;
                                mAdapter = new CommentAdapter(FullNoticeViewActivity.this, listComment);
                                rvComments.setAdapter(mAdapter);
                                noticeObject.listComment = new RealmList<>();
                                noticeObject.listComment.addAll(listComment);

                            }

                        } catch (JSONException e) {
                            alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                pd1.hidepd();
                // mTextView.setText("That didn't work!");
            }


        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.iv_send)
    void onSendClick() {
        if (!edtComment.getText().toString().isEmpty()) {
            if (noticeObject.days_remaining > 0) {
                doComment(edtComment.getText().toString());
            } else {
                alerts.showToastMsg("म्याद सकियो");
            }
        }
    }

    private void doComment(final String comment) {
        if (sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) == 0) {
            Intent intent = new Intent(FullNoticeViewActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            pd1.showpd(getResources().getString(R.string.please_wait));
            JSONObject jsonObject;
            RequestQueue queue = Volley.newRequestQueue(FullNoticeViewActivity.this);
            String commentUrl = UrlHelper.BASE_URL + "api/v3/notice/" + notice_id + "/comment?apikey=" + UrlHelper.API_KEY;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, commentUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pd1.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObj = new JSONObject(response);
                                JSONObject codeJson = jsonObj.getJSONObject("code");
                                String status = codeJson.getString("status");
                                String message = codeJson.getString("message");
                                if (status.equalsIgnoreCase("1")) {
                                    newComment = new Comment();
                                    newComment.comment = comment;
                                    newComment.commented_by = name;
                                    newComment.date = new SimpleDateFormat("yyyy MMM dd HH:mm:ss").format(Calendar.getInstance().getTime());
//                                    mAdapter.addComment(newComment);
                                    listComment.add(newComment);
                                    mAdapter.notifyItemInserted(listComment.size());
                                    tvCommentCount.setText(++commentCount + " Comments");
                                    rvComments.smoothScrollToPosition(listComment.size());
                                    nestedScrollView.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            nestedScrollView.fullScroll(View.FOCUS_DOWN);
                                        }
                                    });
                                    edtComment.setText("");
                                } else
                                    alerts.showErrorAlert(message);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                    pd1.hidepd();
                    // mTextView.setText("That didn't work!");
                }


            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("tag", "login");
                    params.put("user_id", String.valueOf(user_id));
                    params.put("comment", comment);
                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    @Override
    public void onBackPressed() {
        try {
//            noticeObject.comment_count = commentCount;
//            noticeObject.listComment.addAll(realm.copyFromRealm(listComment));
//
//            Intent intent = new Intent();
//            intent.putExtra(CommonDef.NOTICE, noticeObject);
//            setResult(RESULT_OK, intent);
        } catch (Exception exx) {

        }

        Log.i("prajit", "onBackPressed: " + "bacpressed");
        finish();
    }

    @OnClick(R.id.ll_call)
    void onCallClicked() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(noticeObject.phone.trim())));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
    }

    @OnClick(R.id.ll_share)
    void onShareClicked() {
        Bitmap b = getBitmapRootView(findViewById(R.id.llNotice));
        shareToFb(b);
    }

    private void shareToFb(Bitmap b) {
        if (shareDialog.canShow(SharePhotoContent.class)) {

            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(b)
                    .setCaption("Hey... \nI earned by money by running/walking for the charity.")
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();
            shareDialog.show(content);
        }
    }


    private Bitmap getBitmapRootView(View view) {
        View rootView = view;
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

    public void downloadPdf(String url) {
        Toast.makeText(this, "Downloading...", Toast.LENGTH_LONG).show();
        Uri Download_Uri = Uri.parse("http://admin.ict4agri.com/assets/images/org_notice_file/" + url);
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

        //Restrict the types of networks over which this download may proceed.
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        //Set whether this download may proceed over a roaming connection.
        request.setAllowedOverRoaming(false);
        //Set the title of this download, to be displayed in notifications (if enabled).
        request.setTitle("Downloading");
        //Set a description of this download, to be displayed in notifications (if enabled)
        request.setDescription("Downloading File");
        //Set the local destination for the downloaded file to a path within the application's external files directory
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Municipality_" + System.currentTimeMillis() + ".pdf");

        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        //Enqueue a new download and same the referenceId
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
