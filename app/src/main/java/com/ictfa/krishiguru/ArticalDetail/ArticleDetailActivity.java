package com.ictfa.krishiguru.ArticalDetail;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.ShareDialog;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.krishilibrary.Object.Article;
import com.ictfa.krishiguru.krishilibrary.Object.CommentLibraryObject;
import com.ictfa.krishiguru.library.task.ArticleComments.ArticleCommentsActivity;
import com.ictfa.krishiguru.login.LoginActivity;
import com.ictfa.krishiguru.realm.RealmController;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by Prajeet on 14/07/2017.
 */

public class ArticleDetailActivity extends AppCompatActivity {

    Article article;
    @BindView(R.id.tv_descrition)
    HtmlTextView tvDescription;
    @BindView(R.id.tv_article_title)
    TextView tvArticleTitle;
    @BindView(R.id.tv_article_type)
    TextView tvArticleType;
    @BindView(R.id.tv_author)
    TextView tvAuthor;
    @BindView(R.id.tv_created_at)
    TextView tvCreatedAt;
    @BindView(R.id.tv_like_count)
    TextView tvLikeCount;
    @BindView(R.id.tv_comment_count)
    TextView tvCommentCount;
    @BindView(R.id.tv_share_count)
    TextView tvShareCount;
    @BindView(R.id.tv_like)
    TextView tvLike;
    @BindView(R.id.ll_like)
    LinearLayout llLike;
    @BindView(R.id.ll_comments)
    LinearLayout llComments;
    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.ll_share)
    LinearLayout llShare;
    int articleId;
    boolean isLiked = false;
    SharedPreference sharedPreference;
    Alerts alerts;
    CustomProgressDialog pd;
    int user_id;

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ShareOpenGraphObject object;
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);
        ButterKnife.bind(this);
//      init
        sharedPreference = new SharedPreference(this);
        user_id = sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
        alerts = new Alerts(this);
        pd = new CustomProgressDialog(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        articleId = (int) getIntent().getIntExtra(CommonDef.ARTICLE_ID, 0);

        //get realm instance
        this.realm = RealmController.with(this).getRealm();
        article = realm.where(Article.class).equalTo("id", articleId).findFirst();

        if (Build.VERSION.SDK_INT >= 24) {
            tvDescription.setText(Html.fromHtml(article.getDescription(), Html.FROM_HTML_MODE_LEGACY)); // for 24 api and more
        } else {
            tvDescription.setText(Html.fromHtml(article.getDescription())); // or for older api
        }

//        tvDescription.setHtml(article.getDescription());

        tvArticleTitle.setText(article.getLibraryTitle());
        tvArticleType.setText(article.getLibraryType());
        tvCreatedAt.setText(article.getCreatedAt());
        tvLikeCount.setText(article.getLikeCount() + " Likes");
        tvCommentCount.setText(article.getCommentCount() + " Comments");
        tvShareCount.setText(article.getShareCount() + " Shares");
        tvAuthor.setText("Author/Source: " + article.getAuthor());
        articleId = article.getId();

        if (!article.getImage().equalsIgnoreCase("")) {
            Picasso.with(this).load(article.getImage()).
                    placeholder(getResources().getDrawable(R.drawable.images)).
                    into(ivImage);
        }

        if (article.isLiked()) {
            isLiked = true;
            tvLike.setTextColor(Color.GREEN);
        } else
            isLiked = false;

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(article.getLibraryTitle());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.ll_like)
    void onLikeClicked() {
        if (!isLiked)
            doLikeArticle();
    }

    @OnClick(R.id.ll_share)
    void doShare() {
        if (!isLiked)
            // Create an object
            object = new ShareOpenGraphObject.Builder()
                    .putString("og:type", "notice.notice")
                    .putString("og:title", article.getLibraryTitle())
                    .putString("og:description", article.getLibraryType())
                    .putString("books:isbn", "0-553-57340-3")
                    .build();

        // Create an action
        ShareOpenGraphAction action = new ShareOpenGraphAction.Builder()
                .setActionType("status.reads")
                .putObject("book", object)
                .build();

        // Create the content
        ShareOpenGraphContent content = new ShareOpenGraphContent.Builder()
                .setPreviewPropertyName("book")
                .setAction(action)
                .build();

        ShareDialog.show(this, content);
    }


    private void doLikeArticle() {
        if (sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) == 0) {
            Intent intent = new Intent(ArticleDetailActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            pd.showpd(getResources().getString(R.string.please_wait));
            JSONObject jsonObject;
            RequestQueue queue = Volley.newRequestQueue(ArticleDetailActivity.this);
            String commentUrl = UrlHelper.BASE_URL + "api/v3/library/" + articleId + "/like?apikey=" + UrlHelper.API_KEY + "&user_id=" + user_id;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, commentUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pd.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObj = new JSONObject(response);
                                JSONObject codeJson = jsonObj.getJSONObject("code");
                                String status = codeJson.getString("status");
                                String message = codeJson.getString("message");
                                if (status.equalsIgnoreCase("1")) {
                                    Toast.makeText(ArticleDetailActivity.this, "You Liked the post", Toast.LENGTH_SHORT).show();
                                    tvLike.setTextColor(Color.GREEN);
                                    isLiked = true;
                                } else
                                    alerts.showErrorAlert(message);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                    pd.hidepd();
                    // mTextView.setText("That didn't work!");
                }


            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("tag", "login");
                    params.put("user_id", String.valueOf(user_id));
                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    @OnClick(R.id.ll_comments)
    void onGetComments() {
        pd.showpd(getResources().getString(R.string.please_wait));
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = UrlHelper.BASE_URL + "api/v3/library/" + articleId + "/comments?apikey=" + UrlHelper.API_KEY;
        Log.d("url", url);
//        articleList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject object = new JSONObject(utfStr);
                            JSONObject jsonResponse = object.getJSONObject("code");
                            int status = jsonResponse.getInt("status");
                            String message = jsonResponse.getString("message");
                            ArrayList<CommentLibraryObject> listComment = new ArrayList<>();
                            CommentLibraryObject comment;
                            if (status == 1) {


                                JSONArray jsonArray = object.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    comment = new CommentLibraryObject();
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    comment.id = jsonObject.getString("id");
                                    comment.comment = jsonObject.getString("comment");
                                    comment.commentedBy = jsonObject.getString("commented_by");
                                    comment.createdAt = jsonObject.getString("created_at");
                                    listComment.add(comment);
                                }


                                Toast.makeText(ArticleDetailActivity.this, "Listed Successfully", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ArticleDetailActivity.this, ArticleCommentsActivity.class);
                                intent.putExtra("ListComment", listComment);
                                intent.putExtra("articleId", articleId);
                                startActivity(intent);
                            } else {
                                Toast.makeText(ArticleDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
//                            mAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alerts.showToastMsg(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            alerts.showToastMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.hidepd();
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
