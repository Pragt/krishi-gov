package com.ictfa.krishiguru.WeatherFragment;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.Weather.WeatherAdapter;
import com.ictfa.krishiguru.Weather.WeatherResponse;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.ui.activity.WeatherActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherFragmentDay1 extends Fragment {

    private WeatherActivity mActivity;
    @BindView(R.id.tv_location)
    TextView tv_location;
    @BindView(R.id.tvTodayDateTime)
    TextView tvTodayDateTime;
    @BindView(R.id.tvPressure)
    TextView tvPressure;
    @BindView(R.id.tvHumidity)
    TextView tvHumidity;
    @BindView(R.id.todayTemperature)
    TextView todayTemperature;
    @BindView(R.id.todayPrediction)
    TextView todayPrediction;
    @BindView(R.id.ivWeather)
    ImageView ivWeather;
    @BindView(R.id.rvWeather)
    RecyclerView rvWeather;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;

    public WeatherFragmentDay1() {
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tv_location.setText(mActivity.location);
        tvTodayDateTime.setText(CommonMethods.convertDateIntoTime(mActivity.forecasts.dayAfterTomorrow.get(0).dateTime));
        tvPressure.setText("Rain: " + mActivity.forecasts.today.get(0).rain + " mm");
        tvHumidity.setText("Humidity: " + mActivity.forecasts.today.get(0).humidity + "%");
        todayTemperature.setText(mActivity.forecasts.today.get(0).temperature + " \u2103");
        todayPrediction.setText(mActivity.forecasts.today.get(0).weatherDescription);
        Picasso.with(getActivity()).load(mActivity.forecasts.today.get(0).icon).resize(500, 500).into(ivWeather);

        rvWeather.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvWeather.setAdapter(new WeatherAdapter(getActivity(), (ArrayList<WeatherResponse.WeatherData>) mActivity.forecasts.today));

        rvWeather.setNestedScrollingEnabled(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        ButterKnife.bind(this, view);
        //  ProductDataListenerManager.getInstance().addDataGotTodayWeather(this);


        return view;

    }

//    void loadData() {
//        pos = 1;
//        if (listWeather != null && listWeather.size() > 1) {
//            Picasso.with(getActivity()).load(listWeather.get(pos).iconDay).resize(500, 500).into(daywebIcon);
//            Picasso.with(getActivity()).load(listWeather.get(pos).iconNight).resize(500, 500).into(nightWebicon);
//            dayMaxTemp.setText(listWeather.get(pos).maxTemp + " °c");
//            dayMinTemp.setText(listWeather.get(pos).minTemp + " °c");
//            nightMaxTemp.setText(listWeather.get(pos).maxTemp + " °c");
//            nightMinTemp.setText(listWeather.get(pos).minTemp + " °c");
//            dayDescView.setText(listWeather.get(pos).dayDescription);
//            nightDescView.setText(listWeather.get(pos).nightDescription);
//            dayThunderStormView.setText("मेघगर्जनकाे सम्भावना :  " + listWeather.get(pos).chanceOfThunderDay + " प्रतिशत");
//            nightThunderStormView.setText("मेघगर्जनकाे सम्भावना : " + listWeather.get(pos).chanceOfThunderNight + " प्रतिशत");
//            dayChanceOfRain.setText("पानी पर्ने सम्भावना : " + listWeather.get(pos).chanceOfRainDay + " प्रतिशत");
//            nightChanceOfrain.setText("पानी पर्ने सम्भावना : " + listWeather.get(pos).chanceOfRainNight + " प्रतिशत");
//            dayRainDuration.setText("पानी पर्ने अवधि  : " + listWeather.get(pos).durationOfRainDay + " घण्टा");
//            nightRainDuration.setText("पानी पर्ने अवधि  : " + listWeather.get(pos).durationOfRainNight + " घण्टा");
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (WeatherActivity) context;
    }
}
