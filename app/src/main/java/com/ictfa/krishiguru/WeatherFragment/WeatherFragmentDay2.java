package com.ictfa.krishiguru.WeatherFragment;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.Weather.WeatherAdapter;
import com.ictfa.krishiguru.Weather.WeatherResponse;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.ui.activity.WeatherActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherFragmentDay2 extends Fragment {

    private WeatherActivity mActivity;
    @BindView(R.id.tv_location)
    TextView tv_location;
    @BindView(R.id.tvTodayDateTime)
    TextView tvTodayDateTime;
    @BindView(R.id.tvPressure)
    TextView tvPressure;
    @BindView(R.id.tvHumidity)
    TextView tvHumidity;
    @BindView(R.id.todayTemperature)
    TextView todayTemperature;
    @BindView(R.id.ivWeather)
    ImageView ivWeather;
    @BindView(R.id.todayPrediction)
    TextView todayPrediction;
    @BindView(R.id.rvWeather)
    RecyclerView rvWeather;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;

    public WeatherFragmentDay2() {
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tv_location.setText(mActivity.location);
        tvTodayDateTime.setText(CommonMethods.convertDateIntoTime(mActivity.forecasts.dayAfterTomorrow.get(0).dateTime));
        tvPressure.setText("Rain: " + mActivity.forecasts.tomorrow.get(0).rain + " mm");
        tvHumidity.setText("Humidity: " + mActivity.forecasts.tomorrow.get(0).humidity + "%");
        todayTemperature.setText(mActivity.forecasts.tomorrow.get(0).temperature + " \u2103");
        todayPrediction.setText(mActivity.forecasts.tomorrow.get(0).weatherDescription);
        Picasso.with(getActivity()).load(mActivity.forecasts.tomorrow.get(0).icon).resize(500, 500).into(ivWeather);

        rvWeather.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvWeather.setAdapter(new WeatherAdapter(getActivity(), (ArrayList<WeatherResponse.WeatherData>) mActivity.forecasts.tomorrow));

        rvWeather.setNestedScrollingEnabled(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        ButterKnife.bind(this, view);
        //  ProductDataListenerManager.getInstance().addDataGotTodayWeather(this);


        return view;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (WeatherActivity) context;
    }
}
