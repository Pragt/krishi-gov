package com.ictfa.krishiguru.library.task;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.krishilibrary.Object.CommentLibraryObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pragt on 3/5/17.
 */

public class CommentArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    ArrayList<CommentLibraryObject> listComment;

    public CommentArticleAdapter(Context mContext, ArrayList<CommentLibraryObject> listNotice) {
        this.mContext = mContext;
        this.listComment = listNotice;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_comment, parent, false);
        return new myNoticeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myNoticeViewHolder) {
            final myNoticeViewHolder holder1 = (myNoticeViewHolder) holder;
            holder1.tvName.setText(listComment.get(position).commentedBy);
            holder1.tvComment.setText(listComment.get(position).comment);
            holder1.tvDateTime.setText(listComment.get(position).createdAt);
        }
    }

    @Override
    public int getItemCount() {
        return listComment.size();
    }

    public void addComment(CommentLibraryObject newComment) {
        listComment.add(0, newComment);
        notifyItemInserted(0);
    }


    public class myNoticeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_comment)
        TextView tvComment;
        @BindView(R.id.tv_date_time)
        TextView tvDateTime;


        public myNoticeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
