package com.ictfa.krishiguru.library.task;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;

/**
 * Created by 3421 on 8/2/2016.
 */
public class Permissions {


    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_STORAGE = 1;
    private static final int REQUEST_PHONE = 1;
    private static String[] PERMISSIONS_LOCATION = {
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static String[] PERMISSIONS_EXTERNAL_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static String[] PERMISSIONS_PHONE = {Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE};


    //persmission method.

    public static void verifyMapPermissions(Activity activity) {
        int accsessFineLocationPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCoarseLocationPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (accsessFineLocationPermission != PackageManager.PERMISSION_GRANTED || accessCoarseLocationPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_LOCATION,
                    REQUEST_LOCATION
            );
        }
    }

    public static void verifyExternalStorage(Activity activity) {
        int readStorage = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStorage = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (readStorage != PackageManager.PERMISSION_GRANTED || writeStorage != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_EXTERNAL_STORAGE,
                    REQUEST_STORAGE
            );
        }

    }

    public static void verifyPhonePermissions(Activity activity) {
        int accessPhonePermissions = ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE);
        //  int accessContactPermissions = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS);
        int accessPhoneStatePermissions = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE);
        if (accessPhonePermissions != PackageManager.PERMISSION_GRANTED || accessPhoneStatePermissions != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS_PHONE, REQUEST_PHONE);
        }
    }
}
