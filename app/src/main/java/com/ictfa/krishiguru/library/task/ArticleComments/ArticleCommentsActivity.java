package com.ictfa.krishiguru.library.task.ArticleComments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.customViews.GridSpacingItemDecoration;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.krishilibrary.Object.CommentLibraryObject;
import com.ictfa.krishiguru.library.task.CommentArticleAdapter;
import com.ictfa.krishiguru.login.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prajeet on 08/07/2017.
 */

public class ArticleCommentsActivity extends AppCompatActivity {
    private CustomProgressDialog pd1;
    private ArrayList<CommentLibraryObject> listComment;
    private Alerts alerts;
    SharedPreference sharedPreference;

    private CommentLibraryObject newComment;

    @BindView(R.id.rv_comments)
    RecyclerView rvComments;

    @BindView(R.id.edt_comment)
    EditText edtComment;
    private CommentArticleAdapter mAdapter;
    int articleId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_coments);
        CommonMethods.setupUI(findViewById(R.id.rl_full_notice), this);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.comments));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        listComment = new ArrayList<>();
        pd1 = new CustomProgressDialog(this);
        sharedPreference = new SharedPreference(this);
        alerts = new Alerts(this);
//        sharedPreference = new SharedPreference(this);
//        user_id = sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID);
//        name = sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            listComment = (ArrayList<CommentLibraryObject>) bundle.getSerializable("ListComment");
            articleId = bundle.getInt("articleId");

        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(this, 0), true));
        rvComments.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CommentArticleAdapter(this, listComment);
        rvComments.setAdapter(mAdapter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.iv_send)
    void onSendClick() {
        if (!edtComment.getText().toString().isEmpty()) {
            doComment(edtComment.getText().toString());
        }
    }

    private void doComment(final String comment) {
        if (sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) == 0) {
            Intent intent = new Intent(ArticleCommentsActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            pd1.showpd(getResources().getString(R.string.registering));
            JSONObject jsonObject;
            RequestQueue queue = Volley.newRequestQueue(ArticleCommentsActivity.this);
            String commentUrl = UrlHelper.BASE_URL + "api/v3/library/" + articleId + "/comment?apikey=" + UrlHelper.API_KEY;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, commentUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pd1.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObj = new JSONObject(response);
                                JSONObject codeJson = jsonObj.getJSONObject("code");
                                String status = codeJson.getString("status");
                                String message = codeJson.getString("message");
                                if (status.equalsIgnoreCase("1")) {
                                    newComment = new CommentLibraryObject();
                                    newComment.comment = comment;
                                    newComment.commentedBy = sharedPreference.getStringValues(CommonDef.SharedPreference.USERNAME);
                                    newComment.createdAt = "now";
                                    mAdapter.addComment(newComment);
                                    listComment.add(0, newComment);
                                    rvComments.smoothScrollToPosition(0);
                                    edtComment.setText("");
                                } else
                                    alerts.showErrorAlert(message);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                    pd1.hidepd();
                    // mTextView.setText("That didn't work!");
                }


            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("tag", "login");
                    params.put("user_id", String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)));
                    params.put("comment", comment);
                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
