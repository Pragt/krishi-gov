package com.ictfa.krishiguru.Weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WeatherResponse {

@SerializedName("meta")
@Expose
public List<Object> meta = null;
@SerializedName("data")
@Expose
public Data data;
@SerializedName("code")
@Expose
public Code code;

    public class Code {

        @SerializedName("status")
        @Expose
        public Integer status;
        @SerializedName("message")
        @Expose
        public String message;

    }

    public class Data {

        @SerializedName("location")
        @Expose
        public String location;
        @SerializedName("forecasts")
        @Expose
        public Forecasts forecasts;

    }

    public class Forecasts implements Serializable {

        @SerializedName("today")
        @Expose
        public List<WeatherData> today = null;
        @SerializedName("tomorrow")
        @Expose
        public List<WeatherData> tomorrow = null;
        @SerializedName("day_after_tomorrow")
        @Expose
        public List<WeatherData> dayAfterTomorrow = null;

    }


    public class WeatherData implements Serializable {

        @SerializedName("date_time")
        @Expose
        public String dateTime;
        @SerializedName("temperature")
        @Expose
        public Double temperature;
        @SerializedName("pressure")
        @Expose
        public Double pressure;
        @SerializedName("humidity")
        @Expose
        public Integer humidity;
        @SerializedName("weather_main")
        @Expose
        public String weatherMain;
        @SerializedName("weather_description")
        @Expose
        public String weatherDescription;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("wind_speed")
        @Expose
        public Double windSpeed;
        @SerializedName("wind_deg")
        @Expose
        public Double windDeg;
        @SerializedName("rain")
        @Expose
        public Double rain;

    }

}

