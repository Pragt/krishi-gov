package com.ictfa.krishiguru.Weather;

import java.io.Serializable;

/**
 * Created by Prajeet on 19/08/2017.
 */

public class WeatherObject implements Serializable {
    public String minTemp;
    public String maxTemp;
    public String dayDescription;
    public String chanceOfThunderDay;
    public String chanceOfRainDay;
    public String durationOfRainDay;

    public String nightDescription;
    public String chanceOfThunderNight;
    public String chanceOfRainNight;
    public String durationOfRainNight;

    public String iconNight;
    public String iconDay;
    public String location = "Kathmandu";
}
