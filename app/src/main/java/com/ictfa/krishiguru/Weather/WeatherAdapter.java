package com.ictfa.krishiguru.Weather;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sureshlama on 2/24/17.
 */

public class WeatherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mActivity;
    ArrayList<WeatherResponse.WeatherData> listWeather;

    // ForumData formData;
    public WeatherAdapter(Activity activity, ArrayList<WeatherResponse.WeatherData> listWeather) {
        this.mActivity = activity;
        this.listWeather = listWeather;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(mActivity).inflate(R.layout.item_weather, parent, false);
        return new myViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myViewHolder) {
            myViewHolder holder1 = (myViewHolder) holder;
            holder1.tvTodayDateTime.setText(CommonMethods.convertDateIntoTime(listWeather.get(position + 1).dateTime));
            holder1.tvPressure.setText("rain: " + listWeather.get(position + 1).rain + " mm");
            holder1.tvHumidity.setText("Humidity: " + listWeather.get(position + 1).humidity + "%");
            holder1.todayTemperature.setText(listWeather.get(position + 1).temperature + " \u2103");
            holder1.todayPrediction.setText(listWeather.get(position + 1).weatherDescription);
            Picasso.with(mActivity).load(listWeather.get(position + 1).icon).resize(500, 500).into(holder1.ivWeather);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listWeather.size() - 1;
    }


    public class myViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTodayDateTime)
        TextView tvTodayDateTime;
        @BindView(R.id.tvPressure)
        TextView tvPressure;
        @BindView(R.id.tvHumidity)
        TextView tvHumidity;
        @BindView(R.id.todayTemperature)
        TextView todayTemperature;
        @BindView(R.id.ivWeather)
        ImageView ivWeather;
        @BindView(R.id.todayPrediction)
        TextView todayPrediction;

        public myViewHolder(View convertView) {
            super(convertView);
            ButterKnife.bind(this, convertView);

        }
    }
}
