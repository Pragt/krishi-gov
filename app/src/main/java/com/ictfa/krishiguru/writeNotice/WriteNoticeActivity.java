package com.ictfa.krishiguru.writeNotice;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.FileUtils;
import com.ictfa.krishiguru.helpers.ImagePicker;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myProfile.UserType;
import com.ictfa.krishiguru.registerOrganization.view.RegisterOrganizationActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Prajeet on 01/07/2017.
 */

public class WriteNoticeActivity extends AppCompatActivity {

    @BindView(R.id.et_comment)
    EditText edtComment;
    @BindView(R.id.et_exp_date)
    EditText edtExpiryDate;

    @BindView(R.id.et_title)
    EditText edtTitle;

    @BindView(R.id.sp_type)
    Spinner spType;

    @BindView(R.id.btn_publish)
    Button btnPublish;
    @BindView(R.id.ivPicture)
    ImageView selectImage;

    @BindView(R.id.rlChooseImage)
    RelativeLayout rlChooseImage;
    Alerts alerts;
    SharedPreference sharedPreference;
    CustomProgressDialog pd;
    private String image;
    private ArrayList listType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_write_notice);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(getResources().getString(R.string.write_notice));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        alerts = new Alerts(this);
        pd = new CustomProgressDialog(this);
        sharedPreference = new SharedPreference(this);

        listType = new ArrayList();
        listType.add("तालिम");
        listType.add("अनुदान");
        listType.add("सुचना");

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listType); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spType.setAdapter(spinnerArrayAdapter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_publish)
    void onBtnPublishClicked() {
        if (isAllValid()) {
            if (sharedPreference.getStringValues(CommonDef.SharedPreference.USERTYPE).equalsIgnoreCase(UserType.TYPE_ORGANISATION))
                publishNotice();
            else {
                Intent intent = new Intent(WriteNoticeActivity.this, RegisterOrganizationActivity.class);
                startActivity(intent);
            }
        }
    }

    private void publishNotice() {
        pd.showpd(getResources().getString(R.string.please_wait));
        String url = UrlHelper.BASE_URL + "api/v3/organisation/" + sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) + "/notice?apikey=" + UrlHelper.API_KEY;
        RequestParams params = new RequestParams();
        params.put("organisation_id", sharedPreference.getStringValues(CommonDef.SharedPreference.ORG_ID));
        params.put("valid_to", edtExpiryDate.getText().toString());
        params.put("notice", edtComment.getText().toString());
        params.put("title", edtTitle.getText().toString());
        params.put("type", listType.get(spType.getSelectedItemPosition()));

        if (image != null && !image.equalsIgnoreCase("")) {
            final File myFile = new File(image);
            try {
                params.put("image", myFile, "image/jpeg");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }


        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(10000);
        client.setTimeout(10000);
        client.setResponseTimeout(30000);
        client.post(url, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                pd.hidepd();
                //formDatas.clear();
                //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                try {
//                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
//                            JSONObject jsonObject = new JSONObject(response);
                    int status = responseBody.getInt("status");
                    String message = responseBody.getString("message");
                    if (status == 1)
                        alerts.showSuccessAlert(message);
                    else
                        alerts.showErrorAlert(message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.hidepd();
            }

        });
    }

    @OnClick(R.id.rlChooseImage)
    void onChooseImageClicked() {
        showFileChooser();
    }

    private void showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CommonDef.REQUEST_STORAGE_CAMERA);
                return;
            }
        }
        startActivityForResult(ImagePicker.getPickImageChooserIntent(this), CommonDef.PICK_IMAGE_REQUEST);
    }

    private boolean isAllValid() {
        if (edtTitle.getText().toString().isEmpty()) {
            alerts.showToastMsg(getResources().getString(R.string.empty_notice_title));
            return false;
        } else if (edtComment.getText().toString().isEmpty()) {
            alerts.showToastMsg(getResources().getString(R.string.empty_notice));
            return false;
        } else if (edtExpiryDate.getText().toString().isEmpty()) {
            alerts.showToastMsg(getResources().getString(R.string.empty_expiry_date));
            return false;
        }
        return true;
    }

    @OnClick(R.id.et_exp_date)
    void onExpiryDateClicked() {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        int hour = mcurrentDate.get(Calendar.HOUR_OF_DAY);
        int minutes = mcurrentDate.get(Calendar.MINUTE);
        int second = mcurrentDate.get(Calendar.SECOND);
        String currentDate = mYear + "-" + mMonth + "-" + mDay + " " + hour + ":" + minutes + ":" + second;

        DatePickerDialog mDatePicker = new DatePickerDialog(WriteNoticeActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                edtExpiryDate.setText(selectedyear + "-" + (selectedmonth + 1) + "-" + selectedday);
                //month starts from 0 for janaury so +1 while displaying
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select date");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 6);
        DatePicker dp = mDatePicker.getDatePicker();
        dp.setMinDate(Calendar.getInstance().getTimeInMillis());//get the current day
        mDatePicker.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CommonDef.PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            Uri imageUri;
            if (data != null && data.getData() != null) {
                imageUri = data.getData();
                selectImage.setImageURI(imageUri);
                Bitmap imageBmp;
                try {
                    imageBmp = FileUtils.getThumbnail(this, imageUri, 1080);
                    image = FileUtils.storeBitmapToFile(imageBmp, this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else // From camera
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    image = ImagePicker.getImage.getAbsolutePath();
                } else
                    image = ImagePicker.outputFileUri.getPath();

                selectImage.setImageURI(Uri.fromFile(new File(image)));
            }


            //Getting the Bitmap from Gallery
            //Setting the Bitmap to ImageView
//                image = getPath(imageUri);


        }
    }
}
