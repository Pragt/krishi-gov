package com.ictfa.krishiguru.forgetPassword;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.UrlHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.protocol.HTTP;

/**
 * Created by prajit on 10/28/17.
 */

public class ForgetPasswordActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 12;
    @BindView(R.id.edt_sms_code)
    EditText edtSmsCode;

    @BindView(R.id.edt_password)
    EditText edtPassword;

    @BindView(R.id.btn_change_pass)
    Button btnChangePass;

    @BindView(R.id.btn_get_code)
    Button btnGetCode;

    Alerts alerts;
    CustomProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        CommonMethods.setupUI(findViewById(R.id.rl_forget_password), this);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.forget_password));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        alerts = new Alerts(this);
        progressDialog = new CustomProgressDialog(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_get_code)
    void getCode() {
//        Intent intent = new Intent(Intent.ACTION_SENDTO);
//        intent.setType(HTTP.PLAIN_TEXT_TYPE);
//        intent.putExtra("sms_body", "ifa password");
//        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("sms:34001"));
//        if (intent.resolveActivity(getPackageManager()) != null) {
//            startActivity(intent);
//        }

        Intent intent = new Intent( Intent.ACTION_SENDTO, Uri.parse( "sms:" + "34001"));
        intent.putExtra( "sms_body", "ifa password" );
        startActivity(intent);
    }

    @OnClick(R.id.btn_change_pass)
    void changePassword() {
        if (edtSmsCode.getText().toString().isEmpty())
            alerts.showToastMsg("कृपया एस. एम. एस. कोड लेख्नुहोस");
        else if (edtPassword.getText().toString().isEmpty())
            alerts.showToastMsg("कृपया नँया पासवर्ड लेख्नुहोस");
        else
            doChangePassword();
    }

    private void doChangePassword() {
        progressDialog.showpd(getResources().getString(R.string.please_wait));
        JSONObject jsonObject;
        RequestQueue queue = Volley.newRequestQueue(this);

        String url;
        url = UrlHelper.BASE_URL + "api/v3/update/" + edtSmsCode.getText().toString() + "/password?apikey=" + UrlHelper.API_KEY + "&password=" + edtPassword.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hidepd();
                        //formDatas.clear();
                        //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                        try {
                            String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                            JSONObject jsonObject = new JSONObject(utfStr);
                            JSONObject code = jsonObject.getJSONObject("code");
                            String status = code.getString("status");
                            String message = code.getString("message");
                            if (status.equalsIgnoreCase("1")) {

                                finish();
                                alerts.showToastMsg(getResources().getString(R.string.success));
//                                    sharedPreference.setKeyValues(CommonDef.SharedPreference.ORG_ID, data.getInt("id"));
//                                    alerts.showSuccessAlert(message);
                            } else
                                alerts.showErrorAlert(message);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alerts.showToastMsg(getResources().getString(R.string.no_internet_connection));
                progressDialog.hidepd();
                // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    /*
 * BroadcastReceiver mBrSend; BroadcastReceiver mBrReceive;
 */
    private void sendSMS(String phoneNumber, String message) {
//        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<PendingIntent>();
//        ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<PendingIntent>();
//        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
//                new Intent(this, SmsSentReceiver.class), 0);
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
//                new Intent(this, SmsDeliveredReceiver.class), 0);
//        try {
//            SmsManager sms = SmsManager.getDefault();
//            ArrayList<String> mSMSMessage = sms.divideMessage(message);
//            for (int i = 0; i < mSMSMessage.size(); i++) {
//                sentPendingIntents.add(i, sentPI);
//                deliveredPendingIntents.add(i, deliveredPI);
//            }
//            sms.sendMultipartTextMessage(phoneNumber, null, mSMSMessage,
//                    sentPendingIntents, deliveredPendingIntents);
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//            Toast.makeText(getBaseContext(), "SMS sending failed...", Toast.LENGTH_SHORT).show();
//        }

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType(HTTP.PLAIN_TEXT_TYPE);
        intent.putExtra("sms_body", message);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("sms:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }

    public void requestpermisson() {
//        if (ContextCompat.checkSelfPermission(this,
//                android.Manifest.permission.SEND_SMS)
//                != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    android.Manifest.permission.SEND_SMS)) {
//            } else {
//                ActivityCompat.requestPermissions(this,
//                        new String[]{android.Manifest.permission.SEND_SMS},
//                        MY_PERMISSIONS_REQUEST_SEND_SMS);
//            }
//        } else {
            sendSMS("34001", "ifa password");
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendSMS("34001", "ifa password");
                    Toast.makeText(getApplicationContext(), "SMS sent.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }
    }
}
