package com.ictfa.krishiguru.news;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.app.BaseApplication;
import com.ictfa.krishiguru.customViews.EndlessRecyclerOnScrollListener;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.realm.RealmController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class NewsListActivity extends AppCompatActivity {
    LinearLayoutManager linearLayoutManager;
    private RecyclerView rvNewsList;
    private NewsAdapter newsAdapter;
    private List<NewsData> newsDatas = new ArrayList<>();
    private CustomProgressDialog progressDialog;
    private int total_count;
    private boolean isLoading = false;
    private Realm realm;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        getSupportActionBar().setTitle("कृषि समाचार");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rvNewsList = (RecyclerView) findViewById(R.id.newList);
        linearLayoutManager = new LinearLayoutManager(this);
        rvNewsList.setLayoutManager(linearLayoutManager);
        newsAdapter = new NewsAdapter(NewsListActivity.this, newsDatas, false);
        progressDialog = new CustomProgressDialog(this);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        rvNewsList.setAdapter(newsAdapter);

        newsDatas = new ArrayList<>();
        this.realm = RealmController.with(this).getRealm();
        RealmResults<NewsData> realmResults = realm.where(NewsData.class).findAll();
        for (NewsData a : realmResults)
            newsDatas.add(a);

        newsAdapter.notifyDataSetChanged();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getnewsData(true);
            }
        });
        if (newsDatas.size() == 0)
            progressDialog.showpd(getResources().getString(R.string.please_wait));
        else
            swipeRefreshLayout.setRefreshing(true);
        getnewsData(true);

        rvNewsList.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (total_count > newsDatas.size())
                    getnewsData(false);
            }
        });

        //get realm instance
    }

    private void getnewsData(final boolean clearOld) {
        if (!isLoading) {
            newsDatas.add(null);

            newsAdapter.addAll(newsDatas);
            isLoading = true;

            RequestQueue queue = Volley.newRequestQueue(NewsListActivity.this);
            //  String url ="http://www.google.com";

// Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, BaseApplication.NEWS_API + "&count=" + 10 + "&start=" + (int) (!clearOld ? newsDatas.size() : 0),
                    new Response.Listener<String>() {
                        public int total_displayed;

                        @Override
                        public void onResponse(String response) {
                            swipeRefreshLayout.setRefreshing(false);
                            isLoading = false;
                            newsDatas.remove(newsDatas.size() - 1);
                            newsAdapter.notifyDataSetChanged();
                            progressDialog.hidepd();

                            try {
                                String utfStr = new String(response.getBytes("ISO-8859-1"), "UTF-8");

                                JSONObject jsonObject = new JSONObject(utfStr);

                                JSONObject meta = jsonObject.getJSONObject("meta");
                                total_count = meta.getInt("total_count");
                                total_displayed = meta.getInt("total_displayed");

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                if (clearOld) {
                                    newsDatas = new ArrayList<>();
                                    newsAdapter = new NewsAdapter(NewsListActivity.this, newsDatas, false);
                                    rvNewsList.setAdapter(newsAdapter);
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.delete(NewsData.class);
                                        }
                                    });
                                }

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject newsValue = jsonArray.getJSONObject(i);
                                    String title = newsValue.getString("title");
                                    String imageresource = newsValue.getString("image");
                                    String description = newsValue.getString("description");
                                    String id = newsValue.getString("id");

                                    NewsData newsData = new NewsData();

                                    newsData.setDesc(description);
                                    newsData.setTitle(title);
                                    newsData.setId(id);
                                    newsData.setNewsImage(imageresource);
                                    newsDatas.add(newsData);

                                }

                                for (NewsData b : newsDatas) {
                                    // Persist your data easily
                                    if (b != null) {
                                        realm.beginTransaction();
                                        realm.copyToRealmOrUpdate(b);
                                        realm.commitTransaction();
                                    }
                                }

                                newsAdapter.notifyDataSetChanged();
                                Log.d("JSONRESPONSE=>>>>>>>>", jsonArray.toString());
                            } catch (JSONException e) {
                                progressDialog.hidepd();
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                progressDialog.hidepd();
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // mTextView.setText("That didn't work!");
                    progressDialog.hidepd();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
