package com.ictfa.krishiguru.news;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sureshlama on 2/24/17.
 */

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_PROGRESS = 1;
    private static final int TYPE_ITEM = 2;
    private Activity activity;
    private boolean isFromDashboard = false;
    private List<NewsData> newsDatas;

    public NewsAdapter(Activity activity, List<NewsData> newsDatas, boolean isFromDashboard) {
        this.activity = activity;
        this.newsDatas = newsDatas;
        this.isFromDashboard = isFromDashboard;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_ITEM) {
            if (!isFromDashboard) {
                view = LayoutInflater.from(activity).inflate(R.layout.news_custome_list, parent, false);
                return new myViewHolder(view);
            } else {
                view = LayoutInflater.from(activity).inflate(R.layout.layout_news_item, parent, false);
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.width = (int) (parent.getWidth() * 0.45);
                view.setLayoutParams(layoutParams);
                return new myDashboardViewHolder(view);
            }
        } else
            view = LayoutInflater.from(activity).inflate(R.layout.item_progress_dialog, parent, false);
        return new myProgressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof myViewHolder) {
            myViewHolder holder1 = (myViewHolder) holder;
            final NewsData newsData = newsDatas.get(position);

            Typeface mangalTitle = Typeface.createFromAsset(activity.getAssets(), "fonts/mangal.ttf");

            holder1.title.setTypeface(mangalTitle);

            if (newsData.getNewsImage().isEmpty()) {
                Picasso.with(activity).load("http://www.eco-business.com/media/_versions/ebmedia/fileuploads/shutterstock_199573592_nepal_rice_field_news_featured.jpg").resize(500, 500).into(holder1.icon);
            } else {
                Picasso.with(activity).load(newsData.getNewsImage()).resize(500, 500).into(holder1.icon);
            }

            holder1.title.setText(newsData.getTitle());
            holder1.desc.setText(newsData.getDesc());

            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, NewsDetailActivity.class);
                    intent.putExtra(CommonDef.NEWS_ID, newsData.getId());
                    activity.startActivity(intent);
                }
            });
        } else {
            try {
                final NewsData newsData = newsDatas.get(position);
                myDashboardViewHolder holder1 = (myDashboardViewHolder) holder;
                holder1.newsTitle.setText(newsData.getTitle());
                holder1.newsDesc.setText(newsData.getDesc());

                if (newsData.getNewsImage().isEmpty()) {
                    Picasso.with(activity).load("http://www.eco-business.com/media/_versions/ebmedia/fileuploads/shutterstock_199573592_nepal_rice_field_news_featured.jpg").resize(500, 500).into(holder1.newsImage);
                } else {
                    Picasso.with(activity).load(newsData.getNewsImage()).resize(500, 500).into(holder1.newsImage);
                }

                holder1.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, NewsDetailActivity.class);
                        intent.putExtra(CommonDef.NEWS_ID, newsData.getId());
                        activity.startActivity(intent);
                    }
                });
            } catch (Exception exx) {
            }

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (newsDatas.get(position) == null)
            return TYPE_PROGRESS;
        else
            return TYPE_ITEM;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return this.newsDatas.size();
    }

    public void addAll(List<NewsData> newsDatas) {
        this.newsDatas = newsDatas;
        notifyDataSetChanged();
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        private final TextView desc;
        private final TextView title;
        private final TextView tvViewMore;
        private ImageView icon;

        public myViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.newsImage);
            title = (TextView) itemView.findViewById(R.id.newsTitle);
            desc = (TextView) itemView.findViewById(R.id.newsDesc);
            tvViewMore = (TextView) itemView.findViewById(R.id.view_more);
        }
    }

    public class myDashboardViewHolder extends RecyclerView.ViewHolder {
        private final TextView newsTitle, newsDesc;
        private ImageView newsImage;

        public myDashboardViewHolder(View itemView) {
            super(itemView);
            newsImage = (ImageView) itemView.findViewById(R.id.newsImage);
            newsTitle = (TextView) itemView.findViewById(R.id.newsTitle);
            newsDesc = (TextView) itemView.findViewById(R.id.newsDesc);
        }
    }


    private class myProgressViewHolder extends RecyclerView.ViewHolder {
        public myProgressViewHolder(View view) {
            super(view);
        }
    }
}
