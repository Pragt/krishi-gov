package com.ictfa.krishiguru.news;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.realm.RealmController;
import com.squareup.picasso.Picasso;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by Prajeet on 14/07/2017.
 */

public class NewsDetailActivity extends AppCompatActivity {

    NewsData newsData;
    @BindView(R.id.tv_descrition)
    HtmlTextView tvDescription;
    @BindView(R.id.tv_article_title)
    TextView tvArticleTitle;
    @BindView(R.id.ivImage)
    ImageView ivImage;

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Realm realm;
    private String newsId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_news);
        ButterKnife.bind(this);
//      init

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if (getIntent().getBooleanExtra("isFromNotification", false)) {
            newsData = (NewsData) getIntent().getSerializableExtra("newsData");
        } else {
            newsId = getIntent().getStringExtra(CommonDef.NEWS_ID);

            //get realm instance
            this.realm = RealmController.with(this).getRealm();
            newsData = realm.where(NewsData.class).equalTo("id", newsId).findFirst();
        }
//        if (Build.VERSION.SDK_INT >= 24) {
//          newsData.setDesc(String.valueOf(Html.fromHtml(newsData.getDesc(), Html.FROM_HTML_MODE_LEGACY))); // for 24 api and more
//        } else {
//            newsData.setDesc(String.valueOf(Html.fromHtml(newsData.getDesc()))); // or for older api
//        }

//        tvDescription.setText(newsData.getDesc());
        if (Build.VERSION.SDK_INT >= 24) {
            tvDescription.setText(Html.fromHtml(newsData.getDesc(), Html.FROM_HTML_MODE_LEGACY)); // for 24 api and more
        } else {
            tvDescription.setText(Html.fromHtml(newsData.getDesc())); // or for older api
        }
        tvDescription.setText(newsData.getDesc());
        tvArticleTitle.setText(newsData.getTitle());
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(newsData.getTitle());

        if (newsData.getNewsImage().isEmpty()) {

            Picasso.with(this).load("http://www.eco-business.com/media/_versions/ebmedia/fileuploads/shutterstock_199573592_nepal_rice_field_news_featured.jpg").resize(500, 500).into(ivImage);
        } else {
            Picasso.with(this).load(newsData.getNewsImage()).resize(500, 500).into(ivImage);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
