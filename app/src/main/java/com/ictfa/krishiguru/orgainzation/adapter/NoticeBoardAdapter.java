package com.ictfa.krishiguru.orgainzation.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.Alerts;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.helpers.CustomProgressDialog;
import com.ictfa.krishiguru.helpers.SharedPreference;
import com.ictfa.krishiguru.helpers.UrlHelper;
import com.ictfa.krishiguru.myOrganization.view.Objects.Organisation;
import com.ictfa.krishiguru.login.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pragt on 3/5/17.
 */

public class NoticeBoardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    ArrayList<Organisation> listOrganisation;
    SharedPreference sharedPreference;
    Alerts alerts;
    CustomProgressDialog pd1;

    public NoticeBoardAdapter(Context mContext, ArrayList<Organisation> listOrganisation) {
        this.mContext = mContext;
        this.listOrganisation = listOrganisation;
        sharedPreference = new SharedPreference(mContext);
        alerts = new Alerts((Activity) mContext);
        pd1 = new CustomProgressDialog((Activity) mContext);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_notice_board, null);
        return new myCategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myCategoriesViewHolder) {
            final myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;
            holder1.tvOrgName.setText(listOrganisation.get(position).organization_name);
            holder1.tvDistrict.setText(listOrganisation.get(position).district);
            holder1.tvPhoneNo.setText(listOrganisation.get(position).phone);
            holder1.tvTollFreeNo.setText(listOrganisation.get(position).toll_free);

            holder1.btnGetNotice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            if (listOrganisation.get(position).isSubscribed == 1)
            {
                holder1.btnGetNotice.setText(mContext.getResources().getString(R.string.do_not_get_notice));
                holder1.btnGetNotice.setBackground(mContext.getResources().getDrawable(R.drawable.red_button_background));
            }else
            {
                holder1.btnGetNotice.setText(mContext.getResources().getString(R.string.get_notice));
                holder1.btnGetNotice.setBackground(mContext.getResources().getDrawable(R.drawable.login_rounded_button));
            }

            holder1.btnGetNotice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    doSubscribeUnsubscribe(position, listOrganisation.get(position).isSubscribed);
                }
            });

        }
    }

    private void doSubscribeUnsubscribe(final int position, int isSubscribed) {
        if (sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID) == 0) {
            Intent intent = new Intent(mContext, LoginActivity.class);
            mContext.startActivity(intent);
        } else {
            pd1.showpd(mContext.getResources().getString(R.string.registering));
            final JSONObject jsonObject;
            RequestQueue queue = Volley.newRequestQueue(mContext);
            String commentUrl = UrlHelper.BASE_URL + "api/v3/organisation/" + listOrganisation.get(position).id + "/subscribe?apikey=" + UrlHelper.API_KEY;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, commentUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pd1.hidepd();
                            //formDatas.clear();
                            //   Toast.makeText(FormListActivity.this,response,Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObj = new JSONObject(response);
                                JSONObject codeJson = jsonObj.getJSONObject("code");
                                int status = codeJson.getInt("status");
                                String message = codeJson.getString("message");
                                if (status == 1)
                                {
                                    JSONObject data = jsonObj.getJSONObject("data");
                                    int isSubscribed = data.getInt("subscribe");
                                    listOrganisation.get(position).isSubscribed = isSubscribed;
                                    notifyItemChanged(position);

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alerts.showToastMsg(mContext.getResources().getString(R.string.no_internet_connection));
                    pd1.hidepd();
                    // mTextView.setText("That didn't work!");
                }


            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("tag", "login");
                    params.put("user_id", String.valueOf(sharedPreference.getIntValues(CommonDef.SharedPreference.USER_ID)));
//                    params.put("comment", comment);
                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    @Override
    public int getItemCount() {
        return listOrganisation.size();
    }


    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_org_name)
        TextView tvOrgName;
        @BindView(R.id.tv_district)
        TextView tvDistrict;
        @BindView(R.id.tv_phone_no)
        TextView tvPhoneNo;
        @BindView(R.id.tv_toll_free_no)
        TextView tvTollFreeNo;
        @BindView(R.id.btn_get_notice)
        Button btnGetNotice;

        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
