package com.ictfa.krishiguru.orgainzation;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.customViews.GridSpacingItemDecoration;
import com.ictfa.krishiguru.helpers.CommonMethods;
import com.ictfa.krishiguru.myOrganization.view.Objects.Organisation;
import com.ictfa.krishiguru.orgainzation.adapter.NoticeBoardAdapter;
import com.ictfa.krishiguru.registerOrganization.view.RegisterOrganizationActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by DAT on 9/1/2015.
 */
public class FragmentChild extends Fragment {
    ArrayList<Organisation> listOrganisation;
    @BindView(R.id.rv_notice_board)
    RecyclerView rvListOrg;
    NoticeBoardAdapter mAdapter;
    boolean isProfile;
    @BindView(R.id.ll_profile)
    LinearLayout llProfile;
    @BindView(R.id.btn_register)
    Button btnRegister;
    Organisation profile;
    @BindView(R.id.tv_org_name)
    TextView tvOrgname;
    @BindView(R.id.tv_district)
    TextView tvDistrict;
    @BindView(R.id.tv_phone_no)
    TextView tvPhone;
    @BindView(R.id.tv_toll_free_no)
    TextView tvTollFree;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_child, container, false);
        ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        if (bundle != null) {
            isProfile = bundle.getBoolean("is_profile");

            if (isProfile) {
                profile = (Organisation) bundle.getSerializable("profile");
                rvListOrg.setVisibility(View.GONE);
                if (profile.id.equalsIgnoreCase("")) {
                    btnRegister.setVisibility(View.VISIBLE);
                    llProfile.setVisibility(View.GONE);
                } else {
                    llProfile.setVisibility(View.VISIBLE);
                    setProfile();
                }
            } else {
                rvListOrg.setVisibility(View.VISIBLE);
                llProfile.setVisibility(View.GONE);
                btnRegister.setVisibility(View.GONE);
                listOrganisation = (ArrayList<Organisation>) bundle.getSerializable("data");
                setOrganisation();
            }
        }
        return view;
    }

    private void setProfile() {
        tvOrgname.setText(profile.organization_name);
        tvDistrict.setText(profile.district);
        tvPhone.setText(profile.phone);
        tvTollFree.setText(profile.toll_free);
    }

    private void setOrganisation() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvListOrg.setLayoutManager(linearLayoutManager);
        rvListOrg.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(getActivity(), 0), true));
        rvListOrg.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new NoticeBoardAdapter(getActivity(), listOrganisation);
        rvListOrg.setAdapter(mAdapter);
    }

    @OnClick(R.id.btn_register)
    void onRegisterClicked() {
        Intent intent = new Intent(getActivity(), RegisterOrganizationActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
