package com.ictfa.krishiguru.orgainzation;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.ictfa.krishiguru.R;
import com.ictfa.krishiguru.helpers.CommonDef;
import com.ictfa.krishiguru.myOrganization.view.Objects.Organisation;
import com.ictfa.krishiguru.myOrganization.view.Objects.OrganizationsType;

import java.util.ArrayList;

/**
 * Created by ebpearls on 6/27/2017.
 */

public class OrganizationsActivity extends AppCompatActivity {
    FragmentParent fragmentParent;
    ArrayList<OrganizationsType> listOrganisationType;
    Organisation profie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_organizations);
        getSupportActionBar().setTitle(getResources().getString(R.string.my_organisatons));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fragmentParent = (FragmentParent) this.getSupportFragmentManager().findFragmentById(R.id.fragmentParent);
        init();

        Bundle b = getIntent().getExtras();
        if (b != null) {
            listOrganisationType = (ArrayList<OrganizationsType>) b.getSerializable(CommonDef.ORGANIZATION_LIST);
            profie = (Organisation) b.getSerializable(CommonDef.PROFILE);
        }

        fragmentParent.addPage(getResources().getString(R.string.my_profile), profie);

        for (int i = 0; i < listOrganisationType.size(); i++) {
                fragmentParent.addPage(listOrganisationType.get(i).organizations_name, listOrganisationType.get(i).listOrganisations);
        }
    }

    private void init() {
        listOrganisationType = new ArrayList<>();
        profie = new Organisation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
